package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.repositories.BankAccountRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BankAccountServiceImplTest {
    @Mock
    private BankAccountRepository bankAccountRepository;

    @InjectMocks
    private BankAccountServiceImpl bankAccountService;

    @Mock
    private CheckEntityService checkEntityService;
    private static BankAccountDto bankAccountDto;
    private static List<BankAccountDto> bankAccountDtoList;

    @BeforeAll
    static void init() {

        bankAccountDto = BankAccountDto.builder()
                .id(1L)
                .rcbic("6516516516")
                .bank("ВТБ")
                .correspondentAccount("61565161616")
                .account("888888888888888")
                .mainAccount(true)
                .sortNumber("11")
//                .address(AddressDto.builder().fullAddress("Москва, ул.Первая д.1").build())
                .build();
        bankAccountDtoList = List.of(bankAccountDto);
    }


    @Test
    void getAll() {
        when(bankAccountRepository.getAll()).thenReturn(bankAccountDtoList);
        List<BankAccountDto> result = bankAccountService.getAll();
        assertNotNull(result, "Null here");
        assertEquals(result, bankAccountDtoList);
        verify(bankAccountRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(bankAccountRepository.getById(1L)).thenReturn(bankAccountDto);

        assertEquals(bankAccountService.getById(1L), bankAccountDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(bankAccountRepository),
                        ArgumentMatchers.eq("BankAccount"));
        verify(bankAccountRepository, times(1)).getById(1L);
    }

    @Test
    void create() {
        bankAccountService.create(bankAccountDto);
        verify(bankAccountRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(bankAccountDto)));
    }

    @Test
    void update() {
        bankAccountService.update(bankAccountDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(bankAccountRepository),
                        ArgumentMatchers.eq("BankAccount"));
        verify(bankAccountRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(bankAccountDto)));
    }

    @Test
    void deleteById() {
        bankAccountService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(bankAccountRepository),
                        ArgumentMatchers.eq("BankAccount"));
        verify(bankAccountRepository, times(1)).
                deleteById(ArgumentMatchers.eq(1L));
    }
}
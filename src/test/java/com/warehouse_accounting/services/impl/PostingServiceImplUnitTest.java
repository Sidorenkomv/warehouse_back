package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PostingDto;
import com.warehouse_accounting.repositories.PostingRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class PostingServiceImplUnitTest {

    private static PostingDto postingDto1;
    private static PostingDto postingDto2;
    private static List<PostingDto> postingDtoList;

    @Mock
    private PostingRepository postingRepository;

    @InjectMocks
    private PostingServiceImpl postingService;

    @Mock
    CheckEntityService checkEntityService;
    @BeforeEach
    void initMethod() {
        postingDto1 = PostingDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Покупай")
                .build();

        postingDto2 = PostingDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Бери")
                .build();

        postingDtoList = List.of(postingDto1, postingDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(postingRepository.getAll()).thenReturn(postingDtoList);
        List<PostingDto> postingDtoListTest = postingService.getAllTest();
        assertNotNull(postingDtoListTest, "postingDtoList is not null");
        assertEquals(postingDtoList, postingDtoListTest);
        verify(postingRepository, times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(postingRepository.getById(2L)).thenReturn(postingDto1);

        assertEquals(postingService.getById(2L), postingDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(postingRepository), ArgumentMatchers.eq("Posting"));
        Mockito.verify(postingRepository, Mockito.times(1))
                .getById(ArgumentMatchers.eq(2L));
    }

    @DisplayName("create method test")
    @Test
    void create() {
        postingService.create(postingDto1);
        Mockito.verify(postingRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(postingDto1)));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        postingService.update(postingDto2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(postingDto2.getId()), ArgumentMatchers.eq(postingRepository), ArgumentMatchers.eq("Posting"));
        Mockito.verify(postingRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(postingDto2)));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        postingService.deleteById(3L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(postingRepository), ArgumentMatchers.eq("Posting"));
        Mockito.verify(postingRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}
package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.ShipmentDto;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.repositories.ShipmentRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ShipmentServiceImplTest {
    @Mock
    private ShipmentRepository shipmentRepository;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ShipmentServiceImpl shipmentService;

    @Mock
    CheckEntityService checkEntityService;
    private static ShipmentDto shipmentDto;
    private static List<ShipmentDto> shipmentDtoList;

    @BeforeAll
    static void init() {
        ProductDto productDto = ProductDto.builder()
                .id(1L)
                .name("prod")
                .build();

        shipmentDto = ShipmentDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.now())
                .warehouseId(1L)
                .contractId(1L)
                .companyId(1L)
                .productDtos(List.of(productDto))
                .sum(BigDecimal.valueOf(111))
                .paid(BigDecimal.valueOf(111))
                .isSent(false)
                .isPrinted(true)
                .comment("first")
                .consigneeId(1L)
                .carrierId(1L)
                .isPaid(true)
                .deliveryAddress(AddressDto.builder().fullAddress("address").build())
                .build();

        shipmentDtoList = List.of(shipmentDto);
    }

    @Test
    void getAll() {
        Mockito.when(shipmentRepository.getAll()).thenReturn(shipmentDtoList);
        List<ShipmentDto> result = shipmentService.getAll();

        assertEquals(result, shipmentDtoList);
        assertNotNull(result, "Null here");
        Mockito.verify(shipmentRepository, Mockito.times(1)).getAll();
    }

    @Test
    void getById() {
        Mockito.when(shipmentRepository.getById(1L)).thenReturn(shipmentDto);

        assertEquals(shipmentService.getById(1L), shipmentDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(shipmentRepository),
                        ArgumentMatchers.eq("Shipment"));
        verify(shipmentRepository, Mockito.times(1)).getById(1L);
    }

    @Test
    void create() {
        shipmentService.create(shipmentDto);
        verify(shipmentRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(shipmentDto)));
    }

    @Test
    void update() {
        shipmentService.update(shipmentDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(shipmentRepository),
                        ArgumentMatchers.eq("Shipment"));
        verify(shipmentRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(shipmentDto)));
    }

    @Test
    void deleteById() {
        shipmentService.deleteById(2L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(shipmentRepository),
                        ArgumentMatchers.eq("Shipment"));
        Mockito.verify(shipmentRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(2L));
    }
}


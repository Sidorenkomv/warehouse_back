package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.ProductionStage;
import com.warehouse_accounting.models.dto.ProductionStageDto;
import com.warehouse_accounting.repositories.DepartmentRepository;
import com.warehouse_accounting.repositories.EmployeeRepository;
import com.warehouse_accounting.repositories.ProductionStageRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductionStageServiceImplTest {
    // тесты работают с изначально пустой бд
    // перед запуском выключить Datainitialaizer
    //Если будет добавлен скрипт по созданию ProductionStage он тоже повлияет на работу тестов.

    @Mock
    private ProductionStageRepository repository;
    @Mock
    private EmployeeRepository repositoryEmployee;

    @Mock
    private DepartmentRepository repositoryDepartment;

    @InjectMocks
    private ProductionStageServiceImpl service;

    @Mock
    CheckEntityService checkEntityService;
    private ProductionStageDto productionStageDto;

    private List<ProductionStageDto> dtos;

    @BeforeEach
    private void init() {
        productionStageDto = ProductionStageDto.builder()
                .id(1l)
                .name("Основной этап")
                .description("Описание")
                .generalAccess(true)
                .ownerEmployeeId(1L)
                .ownerDepartmentId(1L)
                .editorEmployeeId(1L)
                .build();
        dtos = List.of(productionStageDto);
    }

    @Test
    void getAll() {
        when(repository.getAll(false)).thenReturn(dtos);
        List<ProductionStageDto> list = service.getAll(false);
        assertNotNull(list, "Null here");
        assertEquals(list, dtos);
        verify(repository, times(1)).getAll(false);
    }

    @Test
    void getById() {
        when(repository.getById(1L)).thenReturn(productionStageDto);
        assertEquals(service.getById(1L), productionStageDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L),ArgumentMatchers.eq(repository), ArgumentMatchers.eq("ProductionStage"));
        verify(repository, times(1)).getById(1L);
    }

    @Test
    void create() {
        service.create(productionStageDto);
        ProductionStage productionStage = ConverterDto.convertToModel(productionStageDto);
        if (!repositoryEmployee.existsById(productionStageDto.getOwnerEmployeeId())) {
            productionStage.setOwnerEmployee(null);
            productionStage.setEditorEmployee(null);
        }
        if (!repositoryDepartment.existsById(productionStageDto.getOwnerDepartmentId())) {
            productionStage.setOwnerDepartment(null);
        }
        verify(repository, times(1))
                .save(ArgumentMatchers.eq(productionStage));
    }

    @Test
    void update() {
        service.update(productionStageDto);
        ProductionStage productionStage = ConverterDto.convertToModel(productionStageDto);
        if (!repositoryEmployee.existsById(productionStageDto.getOwnerEmployeeId())) {
            productionStage.setOwnerEmployee(null);
            productionStage.setEditorEmployee(null);
        }
        if (!repositoryDepartment.existsById(productionStageDto.getOwnerDepartmentId())) {
            productionStage.setOwnerDepartment(null);
        }
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(productionStageDto.getId()), ArgumentMatchers.eq(repository), ArgumentMatchers.eq("ProductionStage"));
        verify(repository, times(1)).save(ArgumentMatchers.eq(productionStage));
    }

    @Test
    void delete() {
        service.delete(1L);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(repository), ArgumentMatchers.eq("ProductionStage"));
        verify(repository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }


}

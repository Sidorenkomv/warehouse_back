package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.exceptions.EntityFoundException;
import com.warehouse_accounting.models.Department;
import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.Image;
import com.warehouse_accounting.models.Position;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.repositories.EmployeeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    private static Employee employee;
    private static final List<Employee> employees = new ArrayList<>();

    @BeforeAll
    static void init() {
        employee = Employee.builder()
                .id(1L)
                .firstName("test1")
                .lastName("test1")
                .middleName("middleName1")
                .sortNumber("sortNumber")
                .phone("phone")
                .inn("inn")
                .description("description")
                .email("email1")
                .password("password")
                .position(Position.builder().build())
                .department(Department.builder().build())
                .roles(Set.of(Role.builder().build()))
                .image(Image.builder().build())
                .tariff(new HashSet<>())
                .build();
        Employee employee2 = Employee.builder()
                .id(2L)
                .firstName("test2")
                .lastName("test2")
                .middleName("middleName2")
                .sortNumber("sortNumber")
                .phone("phone")
                .inn("inn")
                .description("description")
                .email("email2")
                .password("password")
                .position(Position.builder().build())
                .department(Department.builder().build())
                .roles(Set.of(Role.builder().build()))
                .image(Image.builder().build())
                .tariff(new HashSet<>())
                .build();
        employees.addAll(List.of(employee, employee2));
    }

    @Test
    void shouldSaveUserSuccessfully() {
        given(employeeRepository.findByEmail(employee.getEmail())).willReturn(Optional.empty());
        given(employeeRepository.save(employee)).willAnswer(invocation -> invocation.getArgument(0));
        final Employee savedEmployee = employeeService.create(employee);
        assertThat(savedEmployee).isNotNull();
        then(employeeRepository).should().save(employee);
    }

    @Test
    void shouldThrowErrorWhenSaveUserWithExistingEmail() {
        given(employeeRepository.findByEmail(employee.getEmail())).willReturn(Optional.of(employee));
        assertThrows(EntityFoundException.class, () -> employeeService.create(employee));
        then(employeeRepository).should(never()).save(employee);
    }

    @Test
    void shouldUpdateUserSuccessfully() {
        employee.setFirstName("testChanged");
        given(employeeRepository.save(employee)).willReturn(employee);
        final Employee expected = employeeService.update(employee);
        assertThat(expected).isNotNull();
        then(employeeRepository).should().save(employee);
    }

    @Test
    void shouldFindAll() {
        given(employeeRepository.findAll()).willReturn(employees);
        List<Employee> expected = employeeService.findAll();
        assertEquals(expected, employees);
    }

    @Test
    void shouldFindUserById() {
        Long id = employee.getId();
        given(employeeRepository.findById(id)).willReturn(Optional.of(employee));
        final Employee expected = employeeService.findById(id);
        assertEquals(expected, employee);
    }

    @Test
    // TODO: fix EmployeeServiceImplTest::shouldBeDelete test
    void shouldBeDelete() {
        Long id = employee.getId();
        employeeService.deleteById(id);
        employeeService.deleteById(id);
        then(employeeRepository).should(times(2)).deleteById(id);
    }
}

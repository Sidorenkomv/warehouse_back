package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.ProjectDto;
import com.warehouse_accounting.repositories.ProjectRepository;
import com.warehouse_accounting.services.impl.ProjectServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProjectServiceImplTest {

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ProjectServiceImpl projectService;

    @Mock
    private ProjectRepository projectRepository;


    private final ProjectDto projectDto = ProjectDto.builder()
            .id(1L)
            .name("Test1")
            .code("test1")
            .description("test1")
            .build();
    private final List<ProjectDto> projectDtoList = List.of(projectDto);


    @Test
    void getAll() {
        when(projectRepository.getAll()).thenReturn(projectDtoList);
        List<ProjectDto> projectDtoListTest = projectService.getAll();
        assertNotNull(projectDtoListTest, "projectDtoListTest = null");
        assertEquals(projectDtoListTest, projectDtoList);
        verify(projectRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(projectRepository.getById(1L)).thenReturn(projectDto);

        assertEquals(projectService.getById(1L), projectDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(projectRepository), ArgumentMatchers.eq("Project"));
        verify(projectRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        projectService.create(projectDto);
        verify(projectRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(projectDto)));
    }

    @Test
    void update() {
        projectService.update(projectDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(projectDto.getId()), ArgumentMatchers.eq(projectRepository), ArgumentMatchers.eq("Project"));
        verify(projectRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(projectDto)));
    }

    @Test
    void deleteById() {
        projectService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(projectRepository), ArgumentMatchers.eq("Project"));
        verify(projectRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}
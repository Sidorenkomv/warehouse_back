package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.WriteOffsDto;
import com.warehouse_accounting.repositories.WriteOffsRepository;
import com.warehouse_accounting.services.impl.WriteOffsServiceIml;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WriteOffsServiceImplUnitTest {

    private static WriteOffsDto writeOffsDto1;
    private static WriteOffsDto writeOffsDto2;
    private static List<WriteOffsDto> writeOffsDtoList;

    @Mock
    private WriteOffsRepository writeOffsRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private WriteOffsServiceIml writeOffsService;

    @BeforeEach
    void initMethod() {
        writeOffsDto1 = WriteOffsDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(2000L))
                .moved(true)
                .printed(true)
                .comment("Коммент2")
                .build();

        writeOffsDto2 = WriteOffsDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(3000L))
                .moved(true)
                .printed(true)
                .comment("Коммент3")
                .build();

        writeOffsDtoList = List.of(writeOffsDto1, writeOffsDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(writeOffsRepository.getAll()).thenReturn(writeOffsDtoList);
        List<WriteOffsDto> writeOffsDtoListTest = writeOffsService.getAllTest();
        assertNotNull(writeOffsDtoListTest, "writeIOffsDtoList is not null");
        assertEquals(writeOffsDtoList, writeOffsDtoListTest);
        verify(writeOffsRepository, times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(writeOffsRepository.getById(2L)).thenReturn(writeOffsDto1);

        Assert.assertEquals(writeOffsService.getById(2L), writeOffsDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(writeOffsRepository), ArgumentMatchers.eq("WriteOffs"));
        Mockito.verify(writeOffsRepository, Mockito.times(1))
                .getById(ArgumentMatchers.eq(2L));
    }

    @DisplayName("create method test")
    @Test
    void create() {
        writeOffsService.create(writeOffsDto1);
        Mockito.verify(writeOffsRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(writeOffsDto1)));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        writeOffsService.update(writeOffsDto2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(writeOffsDto2.getId()), ArgumentMatchers.eq(writeOffsRepository), ArgumentMatchers.eq("WriteOffs"));
        Mockito.verify(writeOffsRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(writeOffsDto2)));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        writeOffsService.deleteById(3L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(writeOffsRepository), ArgumentMatchers.eq("WriteOffs"));
        Mockito.verify(writeOffsRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}
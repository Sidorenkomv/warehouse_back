package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.PriceListDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.PriceListRepository;
import com.warehouse_accounting.services.impl.PriceListServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
class PriceListServiceImplTest {

    private static PriceListDto priceListDto;
    private static List<PriceListDto> priceListDtos;

    private static CompanyDto companyDto;
    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private PriceListServiceImpl priceListService;

    @Mock
    private PriceListRepository priceListRepository;

    @Mock
    private CompanyRepository companyRepository;

    @BeforeAll
    static void setUp() {
        companyDto = CompanyDto.builder()
                .id(1L)
                .name("company1")
                .email("email@yandex.ru")
                .phone("89997776655")
                .build();

        priceListDto = PriceListDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.now())
                .company(companyDto)
                .moved(true)
                .printed(true)
                .comment("yes")
                .build();
        priceListDtos = List.of(priceListDto);
    }

    @Test
    void getAll() {
        when(priceListRepository.getAll()).thenReturn(priceListDtos);
        when(companyRepository.getById(anyLong())).thenReturn(companyDto);

        List<PriceListDto> priceListDtoListTest = priceListRepository.getAll();

        assertNotNull(priceListDtoListTest, "priceListDtoListTest == null");
        assertEquals(priceListDtoListTest, priceListDtos);
        verify(priceListRepository, times(1)).getAll();

    }

    @Test
    void getById() {
        when(priceListRepository.getById(1L)).thenReturn(priceListDto);
        when(companyRepository.getById(anyLong())).thenReturn(companyDto);

        assertEquals(priceListService.getById(1L), priceListDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(priceListRepository), ArgumentMatchers.eq("PriceList"));
        verify(priceListRepository, times(1)).getById(ArgumentMatchers.eq(1L));

    }


    @Test
    void create() {
        priceListService.create(priceListDto);
        verify(priceListRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(priceListDto)));
    }

    @Test
    void update() {
        priceListService.update(priceListDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(priceListDto.getId()), ArgumentMatchers.eq(priceListRepository), ArgumentMatchers.eq("PriceList"));
        verify(priceListRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(priceListDto)));
    }

    @Test
    void deleteById() {
        priceListService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(priceListRepository), ArgumentMatchers.eq("PriceList"));
        verify(priceListRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}
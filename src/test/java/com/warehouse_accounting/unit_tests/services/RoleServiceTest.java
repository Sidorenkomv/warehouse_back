package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.repositories.RoleRepository;
import com.warehouse_accounting.services.impl.RoleServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    private static Role role1;
    private static Role role2;
    private static List<Role> roleList;

    @InjectMocks
    private RoleServiceImpl roleService;
    @Mock
    private RoleRepository roleRepository;

    @Mock
    private CheckEntityService checkEntityService;

    @BeforeAll
    static void initMethod() {
        role1 = Role.builder()
                .id((long) 1)
                .name("first")
                .build();
        role2 = Role.builder()
                .id((long) 2)
                .name("second")
                .build();
        roleList = List.of(role1, role2);
    }

    @Test
    void getAll() {
        when(roleRepository.findAll()).thenReturn(roleList);
        List<Role> resultRoleDtoList = roleService.getAll();
        Assert.notNull(resultRoleDtoList, "а тут вылез null");
        Assertions.assertEquals(resultRoleDtoList, roleList);
        Mockito.verify(roleRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void getById() {
        when(roleRepository.getById((long) 1)).thenReturn(role1);

        Assertions.assertEquals(roleService.getById((long) 1), role1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(roleRepository), ArgumentMatchers.eq("Role"));
        Mockito.verify(roleRepository, Mockito.times(1))
                .getById(ArgumentMatchers.eq((long) 1));

    }

    @Test
    void create() {
        roleService.create(role1);
        Mockito.verify(roleRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(role1));
    }

    @Test
    void update() {
        roleService.update(role2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(role2.getId()), ArgumentMatchers.eq(roleRepository), ArgumentMatchers.eq("Role"));
        Mockito.verify(roleRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(role2));
    }

    @Test
    void deleteById() {
        roleService.deleteById((long) 999);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(999L), ArgumentMatchers.eq(roleRepository), ArgumentMatchers.eq("Role"));
        Mockito.verify(roleRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq((long) 999));
    }
}
package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.repositories.InvoicesIssuedRepository;
import com.warehouse_accounting.services.impl.InvoicesIssuedServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class InvoicesIssuedServiceTest {

    @Mock
    private InvoicesIssuedRepository invoicesIssuedRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private InvoicesIssuedServiceImpl invoicesIssuedService;

    private static InvoicesIssuedDto invoicesIssuedDto;

    private static List<InvoicesIssuedDto> invoicesIssuedDtoList;

    @BeforeAll
    static void init(){
        invoicesIssuedDto = InvoicesIssuedDto.builder()
                .id(2L)
                .data(LocalDateTime.now())
                .sum(BigDecimal.valueOf(100))
                .sent(false)
                .printed(false)
                .companyId(1L)
                .contractorId(1L)
                .comment("Второй")
                .build();

        invoicesIssuedDtoList = List.of(invoicesIssuedDto);
    }

    @Test
    void getAllTest(){
        when(invoicesIssuedRepository.getAll()).thenReturn(invoicesIssuedDtoList);
        List<InvoicesIssuedDto> invoicesIssuedServiceList = invoicesIssuedService.getAll();
        assertNotNull(invoicesIssuedServiceList, "Null");
        assertEquals(invoicesIssuedServiceList, invoicesIssuedDtoList);
        verify(invoicesIssuedRepository, times(1)).getAll();
    }

    @Test
    void getByIdTest(){
        when(invoicesIssuedRepository.getById(2L)).thenReturn(invoicesIssuedDto);

        assertEquals(invoicesIssuedService.getById(2L), invoicesIssuedDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(invoicesIssuedRepository), ArgumentMatchers.eq("InvoicesIssued"));
        verify(invoicesIssuedRepository, times(1)).getById(ArgumentMatchers.eq(2L));
    }

    @Test
    void createTest(){
        invoicesIssuedService.create(invoicesIssuedDto);
        verify(invoicesIssuedRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(invoicesIssuedDto)));
    }

    @Test
    void updateTest(){
        invoicesIssuedService.update(invoicesIssuedDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(invoicesIssuedDto.getId()), ArgumentMatchers.eq(invoicesIssuedRepository), ArgumentMatchers.eq("InvoicesIssued"));
        verify(invoicesIssuedRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(invoicesIssuedDto)));
    }

    @Test
    void deleteTest(){
        invoicesIssuedService.deleteById(2L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(invoicesIssuedRepository), ArgumentMatchers.eq("InvoicesIssued"));
        verify(invoicesIssuedRepository, times(1))
                .deleteById(ArgumentMatchers.eq(2L));
    }
}

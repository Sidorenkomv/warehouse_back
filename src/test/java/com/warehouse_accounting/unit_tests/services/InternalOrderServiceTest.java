package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.InternalOrderDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.InternalOrderRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.impl.InternalOrderServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InternalOrderServiceTest {

    @Mock
    private InternalOrderRepository internalOrderRepository;

    @Mock
    private TaskRepository tasksRepository;

    @Mock
    private FileRepository fileRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private InternalOrderServiceImpl internalOrderService;

    private static InternalOrderDto internalOrderDto;

    private static List<InternalOrderDto> internalOrderDtoList = new ArrayList<>();

    @BeforeAll
    static void init() {
        internalOrderDto = InternalOrderDto.builder()
                .id(2L)
                .number("num1")
                .localDateTime(LocalDateTime.now())
                .organization(CompanyDto.builder().build())
                .sum(BigDecimal.valueOf(80000L))
                .shipped(BigDecimal.ZERO)
                .isSharedAccess(true)
                .sent(false)
                .print(false)
                .comment("comment1")
                .build();

        internalOrderDtoList.add(internalOrderDto);
    }

    @Test
    void getAll() {
        when(tasksRepository.getListTaskById(anyLong())).thenReturn(new ArrayList<>());
        when(fileRepository.getListFileById(anyLong())).thenReturn(new ArrayList<>());
        when(internalOrderRepository.getAll()).thenReturn(internalOrderDtoList);

        List<InternalOrderDto> internalOrderDtos = internalOrderService.getAll();

        assertNotNull(internalOrderDtos);
        assertEquals(internalOrderDtoList, internalOrderDtos);
        verify(internalOrderRepository, times(1)).getAll();
        verify(fileRepository, times(1)).getListFileById(internalOrderDto.getId());
        verify(tasksRepository, times(1)).getListTaskById(internalOrderDto.getId());
    }

    @Test
    void getById() {
        when(tasksRepository.getListTaskById(anyLong())).thenReturn(new ArrayList<>());
        when(fileRepository.getListFileById(anyLong())).thenReturn(new ArrayList<>());
        when(internalOrderRepository.getById(internalOrderDto.getId())).thenReturn(internalOrderDto);

        assertEquals(internalOrderService.getById(internalOrderDto.getId()), internalOrderDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(internalOrderDto.getId()), ArgumentMatchers.eq(internalOrderRepository), ArgumentMatchers.eq("InternalOrder"));
        verify(internalOrderRepository, times(1)).getById(ArgumentMatchers.eq(internalOrderDto.getId()));
    }

    @Test
    void create() {
        internalOrderService.create(internalOrderDto);

        verify(internalOrderRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(internalOrderDto)));
    }

    @Test
    void update() {
        internalOrderService.update(internalOrderDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(internalOrderDto.getId()), ArgumentMatchers.eq(internalOrderRepository), ArgumentMatchers.eq("InternalOrder"));
        verify(internalOrderRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(internalOrderDto)));
    }

    @Test
    void deleteById() {
        internalOrderService.deleteById(internalOrderDto.getId());

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(internalOrderDto.getId()), ArgumentMatchers.eq(internalOrderRepository), ArgumentMatchers.eq("InternalOrder"));
        verify(internalOrderRepository, times(1)).deleteById(ArgumentMatchers.eq(internalOrderDto.getId()));
    }
}

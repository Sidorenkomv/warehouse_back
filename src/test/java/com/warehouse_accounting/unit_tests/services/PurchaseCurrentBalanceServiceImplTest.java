package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.repositories.PurchaseCurrentBalanceRepository;
import com.warehouse_accounting.services.impl.PurchaseCurrentBalanceServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PurchaseCurrentBalanceServiceImplTest {

    @Mock
    private PurchaseCurrentBalanceRepository purchaseCurrentBalanceRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private PurchaseCurrentBalanceServiceImpl purchaseCurrentBalanceService;

    private static PurchaseCurrentBalanceDto purchaseCurrentBalanceDto;

    private static List<PurchaseCurrentBalanceDto> purchaseCurrentBalanceDtoList;

    @BeforeEach
    void init() {
        purchaseCurrentBalanceDto = PurchaseCurrentBalanceDto.builder()
                .id(1L)
                .remainder(1L)
                .productReserved(1L)
                .productsAwaiting(1L)
                .productAvailableForOrder(1L)
                .daysStoreOnTheWarehouse(1L)
                .build();

        purchaseCurrentBalanceDtoList = List.of(purchaseCurrentBalanceDto);
    }

    @Test
    void getAll() {
        when(purchaseCurrentBalanceRepository.getAll()).thenReturn(purchaseCurrentBalanceDtoList);
        List<PurchaseCurrentBalanceDto> purchaseCurrentBalanceDtos = purchaseCurrentBalanceService.getAll();
        assertNotNull(purchaseCurrentBalanceDtos, "Null");
        assertEquals(purchaseCurrentBalanceDtos, purchaseCurrentBalanceDtos);
        verify(purchaseCurrentBalanceRepository, times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseCurrentBalanceRepository.getById(1L)).thenReturn(purchaseCurrentBalanceDto);

        assertEquals(purchaseCurrentBalanceService.getById(1L), purchaseCurrentBalanceDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseCurrentBalanceRepository), ArgumentMatchers.eq("PurchaseCurrentBalance"));
        verify(purchaseCurrentBalanceRepository, times(1))
                .getById(1L);
    }

    @Test
    void create() {
        purchaseCurrentBalanceService.create(purchaseCurrentBalanceDto);
        verify(purchaseCurrentBalanceRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseCurrentBalanceDto)));
    }

    @Test
    void update() {
        purchaseCurrentBalanceService.update(purchaseCurrentBalanceDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(purchaseCurrentBalanceDto.getId()), ArgumentMatchers.eq(purchaseCurrentBalanceRepository), ArgumentMatchers.eq("PurchaseCurrentBalance"));
        verify(purchaseCurrentBalanceRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseCurrentBalanceDto)));
    }

    @Test
    void delete() {
        purchaseCurrentBalanceService.delete(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseCurrentBalanceRepository), ArgumentMatchers.eq("PurchaseCurrentBalance"));
        verify(purchaseCurrentBalanceRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

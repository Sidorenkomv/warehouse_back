package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.repositories.AddressRepository;
import com.warehouse_accounting.repositories.CityRepository;
import com.warehouse_accounting.repositories.CountryRepository;
import com.warehouse_accounting.repositories.RegionRepository;
import com.warehouse_accounting.repositories.StreetRepository;
import com.warehouse_accounting.services.impl.AddressServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AddressServiceImplTest {

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private RegionRepository regionRepository;

    @Mock
    private CityRepository cityRepository;

    @Mock
    private StreetRepository streetRepository;

    @Mock
    private CheckEntityService checkEntityService;

    @InjectMocks
    private AddressServiceImpl addressService;

    private static AddressDto addressDto;

    private static List<AddressDto> addressDtoList = new ArrayList<>();

    @BeforeAll
    static void init(){
        addressDto = AddressDto.builder()
                .id(1L)
                .countryId(2L)
                .postCode("postCode")
                .regionId(3L)
                .cityId(2L)
                .cityName("cityName")
                .streetId(5L)
                .streetName("streetName")
                .buildingId(5L)
                .buildingName("buildingName")
                .office("office")
                .fullAddress("fullAddress")
                .other("other")
                .comment("comment")
                .build();
        addressDtoList.add(addressDto);
    }

    @Test
    void getAll() {
        when(addressRepository.getAll()).thenReturn(addressDtoList);
        List<AddressDto> addressDtoListTest = addressService.getAll();
        assertNotNull(addressDtoListTest);
        assertEquals(addressDtoListTest, addressDtoList);
        verify(addressRepository).getAll();
    }

    @Test
    void getById() {
        when(addressRepository.getById(1L)).thenReturn(addressDto);
        AddressDto addressDtoTest = addressService.getById(1L);
        assertNotNull(addressDtoTest);
        assertEquals(addressDtoTest, addressDto);
        verify(checkEntityService).checkExist(eq(1L), eq(addressRepository), eq("AddressDto"));
        verify(addressRepository).getById(1L);
    }

    @Test
    void create() {
        addressService.create(addressDto);
        verify(addressRepository, times(1)).save(eq(ConverterDto.convertToModel(addressDto)));
    }

    @Test
    void update() {
        addressService.update(addressDto);
        verify(checkEntityService, times(1))
                .checkExist(eq(addressDto.getId()), eq(addressRepository), ArgumentMatchers.eq("Address"));
        verify(addressRepository, times(1))
                .save(eq(ConverterDto.convertToModel(addressDto)));
    }

    @Test
    void deleteById() {
        addressService.deleteById(1L);
        verify(checkEntityService, times(1))
                .checkExist(eq(1L), eq(addressRepository), eq("Address"));
        verify(addressRepository, times(1)).deleteById(eq(1L));
    }
}
package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.ProductionTasks;
import com.warehouse_accounting.models.dto.ProductionTasksDto;
import com.warehouse_accounting.repositories.ProductionTasksRepository;
import com.warehouse_accounting.services.impl.ProductionTasksServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductionTasksTest {

    @Mock
    private ProductionTasksRepository productionTasksRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ProductionTasksServiceImpl productionTasksService;

    private final ProductionTasksDto productionTasksDto = ProductionTasksDto.builder()
            .id(1L)
            .isAccessed(true)
            .additionalFieldsNames(new ArrayList<>())
            .additionalFieldsIds(new ArrayList<>())
            .build();

    private final List<ProductionTasksDto> productionTasksDtoList = List.of(productionTasksDto);

    private final List<ProductionTasks> productionTasksList = List.of(ConverterDto.convertToModel(productionTasksDto));

    @Test
    void getAllTest() {
        doReturn(productionTasksList).when(productionTasksRepository).findAll();
        List<ProductionTasksDto> productionTasksDtoListTest = productionTasksService.getAll();
        assertNotNull(productionTasksDtoListTest, "productionTasksDtoListTest = null");
        assertEquals(productionTasksDtoListTest, productionTasksDtoList);
        verify(productionTasksRepository, times(1)).findAll();
    }

    @Test
    void getByIdTest() {
        doReturn(Optional.of(ConverterDto.convertToModel(productionTasksDto)))
                .when(productionTasksRepository).findById(1L);

        assertEquals(productionTasksService.getById(1L), productionTasksDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productionTasksRepository), ArgumentMatchers.eq("ProductionTasks"));
        verify(productionTasksRepository, times(1)).findById(1L);
    }

    @Test
    void createTest() {
        productionTasksService.create(productionTasksDto);
        verify(productionTasksRepository, times(1))
                .save(ConverterDto.convertToModel(productionTasksDto));
    }

    @Test
    void updateTest() {
        productionTasksService.update(productionTasksDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(productionTasksDto.getId()), ArgumentMatchers.eq(productionTasksRepository), ArgumentMatchers.eq("ProductionTasks"));
        verify(productionTasksRepository, times(1))
                .save(ConverterDto.convertToModel(productionTasksDto));
    }

    @Test
    void deleteTest() {
        productionTasksService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productionTasksRepository), ArgumentMatchers.eq("ProductionTasks"));
        verify(productionTasksRepository, times(1))
                .deleteById(1L);
    }
}

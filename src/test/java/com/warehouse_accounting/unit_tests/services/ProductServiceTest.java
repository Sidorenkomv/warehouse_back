package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.Image;
import com.warehouse_accounting.models.ProductPrice;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.repositories.ImageRepository;
import com.warehouse_accounting.repositories.ProductPriceRepository;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.services.impl.ProductServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductPriceRepository productPriceRepository;

    @Mock
    private ImageRepository imageRepository;
    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ProductServiceImpl productService;

    private static ProductDto productDto;
    private static List<ProductDto> productDtoList;

    private static ProductPrice productPrice;

    private static List<ProductPrice> productPrices;

    private static Image image;

    private static List<Image> images;

    @BeforeAll
    static void initMethod() {
        productDto = ProductDto.builder()
                .id(1L)
                .name("Product")
                .weight(BigDecimal.valueOf(0))
                .volume(BigDecimal.valueOf(0))
                .purchasePrice(BigDecimal.valueOf(0))
                .description("Description")
                .unitDto(UnitDto.builder().build())
                .archive(false)
                .contractorDto(ContractorDto.builder().build())
                .productPricesDto(new ArrayList<>())
                .taxSystemDto(TaxSystemDto.builder().build())
                .imagesDto(new ArrayList<>())
//                .productGroupDto(ProductGroupDto.builder().build())
                .attributeOfCalculationObjectDto(AttributeOfCalculationObjectDto.builder().build())
                .build();

        productPrice = ProductPrice.builder()
                .price(BigDecimal.valueOf(60000L))
                .build();

        image = Image.builder()
                .imageUrl("url1")
                .sortNumber("sortNumber1")
                .build();

        productDtoList = List.of(productDto);
        productPrices = List.of(productPrice);
        images = List.of(image);
    }

    @Test
    void getAll() {
        when(productRepository.getAll()).thenReturn(productDtoList);
        when(productPriceRepository.getListProductPriceById(anyLong())).thenReturn(productPrices);
        when(imageRepository.getListImageById(anyLong())).thenReturn(images);

        List<ProductDto> productDtoListInTest = productService.getAll();
        assertNotNull(productDtoListInTest, "productDtoListInTest == null");
        assertEquals(productDtoListInTest, productDtoList);
        verify(productRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(productRepository.getById(1L)).thenReturn(productDto);
        when(productPriceRepository.getListProductPriceById(anyLong())).thenReturn(productPrices);
        when(imageRepository.getListImageById(anyLong())).thenReturn(images);

        assertEquals(productService.getById(1L), productDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productRepository), ArgumentMatchers.eq("Product"));
        verify(productRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        productService.create(productDto);
        verify(productRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertToModel(productDto))));
    }

    @Test
    void update() {
        productService.update(productDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(productDto.getId()), ArgumentMatchers.eq(productRepository), ArgumentMatchers.eq("Product"));
        verify(productRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertToModel(productDto))));
    }

    @Test
    void deleteById() {
        productService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productRepository), ArgumentMatchers.eq("Product"));
        verify(productRepository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }

}
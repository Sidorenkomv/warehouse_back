package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.Task;
import com.warehouse_accounting.models.dto.TechnologicalOperationDto;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.repositories.TechnologicalOperationRepository;
import com.warehouse_accounting.services.impl.TechnologicalOperationServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TechnologicalOperationServiceImpTest {

    @Mock
    private TechnologicalOperationRepository technologicalOperationRepository;

    @Mock
    private CheckEntityService checkEntityService;

    @Mock
    private TaskRepository taskRepository;
    @InjectMocks
    private TechnologicalOperationServiceImpl technologicalOperationService;

    private static TechnologicalOperationDto technologicalOperationDto;
    private static List<TechnologicalOperationDto> technologicalOperationDtoList;

    private static Task task;

    private static List<Task> taskList;

    @BeforeAll
    static void init() {

        technologicalOperationDto = TechnologicalOperationDto.builder()
                .id(1L)
                .build();

        task = Task.builder()
                .id(2L)
                .isDone(false)
                .build();

        technologicalOperationDtoList = List.of(technologicalOperationDto);
        taskList = List.of(task);
    }

    @Test
    void testGetAll() {
        when(technologicalOperationRepository.getAll()).thenReturn(technologicalOperationDtoList);
        when(taskRepository.getListTaskById(anyLong())).thenReturn(taskList);

        List<TechnologicalOperationDto> technologicalOperationDtoListTest = technologicalOperationService.getAll();
        assertNotNull(technologicalOperationDtoListTest, "technologicalOperationDtoListTest is null");
        assertEquals(technologicalOperationDtoList, technologicalOperationDtoListTest);
        verify(technologicalOperationRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(technologicalOperationRepository.getById(1L)).thenReturn(technologicalOperationDto);
        when(taskRepository.getListTaskById(anyLong())).thenReturn(taskList);

        TechnologicalOperationDto technologicalOperationDtoTest = technologicalOperationService.getById(1L);

        assertNotNull(technologicalOperationDtoTest, "technologicalOperationDtoTest is null");
        assertEquals(technologicalOperationDtoTest, technologicalOperationDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(technologicalOperationRepository), ArgumentMatchers.eq("TechnologicalOperation"));
        verify(technologicalOperationRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        TechnologicalOperationDto dto = TechnologicalOperationDto.builder()
                .id(3L)
                .number("num1")
                .comments("comments1")
                .build();

        technologicalOperationService.create(dto);

        verify(technologicalOperationRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(dto)));
    }

    @Test
    void testUpdate() {
        TechnologicalOperationDto dto = TechnologicalOperationDto.builder()
                .id(3L)
                .number("num1")
                .comments("comments1")
                .build();

        technologicalOperationService.update(dto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(dto.getId()), ArgumentMatchers.eq(technologicalOperationRepository), ArgumentMatchers.eq("TechnologicalOperation"));
        verify(technologicalOperationRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(dto)));
    }

    @Test
    void testDelete() {
        technologicalOperationService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(technologicalOperationRepository), ArgumentMatchers.eq("TechnologicalOperation"));
        verify(technologicalOperationRepository, times(1)).deleteById(1L);
    }
}

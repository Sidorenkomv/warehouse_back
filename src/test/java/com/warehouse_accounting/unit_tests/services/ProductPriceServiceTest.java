package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.ProductPrice;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.models.dto.ProductPriceForPriceListDto;
import com.warehouse_accounting.models.dto.TypeOfPriceDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.repositories.ProductPriceRepository;
import com.warehouse_accounting.services.impl.ProductPriceServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ProductPriceServiceTest {

    @Mock
    private ProductPriceRepository productPriceRepository;
    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ProductPriceServiceImpl productPriceService;

    private static ProductPrice productPrice;
    private static ProductPriceDto productPriceDto;
    private static ProductPriceForPriceListDto productPriceForPriceListDto;
    private static List<ProductPrice> productPriceList;
    private static List<ProductPriceDto> productPriceDtoList;
    private static List<ProductPriceForPriceListDto> productPriceForPriceListDtoList;

    @BeforeAll
    static void initMethod() {

        productPrice = ProductPrice.builder()
                .build();

        productPriceList = List.of(productPrice);

        productPriceDto = ProductPriceDto.builder()
                .id(1L)
                .productDto(ProductDto.builder().id(5L).build())
                .typeOfPriceDto(TypeOfPriceDto.builder().id(2L).build())
                .price(BigDecimal.valueOf(100))
                .build();

        productPriceDtoList = List.of(productPriceDto);

        productPriceForPriceListDto = ProductPriceForPriceListDto.builder()
                .id(1L)
                .typeOfPriceDto(TypeOfPriceDto.builder().id(2L).build())
                .price(BigDecimal.valueOf(100))
                .build();

        productPriceForPriceListDtoList = List.of(productPriceForPriceListDto);
    }

    @Test
    void getAll() {
        when(productPriceRepository.findAll()).thenReturn(productPriceList);
        List<ProductPriceForPriceListDto> productPriceDtoListInTest = productPriceService.getAll();

        assertThat(productPriceDtoListInTest).hasSize(1)
                .isEqualTo(productPriceForPriceListDtoList);

        verify(productPriceRepository, times(1)).findAll();
    }

    @Test
    void getById() {
        when(productPriceRepository.findById(1L)).thenReturn(Optional.of(productPrice));

        assertEquals(productPriceService.getById(1L), productPriceForPriceListDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productPriceRepository), ArgumentMatchers.eq("ProductPrice"));
        verify(productPriceRepository, times(1)).findById(ArgumentMatchers.eq(1L));
    }


    @Test
    void create() {
        productPriceService.create(productPriceDtoList);
        verify(productPriceRepository, times(1))
                .saveAll(ArgumentMatchers.eq(productPriceDtoList.stream()
                        .map(a -> ConverterDto.convertToModel(a))
                        .collect(Collectors.toList())));
    }

    @Test
    void update() {
        productPriceService.update(productPriceDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(productPriceDto.getId()), ArgumentMatchers.eq(productPriceRepository), ArgumentMatchers.eq("ProductPrice"));
        verify(productPriceRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertToModel(productPriceDto))));
    }

    @Test
    void deleteById() {
        productPriceService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(productPriceRepository), ArgumentMatchers.eq("ProductPrice"));
        verify(productPriceRepository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }

}

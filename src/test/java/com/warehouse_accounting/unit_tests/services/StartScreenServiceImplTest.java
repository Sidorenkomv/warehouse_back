package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.StartScreenDto;
import com.warehouse_accounting.repositories.StartScreenRepository;
import com.warehouse_accounting.services.impl.StartScreenServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StartScreenServiceImplTest {

    private static StartScreenDto startScreenDto1;
    private static StartScreenDto startScreenDto2;
    private static List<StartScreenDto> startScreenDtoList;

    @InjectMocks
    private StartScreenServiceImpl startScreenService;
    @Mock
    private CheckEntityService checkEntityService;
    @Mock
    private StartScreenRepository startScreenRepository;

    @BeforeAll
    static void initStartScreen() {
        startScreenDto1 = StartScreenDto.builder()
                .id(1L)
                .startScreen("meow")
                .build();
        startScreenDto2 = StartScreenDto.builder()
                .id(2L)
                .startScreen("meow_meow")
                .build();
        startScreenDtoList = List.of(startScreenDto1, startScreenDto2);
    }

    @Test
    void getAll() {
        when(startScreenRepository.getAll()).thenReturn(startScreenDtoList);
        List<StartScreenDto> startScreenDtoList1 = startScreenService.getAll();
        Assert.notNull(startScreenDtoList1, "It's a Null");
        Assertions.assertEquals(startScreenDtoList1, startScreenDtoList);
        Mockito.verify(startScreenRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(startScreenRepository.getById(1L)).thenReturn(startScreenDto1);

        Assertions.assertEquals(startScreenService.getById(1L), startScreenDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(startScreenRepository), ArgumentMatchers.eq("StartScreen"));
        Mockito.verify(startScreenRepository, Mockito.times(1))
                .getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        startScreenService.create(startScreenDto1);
        Mockito.verify(startScreenRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(startScreenDto1)));
    }

    @Test
    void update() {
        startScreenService.update(startScreenDto2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(startScreenDto2.getId()), ArgumentMatchers.eq(startScreenRepository), ArgumentMatchers.eq("StartScreen"));
        Mockito.verify(startScreenRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(startScreenDto2)));
    }

    @Test
    void deleteById() {
        startScreenService.deleteById(3L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(startScreenRepository), ArgumentMatchers.eq("StartScreen"));
        Mockito.verify(startScreenRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

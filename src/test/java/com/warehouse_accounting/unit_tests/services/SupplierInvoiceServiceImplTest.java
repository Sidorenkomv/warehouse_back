package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import com.warehouse_accounting.repositories.SupplierInvoiceRepository;
import com.warehouse_accounting.services.impl.SupplierInvoiceServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SupplierInvoiceServiceImplTest {
    @Mock
    private SupplierInvoiceRepository supplierInvoiceRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private SupplierInvoiceServiceImpl supplierInvoiceService;

    private SupplierInvoiceDto supplierInvoiceDto = SupplierInvoiceDto.builder()
            .id(1L)
            .invoiceNumber("number")
            .dateInvoiceNumber("10-02-2022")
            .checkboxProd(false)
            .organization("companyName")
            .warehouse("warehouseName")
            .contrAgent("contrAgentName")
            .contract("contractName")
            .datePay("15-02-2022")
            .project("projectName")
            .incomingNumber("number")
            .dateIncomingNumber("number")
            .checkboxName(false)
            .checkboxNDS(false)
            .checkboxOnNDS(false)
            .addPosition("position")
            .addComment("comment")
            .build();

    private final List<SupplierInvoiceDto> supplierInvoiceDtoList = List.of(supplierInvoiceDto);

    @Test
    void test_getAll() {
        when(supplierInvoiceRepository.getAll()).thenReturn(supplierInvoiceDtoList);
        List<SupplierInvoiceDto> supplierInvoiceDtoListInTest = supplierInvoiceService.getAll();
        assertNotNull(supplierInvoiceDtoListInTest, "supplierInvoiceDtoListInTest == null");
        assertEquals(supplierInvoiceDtoListInTest, supplierInvoiceDtoList);
        verify(supplierInvoiceRepository, times(1)).getAll();
    }

    @Test
    void test_getById() {
        when(supplierInvoiceRepository.getById(1L)).thenReturn(supplierInvoiceDto);

        assertEquals(supplierInvoiceService.getById(1L), supplierInvoiceDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(supplierInvoiceRepository), ArgumentMatchers.eq("SupplierInvoice"));
        verify(supplierInvoiceRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void test_create() {
        supplierInvoiceService.create(supplierInvoiceDto);
        verify(supplierInvoiceRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertToModel(supplierInvoiceDto))));
    }

    @Test
    void test_update() {
        supplierInvoiceService.update(supplierInvoiceDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(supplierInvoiceDto.getId()), ArgumentMatchers.eq(supplierInvoiceRepository), ArgumentMatchers.eq("SupplierInvoice"));
        verify(supplierInvoiceRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertToModel(supplierInvoiceDto))));
    }

    @Test
    void test_delete() {
        supplierInvoiceService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(supplierInvoiceRepository), ArgumentMatchers.eq("SupplierInvoice"));
        verify(supplierInvoiceRepository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }
}

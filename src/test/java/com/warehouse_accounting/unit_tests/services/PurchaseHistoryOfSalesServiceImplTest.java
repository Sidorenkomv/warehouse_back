package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.repositories.PurchaseHistoryOfSalesRepository;
import com.warehouse_accounting.services.impl.PurchaseHistoryOfSalesServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PurchaseHistoryOfSalesServiceImplTest {

    @Mock
    private PurchaseHistoryOfSalesRepository purchaseHistoryOfSalesRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private PurchaseHistoryOfSalesServiceImpl purchaseHistoryOfSalesService;

    private static PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto;

    private static List<PurchaseHistoryOfSalesDto> purchaseHistoryOfSalesDtoList;

    @BeforeEach
    void init() {
        purchaseHistoryOfSalesDto = PurchaseHistoryOfSalesDto.builder()
                .id(1L)
                .number(1L)
                .productPriceId(1L)
                .sum(BigDecimal.valueOf(100))
                .profit(BigDecimal.valueOf(100))
                .profitability(BigDecimal.valueOf(100))
                .saleOfDay(BigDecimal.valueOf(100))
                .build();

        purchaseHistoryOfSalesDtoList = List.of(purchaseHistoryOfSalesDto);
    }

    @Test
    void getAll() {
        when(purchaseHistoryOfSalesRepository.getAll()).thenReturn(purchaseHistoryOfSalesDtoList);
        List<PurchaseHistoryOfSalesDto> purchaseHistoryOfSalesDtos = purchaseHistoryOfSalesService.getAll();
        assertNotNull(purchaseHistoryOfSalesDtos, "Null");
        assertEquals(purchaseHistoryOfSalesDtos, purchaseHistoryOfSalesDtoList);
        verify(purchaseHistoryOfSalesRepository, times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseHistoryOfSalesRepository.getById(1L)).thenReturn(purchaseHistoryOfSalesDto);

        assertEquals(purchaseHistoryOfSalesService.getById(1L), purchaseHistoryOfSalesDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseHistoryOfSalesRepository), ArgumentMatchers.eq("PurchaseHistoryOfSales"));
        verify(purchaseHistoryOfSalesRepository, times(1))
                .getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        purchaseHistoryOfSalesService.create(purchaseHistoryOfSalesDto);
        verify(purchaseHistoryOfSalesRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseHistoryOfSalesDto)));
    }

    @Test
    void update() {
        purchaseHistoryOfSalesService.update(purchaseHistoryOfSalesDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(purchaseHistoryOfSalesDto.getId()), ArgumentMatchers.eq(purchaseHistoryOfSalesRepository), ArgumentMatchers.eq("PurchaseHistoryOfSales"));
        verify(purchaseHistoryOfSalesRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseHistoryOfSalesDto)));
    }

    @Test
    void delete() {
        purchaseHistoryOfSalesService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseHistoryOfSalesRepository), ArgumentMatchers.eq("PurchaseHistoryOfSales"));
        verify(purchaseHistoryOfSalesRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));

    }
}

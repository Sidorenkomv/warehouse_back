package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.SalesChannelDto;
import com.warehouse_accounting.repositories.SalesChannelRepository;
import com.warehouse_accounting.services.impl.SalesChannelServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class SalesChannelTest {

    @Mock
    private SalesChannelRepository salesChannelRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private SalesChannelServiceImpl salesChannelService;

    private static SalesChannelDto salesChannelDto;
    private static List<SalesChannelDto> salesChannelDtoList;

    @BeforeAll
    static void initMethod() {
        salesChannelDto = new SalesChannelDto.Builder()
                .id(1L)
                .name("SalesChannel")
                .type("someType")
                .description("Description")
                .generalAccessC("someAccess")
                .ownerDepartment("John")
                .ownerEmployee("Kevin")
                .whenChanged("25/06/2017")
                .whoChanged("Sarah")
                .build();

        salesChannelDtoList = List.of(salesChannelDto);
    }

    @Test
    void getAll() {
        when(salesChannelRepository.getAll()).thenReturn(salesChannelDtoList);
        List<SalesChannelDto> salesChannelDtoList = salesChannelService.getAll();
        assertNotNull(salesChannelDtoList, "salesChannelDtoList == null");
        assertEquals(salesChannelDtoList, salesChannelDtoList);
        verify(salesChannelRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(salesChannelRepository.getById(1L)).thenReturn(salesChannelDto);

        assertEquals(salesChannelService.getById(1L), salesChannelDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(salesChannelRepository), ArgumentMatchers.eq("SalesChannel"));
        verify(salesChannelRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        salesChannelService.create(salesChannelDto);
        verify(salesChannelRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertSalesChannelDtoToModel(salesChannelDto))));
    }

    @Test
    void update() {
        salesChannelService.update(salesChannelDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(salesChannelDto.getId()), ArgumentMatchers.eq(salesChannelRepository), ArgumentMatchers.eq("SalesChannel"));
        verify(salesChannelRepository, times(1))
                .save(ArgumentMatchers.eq((ConverterDto.convertSalesChannelDtoToModel(salesChannelDto))));
    }

    @Test
    void deleteById() {
        salesChannelService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(salesChannelRepository), ArgumentMatchers.eq("SalesChannel"));
        verify(salesChannelRepository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }

}
package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.FileDto;
import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.models.dto.RoleDto;
import com.warehouse_accounting.models.dto.TariffDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.services.impl.FileServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileServiceImplTest {

    @Mock
    private FileRepository fileRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private FileServiceImpl fileService;

    private static FileDto fileDto;

    private static List<FileDto> fileDtoList = new ArrayList<>();

    @BeforeAll
    static void init() {

        fileDto = FileDto.builder()
                .id(1L)
                .name("Покупки")
                .size(10)
                .createdDate(LocalDate.now())
                .employeeDto(EmployeeDto.builder()
                        .roles(Set.of(RoleDto.builder().build()))
                        .position(PositionDto.builder().build())
                        .tariff(Set.of(TariffDto.builder().build()))
                        .ipAddress(Set.of(IpAddressDto.builder().build()))
                        .build())
                .build();

    }

    @Test
    void getAll() {

        when(fileRepository.findAll().stream()
                .map(ConverterDto::convertToDto
                ).collect(Collectors.toList())).thenReturn(fileDtoList);

        List<FileDto> fileDtoListTest = fileService.getAll();

        assertNotNull(fileDtoListTest, "fileDtoListTest == null");
        assertEquals(fileDtoListTest, fileDtoList);
        verify(fileRepository, times(1)).findAll();

    }

    @Test
    void getById() {
        when(fileRepository.findById(1L)).thenReturn(Optional.of(ConverterDto.convertToModel(fileDto)));

        assertEquals(fileService.getById(1L), fileDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(fileRepository), ArgumentMatchers.eq("File"));
        verify(fileRepository, times(1)).findById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        fileRepository.save(ConverterDto.convertToModel(fileDto));

        verify(fileRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(fileDto)));
    }

    @Test
    void update() {
        fileService.update(fileDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(fileDto.getId()), ArgumentMatchers.eq(fileRepository), ArgumentMatchers.eq("File"));
        verify(fileRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(fileDto)));
    }

    @Test
    void deleteById() {
        fileService.delete(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(fileRepository), ArgumentMatchers.eq("File"));
        verify(fileRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }

}

package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.InventoryDto;
import com.warehouse_accounting.repositories.InventoryRepository;
import com.warehouse_accounting.services.impl.InventoryServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class InventoryServiceImplTest {

    private static InventoryDto inventoryDto1;
    private static InventoryDto inventoryDto2;
    private static List<InventoryDto> inventoryDtoList;

    @Mock
    private InventoryRepository inventoryRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private InventoryServiceImpl inventoryService;

    @BeforeEach
    void initMethod() {
        inventoryDto1 = InventoryDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Коммент2")
                .build();

        inventoryDto2 = InventoryDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Коммент3")
                .build();

        inventoryDtoList = List.of(inventoryDto1, inventoryDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(inventoryRepository.getAll()).thenReturn(inventoryDtoList);
        List<InventoryDto> inventoryDtoListTest = inventoryService.getAllTest();
        assertNotNull(inventoryDtoListTest, "inventoryDtoList is not null");
        assertEquals(inventoryDtoList, inventoryDtoListTest);
        verify(inventoryRepository, times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(inventoryRepository.getById(2L)).thenReturn(inventoryDto1);

        assertEquals(inventoryService.getById(2L), inventoryDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(2L), ArgumentMatchers.eq(inventoryRepository), ArgumentMatchers.eq("Inventory"));
        Mockito.verify(inventoryRepository, Mockito.times(1))
                .getById(ArgumentMatchers.eq(2L));
    }

    @DisplayName("create method test")
    @Test
    void create() {
        inventoryService.create(inventoryDto1);
        Mockito.verify(inventoryRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(inventoryDto1)));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        inventoryService.update(inventoryDto2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(inventoryDto2.getId()), ArgumentMatchers.eq(inventoryRepository), ArgumentMatchers.eq("Inventory"));
        Mockito.verify(inventoryRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(inventoryDto2)));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        inventoryService.deleteById(3L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(inventoryRepository), ArgumentMatchers.eq("Inventory"));
        Mockito.verify(inventoryRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}

package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import com.warehouse_accounting.repositories.SupplierOrdersRepository;
import com.warehouse_accounting.services.impl.SupplierOrdersServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SupplierOrdersServiceImplTest {

    @Mock
    private SupplierOrdersRepository supplierOrdersRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private SupplierOrdersServiceImpl supplierOrdersService;

    private static SupplierOrdersDto supplierOrdersDto;
    private static List<SupplierOrdersDto> supplierOrdersDtoList;

    @BeforeAll
    static void initSupplierDto() {
        supplierOrdersDto = SupplierOrdersDto.builder()
                .id(1L)
                .number("1000")
                .createdAt(LocalDateTime.now())
                .contrAgentId(1L)
                .contrAgentName("Петя")
                .contrAgentAccount(100L)
                .companyId(1L)
                .companyName("Сбербанк")
                .companyAccount(100L)
                .sum(BigDecimal.valueOf(10000L))
                .invoiceIssued(10000L)
                .paid(5000L)
                .notPaid(5000L)
                .accepted(5000L)
                .waiting(5000L)
                .refundAmount(null)
                .acceptanceDate(LocalDateTime.now().plusDays(5L))
                .projectId(1L)
                .projectName("warehouse_accounting")
                .warehouseId(1L)
                .warehouseName("Склад1")
                .contractId(1L)
                .contractNumber("123SAD3")
                .generalAccess(false)
                .departmentId(1L)
                .departmentName("IT")
                .employeeId(1L)
                .employeeFirstname("Вася")
                .sent(true)
                .print(true)
                .comment(null)
                .updatedAt(LocalDateTime.now())
                .updatedFromEmployeeId(1L)
                .updatedFromEmployeeFirstname("Маша")
                .build();

        supplierOrdersDtoList = List.of(supplierOrdersDto);
    }

    @Test
    void testGetAll() {
        when(supplierOrdersRepository.getAll()).thenReturn(supplierOrdersDtoList);
        List<SupplierOrdersDto> supplierOrdersDtoListTest = supplierOrdersService.getAll();
        assertNotNull(supplierOrdersDtoListTest, "supplierOrdersDtoListTest is not null");
        assertEquals(supplierOrdersDtoListTest, supplierOrdersDtoList);
        verify(supplierOrdersRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(supplierOrdersRepository.getById(1L)).thenReturn(supplierOrdersDto);

        SupplierOrdersDto supplierOrdersDtoTest = supplierOrdersService.getById(1L);

        assertNotNull(supplierOrdersDtoTest, "supplierOrdersDtoTest is null");
        assertEquals(supplierOrdersDtoTest, supplierOrdersDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(supplierOrdersRepository), ArgumentMatchers.eq("SupplierOrders"));
        verify(supplierOrdersRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        supplierOrdersService.create(supplierOrdersDto);
        verify(supplierOrdersRepository, times(1))
                .save(ConverterDto.convertToModel(supplierOrdersDto));
    }

    @Test
    void testUpdate() {
        supplierOrdersService.update(supplierOrdersDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(supplierOrdersDto.getId()), ArgumentMatchers.eq(supplierOrdersRepository), ArgumentMatchers.eq("SupplierOrders"));
        verify(supplierOrdersRepository, times(1))
                .save(ConverterDto.convertToModel(supplierOrdersDto));
    }

    @Test
    void testDelete() {
        supplierOrdersService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(supplierOrdersRepository), ArgumentMatchers.eq("SupplierOrders"));
        verify(supplierOrdersRepository, times(1)).deleteById(1L);
    }
}

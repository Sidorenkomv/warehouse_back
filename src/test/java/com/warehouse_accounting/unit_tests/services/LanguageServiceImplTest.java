package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.LanguageDto;
import com.warehouse_accounting.repositories.LanguageRepository;
import com.warehouse_accounting.services.impl.LanguageServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LanguageServiceImplTest {

    @Mock
    private LanguageRepository languageRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private LanguageServiceImpl languageService;

    private static LanguageDto languageDto;

    private static List<LanguageDto> languageDtoList;

    @BeforeAll
    static void init() {
        languageDto = LanguageDto.builder()
                .id(1L)
                .language("Русский")
                .build();

        languageDtoList = List.of(languageDto);
    }

    @Test
    void getAll() {
        when(languageRepository.getAll()).thenReturn(languageDtoList);
        List<LanguageDto> result = languageService.getAll();
        assertNotNull(result, "languageDtoListTest is null");
        assertEquals(result, languageDtoList);
        verify(languageRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(languageRepository.getById(1L)).thenReturn(languageDto);

        assertEquals(languageService.getById(1L), languageDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(languageRepository), ArgumentMatchers.eq("Language"));
        verify(languageRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        languageService.create(languageDto);
        verify(languageRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(languageDto)));
    }

    @Test
    void update() {
        languageService.update(languageDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(languageDto.getId()), ArgumentMatchers.eq(languageRepository), ArgumentMatchers.eq("Language"));
        verify(languageRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(languageDto)));
    }

    @Test
    void deleteById() {
        languageService.deleteById(1L);
        verify(languageRepository, times(1)).deleteById(ArgumentMatchers.eq(1L));
    }
}

package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.repositories.PurchaseForecastRepository;
import com.warehouse_accounting.services.impl.PurchaseForecastServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PurchaseForecastServiceImplTest {

    @Mock
    private PurchaseForecastRepository purchaseForecastRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private PurchaseForecastServiceImpl purchaseForecastService;

    private static PurchaseForecastDto purchaseForecastDto;

    private static List<PurchaseForecastDto> purchaseForecastDtoList;

    @BeforeEach
    void init() {
        purchaseForecastDto = PurchaseForecastDto.builder()
                .id(1L)
                .reservedProduct(1L)
                .reservedDays(1L)
                .ordered(false)
                .build();

        purchaseForecastDtoList = List.of(purchaseForecastDto);
    }

    @Test
    void getAll() {
        when(purchaseForecastRepository.getAll()).thenReturn(purchaseForecastDtoList);
        List<PurchaseForecastDto> forecastDtoList = purchaseForecastService.getAll();
        assertNotNull(forecastDtoList, "Null");
        assertEquals(forecastDtoList, purchaseForecastDtoList);
        verify(purchaseForecastRepository, times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseForecastRepository.getById(1L)).thenReturn(purchaseForecastDto);

        assertEquals(purchaseForecastDto, purchaseForecastService.getById(1L));
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseForecastRepository), ArgumentMatchers.eq("PurchaseForecast"));
        verify(purchaseForecastRepository).getById(1L);
    }

    @Test
    void create() {
        purchaseForecastService.create(purchaseForecastDto);
        verify(purchaseForecastRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseForecastDto)));
    }

    @Test
    void update() {
        purchaseForecastService.update(purchaseForecastDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(purchaseForecastDto.getId()), ArgumentMatchers.eq(purchaseForecastRepository), ArgumentMatchers.eq("PurchaseForecast"));
        verify(purchaseForecastRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchaseForecastDto)));
    }

    @Test
    void delete() {
        purchaseForecastService.delete(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchaseForecastRepository), ArgumentMatchers.eq("PurchaseForecast"));
        verify(purchaseForecastRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

package com.warehouse_accounting.unit_tests.services;


import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.repositories.GoodsToRealizeGetRepository;
import com.warehouse_accounting.services.impl.GoodsToRealizeGetImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GoodsToRealizeGetImplTest {

    @Mock
    private GoodsToRealizeGetRepository goodsToRealizeGetRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private GoodsToRealizeGetImpl goodsToRealizeGetService;

    private static GoodsToRealizeGetDto goodsToRealizeGetDto;

    private static List<GoodsToRealizeGetDto> goodsToRealizeGetDtoList = new ArrayList<>();

    @BeforeAll
    static void init() {

        goodsToRealizeGetDto = GoodsToRealizeGetDto.builder()
                .id(1L)
                .productDtoId(2L)
                .unitId(3L)
                .getGoods(4)
                .quantity(5)
                .amount(6)
                .arrive(7)
                .remains(8)
                .quantity_report(9)
                .amount_report(10)
                .quantity_Noreport(11)
                .quantity_Noreport(12)
                .build();

    }

    @Test
    void getAll() {
        when(goodsToRealizeGetRepository.getAll()).thenReturn(goodsToRealizeGetDtoList);
        List<GoodsToRealizeGetDto> goodsToRealizeGetDtoListNew = goodsToRealizeGetService.getAll();
        assertNotNull(goodsToRealizeGetDtoListNew, "goodsToRealizeGetDtoList is not null");
        assertEquals(goodsToRealizeGetDtoList, goodsToRealizeGetDtoListNew);
        verify(goodsToRealizeGetRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(goodsToRealizeGetRepository.getById(1L)).thenReturn(goodsToRealizeGetDto);

        GoodsToRealizeGetDto goodsToRealizeGetDtoTest = goodsToRealizeGetService.getById(1L);

        assertNotNull(goodsToRealizeGetDtoTest, "goodsToRealizeGetDtoList is not null");
        assertEquals(goodsToRealizeGetDtoTest, goodsToRealizeGetDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(goodsToRealizeGetRepository), ArgumentMatchers.eq("Goods To Realize GET"));
        verify(goodsToRealizeGetRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        goodsToRealizeGetService.create(goodsToRealizeGetDto);
        verify(goodsToRealizeGetRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(goodsToRealizeGetDto)));
    }

    @Test
    void testUpdate() {
        goodsToRealizeGetService.update(goodsToRealizeGetDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(goodsToRealizeGetDto.getId()), ArgumentMatchers.eq(goodsToRealizeGetRepository), ArgumentMatchers.eq("Goods To Realize GET"));
        verify(goodsToRealizeGetRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(goodsToRealizeGetDto)));
    }

    @Test
    void testDelete() {
        goodsToRealizeGetService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(goodsToRealizeGetRepository), ArgumentMatchers.eq("Goods To Realize GET"));
        verify(goodsToRealizeGetRepository, times(1)).deleteById(1L);
    }
}

package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.ContractorGroupDto;
import com.warehouse_accounting.repositories.ContractorGroupRepository;
import com.warehouse_accounting.services.impl.ContractorGroupServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContractorGroupServiceImplTest {

    @Mock
    private ContractorGroupRepository groupRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ContractorGroupServiceImpl groupService;


    private final ContractorGroupDto contractorGroupDto = ContractorGroupDto.builder()
            .id(1L)
            .name("Пять")
            .sortNumber("55")
            .build();

    private final List<ContractorGroupDto> contractorGroupDtoList = List.of(contractorGroupDto);

    @Test
    void giveMeThemAll() {
        when(groupRepository.getAll()).thenReturn(contractorGroupDtoList);
        List<ContractorGroupDto> groupDtoListIn = groupRepository.getAll();
        assertNotNull(groupDtoListIn, "groupDtoListIn == null");
        assertEquals(groupDtoListIn, contractorGroupDtoList);
        verify(groupRepository).getAll();
    }

    @Test
    void shouldRetrieveOneById() {
        when(groupRepository.getById(1L)).thenReturn(contractorGroupDto);

        assertEquals(groupService.getById(1L), contractorGroupDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(groupRepository), ArgumentMatchers.eq("ContractorGroup"));
        verify(groupRepository).getById(eq(1L));
    }

    @Test
    void shouldCreateNewOne() {
        groupService.create(contractorGroupDto);
        verify(groupRepository).save(eq(ConverterDto.convertToModel(contractorGroupDto)));
    }

    @Test
    void shouldUpdateOldOne() {
        groupService.update(contractorGroupDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(contractorGroupDto.getId()), ArgumentMatchers.eq(groupRepository), ArgumentMatchers.eq("ContractorGroup"));
        verify(groupRepository).save(eq(ConverterDto.convertToModel(contractorGroupDto)));
    }

    @Test
    void shouldDeleteById() {
        groupService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(groupRepository), ArgumentMatchers.eq("ContractorGroup"));
        verify(groupRepository).deleteById(eq(1L));
    }
}

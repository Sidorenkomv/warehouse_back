package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.repositories.PurchasesManagementRepository;
import com.warehouse_accounting.services.impl.PurchasesManagementServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PurchasesManagementServiceImplTest {

    @Mock
    private PurchasesManagementRepository purchasesManagementRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private PurchasesManagementServiceImpl purchasesManagementService;

    private static PurchasesManagementDto purchasesManagementDto;

    private static List<PurchasesManagementDto> purchasesManagementDtoList;

    @BeforeEach
    void init() {
        purchasesManagementDto = PurchasesManagementDto.builder()
                .id(1L)
                .productId(1L)
                .purchasesHistoryOfSalesId(1L)
                .purchasesForecastId(1L)
                .purchasesCurrentBalanceId(1L)
                .build();

        purchasesManagementDtoList = List.of(purchasesManagementDto);
    }

    @Test
    void getAll() {
        when(purchasesManagementRepository.getAll()).thenReturn(purchasesManagementDtoList);
        List<PurchasesManagementDto> purchasesManagementServiceAll = purchasesManagementService.getAll();
        assertNotNull(purchasesManagementServiceAll, "Null");
        assertEquals(purchasesManagementServiceAll, purchasesManagementDtoList);
        verify(purchasesManagementRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(purchasesManagementRepository.getById(1L)).thenReturn(purchasesManagementDto);

        assertEquals(purchasesManagementService.getById(1L), purchasesManagementDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchasesManagementRepository), ArgumentMatchers.eq("PurchasesManagement"));
        verify(purchasesManagementRepository, times(1))
                .getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        purchasesManagementService.create(purchasesManagementDto);
        verify(purchasesManagementRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchasesManagementDto)));
    }

    @Test
    void update() {
        purchasesManagementService.update(purchasesManagementDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(purchasesManagementDto.getId()), ArgumentMatchers.eq(purchasesManagementRepository), ArgumentMatchers.eq("PurchasesManagement"));
        verify(purchasesManagementRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(purchasesManagementDto)));
    }

    @Test
    void delete() {
        purchasesManagementService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(purchasesManagementRepository), ArgumentMatchers.eq("PurchasesManagement"));
        verify(purchasesManagementRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

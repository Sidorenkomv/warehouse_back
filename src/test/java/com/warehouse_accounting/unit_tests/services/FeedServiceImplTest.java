package com.warehouse_accounting.unit_tests.services;


import com.warehouse_accounting.models.dto.FeedDto;
import com.warehouse_accounting.repositories.FeedRepository;
import com.warehouse_accounting.services.impl.FeedServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FeedServiceImplTest {

    @Mock
    private FeedRepository feedRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private FeedServiceImpl feedService;

    private static FeedDto feedDto;

    private static List<FeedDto> feedDtoList = new ArrayList<>();

    @BeforeAll
    public static void init() {
        Date data = new Date();
        feedDto = FeedDto.builder()
                .id(1L)
                .feedBody("Тело Новости Тест")
                .feedHead("Заголовок Тест")
                .feedDate(data)
                .build();
        feedDtoList.add(feedDto);
    }

    @Test
    void testGetAll() {
        when(feedRepository.getAll()).thenReturn(feedDtoList);
        List<FeedDto> feedDtoListTest = feedService.getAll();
        assertNotNull(feedDtoListTest, "feedDtoListTest == null");
        assertEquals(feedDtoListTest, feedDtoList);
        verify(feedRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(feedRepository.getById(1L)).thenReturn(feedDto);

        assertEquals(feedService.getById(1L), feedDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(feedRepository), ArgumentMatchers.eq("Feed"));
        verify(feedRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        feedService.create(feedDto);
        verify(feedRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(feedDto)));
    }

    @Test
    void testDeleteById() {
        feedService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(feedRepository), ArgumentMatchers.eq("Feed"));
        verify(feedRepository, times(1))
                .deleteById(ArgumentMatchers.eq(1L));

    }

    @Test
    void testUpdate() {
        feedService.update(feedDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(feedDto.getId()), ArgumentMatchers.eq(feedRepository), ArgumentMatchers.eq("Feed"));
        verify(feedRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(feedDto)));
    }
}

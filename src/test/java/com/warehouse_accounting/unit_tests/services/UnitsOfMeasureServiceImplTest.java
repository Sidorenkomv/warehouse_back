package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import com.warehouse_accounting.repositories.UnitsOfMeasureRepository;
import com.warehouse_accounting.services.impl.UnitsOfMeasureServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UnitsOfMeasureServiceImplTest {

    @Mock
    private UnitsOfMeasureRepository measureRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private UnitsOfMeasureServiceImpl measureService;

    private UnitsOfMeasureDto measureDto = UnitsOfMeasureDto.builder()
            .id(1L)
            .type("Test")
            .name("Пядень")
            .fullName("Пядень с кувырком")
            .code("22")
            .build();

    private final List<UnitsOfMeasureDto> measureDtoList = List.of(measureDto);

    @Test
    void giveMeThemAll() {
        when(measureRepository.getAll()).thenReturn(measureDtoList);
        List<UnitsOfMeasureDto> measureDtoListIn = measureRepository.getAll();
        assertNotNull(measureDtoListIn, "measureDtoListIn == null");
        assertEquals(measureDtoListIn, measureDtoList);
        verify(measureRepository).getAll();
    }

    @Test
    void shouldRetrieveOneById() {
        when(measureRepository.getById(1L)).thenReturn(measureDto);

        assertEquals(measureService.getById(1L), measureDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(measureRepository), ArgumentMatchers.eq("UnitsOfMeasure"));
        verify(measureRepository).getById(eq(1L));
    }

    @Test
    void shouldCreateNewOne() {
        measureService.create(measureDto);
        verify(measureRepository).save(eq(ConverterDto.convertToModel(measureDto)));
    }

    @Test
    void shouldUpdateOldOne() {
        measureService.update(measureDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(measureDto.getId()), ArgumentMatchers.eq(measureRepository), ArgumentMatchers.eq("UnitsOfMeasure"));
        verify(measureRepository).save(eq(ConverterDto.convertToModel(measureDto)));
    }

    @Test
    void shouldDeleteById() {
        measureService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(measureRepository), ArgumentMatchers.eq("UnitsOfMeasure"));
        verify(measureRepository).deleteById(eq(1L));
    }
}
















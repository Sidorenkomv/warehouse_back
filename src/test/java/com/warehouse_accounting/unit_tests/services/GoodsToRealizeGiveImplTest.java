package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.repositories.GoodsToRealizeGiveRepository;
import com.warehouse_accounting.services.impl.GoodsToRealizeGiveImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GoodsToRealizeGiveImplTest {

    @Mock
    private GoodsToRealizeGiveRepository goodsToRealizeGiveRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private GoodsToRealizeGiveImpl goodsToRealizeGiveService;

    private static GoodsToRealizeGiveDto goodsToRealizeGiveDto;

    private static List<GoodsToRealizeGiveDto> goodsToRealizeGiveDtoList = new ArrayList<>();

    @BeforeAll
    static void init() {

        goodsToRealizeGiveDto = GoodsToRealizeGiveDto.builder()
                .id(1L)
                .productDtoId(2L)
                .unitId(3L)
                .giveGoods(4)
                .quantity(5)
                .amount(6)
                .arrive(7)
                .remains(8)
                .build();

    }

    @Test
    void getAll() {
        when(goodsToRealizeGiveRepository.getAll()).thenReturn(goodsToRealizeGiveDtoList);
        List<GoodsToRealizeGiveDto> goodsToRealizeGiveDtoListNew = goodsToRealizeGiveService.getAll();
        assertNotNull(goodsToRealizeGiveDtoListNew, "goodsToRealizeGiveDtoList is not null");
        assertEquals(goodsToRealizeGiveDtoList, goodsToRealizeGiveDtoListNew);
        verify(goodsToRealizeGiveRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(goodsToRealizeGiveRepository.getById(1L)).thenReturn(goodsToRealizeGiveDto);

        GoodsToRealizeGiveDto goodsToRealizeGiveDtoTest = goodsToRealizeGiveService.getById(1L);

        assertNotNull(goodsToRealizeGiveDtoTest, "goodsToRealizeGiveDtoList is not null");
        assertEquals(goodsToRealizeGiveDtoTest, goodsToRealizeGiveDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(goodsToRealizeGiveRepository), ArgumentMatchers.eq("Goods To Realize GIVE"));
        verify(goodsToRealizeGiveRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        goodsToRealizeGiveService.create(goodsToRealizeGiveDto);
        verify(goodsToRealizeGiveRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(goodsToRealizeGiveDto)));
    }

    @Test
    void testUpdate() {
        goodsToRealizeGiveService.update(goodsToRealizeGiveDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(goodsToRealizeGiveDto.getId()), ArgumentMatchers.eq(goodsToRealizeGiveRepository), ArgumentMatchers.eq("Goods To Realize GIVE"));
        verify(goodsToRealizeGiveRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(goodsToRealizeGiveDto)));
    }

    @Test
    void testDelete() {
        goodsToRealizeGiveService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(goodsToRealizeGiveRepository), ArgumentMatchers.eq("Goods To Realize GIVE"));
        verify(goodsToRealizeGiveRepository, times(1)).deleteById(1L);
    }
}

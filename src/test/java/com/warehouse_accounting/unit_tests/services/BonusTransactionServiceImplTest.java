package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.BonusTransaction;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.repositories.BonusTransactionRepository;
import com.warehouse_accounting.services.impl.BonusTransactionServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class BonusTransactionServiceImplTest {

    private static BonusTransactionDto bonusDto1;
    private static BonusTransactionDto bonusDto2;
    private static List<BonusTransactionDto> listBonusDto;

    @Mock
    private BonusTransactionRepository bonusTransactionRepository;

    @InjectMocks
    private BonusTransactionServiceImpl bonusTransactionService;


    @Mock
    private CheckEntityService checkEntityService;

    @BeforeAll
    static void initMethod() {
        bonusDto1 = BonusTransactionDto.builder()
                .id(3L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        bonusDto2 = BonusTransactionDto.builder()
                .id(2L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment2")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        listBonusDto = List.of(bonusDto1, bonusDto2);
    }

    @Test
    void getAll() {
        when(bonusTransactionRepository.findAll()
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()))
                .thenReturn(listBonusDto);

        List<BonusTransactionDto> resultBonusDtoList = bonusTransactionService.getAll();

        Assertions.assertNotNull(resultBonusDtoList, "get NULL");
        Assertions.assertEquals(resultBonusDtoList, listBonusDto);
        Mockito.verify(bonusTransactionRepository, Mockito.times(1))
                .findAll();

    }

    @Test
    void getById() {
        when(bonusTransactionRepository.getOne(1L)).thenReturn(ConverterDto.convertToModel(bonusDto1));

        Assertions.assertEquals(bonusTransactionService.getById(1L), bonusDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(bonusTransactionRepository), ArgumentMatchers.eq("BonusTransaction"));
        Mockito.verify(bonusTransactionRepository, Mockito.times(1))
                .getOne(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        bonusTransactionService.create(bonusDto1);
        Mockito.verify(bonusTransactionRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(bonusDto1)));
    }

    @Test
    void update() {
        bonusTransactionService.update(bonusDto2);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(bonusDto2.getId()), ArgumentMatchers.eq(bonusTransactionRepository), ArgumentMatchers.eq("BonusTransaction"));
        Mockito.verify(bonusTransactionRepository, Mockito.times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(bonusDto2)));
    }

    @Test
    void delete() {
        bonusTransactionService.deleteById(999L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(999L), ArgumentMatchers.eq(bonusTransactionRepository), ArgumentMatchers.eq("BonusTransaction"));
        Mockito.verify(bonusTransactionRepository, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(999L));
    }

}

package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.services.impl.WarehouseServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WarehouseServiceImplTest {

    @Mock
    private WarehouseRepository warehouseRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private WarehouseServiceImpl warehouseService;

    private static WarehouseDto warehouseDto1;
    private static WarehouseDto warehouseDto2;
    private static WarehouseDto warehouseDto3;
    private static List<WarehouseDto> warehouseDtoList;

    @BeforeAll
    static void initWarehouseDto() {
        warehouseDto1 = warehouseDto1.builder()
                .id(3L)
                .name("Склад3")
                .sortNumber("3")
                .address(AddressDto.builder().build())
                .comment("Маленький склад")
                .build();

        warehouseDto2 = warehouseDto2.builder()
                .id(4L)
                .name("Склад4")
                .sortNumber("4")
                .address(AddressDto.builder().build())
                .comment("Большой склад")
                .build();

        warehouseDto3 = warehouseDto3.builder()
                .id(5L)
                .name("Склад5")
                .sortNumber("5")
                .address(AddressDto.builder().build())
                .comment("Средний склад")
                .build();

        warehouseDtoList = List.of(WarehouseServiceImplTest.warehouseDto1, warehouseDto2, warehouseDto3);
    }

    @Test
    void testGetAll() {
        when(warehouseService.getAll()).thenReturn(warehouseDtoList);
        List<WarehouseDto> warehouseDtoListTest = warehouseService.getAll();
        assertNotNull(warehouseDtoListTest, "supplierOrdersDtoListTest is not null");
        assertEquals(warehouseDtoListTest, warehouseDtoList);
        verify(warehouseRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(warehouseRepository.getById(3L)).thenReturn(warehouseDto1);

        WarehouseDto warehouseDtoTest = warehouseService.getById(3L);

        assertNotNull(warehouseDtoTest, "supplierOrdersDtoTest is null");
        assertEquals(warehouseDtoTest, warehouseDto1);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(warehouseRepository), ArgumentMatchers.eq("Warehouse"));
        verify(warehouseRepository, times(1)).getById(3L);
    }

    @Test
    void testCreate() {
        warehouseService.create(warehouseDto1);
        verify(warehouseRepository, times(1))
                .save(ArgumentMatchers.argThat(object -> object.getId() == 3));
    }

    @Test
    void testUpdate() {
        warehouseService.update(warehouseDto1);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(warehouseDto1.getId()), ArgumentMatchers.eq(warehouseRepository), ArgumentMatchers.eq("Warehouse"));

        verify(warehouseRepository, times(1))
                .save(ArgumentMatchers.argThat(object -> object.getId() == 3));
    }

    @Test
    void testDelete() {
        warehouseService.deleteById(3L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(3L), ArgumentMatchers.eq(warehouseRepository), ArgumentMatchers.eq("Warehouse"));
        verify(warehouseRepository, times(1)).deleteById(3L);
    }
}
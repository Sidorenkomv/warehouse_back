package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.File;
import com.warehouse_accounting.models.Product;
import com.warehouse_accounting.models.Task;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.repositories.ReturnRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.impl.ReturnServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReturnServiceImplTest {

    @Mock
    ReturnRepository returnRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private FileRepository fileRepository;

    @Mock
    private TaskRepository taskRepository;
    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    ReturnServiceImpl returnService;

    private static ReturnDto returnDto;

    private static List<ReturnDto> returnDtoList = new ArrayList<>();

    private static File file;

    private static Task task;

    private static Product product;

    private static List<File> files = new ArrayList<>();

    private static List<Task> tasks = new ArrayList<>();

    private static List<Product> products = new ArrayList<>();

    @BeforeAll
    static void initReturns() {
        file = File.builder()
                .name("file1")
                .size(5)
                .build();

        task = Task.builder()
                .isDone(false)
                .build();

        product = Product.builder()
                .name("product1")
                .country("country1")
                .build();

        returnDto = ReturnDto.builder()
                .id(1L)
                .dataTime(LocalDateTime.now())
                .warehouseDto(WarehouseDto.builder().build())
                .companyDto(CompanyDto.builder().build())
                .contractorDto(ContractorDto.builder().build())
                .contractDto(ContractDto.builder().build())
                .projectDto(ProjectDto.builder().build())
                .fileDtos(new ArrayList<>())
                .taskDtos(new ArrayList<>())
                .productDtos(new ArrayList<>())
                .sum(BigDecimal.valueOf(1000))
                .isSent(false)
                .isPrinted(true)
                .comment("comment1")
                .build();
        returnDtoList.add(returnDto);

        files.add(file);
        tasks.add(task);
        products.add(product);
    }

    @Test
    void getAll() {
        when(returnRepository.getAll()).thenReturn(returnDtoList);
        when(fileRepository.getFileReturnById(anyLong())).thenReturn(files);
        when(taskRepository.getListTaskById(anyLong())).thenReturn(tasks);
        when(productRepository.getProductReturnById(anyLong())).thenReturn(products);

        List<ReturnDto> returnDtoListTest = returnService.getAll();
        assertNotNull(returnDtoListTest, "returnDtoList == null");
        assertEquals(returnDtoListTest, returnDtoList);
        verify(returnRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(returnRepository.getById(1L)).thenReturn(returnDto);
        when(fileRepository.getFileReturnById(anyLong())).thenReturn(files);
        when(taskRepository.getListTaskById(anyLong())).thenReturn(tasks);
        when(productRepository.getProductReturnById(anyLong())).thenReturn(products);

        assertEquals(returnService.getById(1L), returnDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(returnRepository), ArgumentMatchers.eq("Return"));
        verify(returnRepository, times(1)).getById(ArgumentMatchers.eq(1l));
    }

    @Test
    void create() {
        returnService.create(returnDto);
        verify(returnRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(returnDto)));
    }

    @Test
    void update() {
        ReturnDto returnDtoUpdate = ReturnDto.builder()
                .id(3L)
                .dataTime(LocalDateTime.now())
                .warehouseDto(WarehouseDto.builder().build())
                .companyDto(CompanyDto.builder().build())
                .contractorDto(ContractorDto.builder().build())
                .contractDto(ContractDto.builder().build())
                .projectDto(ProjectDto.builder().build())
                .fileDtos(new ArrayList<>())
                .taskDtos(new ArrayList<>())
                .productDtos(new ArrayList<>())
                .sum(BigDecimal.valueOf(600))
                .isSent(true)
                .isPrinted(false)
                .comment("comment3")
                .build();

        returnService.update(returnDtoUpdate);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(returnDtoUpdate.getId()), ArgumentMatchers.eq(returnRepository), ArgumentMatchers.eq("Return"));
        verify(returnRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(returnDtoUpdate)));
    }

    @Test
    void deleteById() {
        returnService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(returnRepository), ArgumentMatchers.eq("Return"));
        verify(returnRepository, times(1)).deleteById(ArgumentMatchers.eq(1l));
    }
}

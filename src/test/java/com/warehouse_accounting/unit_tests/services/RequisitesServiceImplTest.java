package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.RequisitesDto;
import com.warehouse_accounting.repositories.RequisitesRepository;
import com.warehouse_accounting.services.impl.RequisitesServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RequisitesServiceImplTest {

    @Mock
    private RequisitesRepository requisitesRepository;

    @InjectMocks
    private RequisitesServiceImpl requisitesService;

    private static RequisitesDto requisitesDto;

    private static List<RequisitesDto> requisitesDtoList;

    @BeforeAll
    public static void setUp() {
        requisitesDto = RequisitesDto.builder()
                .id(1L)
                .organization("organization1")
                .legalAddress(AddressDto.builder().build())
                .INN(111)
                .KPP(111)
                .BIK(111)
                .checkingAccount(111)
                .build();

        requisitesDtoList = List.of(requisitesDto);
    }

    @Test
    void getAll() {
        when(requisitesRepository.getAll()).thenReturn(requisitesDtoList);
        List<RequisitesDto> dtoListTest = requisitesService.getAll();
        assertNotNull(dtoListTest, "adjustmentDtoList is not null");
        assertEquals(requisitesDtoList, dtoListTest);
        verify(requisitesRepository, times(1)).getAll();
    }

    @Test
    void testGetById() {
        when(requisitesRepository.getById(1L)).thenReturn(requisitesDto);
        RequisitesDto adjustmentDtoTest = requisitesService.getById(1L);
        Assertions.assertNotNull(adjustmentDtoTest, "requisitesDto is not null");
        assertEquals(adjustmentDtoTest, requisitesDto);
        verify(requisitesRepository, times(1)).getById(1L);
    }

    @Test
    void testCreate() {
        requisitesService.create(requisitesDto);
        verify(requisitesRepository)
                .save(ArgumentMatchers.argThat(object -> object.getId() == 1));
    }

    @Test
    void testUpdate() {
        requisitesService.update(requisitesDto);
        verify(requisitesRepository)
                .save(ArgumentMatchers.argThat(object -> object.getId() == 1));
    }

    @Test
    void testDelete() {
        requisitesService.deleteById(2L);
        verify(requisitesRepository, times(1)).deleteById(2L);
    }
}

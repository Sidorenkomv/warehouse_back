package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.Currency;
import com.warehouse_accounting.models.dto.CurrencyDto;
import com.warehouse_accounting.repositories.CurrencyRepository;
import com.warehouse_accounting.services.impl.CurrencyServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class CurrencyServiceImplTest {

    @Mock
    private CurrencyRepository currencyRepository;

    @InjectMocks
    private CurrencyServiceImpl сurrencyService;

    @Mock
    private CheckEntityService checkEntityService;

    private static CurrencyDto currencyDto;

    private static List<CurrencyDto> currencyDtoList = new ArrayList<>();
    private List< Currency > currencyList = new ArrayList<>();

    @BeforeAll
    static void setUp() {
        currencyDto = CurrencyDto.builder()
                .id(1L)
                .numcode("123")
                .charcode("456")
                .nominal(7)
                .name("Frank")
                .value(30)
                .build();
        currencyDtoList.add(currencyDto);
    }

    @Test
    void getAll() {
        when(currencyRepository.getAll()).thenReturn(currencyDtoList);
        List<CurrencyDto> currencyDtoListTest = сurrencyService.getAll();
        assertNotNull(currencyDtoListTest);
        assertEquals(currencyDtoListTest, currencyDtoList);
        verify(currencyRepository).getAll();
    }

    @Test
    void getById() {
        when(currencyRepository.getById(1L)).thenReturn(currencyDto);
        CurrencyDto currencyDtoTest = сurrencyService.getById(1L);
        assertNotNull(currencyDtoTest);
        assertEquals(currencyDtoTest, currencyDto);
        verify(checkEntityService).checkExist(eq(1L), eq(currencyRepository), ArgumentMatchers.eq("Currency"));
        verify(currencyRepository).getById(1L);
    }

    @Test
    void create() {
        сurrencyService.create(currencyDto);
        verify(currencyRepository, times(1)).save(ConverterDto.convertToModel(currencyDto));
    }

    @Test
    void update() {
        сurrencyService.update(currencyDto);
        verify(checkEntityService, times(1)).checkExist(eq(currencyDto.getId()), eq(currencyRepository), ArgumentMatchers.eq("Currency"));
        verify(currencyRepository, times(1)).save(ConverterDto.convertToModel(currencyDto));
    }

    @Test
    void deleteById() {
        сurrencyService.deleteById(1L);
        verify(checkEntityService, times(1)).checkExist(eq(1L), eq(currencyRepository), ArgumentMatchers.eq("Currency"));
        verify(currencyRepository, times(1)).deleteById(1L);

    }

    @Test
    void saveAll() {
        сurrencyService.saveAll(currencyList);
        verify(currencyRepository, times(1)).saveAll(currencyList);
    }
}
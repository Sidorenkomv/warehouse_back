package com.warehouse_accounting.unit_tests.services;

import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.repositories.*;
import com.warehouse_accounting.services.impl.ContractServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContractServiceImplTest {

    @Mock
    private ContractRepository contractRepository;
    @Mock
    private CompanyRepository companyRepository;
    @Mock
    private BankAccountRepository bankAccountRepository;
    @Mock
    private ContractorRepository contractorRepository;
    @Mock
    private LegalDetailRepository legalDetailRepository;
    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private ContractServiceImpl contractService;

    private final ContractDto contractDto = ContractDto.builder()
            .id(1L)
            .number("23")
            .contractDate(LocalDate.now())
            .amount(BigDecimal.valueOf(0))
            .archive(false)
            .comment("Empty")
            .build();

    private final List<ContractDto> contractDtoList = List.of(contractDto);

    @Test
    void getAll() {
        when(contractRepository.getAll()).thenReturn(contractDtoList);
        List<ContractDto> contractDtoListTest = contractService.getAll();
        assertNotNull(contractDtoListTest, "contractDtoListTest == null");
        assertEquals(contractDtoListTest, contractDtoList);
        verify(contractRepository).getAll();
    }

    @Test
    void getById() {
        when(contractRepository.getById(1L)).thenReturn(contractDto);

        assertEquals(contractService.getById(1L), contractDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(contractRepository), ArgumentMatchers.eq("Contract"));
        verify(contractRepository).getById(eq(1L));
    }

    @Test
    void update() {
        ContractDto contractDto1 = ContractDto.builder()
                .id(3L)
                .number("43")
                .amount(BigDecimal.valueOf(0))
                .archive(true)
                .comment("comment1")
                .build();

        contractService.update(contractDto1);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(contractDto1.getId()), ArgumentMatchers.eq(contractRepository), ArgumentMatchers.eq("Contract"));
        verify(contractRepository).save(eq(ConverterDto.convertToModel(contractDto1)));
    }

    @Test
    void create() {
        ContractDto contractDto1 = ContractDto.builder()
                .id(2L)
                .number("342")
                .amount(BigDecimal.valueOf(45))
                .archive(false)
                .comment("comment11")
                .build();

        contractService.create(contractDto1);
        verify(contractRepository).save(eq(ConverterDto.convertToModel(contractDto1)));
    }

    @Test
    void deleteById() {
        contractService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(contractRepository), ArgumentMatchers.eq("Contract"));
        verify(contractRepository).deleteById(eq(1L));
    }
}

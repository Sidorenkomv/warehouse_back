package com.warehouse_accounting.unit_tests.services;


import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.repositories.DocumentRepository;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.impl.DocumentServiceImpl;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DocumentServiceTest {

    @Mock
    private DocumentRepository documentRepository;

    @Mock
    private TaskRepository tasksRepository;

    @Mock
    private FileRepository fileRepository;

    @Mock
    private CheckEntityService checkEntityService;
    @InjectMocks
    private DocumentServiceImpl documentService;

    private static DocumentDto documentDto;

    private static List<DocumentDto> documentDtoList = new ArrayList<>();

    @BeforeAll
    static void init() {

        documentDto = DocumentDto.builder()
                .id(1L)
                .type("type1")
                .docNumber("number1")
                .date(LocalDateTime.now())
                .sum(BigDecimal.valueOf(10000))
                .warehouseFromId(1L)
                .warehouseFromName("warehouse1")
                .warehouseToId(2L)
                .warehouseToName("warenhouse2")
                .companyId(1L)
                .companyName("company1")
                .contrAgentId(1L)
                .contrAgentName("contr1")
                .projectId(1L)
                .projectName("project1")
                .salesChannelId(1L)
                .salesChannelName("sales1")
                .contractId(1L)
                .contractNumber("num1")
                .isSharedAccess(true)
                .departmentId(1l)
                .departmentName("dep1")
                .employeeId(1L)
                .employeeFirstname("name1")
                .sent(true)
                .print(false)
                .comments("comment1")
                .updatedFromEmployeeId(2L)
                .updatedFromEmployeeFirstname("name2")
                .build();

        documentDtoList.add(documentDto);
    }

    @Test
    void getAll() {
        when(tasksRepository.getListTaskById(anyLong())).thenReturn(new ArrayList<>());
        when(fileRepository.getListFileById(anyLong())).thenReturn(new ArrayList<>());
        when(documentRepository.getAll()).thenReturn(documentDtoList);

        List<DocumentDto> documentDtos = documentService.getAll();

        assertNotNull(documentDtos);
        assertEquals(documentDtos, documentDtoList);
        verify(documentRepository, times(1)).getAll();
    }

    @Test
    void getById() {
        when(tasksRepository.getListTaskById(anyLong())).thenReturn(new ArrayList<>());
        when(fileRepository.getListFileById(anyLong())).thenReturn(new ArrayList<>());
        when(documentRepository.getById(1l)).thenReturn(documentDto);

        assertEquals(documentService.getById(1l), documentDto);
        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(documentRepository), ArgumentMatchers.eq("Document"));
        verify(documentRepository, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        documentService.create(documentDto);
        verify(documentRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(documentDto)));
    }

    @Test
    void update() {
        documentService.update(documentDto);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(documentDto.getId()), ArgumentMatchers.eq(documentRepository), ArgumentMatchers.eq("Document"));
        verify(documentRepository, times(1))
                .save(ArgumentMatchers.eq(ConverterDto.convertToModel(documentDto)));
    }

    @Test
    void deleteById() {
        documentService.deleteById(1L);

        verify(checkEntityService, times(1))
                .checkExist(ArgumentMatchers.eq(1L), ArgumentMatchers.eq(documentRepository), ArgumentMatchers.eq("Document"));
        verify(documentRepository, times(1)).deleteById(ArgumentMatchers.eq(1l));
    }
}
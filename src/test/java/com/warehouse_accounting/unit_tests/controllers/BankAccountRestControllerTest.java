package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.BankAccountRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.services.interfaces.BankAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BankAccountRestControllerTest {
    private static BankAccountDto bankAccountDto;
    private static List<BankAccountDto> bankAccountDtoList;

    @InjectMocks
    private BankAccountRestController bankAccountRestController;
    @Mock
    private BankAccountService bankAccountService;

    @BeforeEach
    void init() {
        bankAccountDto = BankAccountDto.builder()
                .id(1L)
                .rcbic("rcbic")
                .bank("bank")
//                .address(AddressDto.builder().fullAddress("address").build())
                .correspondentAccount("correspondentAccount")
                .account("BankAccount")
                .mainAccount(true)
                .sortNumber("sortNumber")
                .build();
        bankAccountDtoList = List.of(bankAccountDto);
    }

    @Test
    void getAll() {
        when(bankAccountService.getAll()).thenReturn(bankAccountDtoList);
        ResponseEntity<List<BankAccountDto>> responseEntity = bankAccountRestController.getAll();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), bankAccountDtoList);
        Mockito.verify(bankAccountService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(bankAccountService.getById(1L)).thenReturn(bankAccountDto);
        ResponseEntity<BankAccountDto> bankAccountDtoResponseEntity = bankAccountRestController.getById(1L);
        assertEquals(HttpStatus.OK, bankAccountDtoResponseEntity.getStatusCode());
        assertEquals(bankAccountDtoResponseEntity.getBody(), bankAccountDto);
        Mockito.verify(bankAccountService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, bankAccountRestController.create(bankAccountDto).getStatusCode());
        verify(bankAccountService, times(1))
                .create(ArgumentMatchers.eq(bankAccountDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, bankAccountRestController.update(bankAccountDto).getStatusCode());
        Mockito.verify(bankAccountService, Mockito.times(1))
                .update(ArgumentMatchers.eq(bankAccountDto));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, bankAccountRestController.delete(1L).getStatusCode());
        Mockito.verify(bankAccountService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }

}

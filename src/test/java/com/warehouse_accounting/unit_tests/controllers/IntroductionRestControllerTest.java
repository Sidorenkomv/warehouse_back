package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.IntroductionRestController;
import com.warehouse_accounting.models.dto.IntroductionDto;
import com.warehouse_accounting.services.interfaces.IntroductionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class IntroductionRestControllerTest {

    private IntroductionDto introductionDto;

    private static List<IntroductionDto> introductionDtoList = new ArrayList<>();

    @InjectMocks
    private IntroductionRestController introductionRestController;

    @Mock
    private IntroductionService introductionService;


    @BeforeEach
    void initMethod() {
        introductionDto = IntroductionDto.builder()
                .id(2L)
                .pointId(1L)
                .fromWhomId(1L)
                .build();

        introductionDtoList = List.of(introductionDto);
    }

    @Test
    @DisplayName("getAll method test")
    public void getAll() {
        when(introductionService.getAll()).thenReturn(introductionDtoList);
        ResponseEntity<List<IntroductionDto>> responseEntity = introductionRestController.getAll();
        Assert.notNull(HttpStatus.OK, "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), introductionDtoList);
        Mockito.verify(introductionService, Mockito.times(1))
                .getAll();
    }


    @Test
    @DisplayName("getById method test")
    void getById() {
        when(introductionService.getById(2L)).thenReturn(introductionDto);
        ResponseEntity<IntroductionDto> imageDtoResponseEntity = introductionRestController.getById(2L);
        assertEquals(HttpStatus.OK, imageDtoResponseEntity.getStatusCode());
        assertEquals(imageDtoResponseEntity.getBody(), introductionDto);
        Mockito.verify(introductionService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    @DisplayName("create method test")
    void create() {
        assertEquals(HttpStatus.OK, introductionRestController.create(introductionDto).getStatusCode());
        Mockito.verify(introductionService, Mockito.times(1))
                .create(ArgumentMatchers.eq(introductionDto));
    }

    @Test
    @DisplayName("update method test")
    void update() {
        assertEquals(HttpStatus.OK, introductionRestController.update(introductionDto).getStatusCode());
        Mockito.verify(introductionService, Mockito.times(1))
                .update(ArgumentMatchers.eq(introductionDto));
    }

    @Test
    @DisplayName("delete method test")
    void delete() {
        assertEquals(HttpStatus.OK, introductionRestController.deleteById(2L).getStatusCode());
        Mockito.verify(introductionService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(2L));
    }

}

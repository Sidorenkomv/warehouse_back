package com.warehouse_accounting.unit_tests.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.UnitsOfMeasureRestController;
import com.warehouse_accounting.controllers.rest.exceptions_and_filters.UnitsOfMeasureExceptionHandler;
import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import com.warehouse_accounting.services.interfaces.UnitsOfMeasureService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class UnitsOfMeasureRestControllerIntTest {

    private MockMvc mockMvc;

    @Mock
    private UnitsOfMeasureService mockUnitsOfMeasureService;

    @InjectMocks
    private UnitsOfMeasureRestController controller;

    private JacksonTester<UnitsOfMeasureDto> jsonUnitsOfMeasure;

    private static UnitsOfMeasureDto unitsOfMeasureDto1;
    private static UnitsOfMeasureDto unitsOfMeasureDto2;
    private static List<UnitsOfMeasureDto> listUnitsOfMeasureDto;

    @BeforeAll
    static void initMethod() {

        unitsOfMeasureDto1 = UnitsOfMeasureDto.builder()
                .id(5L)
                .type("Системный")
                .name("горшок1")
                .fullName("Малый горшок")
                .code("77")
                .build();

        unitsOfMeasureDto2 = UnitsOfMeasureDto.builder()
                .id(6L)
                .type("Системный")
                .name("горшок2")
                .fullName("Большой горшок")
                .code("78")
                .build();

        listUnitsOfMeasureDto = List.of(unitsOfMeasureDto1, unitsOfMeasureDto2);
    }

    @BeforeEach
    public void setup() {

        JacksonTester.initFields(this, new ObjectMapper());
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(new UnitsOfMeasureExceptionHandler())
                .build();
    }

    @Test
    void shouldReturnAllUnits() throws Exception {

        // given
        given(mockUnitsOfMeasureService.getAll())
                .willReturn(listUnitsOfMeasureDto);

        // when
        ResultActions resultActions = mockMvc.perform(get("/api/unitsOfMeasure")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].id", is(5)))
                .andExpect(jsonPath("$.[0].type", is("Системный")))
                .andExpect(jsonPath("$.[0].name", is("горшок1")))
                .andExpect(jsonPath("$.[0].fullName", is("Малый горшок")))
                .andExpect(jsonPath("$.[0].code", is("77")))
                .andExpect(jsonPath("$.[1].id", is(6)))
                .andExpect(jsonPath("$.[1].type", is("Системный")))
                .andExpect(jsonPath("$.[1].name", is("горшок2")))
                .andExpect(jsonPath("$.[1].fullName", is("Большой горшок")))
                .andExpect(jsonPath("$.[1].code", is("78")));
    }

    @Test
    void shouldReturnUnitById() throws Exception {

        // given
        given(mockUnitsOfMeasureService.getById(5L))
                .willReturn(unitsOfMeasureDto1);

        // when
        ResultActions resultActions = mockMvc
                .perform(get("/api/unitsOfMeasure/5")
                        .contentType(MediaType.APPLICATION_JSON));

        // then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(5)))
                .andExpect(jsonPath("$.type", is("Системный")))
                .andExpect(jsonPath("$.name", is("горшок1")))
                .andExpect(jsonPath("$.fullName", is("Малый горшок")))
                .andExpect(jsonPath("$.code", is("77")));
    }

    @Test
    void shouldCreateNewUnitOfMeasure() throws Exception {

        // when
        MockHttpServletRequestBuilder content = MockMvcRequestBuilders
                .post("/api/unitsOfMeasure/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUnitsOfMeasure
                        .write(unitsOfMeasureDto1)
                        .getJson()
                );

        MockHttpServletResponse response = mockMvc
                .perform(content)
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(mockUnitsOfMeasureService).create(unitsOfMeasureDto1);
    }

    @Test
    void shouldUpdateUnitOfMeasure() throws Exception {

        // when
        MockHttpServletRequestBuilder content = MockMvcRequestBuilders
                .put("/api/unitsOfMeasure/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUnitsOfMeasure.write(unitsOfMeasureDto2).getJson());

        MockHttpServletResponse response = mockMvc
                .perform(content)
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(mockUnitsOfMeasureService).update(unitsOfMeasureDto2);
    }

    @Test
    void shouldDeleteById() throws Exception {

        // when
        MockHttpServletResponse response = mockMvc
                .perform(delete("/api/unitsOfMeasure/5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(mockUnitsOfMeasureService).deleteById(5L);

    }
}

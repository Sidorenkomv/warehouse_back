package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PriceListRestController;
import com.warehouse_accounting.models.dto.PriceListDto;
import com.warehouse_accounting.services.interfaces.PriceListService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class PriceListRestControllerTest {

    private static PriceListDto priceListDto;

    private static List<PriceListDto> priceListDtoList;

    @InjectMocks
    private PriceListRestController priceListRestController;

    @Mock
    private PriceListService priceListService;

    @BeforeEach
    public void setUp() {
        priceListDto = PriceListDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.of(2021, 11, 11, 2, 0, 0))
                .moved(true)
                .printed(true)
                .comment("test")
                .build();

        priceListDtoList = List.of(priceListDto);
    }

    @Test
    public void getAll() {
        when(priceListService.getAll()).thenReturn(priceListDtoList);

        ResponseEntity<List<PriceListDto>> responseEntity = priceListRestController.getAll();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), priceListDtoList);
        verify(priceListService, times(1)).getAll();
    }

    @Test
    public void getById() {
        when(priceListService.getById(1L)).thenReturn(priceListDto);

        ResponseEntity<PriceListDto> responseEntity = priceListRestController.getById(1L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), priceListDto);

        Mockito.verify(priceListService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    public void create() {
        assertEquals(HttpStatus.OK, priceListRestController.create(priceListDto).getStatusCode());
        Mockito.verify(priceListService, Mockito.times(1))
                .create(ArgumentMatchers.eq(priceListDto));
    }

    @Test
    public void update() {
        priceListDto = PriceListDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.of(2016, 12, 3, 4, 0, 0))
                .moved(false)
                .printed(false)
                .comment("test-update")
                .build();

        assertEquals(HttpStatus.OK, priceListRestController.update(priceListDto).getStatusCode());
        Mockito.verify(priceListService, Mockito.times(1))
                .update(ArgumentMatchers.eq(priceListDto));
    }

    @Test
    public void deleteById() {
        assertEquals(HttpStatus.OK, priceListRestController.deleteById(1L).getStatusCode());
        Mockito.verify(priceListService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

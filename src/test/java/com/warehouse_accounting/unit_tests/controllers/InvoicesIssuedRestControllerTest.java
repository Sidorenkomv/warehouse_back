package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.InvoicesIssuedRestController;
import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.services.interfaces.InvoicesIssuedService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InvoicesIssuedRestControllerTest {

    private static InvoicesIssuedDto invoicesIssuedDto1;
    private static InvoicesIssuedDto invoicesIssuedDto2;
    private static List<InvoicesIssuedDto> invoicesIssuedDtoList;

    @InjectMocks
    private InvoicesIssuedRestController invoicesIssuedRestController;

    @Mock
    private InvoicesIssuedService invoicesIssuedService;

    @BeforeEach
    void initMethod() {
        invoicesIssuedDto1 = InvoicesIssuedDto.builder()
                .id(2L)
                .data(LocalDateTime.now())
                .sum(BigDecimal.valueOf(100))
                .sent(false)
                .printed(false)
                .companyId(1L)
                .contractorId(1L)
                .comment("Первый")
                .build();
        invoicesIssuedDto2 = InvoicesIssuedDto.builder()
                .id(3L)
                .data(LocalDateTime.now())
                .sum(BigDecimal.valueOf(100))
                .sent(false)
                .printed(false)
                .companyId(1L)
                .contractorId(1L)
                .comment("Второй")
                .build();

        invoicesIssuedDtoList = List.of(invoicesIssuedDto1, invoicesIssuedDto2);
    }

    @Test
    void getAll() {
        when(invoicesIssuedService.getAll()).thenReturn(invoicesIssuedDtoList);
        ResponseEntity<List<InvoicesIssuedDto>> responseEntity = invoicesIssuedRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), invoicesIssuedDtoList);
        Mockito.verify(invoicesIssuedService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(invoicesIssuedService.getById(2L)).thenReturn(invoicesIssuedDto1);
        ResponseEntity<InvoicesIssuedDto> invoicesIssuedDtoResponseEntity = invoicesIssuedRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, invoicesIssuedDtoResponseEntity.getStatusCode());
        assertEquals(invoicesIssuedDtoResponseEntity.getBody(), invoicesIssuedDto1);
        Mockito.verify(invoicesIssuedService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, invoicesIssuedRestController.create(invoicesIssuedDto1).getStatusCode());
        Mockito.verify(invoicesIssuedService, Mockito.times(1))
                .create(ArgumentMatchers.eq(invoicesIssuedDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, invoicesIssuedRestController.update(invoicesIssuedDto2).getStatusCode());
        Mockito.verify(invoicesIssuedService, Mockito.times(1))
                .update(ArgumentMatchers.eq(invoicesIssuedDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, invoicesIssuedRestController.delete(3L).getStatusCode());
        Mockito.verify(invoicesIssuedService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

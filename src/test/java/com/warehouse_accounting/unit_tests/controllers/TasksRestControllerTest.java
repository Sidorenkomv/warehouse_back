package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.TasksRestController;
import com.warehouse_accounting.models.dto.TasksDto;
import com.warehouse_accounting.services.interfaces.TasksService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class TasksRestControllerTest {
    private static TasksDto task1;
    private static TasksDto task2;

    private static List<TasksDto> tasksDtoList;

    @InjectMocks
    private TasksRestController tasksRestController;

    @Mock
    private TasksService tasksService;

    @BeforeEach
    void initMethod() {
        task1 = TasksDto.builder()
                .id(1L)
                .description("description task1")
                .deadline("24.04.2020")
                .isDone(true)
                .build();

        task2 = TasksDto.builder()
                .id(2L)
                .deadline("25.01.2022")
                .description("description task2")
                .build();

        tasksDtoList = List.of(task1, task2);
    }

    @DisplayName("getAll() method test")
    @Test
    void getAll() {
        when(tasksService.getAll()).thenReturn(tasksDtoList);
        ResponseEntity<List<TasksDto>> listResponseEntity = tasksRestController.getAll();
        Assert.notNull(listResponseEntity.getBody(), "null");
        assertEquals(HttpStatus.OK, listResponseEntity.getStatusCode());
        assertEquals(listResponseEntity.getBody(), tasksDtoList);
        Mockito.verify(tasksService, Mockito.times(1)).getAll();
    }

    @DisplayName("getById() method test")
    @Test
    void getById() {
        when(tasksService.getById(1L)).thenReturn(task1);
        ResponseEntity<TasksDto> tasksDtoResponseEntity = tasksRestController.getById(1L);
        Assert.notNull(tasksDtoResponseEntity.getBody(), "null");
        assertEquals(HttpStatus.OK, tasksDtoResponseEntity.getStatusCode());
        assertEquals(tasksDtoResponseEntity.getBody(), task1);
        Mockito.verify(tasksService, Mockito.times(1)).getById(1L);
    }

    @DisplayName("create() method test")
    @Test
    void create() {
        assertEquals(HttpStatus.OK, tasksRestController.create(task1).getStatusCode());
        Mockito.verify(tasksService, Mockito.times(1)).create(ArgumentMatchers.eq(task1));
    }
    @DisplayName("update() method test")
    @Test
    void update () {
        assertEquals(HttpStatus.OK, tasksRestController.update(task2).getStatusCode());
        Mockito.verify(tasksService, Mockito.times(1)).update(ArgumentMatchers.eq(task2));
    }

    @DisplayName("delete() method test")
    @Test
    void delete () {
        assertEquals(HttpStatus.OK, tasksRestController.deleteById(2L).getStatusCode());
        Mockito.verify(tasksService, Mockito.times(1)).deleteById(ArgumentMatchers.eq(2L));

    }
}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.SupplierInvoiceRestController;
import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import com.warehouse_accounting.services.interfaces.SupplierInvoiceService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class SupplierInvoiceRestControllerTest {
    private static SupplierInvoiceDto supplierInvoiceDto;
    private static List<SupplierInvoiceDto> supplierInvoiceDtoList;
    @Mock
    private SupplierInvoiceService supplierInvoiceService;

    @InjectMocks
    private SupplierInvoiceRestController supplierInvoiceRestController;

    @BeforeAll
    static void init() {
        supplierInvoiceDto = SupplierInvoiceDto.builder()
                .id(1L)
                .invoiceNumber("number")
                .dateInvoiceNumber("10-02-2022")
                .checkboxProd(false)
                .organization("companyName")
                .warehouse("warehouseName")
                .contrAgent("contrAgentName")
                .contract("contractName")
                .datePay("15-02-2022")
                .project("projectName")
                .incomingNumber("number")
                .dateIncomingNumber("number")
                .checkboxName(false)
                .checkboxNDS(false)
                .checkboxOnNDS(false)
                .addPosition("position")
                .addComment("comment")
                .build();

        supplierInvoiceDtoList = List.of(supplierInvoiceDto);
    }

    @Test
    void getAll() {
        when(supplierInvoiceService.getAll()).thenReturn(supplierInvoiceDtoList);
        ResponseEntity<List<SupplierInvoiceDto>> responseEntity = supplierInvoiceRestController.getAll();
        assertNotNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), supplierInvoiceDtoList);
        verify(supplierInvoiceService, times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(supplierInvoiceService.getById(1L)).thenReturn(supplierInvoiceDto);
        ResponseEntity<SupplierInvoiceDto> responseEntity = supplierInvoiceRestController.getById(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), supplierInvoiceDto);
        verify(supplierInvoiceService, times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, supplierInvoiceRestController.create(supplierInvoiceDto).getStatusCode());
        verify(supplierInvoiceService, times(1))
                .create(ArgumentMatchers.eq(supplierInvoiceDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, supplierInvoiceRestController.update(supplierInvoiceDto).getStatusCode());
        verify(supplierInvoiceService, times(1))
                .update(ArgumentMatchers.eq(supplierInvoiceDto));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, supplierInvoiceRestController.deleteById(1L).getStatusCode());
        verify(supplierInvoiceService, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}


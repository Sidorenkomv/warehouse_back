package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.InventoryRestController;
import com.warehouse_accounting.models.dto.InventoryDto;
import com.warehouse_accounting.services.interfaces.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InventoryRestControllerUnitTest {

    private static InventoryDto inventoryDto1;
    private static InventoryDto inventoryDto2;
    private static List<InventoryDto> inventoryDtoList;

    @InjectMocks
    private InventoryRestController inventoryRestController;

    @Mock
    private InventoryService inventoryService;

    @BeforeEach
    void initMethod() {
        inventoryDto1 = InventoryDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Комментарий2")
                .build();

        inventoryDto2 = InventoryDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Комментарий3")
                .build();

        inventoryDtoList = List.of(inventoryDto1, inventoryDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(inventoryService.getAll()).thenReturn(inventoryDtoList);
        ResponseEntity<List<InventoryDto>> responseEntity = inventoryRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), inventoryDtoList);
        Mockito.verify(inventoryService, Mockito.times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(inventoryService.getById(2L)).thenReturn(inventoryDto1);
        ResponseEntity<InventoryDto> inventoryDtoResponseEntity = inventoryRestController.getById(2L);
        assertEquals(HttpStatus.OK, inventoryDtoResponseEntity.getStatusCode());
        assertEquals(inventoryDtoResponseEntity.getBody(), inventoryDto1);
        Mockito.verify(inventoryService, Mockito.times(1)).getById(2L);
    }

    @DisplayName("create method test")
    @Test
    void create() {
        assertEquals(HttpStatus.OK, inventoryRestController.create(inventoryDto1).getStatusCode());
        Mockito.verify(inventoryService, Mockito.times(1))
                .create(ArgumentMatchers.eq(inventoryDto1));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        assertEquals(HttpStatus.OK, inventoryRestController.update(inventoryDto2).getStatusCode());
        Mockito.verify(inventoryService, Mockito.times(1))
                .update(ArgumentMatchers.eq(inventoryDto2));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        assertEquals(HttpStatus.OK, inventoryRestController.deleteById(3L).getStatusCode());
        Mockito.verify(inventoryService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}

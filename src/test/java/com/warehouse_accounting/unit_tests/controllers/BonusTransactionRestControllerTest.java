package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.BonusTransactionRestController;
import com.warehouse_accounting.models.BonusTransaction;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.services.impl.BonusTransactionServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.util.Assert.notNull;

@ExtendWith(MockitoExtension.class)
public class BonusTransactionRestControllerTest {

    private static BonusTransactionDto bonusDto1;
    private static BonusTransactionDto bonusDto2;
    private static List<BonusTransactionDto> listBonusDto;

    @InjectMocks
    BonusTransactionRestController bonusTransactionRestController;

    @Mock
    BonusTransactionServiceImpl bonusTransactionService;

    @BeforeAll
    static void initMethod() {
        bonusDto1 = BonusTransactionDto.builder()
                .id(3L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        bonusDto2 = BonusTransactionDto.builder()
                .id(3L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        listBonusDto = List.of(bonusDto1, bonusDto2);
    }

    @Test
    void getAll() {
        when(bonusTransactionService.getAll()).thenReturn(listBonusDto);
        ResponseEntity<List<BonusTransactionDto>> responseEntity = bonusTransactionRestController.getAll();
        notNull(responseEntity.getBody(), "Result: NULL");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), listBonusDto);
        verify(bonusTransactionService, times(1))
                .getAll();

    }

    @Test
    void getById() {
        when(bonusTransactionService.getById(1L)).thenReturn(bonusDto1);
        ResponseEntity<BonusTransactionDto> responseEntity = bonusTransactionRestController.getById(1L);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), bonusDto1);
        verify(bonusTransactionService, times(1))
                .getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        assertEquals(bonusTransactionRestController.create(bonusDto1).getStatusCode(), HttpStatus.OK);
        verify(bonusTransactionService, times(1))
                .create(ArgumentMatchers.eq(bonusDto1));
    }

    @Test
    void update() {
        assertEquals(bonusTransactionRestController.update(bonusDto2).getStatusCode(), HttpStatus.OK);
        verify(bonusTransactionService, times(1))
                .update(ArgumentMatchers.eq(bonusDto2));
    }

    @Test
    void deleteById() {
        assertEquals(bonusTransactionRestController.deleteById(999L).getStatusCode(), HttpStatus.OK);
        verify(bonusTransactionService, times(1))
                .deleteById(ArgumentMatchers.eq(999L));
    }
}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.LanguageRestController;
import com.warehouse_accounting.models.dto.LanguageDto;
import com.warehouse_accounting.services.interfaces.LanguageService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class LanguageRestControllerTest {

    private static LanguageDto languageDto;
    private static List<LanguageDto> languageDtoList;

    @Mock
    private LanguageService languageService;

    @InjectMocks
    private LanguageRestController languageRestController;

    @BeforeAll
    static void init() {
        languageDto = LanguageDto.builder()
                .id(1L)
                .language("English")
                .build();

        languageDtoList = List.of(languageDto);
    }

    @Test
    void getAll() {
        when(languageService.getAll()).thenReturn(languageDtoList);
        ResponseEntity<List<LanguageDto>> responseEntity = languageRestController.getAll();
        assertNotNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), languageDtoList);
        verify(languageService, times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(languageService.getById(1L)).thenReturn(languageDto);
        ResponseEntity<LanguageDto> responseEntity = languageRestController.getById(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), languageDto);
        verify(languageService, times(1))
                .getById(1L);
    }

 /*   @Test
    void create() {
        assertEquals(HttpStatus.OK, languageRestController.create(languageDto).getStatusCode());
        verify(languageService, times(1))
                .create(ArgumentMatchers.eq(languageDto));
    }*/

    @Test
    void update() {
        assertEquals(HttpStatus.OK, languageRestController.update(languageDto).getStatusCode());
        verify(languageService, times(1))
                .update(ArgumentMatchers.eq(languageDto));
    }

   /* @Test
    void delete() {
        assertEquals(HttpStatus.OK, languageRestController.deleteById(1L).getStatusCode());
        verify(languageService, times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }*/


}

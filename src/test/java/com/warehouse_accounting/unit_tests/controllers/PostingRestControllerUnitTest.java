package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PostingRestController;
import com.warehouse_accounting.models.dto.PostingDto;
import com.warehouse_accounting.services.interfaces.PostingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PostingRestControllerUnitTest {

    private static PostingDto postingDto1;
    private static PostingDto postingDto2;
    private static List<PostingDto> postingDtoList;

    @InjectMocks
    private PostingRestController postingRestController;

    @Mock
    private PostingService postingService;

    @BeforeEach
    void initMethod() {
        postingDto1 = PostingDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Покупай")
                .build();

        postingDto2 = PostingDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .moved(true)
                .printed(true)
                .comment("Бери")
                .build();

        postingDtoList = List.of(postingDto1, postingDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(postingService.getAll()).thenReturn(postingDtoList);
        ResponseEntity<List<PostingDto>> responseEntity = postingRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), postingDtoList);
        Mockito.verify(postingService, Mockito.times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(postingService.getById(2L)).thenReturn(postingDto1);
        ResponseEntity<PostingDto> postingDtoResponseEntity = postingRestController.getById(2L);
        assertEquals(HttpStatus.OK, postingDtoResponseEntity.getStatusCode());
        assertEquals(postingDtoResponseEntity.getBody(), postingDto1);
        Mockito.verify(postingService, Mockito.times(1)).getById(2L);
    }

    @DisplayName("create method test")
    @Test
    void create() {
        assertEquals(HttpStatus.OK, postingRestController.create(postingDto1).getStatusCode());
        Mockito.verify(postingService, Mockito.times(1))
                .create(ArgumentMatchers.eq(postingDto1));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        assertEquals(HttpStatus.OK, postingRestController.update(postingDto2).getStatusCode());
        Mockito.verify(postingService, Mockito.times(1))
                .update(ArgumentMatchers.eq(postingDto2));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        assertEquals(HttpStatus.OK, postingRestController.deleteById(3L).getStatusCode());
        Mockito.verify(postingService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}
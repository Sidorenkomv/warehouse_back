package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.CallRestController;
import com.warehouse_accounting.models.dto.CallDto;
import com.warehouse_accounting.services.interfaces.CallService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CallRestControllerTest {

    private static CallDto callDto;
    private static List<CallDto> callDtoList;

    @InjectMocks
    private CallRestController callRestController;

    @Mock
    private CallService callService;

    @BeforeEach
    void initMethod() {
        callDto = CallDto.builder()
                .id(1L)
                .callTime(Date.from(Instant.now()))
                .type("type")
                .number(1L)
                .callDuration(1L)
                .comment("comment")
                .callRecord("callRecord")
                .whenChanged(Date.from(Instant.now()))
                .contractorName("contractorName")
                .contractorId(1L)
                .employeeWhoChangedName("employeeWhoChangedName")
                .employeeWhoCalledId(1L)
                .employeeWhoChangedName("employeeWhoChangedName")
                .employeeWhoChangedId(1L)
                .build();

        callDtoList = List.of(callDto);
    }

    @Test
    void getAll() {
        when(callService.getAll()).thenReturn(callDtoList);
        ResponseEntity<List<CallDto>> responseEntity = callRestController.getAll();
        Assert.notNull(HttpStatus.OK, "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), callDtoList);
        Mockito.verify(callService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(callService.getById(1L)).thenReturn(callDto);
        ResponseEntity<CallDto> callDtoResponseEntity = callRestController.getById(1L);
        assertEquals(HttpStatus.OK, callDtoResponseEntity.getStatusCode());
        assertEquals(callDtoResponseEntity.getBody(), callDto);
        Mockito.verify(callService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, callRestController.create(callDto).getStatusCode());
        Mockito.verify(callService, Mockito.times(1))
                .create(ArgumentMatchers.eq(callDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, callRestController.update(callDto).getStatusCode());
        Mockito.verify(callService, Mockito.times(1))
                .update(ArgumentMatchers.eq(callDto));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, callRestController.deleteById(1L).getStatusCode());
        Mockito.verify(callService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.ImageRestController;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.services.interfaces.ImageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ImageRestControllerTest {

    private static ImageDto imageDto;
    private static List<ImageDto> imageDtoList;

    @InjectMocks
    private ImageRestController imageRestController;

    @Mock
    private ImageService imageService;


    @BeforeEach
    void initMethod() {
        imageDto = ImageDto.builder()
                .id(1L)
                .imageUrl("imageUrlChanged")
                .sortNumber("sortNumberChanged")
                .build();

        imageDtoList = List.of(imageDto);
    }

    @Test
    void getAll() {
        when(imageService.getAll()).thenReturn(imageDtoList);
        ResponseEntity<List<ImageDto>> responseEntity = imageRestController.getAll();
        Assert.notNull(HttpStatus.OK, "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), imageDtoList);
        Mockito.verify(imageService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(imageService.getById(1L)).thenReturn(imageDto);
        ResponseEntity<ImageDto> imageDtoResponseEntity = imageRestController.getById(1L);
        assertEquals(HttpStatus.OK, imageDtoResponseEntity.getStatusCode());
        assertEquals(imageDtoResponseEntity.getBody(), imageDto);
        Mockito.verify(imageService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, imageRestController.create(imageDto).getStatusCode());
        Mockito.verify(imageService, Mockito.times(1))
                .create(ArgumentMatchers.eq(imageDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, imageRestController.update(imageDto).getStatusCode());
        Mockito.verify(imageService, Mockito.times(1))
                .update(ArgumentMatchers.eq(imageDto));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, imageRestController.deleteById(1L).getStatusCode());
        Mockito.verify(imageService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PurchaseForecastRestController;
import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.services.interfaces.PurchaseForecastService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PurchaseForecastRestControllerTest {

    private static PurchaseForecastDto purchaseForecastDto1;
    private static PurchaseForecastDto purchaseForecastDto2;
    private static List<PurchaseForecastDto> purchaseForecastDtoList;

    @InjectMocks
    private PurchaseForecastRestController purchaseForecastRestController;


    @Mock
    private PurchaseForecastService purchaseForecastService;

    @BeforeAll
    static void initMethod() {
        purchaseForecastDto1 = PurchaseForecastDto.builder()
                .id(2L)
                .reservedDays(1L)
                .reservedProduct(1L)
                .ordered(false)
                .build();
        purchaseForecastDto2 = PurchaseForecastDto.builder()
                .id(2L)
                .reservedDays(1L)
                .reservedProduct(1L)
                .ordered(false)
                .build();
        purchaseForecastDtoList = List.of(purchaseForecastDto1, purchaseForecastDto2);
    }

    @Test
    void getAll() {
        when(purchaseForecastService.getAll()).thenReturn(purchaseForecastDtoList);
        ResponseEntity<List<PurchaseForecastDto>> responseEntity = purchaseForecastRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), purchaseForecastDtoList);
        Mockito.verify(purchaseForecastService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseForecastService.getById(2L)).thenReturn(purchaseForecastDto1);
        ResponseEntity<PurchaseForecastDto> purchaseForecastDtoResponseEntity = purchaseForecastRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, purchaseForecastDtoResponseEntity.getStatusCode());
        assertEquals(purchaseForecastDtoResponseEntity.getBody(), purchaseForecastDto1);
        Mockito.verify(purchaseForecastService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, purchaseForecastRestController.create(purchaseForecastDto1).getStatusCode());
        Mockito.verify(purchaseForecastService, Mockito.times(1))
                .create(ArgumentMatchers.eq(purchaseForecastDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, purchaseForecastRestController.update(purchaseForecastDto2).getStatusCode());
        Mockito.verify(purchaseForecastService, Mockito.times(1))
                .update(ArgumentMatchers.eq(purchaseForecastDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, purchaseForecastRestController.delete(3L).getStatusCode());
        Mockito.verify(purchaseForecastService, Mockito.times(1))
                .delete(ArgumentMatchers.eq(3L));
    }
}

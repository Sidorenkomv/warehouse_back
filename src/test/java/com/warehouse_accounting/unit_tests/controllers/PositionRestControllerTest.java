package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PositionRestController;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.services.interfaces.PositionService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PositionRestControllerTest {

    private static PositionDto positionDto1;
    private static PositionDto positionDto2;
    private static List<PositionDto> positionDtoList;

    @InjectMocks
    private PositionRestController positionRestController;

    @BeforeAll
    static void initMethod() {
        positionDto1 = PositionDto.builder().id(2L).name("First").sortNumber("wow").build();
        positionDto2 = PositionDto.builder().id(3L).name("Second").sortNumber("noWow").build();

        positionDtoList = List.of(positionDto1, positionDto2);
    }

    @Mock
    private PositionService service;

    @Test
    void getAll() {
        when(service.getAll()).thenReturn(positionDtoList);
        ResponseEntity<List<PositionDto>> responseEntity = positionRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), positionDtoList);
        Mockito.verify(service, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(service.getById(2L)).thenReturn(positionDto1);
        ResponseEntity<PositionDto> positionDtoResponseEntity = positionRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, positionDtoResponseEntity.getStatusCode());
        assertEquals(positionDtoResponseEntity.getBody(), positionDto1);
        Mockito.verify(service, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, positionRestController.create(positionDto1).getStatusCode());
        Mockito.verify(service, Mockito.times(1))
                .create(ArgumentMatchers.eq(positionDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, positionRestController.update(positionDto2).getStatusCode());
        Mockito.verify(service, Mockito.times(1))
                .update(ArgumentMatchers.eq(positionDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, positionRestController.deleteById(3L).getStatusCode());
        Mockito.verify(service, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

package com.warehouse_accounting.unit_tests.controllers;


import com.warehouse_accounting.controllers.rest.ProductionTasksController;
import com.warehouse_accounting.models.dto.ProductionTasksDto;
import com.warehouse_accounting.services.interfaces.ProductionTasksService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductionTasksRestControllerTest {
    // тесты работают с изначально пустой бд
    // перед запуском выключить DataInitializer
    private static ProductionTasksDto productionTasksDto;
    private static List<ProductionTasksDto> productionTasksDtoList;

    @InjectMocks
    private ProductionTasksController controller;
    @Mock
    private ProductionTasksService service;

    @BeforeEach
    void init() {
        productionTasksDto = ProductionTasksDto.builder()
                .id(1L)
                .description("Test1")
                .build();
        ProductionTasksDto productionTasksDto1 = ProductionTasksDto.builder()
                .description("Test2")
                .build();
        productionTasksDtoList = List.of(productionTasksDto, productionTasksDto1);
    }

    @Test
    void getAll() {
        when(service.getAll()).thenReturn(productionTasksDtoList);
        ResponseEntity<List<ProductionTasksDto>> response = controller.getAll();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), productionTasksDtoList);
        Mockito.verify(service, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(service.getById(1L)).thenReturn(productionTasksDto);
        ResponseEntity<ProductionTasksDto> response = controller.getById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), productionTasksDto);
        Mockito.verify(service, Mockito.times(1)).getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, controller.create(productionTasksDto).getStatusCode());
        verify(service, times(1))
                .create(productionTasksDto);
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, controller.update(productionTasksDto).getStatusCode());
        verify(service, times(1))
                .update(productionTasksDto);
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, controller.deleteById(1L).getStatusCode());
        verify(service, times(1))
                .deleteById(1L);
    }
}

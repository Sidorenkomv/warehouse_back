package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PurchasesManagementRestController;
import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.services.interfaces.PurchasesManagementService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PurchasesManagementRestControllerTest {

    private static PurchasesManagementDto purchasesManagementDto1;
    private static PurchasesManagementDto purchasesManagementDto2;
    private static List<PurchasesManagementDto> purchasesManagementDtoList;

    @InjectMocks
    private PurchasesManagementRestController purchasesManagementRestController;

    @Mock
    private PurchasesManagementService purchasesManagementService;

    @BeforeAll
    static void initMethod() {
        purchasesManagementDto1 = PurchasesManagementDto.builder()
                .id(2L)
                .productId(1L)
                .purchasesCurrentBalanceId(1L)
                .purchasesForecastId(1L)
                .purchasesHistoryOfSalesId(1L)
                .build();
        purchasesManagementDto2 = PurchasesManagementDto.builder()
                .id(3L)
                .productId(1L)
                .purchasesCurrentBalanceId(1L)
                .purchasesForecastId(1L)
                .purchasesHistoryOfSalesId(1L)
                .build();

        purchasesManagementDtoList = List.of(purchasesManagementDto1, purchasesManagementDto2);
    }

    @Test
    void getAll() {
        when(purchasesManagementService.getAll()).thenReturn(purchasesManagementDtoList);
        ResponseEntity<List<PurchasesManagementDto>> responseEntity = purchasesManagementRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), purchasesManagementDtoList);
        Mockito.verify(purchasesManagementService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchasesManagementService.getById(2L)).thenReturn(purchasesManagementDto1);
        ResponseEntity<PurchasesManagementDto> purchasesManagementDtoResponseEntity = purchasesManagementRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, purchasesManagementDtoResponseEntity.getStatusCode());
        assertEquals(purchasesManagementDtoResponseEntity.getBody(), purchasesManagementDto1);
        Mockito.verify(purchasesManagementService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, purchasesManagementRestController.create(purchasesManagementDto1).getStatusCode());
        Mockito.verify(purchasesManagementService, Mockito.times(1))
                .create(ArgumentMatchers.eq(purchasesManagementDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, purchasesManagementRestController.update(purchasesManagementDto2).getStatusCode());
        Mockito.verify(purchasesManagementService, Mockito.times(1))
                .update(ArgumentMatchers.eq(purchasesManagementDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, purchasesManagementRestController.delete(3L).getStatusCode());
        Mockito.verify(purchasesManagementService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

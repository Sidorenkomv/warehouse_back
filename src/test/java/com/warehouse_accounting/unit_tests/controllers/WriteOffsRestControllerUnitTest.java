package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.WriteOffsRestController;
import com.warehouse_accounting.models.dto.WriteOffsDto;
import com.warehouse_accounting.services.interfaces.WriteOffsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WriteOffsRestControllerUnitTest {

    private static WriteOffsDto writeOffsDto1;
    private static WriteOffsDto writeOffsDto2;
    private static List<WriteOffsDto> writeOffsDtoList;

    @InjectMocks
    private WriteOffsRestController writeOffsRestController;
    @Mock
    private WriteOffsService writeOffsService;

    @BeforeEach
    void initMethod() {
        writeOffsDto1 = WriteOffsDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(2000L))
                .moved(true)
                .printed(true)
                .comment("Коммент2")
                .build();

        writeOffsDto2 = WriteOffsDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(3000L))
                .moved(true)
                .printed(true)
                .comment("Коммент3")
                .build();

        writeOffsDtoList = List.of(writeOffsDto1, writeOffsDto2);
    }

    @DisplayName("getAll method test")
    @Test
    void getAll() {
        when(writeOffsService.getAll()).thenReturn(writeOffsDtoList);
        ResponseEntity<List<WriteOffsDto>> responseEntity = writeOffsRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), writeOffsDtoList);
        Mockito.verify(writeOffsService, Mockito.times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    void getById() {
        when(writeOffsService.getById(2L)).thenReturn(writeOffsDto1);
        ResponseEntity<WriteOffsDto> writeOffsDtoResponseEntity = writeOffsRestController.getById(2L);
        assertEquals(HttpStatus.OK, writeOffsDtoResponseEntity.getStatusCode());
        assertEquals(writeOffsDtoResponseEntity.getBody(), writeOffsDto1);
        Mockito.verify(writeOffsService, Mockito.times(1)).getById(2L);
    }

    @DisplayName("create method test")
    @Test
    void create() {
        assertEquals(HttpStatus.OK, writeOffsRestController.create(writeOffsDto1).getStatusCode());
        Mockito.verify(writeOffsService, Mockito.times(1))
                .create(ArgumentMatchers.eq(writeOffsDto1));
    }

    @DisplayName("update method test")
    @Test
    void update() {
        assertEquals(HttpStatus.OK, writeOffsRestController.update(writeOffsDto2).getStatusCode());
        Mockito.verify(writeOffsService, Mockito.times(1))
                .update(ArgumentMatchers.eq(writeOffsDto2));
    }

    @DisplayName("delete method test")
    @Test
    void delete() {
        assertEquals(HttpStatus.OK, writeOffsRestController.deleteById(3L).getStatusCode());
        Mockito.verify(writeOffsService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}
package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.GoodsToRealizeGetController;
import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGetService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GoodsToRealizeGiveControllerTest {
    private static GoodsToRealizeGetDto goodsToRealizeGetDto1;
    private static GoodsToRealizeGetDto goodsToRealizeGetDto2;
    private static List<GoodsToRealizeGetDto> goodsToRealizeGetDtoList;

    @InjectMocks
    private GoodsToRealizeGetController goodsToRealizeGetController;

    @Mock
    private GoodsToRealizeGetService goodsToRealizeGetService;

    @BeforeAll
    static void initMethod() {
        goodsToRealizeGetDto1 = GoodsToRealizeGetDto.builder()
                .id(1L)
                .productDtoId(3L)
                .unitId(5L)
                .getGoods(5)
                .quantity(14)
                .amount(16)
                .arrive(17)
                .remains(18)
                .quantity_report(789)
                .amount_report(6699)
                .quantity_Noreport(341)
                .amount_Noreport(652)
                .build();
        goodsToRealizeGetDto2 = GoodsToRealizeGetDto.builder()
                .id(2L)
                .productDtoId(1L)
                .unitId(2L)
                .getGoods(5)
                .quantity(66)
                .amount(6)
                .arrive(7)
                .remains(8)
                .quantity_report(88)
                .amount_report(99)
                .quantity_Noreport(111)
                .amount_Noreport(222)
                .build();
        goodsToRealizeGetDtoList = List.of(goodsToRealizeGetDto1, goodsToRealizeGetDto2);
    }

    @Test
    void getAll() {
        when(goodsToRealizeGetService.getAll()).thenReturn(goodsToRealizeGetDtoList);
        ResponseEntity<List<GoodsToRealizeGetDto>> responseEntity = goodsToRealizeGetController.getAll();
        Assert.notNull(responseEntity.getBody(), "а тут вылез null");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), goodsToRealizeGetDtoList);
        Mockito.verify(goodsToRealizeGetService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(goodsToRealizeGetService.getById(1L)).thenReturn(goodsToRealizeGetDto1);
        ResponseEntity<GoodsToRealizeGetDto> roleDtoResponseEntity = goodsToRealizeGetController.getById(1L);
        assertEquals(roleDtoResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(roleDtoResponseEntity.getBody(), goodsToRealizeGetDto1);
        Mockito.verify(goodsToRealizeGetService, Mockito.times(1))
                .getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        assertEquals(goodsToRealizeGetController.create(goodsToRealizeGetDto1).getStatusCode(), HttpStatus.OK);
        Mockito.verify(goodsToRealizeGetService, Mockito.times(1))
                .create(ArgumentMatchers.eq(goodsToRealizeGetDto1));
    }

    @Test
    void update() {
        assertEquals(goodsToRealizeGetController.update(goodsToRealizeGetDto2).getStatusCode(), HttpStatus.OK);
        Mockito.verify(goodsToRealizeGetService, Mockito.times(1))
                .update(ArgumentMatchers.eq(goodsToRealizeGetDto2));
    }

    @Test
    void deleteById() {
        assertEquals(goodsToRealizeGetController.deleteById((long) 999).getStatusCode(), HttpStatus.OK);
        Mockito.verify(goodsToRealizeGetService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq((long) 999));
    }


}

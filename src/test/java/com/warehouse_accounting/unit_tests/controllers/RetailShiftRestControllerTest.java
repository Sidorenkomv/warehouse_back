package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.RetailShiftRestController;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.services.interfaces.RetailShiftService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RetailShiftRestControllerTest {
        private static RetailShiftDto retailShiftDto;
        private static List<RetailShiftDto> retailShiftDtoList;

        @InjectMocks
        private RetailShiftRestController retailShiftRestController;

        @Mock
        private RetailShiftService retailShiftService;

        @BeforeEach
        void initMethod() {
            retailShiftDto = RetailShiftDto.builder()
                    .id(1L)
                    .dateOfOpen(LocalDate.now())
                    .dateOfClose(LocalDate.now())
                    .pointOfSales(new PointOfSalesDto())
                    .warehouse(new WarehouseDto())
                    .company(new CompanyDto())
                    .bank(new BankAccountDto())
                    .cashlessShiftRevenue(BigDecimal.valueOf(12))
                    .cashShiftRevenue(BigDecimal.valueOf(7))
                    .shiftRevenue(BigDecimal.valueOf(19))
                    .recevied(BigDecimal.valueOf(11))
                    .sale(BigDecimal.valueOf(3))
                    .comission(BigDecimal.valueOf(2))
                    .isAccessed(true)
                    .ownerDepartment(new DepartmentDto())
                    .ownerEmployee(new EmployeeDto())
                    .isSent(true)
                    .isPrinted(false)
                    .description("comment")
                    .dateOfEdit(LocalDate.now())
                    .editEmployee(new EmployeeDto())
                    .build();


            retailShiftDtoList = List.of(retailShiftDto);
        }

        @DisplayName("getAll method test")
        @Test
        void getAll() {
            when(retailShiftService.getAll()).thenReturn(retailShiftDtoList);
            ResponseEntity<List<RetailShiftDto>> responseEntity = retailShiftRestController.getAll();
            Assert.notNull(responseEntity.getBody(), "Null");
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals(responseEntity.getBody(), retailShiftDtoList);
            Mockito.verify(retailShiftService, Mockito.times(1)).getAll();
        }

        @DisplayName("getById method test")
        @Test
        void getById() {
            when(retailShiftService.getById(1L)).thenReturn(retailShiftDto);
            ResponseEntity<RetailShiftDto> retailShiftDtoResponseEntity = retailShiftRestController.getById(1L);
            assertEquals(HttpStatus.OK, retailShiftDtoResponseEntity.getStatusCode());
            assertEquals(retailShiftDtoResponseEntity.getBody(), retailShiftDto);
            Mockito.verify(retailShiftService, Mockito.times(1)).getById(1L);
        }

        @DisplayName("create method test")
        @Test
        void create() {
            assertEquals(HttpStatus.OK, retailShiftRestController.create(retailShiftDto).getStatusCode());
            Mockito.verify(retailShiftService, Mockito.times(1))
                    .create(ArgumentMatchers.eq(retailShiftDto));
        }

        @DisplayName("update method test")
        @Test
        void update() {
            assertEquals(HttpStatus.OK, retailShiftRestController.update(retailShiftDto).getStatusCode());
            Mockito.verify(retailShiftService, Mockito.times(1))
                    .update(ArgumentMatchers.eq(retailShiftDto));
        }

        @DisplayName("delete method test")
        @Test
        void delete() {
            assertEquals(HttpStatus.OK, retailShiftRestController.deleteById(1L).getStatusCode());
            Mockito.verify(retailShiftService, Mockito.times(1))
                    .deleteById(ArgumentMatchers.eq(1L));
        }

}

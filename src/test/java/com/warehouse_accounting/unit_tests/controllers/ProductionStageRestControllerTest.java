package com.warehouse_accounting.unit_tests.controllers;


import com.warehouse_accounting.controllers.rest.ProductionStageRestController;
import com.warehouse_accounting.models.dto.ProductionStageDto;
import com.warehouse_accounting.services.interfaces.ProductionStageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductionStageRestControllerTest {
    // тесты работают с изначально пустой бд
    // перед запуском выключить Datainitialaizer
    //Если будет добавлен скрипт по созданию ProductionStage он тоже повлияет на работу тестов.
    private static ProductionStageDto productionStageDto;
    private static ProductionStageDto productionStageDto2;
    private static List<ProductionStageDto> productionStageDtoList;

    @InjectMocks
    private ProductionStageRestController controller;
    @Mock
    private ProductionStageService stageService;

    @BeforeEach
    void init() {
        productionStageDto = ProductionStageDto.builder()
                .id(1l)
                .name("Основной этап")
                .description("Описание")
                .generalAccess(true)
                .archived(false)
                .build();
        productionStageDto2 = ProductionStageDto.builder()
                .name("Основной этап")
                .description("Описание")
                .generalAccess(true)
                .archived(true)
                .build();
        productionStageDtoList = List.of(productionStageDto, productionStageDto2);
    }

    @Test
    void getAll() {
        when(stageService.getAll(false)).thenReturn(productionStageDtoList);
        ResponseEntity<List<ProductionStageDto>> response = controller.getAll();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), productionStageDtoList);
        Mockito.verify(stageService, Mockito.times(1))
                .getAll(false);
    }

    @Test
    void getById() {
        when(stageService.getById(1L)).thenReturn(productionStageDto);
        ResponseEntity<ProductionStageDto> response = controller.getById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), productionStageDto);
        Mockito.verify(stageService, Mockito.times(1)).getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, controller.create(productionStageDto).getStatusCode());
        verify(stageService, times(1))
                .create(ArgumentMatchers.eq(productionStageDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, controller.update(productionStageDto).getStatusCode());
        verify(stageService, times(1))
                .update(ArgumentMatchers.eq(productionStageDto));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, controller.delete(1L).getStatusCode());
        verify(stageService, times(1))
                .delete(ArgumentMatchers.eq(1L));
    }
}

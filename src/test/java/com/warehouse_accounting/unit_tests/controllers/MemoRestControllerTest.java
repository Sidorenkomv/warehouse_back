package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.MemoRestController;
import com.warehouse_accounting.models.dto.MemoDto;
import com.warehouse_accounting.services.interfaces.MemoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class MemoRestControllerTest {

    private static MemoDto memoDto;
    private static List<MemoDto> memoDtoList;

    @InjectMocks
    private MemoRestController memoRestController;

    @Mock
    private MemoService memoService;


    @Before
    public void setUp() {
        memoDto = MemoDto.builder()
                .id(1L)
                .createDate(LocalDateTime.of(2022, 4, 22, 2, 57))
                .content("123")
                .contractorName("test contractor")
                .contractorId(1L)
                .employeeWhoCreatedName("test creator employee")
                .employeeWhoCreatedId(1L)
                .employeeWhoEditedName("test edit employee")
                .employeeWhoEditedId(1L)
                .build();

        memoDtoList = List.of(memoDto);
    }

    @Test
    public void getAll() {
        when(memoService.getAll()).thenReturn(memoDtoList);
        ResponseEntity<List<MemoDto>> responseEntity = memoRestController.getAll();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), memoDtoList);
        verify(memoService, times(1)).getAll();
    }

    @Test
    public void getById() {
        when(memoService.getById(1L)).thenReturn(memoDto);
        ResponseEntity<MemoDto> responseEntity = memoRestController.getById(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), memoDto);

        Mockito.verify(memoService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    public void create() {
        assertEquals(HttpStatus.OK, memoRestController.create(memoDto).getStatusCode());
        Mockito.verify(memoService, Mockito.times(1))
                .create(ArgumentMatchers.eq(memoDto));
    }

    @Test
    public void update() {
        assertEquals(HttpStatus.OK, memoRestController.update(memoDto).getStatusCode());
        Mockito.verify(memoService, Mockito.times(1))
                .update(ArgumentMatchers.eq(memoDto));
    }

    @Test
    public void deleteById() {
        assertEquals(HttpStatus.OK, memoRestController.deleteById(1L).getStatusCode());
        Mockito.verify(memoService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}
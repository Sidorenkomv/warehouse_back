package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.StartScreenRestController;
import com.warehouse_accounting.models.dto.StartScreenDto;
import com.warehouse_accounting.services.interfaces.StartScreenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StartScreenRestControllerTest {

    private static StartScreenDto startScreenDto1;
    private static StartScreenDto startScreenDto2;
    private static List<StartScreenDto> startScreenDtoList;

    @InjectMocks
    private StartScreenRestController startScreenRestController;
    @Mock
    private StartScreenService startScreenService;

    @BeforeEach
    void init() {
        startScreenDto1 = StartScreenDto.builder()
                .id(1L).startScreen("meow").build();
        startScreenDto2 = StartScreenDto.builder()
                .id(2L).startScreen("meow_meow").build();
        startScreenDtoList = List.of(startScreenDto1, startScreenDto2);
    }

    @Test
    void getAll() {
        when(startScreenService.getAll()).thenReturn(startScreenDtoList);
        ResponseEntity<List<StartScreenDto>> responseEntity = startScreenRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "It's a Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), startScreenDtoList);
        Mockito.verify(startScreenService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(startScreenService.getById(1L)).thenReturn(startScreenDto1);
        ResponseEntity<StartScreenDto> ResponseEntity = startScreenRestController.getById(1L);
        assertEquals(HttpStatus.OK, ResponseEntity.getStatusCode());
        assertEquals(ResponseEntity.getBody(), startScreenDto1);
        Mockito.verify(startScreenService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, startScreenRestController.create(startScreenDto1).getStatusCode());
        Mockito.verify(startScreenService, Mockito.times(1))
                .create(ArgumentMatchers.eq(startScreenDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, startScreenRestController.update(startScreenDto2).getStatusCode());
        Mockito.verify(startScreenService, Mockito.times(1))
                .update(ArgumentMatchers.eq(startScreenDto2));
    }

    @Test
    void deleteById() {
        assertEquals(HttpStatus.OK, startScreenRestController.deleteById(3L).getStatusCode());
        Mockito.verify(startScreenService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.CommissionReportsRestController;
import com.warehouse_accounting.models.dto.CommissionReportsDto;
import com.warehouse_accounting.services.interfaces.CommissionReportsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CommissionReportsRestControllerTest {
    private static CommissionReportsDto commissionReportsDto;
    private static CommissionReportsDto commissionReportsDto1;
    private static List<CommissionReportsDto> commissionReportsDtoList;

    @InjectMocks
    private CommissionReportsRestController commissionReportsRestController;
    @Mock
    private CommissionReportsService commissionReportsService;

    @BeforeEach
    void initMethod() {
        commissionReportsDto = CommissionReportsDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(1L))
                .paid(BigDecimal.valueOf(1L))
                .isSent(true)
                .isPrinted(true)
                .comment("Первый")
                .startOfPeriod(LocalDateTime.now())
                .endOfPeriod(LocalDateTime.now())
                .sumOfReward(BigDecimal.valueOf(0))
                .build();
        commissionReportsDto1 = CommissionReportsDto.builder()
                .id(3L)
                .dateOfCreation(LocalDateTime.now())
                .sum(BigDecimal.valueOf(1L))
                .paid(BigDecimal.valueOf(1L))
                .isSent(true)
                .isPrinted(true)
                .comment("Второй")
                .startOfPeriod(LocalDateTime.now())
                .endOfPeriod(LocalDateTime.now())
                .sumOfReward(BigDecimal.valueOf(0))
                .build();

        commissionReportsDtoList = List.of(commissionReportsDto, commissionReportsDto1);
    }

    @Test
    void getAll() {
        when(commissionReportsService.getAll()).thenReturn(commissionReportsDtoList);
        ResponseEntity<List<CommissionReportsDto>> responseEntity = commissionReportsRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), commissionReportsDtoList);
        Mockito.verify(commissionReportsService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(commissionReportsService.getById(2L)).thenReturn(commissionReportsDto);
        ResponseEntity<CommissionReportsDto> commissionReportsDtoResponseEntity = commissionReportsRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, commissionReportsDtoResponseEntity.getStatusCode());
        assertEquals(commissionReportsDtoResponseEntity.getBody(), commissionReportsDto);
        Mockito.verify(commissionReportsService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, commissionReportsRestController.create(commissionReportsDto).getStatusCode());
        Mockito.verify(commissionReportsService, Mockito.times(1))
                .create(ArgumentMatchers.eq(commissionReportsDto));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, commissionReportsRestController.update(commissionReportsDto1).getStatusCode());
        Mockito.verify(commissionReportsService, Mockito.times(1))
                .update(ArgumentMatchers.eq(commissionReportsDto1));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, commissionReportsRestController.delete(3L).getStatusCode());
        Mockito.verify(commissionReportsService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}

package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PurchaseCurrentBalanceRestController;
import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.services.interfaces.PurchaseCurrentBalanceService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PurchaseCurrentBalanceRestControllerTest {

    private static PurchaseCurrentBalanceDto purchaseCurrentBalanceDto1;
    private static PurchaseCurrentBalanceDto purchaseCurrentBalanceDto2;
    private static List<PurchaseCurrentBalanceDto> purchaseCurrentBalanceDtoList;

    @InjectMocks
    private PurchaseCurrentBalanceRestController purchaseCurrentBalanceRestController;


    @Mock
    private PurchaseCurrentBalanceService purchaseCurrentBalanceService;

    @BeforeAll
    static void initMethod() {
        purchaseCurrentBalanceDto1 = PurchaseCurrentBalanceDto.builder()
                .id(2L)
                .remainder(1L)
                .productReserved(1L)
                .productsAwaiting(1L)
                .productAvailableForOrder(1L)
                .daysStoreOnTheWarehouse(1L)
                .build();
        purchaseCurrentBalanceDto2 = PurchaseCurrentBalanceDto.builder()
                .id(3L)
                .remainder(1L)
                .productReserved(1L)
                .productsAwaiting(1L)
                .productAvailableForOrder(1L)
                .daysStoreOnTheWarehouse(1L)
                .build();

        purchaseCurrentBalanceDtoList = List.of(purchaseCurrentBalanceDto1, purchaseCurrentBalanceDto2);
    }

    @Test
    void getAll() {
        when(purchaseCurrentBalanceService.getAll()).thenReturn(purchaseCurrentBalanceDtoList);
        ResponseEntity<List<PurchaseCurrentBalanceDto>> responseEntity = purchaseCurrentBalanceRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), purchaseCurrentBalanceDtoList);
        Mockito.verify(purchaseCurrentBalanceService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseCurrentBalanceService.getById(2L)).thenReturn(purchaseCurrentBalanceDto1);
        ResponseEntity<PurchaseCurrentBalanceDto> purchaseCurrentBalanceDtoResponseEntity = purchaseCurrentBalanceRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, purchaseCurrentBalanceDtoResponseEntity.getStatusCode());
        assertEquals(purchaseCurrentBalanceDtoResponseEntity.getBody(), purchaseCurrentBalanceDto1);
        Mockito.verify(purchaseCurrentBalanceService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, purchaseCurrentBalanceRestController.create(purchaseCurrentBalanceDto1).getStatusCode());
        Mockito.verify(purchaseCurrentBalanceService, Mockito.times(1))
                .create(ArgumentMatchers.eq(purchaseCurrentBalanceDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, purchaseCurrentBalanceRestController.update(purchaseCurrentBalanceDto2).getStatusCode());
        Mockito.verify(purchaseCurrentBalanceService, Mockito.times(1))
                .update(ArgumentMatchers.eq(purchaseCurrentBalanceDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, purchaseCurrentBalanceRestController.delete(3L).getStatusCode());
        Mockito.verify(purchaseCurrentBalanceService, Mockito.times(1))
                .delete(ArgumentMatchers.eq(3L));
    }
}

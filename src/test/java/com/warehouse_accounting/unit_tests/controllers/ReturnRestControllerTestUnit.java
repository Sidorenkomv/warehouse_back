package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.ReturnRestController;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.services.interfaces.ReturnService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReturnRestControllerTestUnit {

    private static ReturnDto returnDto1;
    private static ReturnDto returnDto2;
    private static List<ReturnDto> returnDtoList;

    @InjectMocks
    private ReturnRestController returnRestController;

    @Mock
    private ReturnService returnService;

    @BeforeEach
    void initReturns() {
        returnDto1 = ReturnDto.builder()
                .id(1L)
                .dataTime(LocalDateTime.now())
                .warehouseDto(WarehouseDto.builder().build())
                .companyDto(CompanyDto.builder().build())
                .contractorDto(ContractorDto.builder().build())
                .contractDto(ContractDto.builder().build())
                .projectDto(ProjectDto.builder().build())
                .fileDtos(new ArrayList<>())
                .taskDtos(new ArrayList<>())
                .productDtos(new ArrayList<>())
                .sum(BigDecimal.valueOf(1000))
                .isSent(false)
                .isPrinted(true)
                .comment("comment1")
                .build();

        returnDto2 = ReturnDto.builder()
                .id(2L)
                .dataTime(LocalDateTime.now())
                .warehouseDto(WarehouseDto.builder().build())
                .companyDto(CompanyDto.builder().build())
                .contractorDto(ContractorDto.builder().build())
                .contractDto(ContractDto.builder().build())
                .projectDto(ProjectDto.builder().build())
                .fileDtos(new ArrayList<>())
                .taskDtos(new ArrayList<>())
                .productDtos(new ArrayList<>())
                .sum(BigDecimal.valueOf(5000))
                .isSent(false)
                .isPrinted(true)
                .comment("comment2")
                .build();

        returnDtoList = List.of(returnDto1, returnDto2);
    }

    @Test
    void getAll() {
        when(returnService.getAll()).thenReturn(returnDtoList);
        ResponseEntity<List<ReturnDto>> responseEntity = returnRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), returnDtoList);
        Mockito.verify(returnService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(returnService.getById(2L)).thenReturn(returnDto1);
        ResponseEntity<ReturnDto> invoicesIssuedDtoResponseEntity = returnRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, invoicesIssuedDtoResponseEntity.getStatusCode());
        assertEquals(invoicesIssuedDtoResponseEntity.getBody(), returnDto1);
        Mockito.verify(returnService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, returnRestController.create(returnDto1).getStatusCode());
        Mockito.verify(returnService, Mockito.times(1))
                .create(ArgumentMatchers.eq(returnDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, returnRestController.update(returnDto2).getStatusCode());
        Mockito.verify(returnService, Mockito.times(1))
                .update(ArgumentMatchers.eq(returnDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, returnRestController.delete(3L).getStatusCode());
        Mockito.verify(returnService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }

}

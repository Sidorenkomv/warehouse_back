package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.InternalOrderRestController;
import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.InternalOrderDto;
import com.warehouse_accounting.services.interfaces.InternalOrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InternalOrderRestControllerTest {

    public static final long ID = 2L;
    private static InternalOrderDto internalOrderDto;

    private static final List<InternalOrderDto> internalOrderDtoList = new ArrayList<>();

    @InjectMocks
    private InternalOrderRestController internalOrderRestController;

    @Mock
    private InternalOrderService internalOrderService;

    @BeforeEach
    public void setUp() {
        internalOrderDto = InternalOrderDto.builder()
                .id(ID)
                .number("num1")
                .localDateTime(LocalDateTime.now())
                .organization(CompanyDto.builder().build())
                .sum(BigDecimal.valueOf(80000L))
                .shipped(BigDecimal.ZERO)
                .isSharedAccess(true)
                .sent(false)
                .print(false)
                .comment("comment1")
                .build();

        internalOrderDtoList.add(internalOrderDto);
    }

    @DisplayName("getAll method test")
    @Test
    public void getAll() {
        when(internalOrderService.getAll()).thenReturn(internalOrderDtoList);

        ResponseEntity<List<InternalOrderDto>> responseEntity = internalOrderRestController.getAll();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), internalOrderDtoList);
        verify(internalOrderService, times(1)).getAll();
    }

    @DisplayName("getById method test")
    @Test
    public void getById() {
        when(internalOrderService.getById(ID)).thenReturn(internalOrderDto);

        ResponseEntity<InternalOrderDto> responseEntity = internalOrderRestController.getById(ID);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), internalOrderDto);

        verify(internalOrderService, Mockito.times(1))
                .getById(ID);
    }

    @DisplayName("create method test")
    @Test
    public void create() {
        ResponseEntity<?> responseEntity = internalOrderRestController.create(internalOrderDto);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(internalOrderService, Mockito.times(1))
                .create(ArgumentMatchers.eq(internalOrderDto));
    }

    @DisplayName("update method test")
    @Test
    public void update() {
        internalOrderDto = InternalOrderDto.builder()
                .id(ID)
                .number("num22")
                .localDateTime(LocalDateTime.of(2022, 11, 11, 3, 0, 0))
                .organization(CompanyDto.builder().build())
                .sum(BigDecimal.valueOf(90000L))
                .shipped(BigDecimal.valueOf(10L))
                .isSharedAccess(false)
                .sent(true)
                .print(true)
                .comment("comment122")
                .build();

        ResponseEntity<?> responseEntity = internalOrderRestController.update(internalOrderDto);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(internalOrderService, Mockito.times(1))
                .update(ArgumentMatchers.eq(internalOrderDto));
    }

    @DisplayName("deleteById method test")
    @Test
    public void deleteById() {

        ResponseEntity<?> responseEntity = internalOrderRestController.deleteById(ID);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(internalOrderService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(ID));
    }
}

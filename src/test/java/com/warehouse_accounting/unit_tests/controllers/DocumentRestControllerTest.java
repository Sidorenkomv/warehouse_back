package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.DocumentRestController;
import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.services.interfaces.DocumentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DocumentRestControllerTest {

    private static DocumentDto documentDto;

    private static List<DocumentDto> documentDtoList;

    @InjectMocks
    private DocumentRestController documentRestController;

    @Mock
    private DocumentService documentService;

    @BeforeEach
    public void setUp() {
        documentDto = DocumentDto.builder()
                .id(1L)
                .type("type1")
                .docNumber("number1")
                .date(LocalDateTime.now())
                .sum(BigDecimal.valueOf(10000))
                .warehouseFromId(1L)
                .warehouseFromName("warehouse1")
                .warehouseToId(2L)
                .warehouseToName("warenhouse2")
                .companyId(1L)
                .companyName("company1")
                .contrAgentId(1L)
                .contrAgentName("contr1")
                .projectId(1L)
                .projectName("project1")
                .salesChannelId(1L)
                .salesChannelName("sales1")
                .contractId(1L)
                .contractNumber("num1")
                .isSharedAccess(true)
                .departmentId(1l)
                .departmentName("dep1")
                .employeeId(1L)
                .employeeFirstname("name1")
                .sent(true)
                .print(false)
                .comments("comment1")
                .updatedFromEmployeeId(2L)
                .updatedFromEmployeeFirstname("name2")
                .build();

        documentDtoList = List.of(documentDto);
    }

    @Test
    public void getAll() {
        when(documentService.getAll()).thenReturn(documentDtoList);

        ResponseEntity<List<DocumentDto>> responseEntity = documentRestController.getAll();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), documentDtoList);
        verify(documentService, times(1)).getAll();
    }

    @Test
    public void getById() {
        when(documentService.getById(1L)).thenReturn(documentDto);

        ResponseEntity<DocumentDto> responseEntity = documentRestController.getById(1L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), documentDto);

        verify(documentService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    public void create() {
        assertEquals(HttpStatus.OK, documentRestController.create(documentDto).getStatusCode());
        verify(documentService, Mockito.times(1))
                .create(ArgumentMatchers.eq(documentDto));
    }

    @Test
    public void update() {
        documentDto = DocumentDto.builder()
                .id(1L)
                .type("type12")
                .docNumber("number111")
                .date(LocalDateTime.now())
                .sum(BigDecimal.valueOf(10000))
                .warehouseFromId(1L)
                .warehouseFromName("warehouse1g")
                .warehouseToId(2L)
                .warehouseToName("warenhouse2")
                .companyId(1L)
                .companyName("company1")
                .contrAgentId(1L)
                .contrAgentName("contr1")
                .projectId(1L)
                .projectName("project1")
                .salesChannelId(1L)
                .salesChannelName("sales1")
                .contractId(1L)
                .contractNumber("num1")
                .isSharedAccess(false)
                .departmentId(1l)
                .departmentName("dep1")
                .employeeId(1L)
                .employeeFirstname("name1")
                .sent(false)
                .print(true)
                .comments("comment2")
                .updatedFromEmployeeId(2L)
                .updatedFromEmployeeFirstname("name32")
                .build();

        assertEquals(HttpStatus.OK, documentRestController.update(documentDto).getStatusCode());
        verify(documentService, Mockito.times(1))
                .update(ArgumentMatchers.eq(documentDto));
    }

    @Test
    public void deleteById() {
        assertEquals(HttpStatus.OK, documentRestController.deleteById(1L).getStatusCode());
        verify(documentService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

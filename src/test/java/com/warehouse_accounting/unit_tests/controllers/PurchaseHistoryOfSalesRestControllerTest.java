package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.PurchaseHistoryOfSalesRestController;
import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.services.interfaces.PurchaseHistoryOfSalesService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PurchaseHistoryOfSalesRestControllerTest {

    private static PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto1;
    private static PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto2;
    private static List<PurchaseHistoryOfSalesDto> purchaseHistoryOfSalesDtoList;

    @InjectMocks
    private PurchaseHistoryOfSalesRestController purchaseHistoryOfSalesRestController;

    @Mock
    private PurchaseHistoryOfSalesService purchaseHistoryOfSalesService;

    @BeforeAll
    static void initMethod() {
        purchaseHistoryOfSalesDto1 = PurchaseHistoryOfSalesDto.builder()
                .id(2L)
                .number(1L)
                .productPriceId(1L)
                .sum(BigDecimal.valueOf(1))
                .profit(BigDecimal.valueOf(1))
                .profitability(BigDecimal.valueOf(1))
                .saleOfDay(BigDecimal.valueOf(1))
                .build();
        purchaseHistoryOfSalesDto2 = PurchaseHistoryOfSalesDto.builder()
                .id(3L)
                .number(1L)
                .productPriceId(1L)
                .sum(BigDecimal.valueOf(1))
                .profit(BigDecimal.valueOf(1))
                .profitability(BigDecimal.valueOf(1))
                .saleOfDay(BigDecimal.valueOf(1))
                .build();
        purchaseHistoryOfSalesDtoList = List.of(purchaseHistoryOfSalesDto1, purchaseHistoryOfSalesDto2);
    }

    @Test
    void getAll() {
        when(purchaseHistoryOfSalesService.getAll()).thenReturn(purchaseHistoryOfSalesDtoList);
        ResponseEntity<List<PurchaseHistoryOfSalesDto>> responseEntity = purchaseHistoryOfSalesRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), purchaseHistoryOfSalesDtoList);
        Mockito.verify(purchaseHistoryOfSalesService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(purchaseHistoryOfSalesService.getById(2L)).thenReturn(purchaseHistoryOfSalesDto1);
        ResponseEntity<PurchaseHistoryOfSalesDto> purchaseHistoryOfSalesDtoResponseEntity = purchaseHistoryOfSalesRestController
                .getById(2L);
        assertEquals(HttpStatus.OK, purchaseHistoryOfSalesDtoResponseEntity.getStatusCode());
        assertEquals(purchaseHistoryOfSalesDtoResponseEntity.getBody(), purchaseHistoryOfSalesDto1);
        Mockito.verify(purchaseHistoryOfSalesService, Mockito.times(1))
                .getById(2L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, purchaseHistoryOfSalesRestController.create(purchaseHistoryOfSalesDto1).getStatusCode());
        Mockito.verify(purchaseHistoryOfSalesService, Mockito.times(1))
                .create(ArgumentMatchers.eq(purchaseHistoryOfSalesDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, purchaseHistoryOfSalesRestController.update(purchaseHistoryOfSalesDto2).getStatusCode());
        Mockito.verify(purchaseHistoryOfSalesService, Mockito.times(1))
                .update(ArgumentMatchers.eq(purchaseHistoryOfSalesDto2));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, purchaseHistoryOfSalesRestController.delete(3L).getStatusCode());
        Mockito.verify(purchaseHistoryOfSalesService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(3L));
    }
}

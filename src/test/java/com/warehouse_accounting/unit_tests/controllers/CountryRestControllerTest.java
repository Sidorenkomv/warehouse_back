package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.CountryRestController;
import com.warehouse_accounting.models.dto.CountryDto;
import com.warehouse_accounting.services.interfaces.CountryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.math.BigInteger;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Deprecated
class CountryRestControllerTest {

    private static CountryDto countryDto1;
    private static List<CountryDto> countryDtoList;

    @InjectMocks
    private CountryRestController countryRestController;

    @Mock
    private CountryService countryService;

    @BeforeEach
    void initMethod() {
        countryDto1 = CountryDto.builder()
                .id(1L)
                .shortName("shortName")
                .longName("longName")
                .code((short)100)
                .codeOne("codeOne")
                .codeTwo("codeTwo")
                .build();

        countryDtoList = List.of(countryDto1);
    }

    @Test
    void getAll() {
        when(countryService.getAll()).thenReturn(countryDtoList);
        ResponseEntity<List<CountryDto>> responseEntity = countryRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "Null");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), countryDtoList);
        Mockito.verify(countryService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(countryService.getById(1L)).thenReturn(countryDto1);
        ResponseEntity<CountryDto> countryDtoResponseEntity = countryRestController.getById(1L);
        assertEquals(HttpStatus.OK, countryDtoResponseEntity.getStatusCode());
        assertEquals(countryDtoResponseEntity.getBody(), countryDto1);
        Mockito.verify(countryService, Mockito.times(1))
                .getById(1L);
    }

    @Test
    void create() {
        assertEquals(HttpStatus.OK, countryRestController.create(countryDto1).getStatusCode());
        Mockito.verify(countryService, Mockito.times(1))
                .create(ArgumentMatchers.eq(countryDto1));
    }

    @Test
    void update() {
        assertEquals(HttpStatus.OK, countryRestController.update(countryDto1).getStatusCode());
        Mockito.verify(countryService, Mockito.times(1))
                .update(ArgumentMatchers.eq(countryDto1));
    }

    @Test
    void delete() {
        assertEquals(HttpStatus.OK, countryRestController.deleteById(1L).getStatusCode());
        Mockito.verify(countryService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq(1L));
    }
}

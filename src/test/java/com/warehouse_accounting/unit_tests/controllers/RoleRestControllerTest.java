package com.warehouse_accounting.unit_tests.controllers;

import com.warehouse_accounting.controllers.rest.RoleRestController;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.services.interfaces.RoleService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RoleRestControllerTest {

    private static Role role1;
    private static Role role2;
    private static List<Role> roleList;

    @InjectMocks
    private RoleRestController roleRestController;

    @Mock
    private RoleService roleService;


    @BeforeAll
    static void initMethod() {
        role1 = Role.builder()
                .id((long) 1)
                .name("first")
                .build();
        role2 = Role.builder()
                .id((long) 2)
                .name("second")
                .build();
        roleList = List.of(role1, role2);
    }

    @Test
    void getAll() {
        when(roleService.getAll()).thenReturn(roleList);
        ResponseEntity<List<Role>> responseEntity = roleRestController.getAll();
        Assert.notNull(responseEntity.getBody(), "а тут вылез null");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), roleList);
        Mockito.verify(roleService, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById() {
        when(roleService.getById((long) 1)).thenReturn(role1);
        ResponseEntity<Role> roleDtoResponseEntity = roleRestController.getById((long) 1);
        assertEquals(roleDtoResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(roleDtoResponseEntity.getBody(), role1);
        Mockito.verify(roleService, Mockito.times(1))
                .getById(ArgumentMatchers.eq((long) 1));
    }

    @Test
    void create() {
        assertEquals(roleRestController.create(role1).getStatusCode(), HttpStatus.OK);
        Mockito.verify(roleService, Mockito.times(1))
                .create(ArgumentMatchers.eq(role1));
    }

    @Test
    void update() {
        assertEquals(roleRestController.update(role2).getStatusCode(), HttpStatus.OK);
        Mockito.verify(roleService, Mockito.times(1))
                .update(ArgumentMatchers.eq(role2));
    }

    @Test
    void deleteById() {
        assertEquals(roleRestController.deleteById((long) 999).getStatusCode(), HttpStatus.OK);
        Mockito.verify(roleService, Mockito.times(1))
                .deleteById(ArgumentMatchers.eq((long) 999));
    }
}
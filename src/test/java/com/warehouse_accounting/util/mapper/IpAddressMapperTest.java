package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.IpAddress;
import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class IpAddressMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        IpAddress ipAddress = new IpAddress(1L, "1.1.1.1");
        IpAddressDto ipAddressDto = mapper.convert(ipAddress);

        Assertions.assertEquals(ipAddress.getId(), ipAddressDto.getId());
        Assertions.assertEquals(ipAddress.getAddress(), ipAddressDto.getAddress());
    }

    @Test
    void convertToModelTest() {
        IpAddressDto ipAddressDto = new IpAddressDto(1L, "1.1.1.1");
        IpAddress ipAddress = mapper.convert(ipAddressDto);

        Assertions.assertEquals(ipAddress.getId(), ipAddressDto.getId());
        Assertions.assertEquals(ipAddress.getAddress(), ipAddressDto.getAddress());
    }
}

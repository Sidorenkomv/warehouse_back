package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Image;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ImageMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        Image image = new Image(1L, "url", "sort");
        ImageDto imageDto = mapper.convert(image);

        Assertions.assertEquals(image.getId(), imageDto.getId());
        Assertions.assertEquals(image.getImageUrl(), imageDto.getImageUrl());
        Assertions.assertEquals(image.getSortNumber(), imageDto.getSortNumber());
    }

    @Test
    void convertToModelTest() {
        ImageDto imageDto = new ImageDto(1L, "url", "sort");
        Image image = mapper.convert(imageDto);

        Assertions.assertEquals(image.getId(), imageDto.getId());
        Assertions.assertEquals(image.getImageUrl(), imageDto.getImageUrl());
        Assertions.assertEquals(image.getSortNumber(), imageDto.getSortNumber());
    }
}

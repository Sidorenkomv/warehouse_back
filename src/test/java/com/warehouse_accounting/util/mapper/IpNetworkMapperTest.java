package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.IpNetwork;
import com.warehouse_accounting.models.dto.IpNetworkDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class IpNetworkMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void testMappingToDto() {
        IpNetwork ipNetwork = new IpNetwork(
                1L, "1.1.1.1", "2.2.2.2");

        IpNetworkDto ipNetworkDto = mapper.convert(ipNetwork);

        Assertions.assertEquals(ipNetworkDto.getNetwork(), ipNetwork.getNetwork());
        Assertions.assertEquals(ipNetworkDto.getId(), ipNetwork.getId());
        Assertions.assertEquals(ipNetworkDto.getMask(), ipNetwork.getMask());
    }

    @Test
    void testMappingToModel() {
        IpNetworkDto ipNetworkDto = new IpNetworkDto(
                1L, "1.1.1.1", "2.2.2.2");

        IpNetwork ipNetwork = mapper.convert(ipNetworkDto);

        Assertions.assertEquals(ipNetworkDto.getNetwork(), ipNetwork.getNetwork());
        Assertions.assertEquals(ipNetworkDto.getId(), ipNetwork.getId());
        Assertions.assertEquals(ipNetworkDto.getMask(), ipNetwork.getMask());
    }
}

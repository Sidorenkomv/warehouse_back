package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Department;
import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.Image;
import com.warehouse_accounting.models.IpAddress;
import com.warehouse_accounting.models.IpNetwork;
import com.warehouse_accounting.models.Position;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.Tariff;
import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.models.dto.IpNetworkDto;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.models.dto.RoleDto;
import com.warehouse_accounting.models.dto.TariffDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


@ExtendWith(SpringExtension.class)
class EmployeeMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);


    private static Employee employee;
    private static EmployeeDto employeeDto;

    @BeforeAll
    static void init() {
        Set<IpAddress> ipAddressSet = new HashSet<>();
        ipAddressSet.add(new IpAddress(1L, "11.11.11.11"));
        ipAddressSet.add(new IpAddress(2L, "22.22.22.22"));
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(new Role(1L, "TestRole1", "TestRole1"));
        roleSet.add(new Role(2L, "TestRole2", "TestRole2"));
        Set<Tariff> tariffSet = new HashSet<>();
        tariffSet.add(new Tariff(1L, "Tariff1", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now()));
        tariffSet.add(new Tariff(2L, "Tariff2", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now()));

        employee = Employee.builder()
                .id(1L)
                .firstName("Test")
                .middleName("Test")
                .lastName("Test")
                .department(new Department(1L, "TestDep", "TestDep"))
                .image(new Image(1L, "url", "sort"))
                .roles(roleSet)
                .ipAddress(ipAddressSet)
                .ipNetwork(new IpNetwork(1L, "1.1.1.1", "1.1.1.1"))
                .description("Some descr")
                .position(new Position(1L, "testPos", "testPos"))
                .inn("inn")
                .sortNumber("sort")
                .password("password")
                .phone("1111")
                .email("e-mail")
                .tariff(tariffSet)
                .build();

        Set<IpAddressDto> ipAddressDtoSet = new HashSet<>();
        ipAddressDtoSet.add(new IpAddressDto(1L, "11.11.11.11"));
        ipAddressDtoSet.add(new IpAddressDto(2L, "22.22.22.22"));
        Set<RoleDto> roleDtoSet = new HashSet<>();
        roleDtoSet.add(new RoleDto(1L, "TestRole1", "TestRole1"));
        roleDtoSet.add(new RoleDto(2L, "TestRole2", "TestRole2"));
        Set<TariffDto> tariffDtoSet = new HashSet<>();
        tariffDtoSet.add(new TariffDto(1L, "Tariff1", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now()));
        tariffDtoSet.add(new TariffDto(2L, "Tariff2", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now()));

        employeeDto = EmployeeDto.builder()
                .id(1L)
                .firstName("Test")
                .middleName("Test")
                .lastName("Test")
                .department(new DepartmentDto(1L, "TestDep", "TestDep"))
                .image(new ImageDto(1L, "url", "sort"))
                .roles(roleDtoSet)
                .ipAddress(ipAddressDtoSet)
                .ipNetwork(new IpNetworkDto(1L, "1.1.1.1", "1.1.1.1"))
                .description("Some descr")
                .position(new PositionDto(1L, "testPos", "testPos"))
                .inn("inn")
                .sortNumber("sort")
                .password("password")
                .phone("1111")
                .email("e-mail")
                .tariff(tariffDtoSet)
                .build();
    }

    @Test
    void convertToDtoTest() {
        EmployeeDto convertedEmployee = mapper.convert(employee);

        Assertions.assertEquals(employeeDto, convertedEmployee);
    }

    @Test
    void convertToModelTest() {
        Employee convertedDto = mapper.convert(employeeDto);

        Assertions.assertEquals(employee, convertedDto);
    }
}

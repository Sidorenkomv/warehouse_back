package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Position;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PositionMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        Position position = new Position(1L, "testPos", "testPos");
        PositionDto positionDto = mapper.convert(position);

        Assertions.assertEquals(position.getId(), positionDto.getId());
        Assertions.assertEquals(position.getName(), positionDto.getName());
        Assertions.assertEquals(position.getSortNumber(), positionDto.getSortNumber());
    }

    @Test
    void convertToModelTest() {
        PositionDto positionDto = new PositionDto(1L, "testPos", "testPos");
        Position position = mapper.convert(positionDto);

        Assertions.assertEquals(position.getId(), positionDto.getId());
        Assertions.assertEquals(position.getName(), positionDto.getName());
        Assertions.assertEquals(position.getSortNumber(), positionDto.getSortNumber());
    }
}

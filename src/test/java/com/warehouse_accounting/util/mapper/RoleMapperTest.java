package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.dto.RoleDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class RoleMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        Role role = new Role(1L, "testRole", "testRole");
        RoleDto roleDto = mapper.convert(role);

        Assertions.assertEquals(role.getId(), roleDto.getId());
        Assertions.assertEquals(role.getName(), roleDto.getName());
        Assertions.assertEquals(role.getSortNumber(), roleDto.getSortNumber());
    }

    @Test
    void convertToModelTest() {
        RoleDto roleDto = new RoleDto(1L, "testRole", "testRole");
        Role role = mapper.convert(roleDto);

        Assertions.assertEquals(role.getId(), roleDto.getId());
        Assertions.assertEquals(role.getName(), roleDto.getName());
        Assertions.assertEquals(role.getSortNumber(), roleDto.getSortNumber());
    }
}

package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Department;
import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class DepartmentMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        Department department = new Department(1L, "testDep", "testDep");
        DepartmentDto departmentDto = mapper.convert(department);

        Assertions.assertEquals(department.getId(), departmentDto.getId());
        Assertions.assertEquals(department.getName(), departmentDto.getName());
        Assertions.assertEquals(department.getSortNumber(), departmentDto.getSortNumber());
    }

    @Test
    void convertToModelTest() {
        DepartmentDto departmentDto = new DepartmentDto(1L, "testDep", "testDep");
        Department department = mapper.convert(departmentDto);

        Assertions.assertEquals(department.getId(), departmentDto.getId());
        Assertions.assertEquals(department.getName(), departmentDto.getName());
        Assertions.assertEquals(department.getSortNumber(), departmentDto.getSortNumber());
    }

}

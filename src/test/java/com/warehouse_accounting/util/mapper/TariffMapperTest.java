package com.warehouse_accounting.util.mapper;

import com.warehouse_accounting.models.Tariff;
import com.warehouse_accounting.models.dto.TariffDto;
import com.warehouse_accounting.util.MapStructMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

@ExtendWith(SpringExtension.class)
class TariffMapperTest {

    private final MapStructMapper mapper = Mappers.getMapper(MapStructMapper.class);

    @Test
    void convertToDtoTest() {
        Tariff tariff = new Tariff(1L, "Tariff1", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now());
        TariffDto tariffDto = mapper.convert(tariff);

        Assertions.assertEquals(tariff.getId(), tariffDto.getId());
        Assertions.assertEquals(tariff.getTariffName(), tariffDto.getTariffName());
        Assertions.assertEquals(tariff.getUsersCount(), tariffDto.getUsersCount());
        Assertions.assertEquals(tariff.getDataSpace(), tariffDto.getDataSpace());
        Assertions.assertEquals(tariff.getSalePointCount(), tariffDto.getSalePointCount());
        Assertions.assertEquals(tariff.getOnlineStoreCount(), tariffDto.getOnlineStoreCount());
        Assertions.assertEquals(tariff.getPaidApplicationOptionCount(), tariffDto.getPaidApplicationOptionCount());
        Assertions.assertEquals(tariff.getIsCRM(), tariffDto.getIsCRM());
        Assertions.assertEquals(tariff.getIsScripts(), tariffDto.getIsScripts());
        Assertions.assertEquals(tariff.getExtendedBonusProgram(), tariffDto.getExtendedBonusProgram());
        Assertions.assertEquals(tariff.getPaymentPeriod(), tariffDto.getPaymentPeriod());
        Assertions.assertEquals(tariff.getTotalPrice(), tariffDto.getTotalPrice());
        Assertions.assertEquals(tariff.getDateStartSubscription(), tariffDto.getDateStartSubscription());
        Assertions.assertEquals(tariff.getDateEndSubscription(), tariffDto.getDateEndSubscription());
    }

    @Test
    void convertToModelTest() {
        TariffDto tariffDto = new TariffDto(1L, "Tariff1", 1, 1,
                1, 1, 1,  true,
                true, true,1, 1, LocalDate.now(), LocalDate.now());
        Tariff tariff = mapper.convert(tariffDto);

        Assertions.assertEquals(tariff.getId(), tariffDto.getId());
        Assertions.assertEquals(tariff.getTariffName(), tariffDto.getTariffName());
        Assertions.assertEquals(tariff.getUsersCount(), tariffDto.getUsersCount());
        Assertions.assertEquals(tariff.getDataSpace(), tariffDto.getDataSpace());
        Assertions.assertEquals(tariff.getSalePointCount(), tariffDto.getSalePointCount());
        Assertions.assertEquals(tariff.getOnlineStoreCount(), tariffDto.getOnlineStoreCount());
        Assertions.assertEquals(tariff.getPaidApplicationOptionCount(), tariffDto.getPaidApplicationOptionCount());
        Assertions.assertEquals(tariff.getIsCRM(), tariffDto.getIsCRM());
        Assertions.assertEquals(tariff.getIsScripts(), tariffDto.getIsScripts());
        Assertions.assertEquals(tariff.getExtendedBonusProgram(), tariffDto.getExtendedBonusProgram());
        Assertions.assertEquals(tariff.getPaymentPeriod(), tariffDto.getPaymentPeriod());
        Assertions.assertEquals(tariff.getTotalPrice(), tariffDto.getTotalPrice());
        Assertions.assertEquals(tariff.getDateStartSubscription(), tariffDto.getDateStartSubscription());
        Assertions.assertEquals(tariff.getDateEndSubscription(), tariffDto.getDateEndSubscription());
    }
}

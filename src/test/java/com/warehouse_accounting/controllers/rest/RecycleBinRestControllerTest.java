package com.warehouse_accounting.controllers.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.models.*;
import com.warehouse_accounting.repositories.RecycleBinRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@FieldDefaults(level = PRIVATE, makeFinal = true)
//@WithMockUser(authorities = "ADMIN")
class RecycleBinRestControllerTest {

    MockMvc mockMvc;

    RecycleBinRepository recycleBinRepository;

    ObjectMapper objectMapper;

    @NonFinal
    RecycleBin recycleBin;
    @NonFinal
    Long id;

    @BeforeEach
    void setUp() {
        recycleBin = new RecycleBin(1L,
                "document",
                "number",
                LocalDate.now(),
                BigDecimal.valueOf(112) ,
                new Warehouse(),
                "склад1",
                "12",
                new Company(),
                "qw",
                new ContractorGroup(),
                "qw",
                "s" ,
                new Project(),
                "qwq",
                "sa",
                "w",
                "comment");
        id = recycleBinRepository
                .save(recycleBin)
                .getId();
    }

    @DisplayName("Getall works correctly")
    @Test
    @SneakyThrows
    void getAll() {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/recycle_bin"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(mvcResult -> Assertions.assertTrue(Objects
                        .requireNonNull(mvcResult.getResponse().getContentType())
                        .startsWith("application/json")));
    }

    @DisplayName("Getbyid works correctly")
    @Test
    @SneakyThrows
    void getById() {
        mockMvc.perform(get("/api/recycle_bin/" + id.toString()))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> Assertions.assertTrue(Objects
                        .requireNonNull(mvcResult.getResponse().getContentType())
                        .startsWith("application/json")));
    }

    @DisplayName("Update works correctly")
    @Test
    @SneakyThrows
    void update() {
        mockMvc.perform(put("/api/recycle_bin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(recycleBin)))
                .andExpect(status().isOk());
    }

    @DisplayName("Deletebyid works correctly")
    @Test
    @SneakyThrows
    void deleteById() {
        mockMvc.perform(delete("/api/recycle_bin/" + id))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/recycle_bin/" + id))
                .andExpect(status().isNotFound());
    }
}


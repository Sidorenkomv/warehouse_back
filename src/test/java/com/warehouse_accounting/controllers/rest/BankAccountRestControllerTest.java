package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.services.interfaces.BankAccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BankAccountRestControllerTest {

    private static BankAccountDto bankAccountDto1;
    private static BankAccountDto bankAccountDto2;
    private static List<BankAccountDto> listBankAccountDto;

    @Mock
    BankAccountService bankAccountService;

    @InjectMocks
    private BankAccountRestController bankAccountRestController;

    @BeforeAll
    static void initMethod() {
        bankAccountDto1 = BankAccountDto.builder()
                .id(1L)
                .rcbic("6516516516")
                .bank("ВТБ")
//                .address(AddressDto.builder().fullAddress("Москва, ул.Первая д.1").build())
                .correspondentAccount("61565161616")
                .account("888888888888888")
                .mainAccount(true)
                .sortNumber("11")
                .build();
        bankAccountDto2 = BankAccountDto.builder()
                .id(2L)
                .rcbic("7777777777777")
                .bank("СБЕР")
//                .address(AddressDto.builder().fullAddress("Москва, ул.Первая д.2").build())
                .correspondentAccount("55555555555555555")
                .account("1888")
                .mainAccount(true)
                .sortNumber("15")
                .build();
        listBankAccountDto = List.of(bankAccountDto1, bankAccountDto2);
    }


    @Test
    void getAll() {
        when(bankAccountService.getAll()).thenReturn(listBankAccountDto);
        ResponseEntity<List<BankAccountDto>> responseEntity = bankAccountRestController.getAll();
        assertNotNull(responseEntity.getBody(), "Result: NULL");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), listBankAccountDto);
        verify(bankAccountService, times(1)).getAll();
    }

    @Test
    void getById() {
        when(bankAccountService.getById(1L)).thenReturn(bankAccountDto1);
        ResponseEntity<BankAccountDto> responseEntity = bankAccountRestController.getById(1L);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), bankAccountDto1);
        verify(bankAccountService, times(1)).getById(ArgumentMatchers.eq(1L));
    }

    @Test
    void create() {
        assertEquals(bankAccountRestController.create(bankAccountDto1).getStatusCode(), HttpStatus.OK);
        verify(bankAccountService, times(1))
                .create(ArgumentMatchers.eq(bankAccountDto1));
    }

    @Test
    void update() {
        assertEquals(bankAccountRestController.update(bankAccountDto2).getStatusCode(), HttpStatus.OK);
        verify(bankAccountService, times(1))
                .update(ArgumentMatchers.eq(bankAccountDto2));
    }

    @Test
    void delete() {
        assertEquals(bankAccountRestController.delete(999L).getStatusCode(), HttpStatus.OK);
        verify(bankAccountService, times(1))
                .deleteById(ArgumentMatchers.eq(999L));
    }
}
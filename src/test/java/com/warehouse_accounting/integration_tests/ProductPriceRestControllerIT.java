package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ProductPriceRestController;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.models.dto.TypeOfPriceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ProductPriceRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductPriceRestController productPriceRestController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testExistence() {
        assertThat(productPriceRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/product_price"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/product_price/1"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {

        ProductPriceDto productPriceDto = ProductPriceDto.builder()
                .id(1L)
                .typeOfPriceDto(TypeOfPriceDto.builder().id(2L).build())
                .productDto(ProductDto.builder().id(5L).build())
                .price(BigDecimal.valueOf(150))
                .build();

        List<ProductPriceDto> productPriceDtos = List.of(productPriceDto);

        mockMvc.perform(post("/api/product_price")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productPriceDtos)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/product_price/2/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.productDto.id").value("4"))
                .andExpect(jsonPath("$.price").value("150.0"));

    }

    @Test
    void update() throws Exception {

        ProductPriceDto productPriceDto3 = ProductPriceDto.builder()
                .id(1L)
                .typeOfPriceDto(TypeOfPriceDto.builder().sortNumber("2").build())
                .productDto(ProductDto.builder().id(4L).build())
                .price(BigDecimal.valueOf(200))
                .build();

        mockMvc.perform(put("/api/product_price")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productPriceDto3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/product_price/1/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.productDto.id").value("5"))
                .andExpect(jsonPath("$.price").value("200.0"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/product_price/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/product_price/1"))
                .andExpect(status().isNotFound());
    }
}
package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PurchaseHistoryOfSalesRestController;
import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.services.interfaces.PurchaseHistoryOfSalesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
//@Sql(value = {"/init-purchases_history_of_sales.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/init-phos_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class PurchaseHistoryOfSalesRestControllerIT {
    @Autowired
    PurchaseHistoryOfSalesService purchaseHistoryOfSalesService;

    @Autowired
    MockMvc mockMvc;

//    private static final List<PurchaseHistoryOfSalesDto> purchaseHistoryOfSalesDtos = new ArrayList<>();

//    private static PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto1;

    @Autowired
    PurchaseHistoryOfSalesRestController purchaseHistoryOfSalesRestController;

    @Autowired
    ObjectMapper objectMapper;

//    private static final String url = "/api/purchases_history_of_sales";

    @Test
    void isExist() {
        assertThat(purchaseHistoryOfSalesRestController).isNotNull();
    }


    @BeforeEach

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/purchase_history"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/purchase_history/1"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.number").value("100"))
                .andExpect(jsonPath("$.productPriceId").value("1"))
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(500.0)))
                .andExpect(jsonPath("$.profit").value(BigDecimal.valueOf(500.0)))
                .andExpect(jsonPath("$.profitability").value(BigDecimal.valueOf(500.0)))
                .andExpect(jsonPath("$.saleOfDay").value(BigDecimal.valueOf(500.0)))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDtoCreate = PurchaseHistoryOfSalesDto.builder()
                .id(2L)
                .productPriceId(1L)
                .number(200L)
                .sum(BigDecimal.valueOf(200))
                .profit(BigDecimal.valueOf(200))
                .profitability(BigDecimal.valueOf(200))
                .saleOfDay(BigDecimal.valueOf(200))
                .build();
        PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDtoCreate3 = PurchaseHistoryOfSalesDto.builder()
                .id(3L)
                .productPriceId(1L)
                .number(200L)
                .sum(BigDecimal.valueOf(200))
                .profit(BigDecimal.valueOf(200))
                .profitability(BigDecimal.valueOf(200))
                .saleOfDay(BigDecimal.valueOf(200))
                .build();

        mockMvc.perform(post("/api/purchase_history")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseHistoryOfSalesDtoCreate)))
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/purchase_history")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseHistoryOfSalesDtoCreate3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_history/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.productPriceId").value("1"))
                .andExpect(jsonPath("$.number").value("200"))
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(200.0)))
                .andExpect(jsonPath("$.profit").value(BigDecimal.valueOf(200.0)))
                .andExpect(jsonPath("$.profitability").value(BigDecimal.valueOf(200.0)))
                .andExpect(jsonPath("$.saleOfDay").value(BigDecimal.valueOf(200.0)))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDtoUpdate = purchaseHistoryOfSalesService.getById(3L);
        purchaseHistoryOfSalesDtoUpdate.setNumber(400L);

        mockMvc.perform(get("/api/purchase_history/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/purchase_history/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseHistoryOfSalesDtoUpdate)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_history/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(purchaseHistoryOfSalesDtoUpdate)));
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/purchase_history/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_history/2"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PurchasesManagementRestController;
import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.services.interfaces.PurchasesManagementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class PurchasesManagementRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PurchasesManagementService purchasesManagementService;

    @Autowired
    private PurchasesManagementRestController purchasesManagementRestController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void isExist(){
        assertThat(purchasesManagementRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception{
        mockMvc.perform(get("/api/purchases_management"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception{
        mockMvc.perform(get("/api/purchases_management/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.productId").value("3"))
                .andExpect(jsonPath("$.purchasesHistoryOfSalesId").value("1"))
                .andExpect(jsonPath("$.purchasesCurrentBalanceId").value("1"))
                .andExpect(jsonPath("$.purchasesForecastId").value("1"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception{
        PurchasesManagementDto purchasesManagementDto2 = PurchasesManagementDto.builder()
                .id(2L)
                .productId(2L)
                .purchasesCurrentBalanceId(1L)
                .purchasesForecastId(1L)
                .purchasesHistoryOfSalesId(1L)
                .build();
        PurchasesManagementDto purchasesManagementDto3 = PurchasesManagementDto.builder()
                .id(3L)
                .productId(2L)
                .purchasesHistoryOfSalesId(1L)
                .purchasesForecastId(1L)
                .purchasesCurrentBalanceId(1L)
                .build();
        mockMvc.perform(post("/api/purchases_management")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(purchasesManagementDto2)))
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/purchases_management")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchasesManagementDto3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchases_management/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.productId").value("2"))
                .andExpect(jsonPath("$.purchasesHistoryOfSalesId").value("1"))
                .andExpect(jsonPath("$.purchasesCurrentBalanceId").value("1"))
                .andExpect(jsonPath("$.purchasesForecastId").value("1"))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception{
        PurchasesManagementDto update = purchasesManagementService.getById(2L);
        update.setProductId(3L);

        mockMvc.perform(get("/api/purchases_management/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/purchases_management")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchases_management/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(update)));
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/purchases_management/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchases_management/3"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

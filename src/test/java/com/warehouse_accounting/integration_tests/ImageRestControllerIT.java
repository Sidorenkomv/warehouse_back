package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ImageRestController;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.services.interfaces.ImageService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@Sql(value = "classpath:init-image.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class ImageRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ImageRestController imageRestController;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        assertThat(imageRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void testGetAll() {
        mockMvc.perform(get("/api/images"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void getById() {
        mockMvc.perform(get("/api/images/1"))
                .andExpect(status().isOk());
    }

    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void create() {
        ImageDto imageDto = ImageDto.builder()
                .id(2L)
                .imageUrl("imageUrl2")
                .sortNumber("2")
                .build();

        mockMvc.perform(post("/api/images")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(imageDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/images/2"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(imageDto)))
                .andExpect(jsonPath("$.imageUrl").value("imageUrl2"))
                .andExpect(jsonPath("$.sortNumber").value("2"));

    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void testUpdate() {

        ImageDto imageDto = imageService.getById(1L);
        imageDto.setImageUrl("ImageUrl1");
        imageDto.setSortNumber("1");

        mockMvc.perform(get("/api/images/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/images")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(imageDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/images/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(imageDto)));
    }

    @DisplayName("delete method test")
    @Test
    @SneakyThrows
    void testDeleteById() {
        mockMvc.perform(delete("/api/images/2"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/images/2"))
                .andExpect(status().isNotFound());
    }

}
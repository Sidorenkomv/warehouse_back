package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.WriteOffsRestController;
import com.warehouse_accounting.models.dto.WriteOffsDto;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class WriteOffsRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WriteOffsRestController writeOffsRestController;

    @Test
    @SneakyThrows
    void testExistence() {
        assertThat(writeOffsRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void testGetAll() {
        mockMvc.perform(get("/api/writeOffs"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void testGetById() {
        mockMvc.perform(get("/api/writeOffs/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void testCreate() {

        WriteOffsDto writeOffsDto = WriteOffsDto.builder()
                .id(2L)
                .sum(BigDecimal.valueOf(2000))
                .moved(true)
                .printed(true)
                .comment("Коммент2")
                .build();

        String jsonWriteOffs = new ObjectMapper().writeValueAsString(writeOffsDto);

        mockMvc.perform(post("/api/writeOffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonWriteOffs))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/writeOffs/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(2000.0)))
                .andExpect(jsonPath("$.moved").value(true))
                .andExpect(jsonPath("$.printed").value(true))
                .andExpect(jsonPath("$.comment").value("Коммент2"));

    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void testUpdate() {

        WriteOffsDto writeOffsDto = WriteOffsDto.builder()
                .id(2L)
                .sum(BigDecimal.valueOf(3000))
                .moved(false)
                .printed(false)
                .comment("Коммент3")
                .build();

        String jsonWriteOffs = new ObjectMapper().writeValueAsString(writeOffsDto);

        mockMvc.perform(post("/api/writeOffs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonWriteOffs))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/writeOffs/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(3000.0)))
                .andExpect(jsonPath("$.moved").value(false))
                .andExpect(jsonPath("$.printed").value(false))
                .andExpect(jsonPath("$.comment").value("Коммент3"));
    }

    @DisplayName("delete method test")
    @Test
    @SneakyThrows
    void testDelete() {
        mockMvc.perform(delete("/api/writeOffs/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/writeOffs/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}
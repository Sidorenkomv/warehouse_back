package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.warehouse_accounting.controllers.rest.ContractRestController;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.services.interfaces.ContractService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@Transactional
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class ContractRestControllerIT {
    static String apiUrl = "/api/contracts";
    static String contractNumber = "test_contract_0123456789";
    MockMvc mockMvc;
    ContractRestController contractRestController;
    ContractService contractService;
    ObjectMapper objectMapper;
    static ContractDto contractDto = ContractDto.builder()
            .id(666L)
            .number(contractNumber)
            .contractDate(LocalDate.of(2022, 1, 1))
            .amount(BigDecimal.ONE)
            .archive(true)
            .comment("new_comment")
            .build();

    @Test
    @DisplayName("testExistence(): Проверка наличия контроллера")
    @Order(1)
    void testExistence() {
        Assertions.assertNotNull(contractRestController);
    }

    @Test
    @DisplayName("getAll(): Получение списка договоров")
    @Order(3)
    void getAll() throws Exception {
        contractService.create(ContractDto.builder()
                .number(contractNumber)
                .contractDate(LocalDate.of(2022, 1, 1))
                .amount(BigDecimal.ONE)
                .archive(true)
                .comment("new_comment")
                .build());

        JsonPath.parse(mockMvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[-1].number").value(contractNumber)));
    }

    @Test
    @DisplayName("getById(): Получение договора по Id")
    @Order(4)
    void getById() throws Exception {
        contractService.create(ContractDto.builder()
                .number(contractNumber)
                .contractDate(LocalDate.of(2022, 1, 1))
                .amount(BigDecimal.ONE)
                .archive(true)
                .comment("new_comment")
                .build());

        final long id = JsonPath.parse(mockMvc.perform(get(apiUrl))
                        .andReturn()
                        .getResponse()
                        .getContentAsString())
                .read("$[-1].id", Long.class);

        mockMvc.perform(get(apiUrl + "/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.number").value(contractNumber));
    }

    @Test
    @DisplayName("create(): Создание договора")
    @Order(2)
    void create() throws Exception {
        mockMvc.perform(post(apiUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(contractDto)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("update(): Изменение договора")
    @Order(5)
    void update() throws Exception {
        final String newComment = "changed_comment";
        final ContractDto contract = ContractDto.builder()
                .number(contractNumber)
                .contractDate(LocalDate.of(2022, 1, 1))
                .amount(BigDecimal.ONE)
                .archive(true)
                .comment("new_comment")
                .build();

        contractService.create(contract);

        contract.setComment(newComment);

        final long id = JsonPath.parse(mockMvc.perform(get(apiUrl))
                        .andReturn()
                        .getResponse()
                        .getContentAsString())
                .read("$[-1].id", Long.class);

        contract.setId(id);
        contract.setComment(newComment);

        mockMvc.perform(MockMvcRequestBuilders.put(apiUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(contract)))
                .andExpect(status().isOk());

        mockMvc.perform(get(apiUrl + "/" + id))
                .andExpect(jsonPath("$.comment").value(newComment));
    }

    @Test
    @DisplayName("deleteById(): Удаление договора по Id")
    @Order(6)
    void deleteById() throws Exception {
        contractService.create(ContractDto.builder()
                .number(contractNumber)
                .contractDate(LocalDate.of(2022, 1, 1))
                .amount(BigDecimal.ONE)
                .archive(true)
                .comment("new_comment")
                .build());

        final long id = JsonPath.parse(mockMvc.perform(get(apiUrl))
                        .andReturn()
                        .getResponse()
                        .getContentAsString())
                .read("$[-1].id", Long.class);

        mockMvc.perform(delete(apiUrl + "/" + id))
                .andExpect(status().isOk());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ContractorRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.services.interfaces.ContractorGroupService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.TypeOfContractorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class ContractorRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContractorRestController contractorRestController;

    @Autowired
    private ContractorService contractorService;

    @Autowired
    TypeOfContractorService typeOfContractorService;

    @Autowired
    ContractorGroupService contractorGroupService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(contractorRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/contractors"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("contractor1"))
                .andExpect(jsonPath("$[2].name").value("contractor3"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("contractor2"))
                .andExpect(jsonPath("$[1].status").value("status2"))
                .andExpect(jsonPath("$[1].code").value("code2"))
                .andExpect(jsonPath("$[1].sortNumber").value("sortNumber2"))
                .andExpect(jsonPath("$[1].phone").value("phone2"))
                .andExpect(jsonPath("$[1].fax").value("fax2"))
                .andExpect(jsonPath("$[1].email").value("email2"))
                .andExpect(jsonPath("$[1].address.postCode").value("postCodeContractor2"))
                .andExpect(jsonPath("$[1].comment").value("comment2"))
                .andExpect(jsonPath("$[1].numberDiscountCard").value("numberDiscountCard2"));
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/contractors/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("contractor2"))
                .andExpect(jsonPath("$.status").value("status2"))
                .andExpect(jsonPath("$.code").value("code2"))
                .andExpect(jsonPath("$.outerCode").value("outerCode2"))
                .andExpect(jsonPath("$.sortNumber").value("sortNumber2"))
                .andExpect(jsonPath("$.phone").value("phone2"))
                .andExpect(jsonPath("$.fax").value("fax2"))
                .andExpect(jsonPath("$.email").value("email2"))
                .andExpect(jsonPath("$.address.postCode").value("postCodeContractor2"))
                .andExpect(jsonPath("$.comment").value("comment2"))
                .andExpect(jsonPath("$.numberDiscountCard").value("numberDiscountCard2"))
                .andExpect(jsonPath("$.contractorGroupId").value("1"))
                .andExpect(jsonPath("$.contractorGroupName").value("name1"))
                .andExpect(jsonPath("$.typeOfPriceId").value("1"))
                .andExpect(jsonPath("$.typeOfPriceName").value("typeOfPriceName1"))
        ;
    }

    @Test
    void create() throws Exception {
        ContractorDto contractorDto = ContractorDto.builder()
                .id(4L)
                .name("contractor4")
                .status("status4")
                .code("code4")
                .outerCode("outerCode4")
                .sortNumber("sortNumber4")
                .phone("phone4")
                .fax("fax4")
                .email("email4")
                .address(AddressDto.builder()
                        .postCode("createAddress")
                        .build())
                .comment("comment4")
                .numberDiscountCard("numberDiscountCard4")
//                .contractorGroupId(1L)
//                .typeOfPriceId(1L)
//                .typeOfPriceName("typeOfPriceName4")
//                .bankAccountDtos(List.of(
//                        BankAccountDto.builder()
//                                .id(1L)
//                                .rcbic("new")
//                                .bank("createBank")
//                                .build()
//                ))
//                .legalDetailDto(LegalDetailDto.builder()
//                        .id(1L)
//                        .fullName("createLegalDetail")
//                        .typeOfContractorId(1L)
//                        .build())
                .taskDtos(null)
                .build();

        mockMvc.perform(post("/api/contractors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(contractorDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/contractors/4"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.name").value("contractor4"))
                .andExpect(jsonPath("$.email").value("email4"))
                .andExpect(jsonPath("$.typeOfPriceName").value("typeOfPriceName4"))
                .andExpect(jsonPath("$.address.postCode").value("createAddress"))
                .andExpect(jsonPath("$.bankAccountDtos[0].bank").value("createBank"))
                .andExpect(jsonPath("$.legalDetailDto.fullName").value("createLegalDetail"))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        ContractorDto contractorDto = contractorService.getById(2L);

        Long oldAddressId = contractorDto.getAddress().getId();

        contractorDto.setName("updateContractor");
        contractorDto.setAddress(AddressDto.builder()
                .postCode("updateAddress")
                .build());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/contractors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(contractorDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/contractors/2"))
                .andExpect(jsonPath("$.name").value("updateContractor"))
                .andExpect(jsonPath("$.email").value("email2"))
                .andExpect(jsonPath("$.address.postCode").value("updateAddress"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById() throws Exception {
        ContractorDto contractorDto = contractorService.getById(3L);

        Long oldAddressId = contractorDto.getAddress().getId();

        mockMvc.perform(delete("/api/contractors/3"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/contractors/3"))
                .andExpect(status().isNotFound());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

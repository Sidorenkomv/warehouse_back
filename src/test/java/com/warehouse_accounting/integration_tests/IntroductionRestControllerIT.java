package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.IntroductionRestController;
import com.warehouse_accounting.models.dto.IntroductionDto;
import com.warehouse_accounting.services.interfaces.IntroductionService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class IntroductionRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IntroductionRestController introductionRestController;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private IntroductionService introductionService;

    @Test
    @SneakyThrows
    void testExistence(){
        assertThat(introductionRestController).isNotNull();
    }

    @Test
    @DisplayName("getAll method test")
    @SneakyThrows
    void integrationTestGetAll() {
        mockMvc.perform(get("/api/introduction"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("getById method test")
    @SneakyThrows
    void integrationTestGetById() {
        mockMvc.perform(get("/api/introduction/6"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("6"));
    }

    @Test
    @DisplayName("create method test")
    @SneakyThrows
    void integrationTestCreate() {
        IntroductionDto introductionDto = IntroductionDto.builder()
                .id(2L)
                .fromWhomId(1L)
                .pointId(1L)
                .build();

        String jsonIntroduction = new ObjectMapper().writeValueAsString(introductionDto);
        mockMvc.perform(post("/api/introduction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonIntroduction))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("update method test")
    @SneakyThrows
    void integrationTestUpdate() {
        IntroductionDto introductionDto = introductionService.getById(6L);

        introductionDto.setComment("comment");
        introductionDto.setEditorEmployeeId(1L);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/introduction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(introductionDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/introduction/6"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(introductionDto)));
    }

    @Test
    @DisplayName("delete method test")
    @SneakyThrows
    void integrationTestDelete() {
        mockMvc.perform(delete("/api/introduction/6"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/introduction/6"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

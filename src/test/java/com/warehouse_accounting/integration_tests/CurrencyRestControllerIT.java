package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.CurrencyRestController;
import com.warehouse_accounting.models.dto.CurrencyDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class CurrencyRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CurrencyRestController currencyRestController;

    @Test
    void shouldPassIfCurrencyRestControllerExists() {
        assertThat(currencyRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/currencies"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void shouldRetrieveCurrencyById() throws Exception {
        mockMvc.perform(get("/api/currencies/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.numcode").value("666"))
                .andExpect(jsonPath("$.charcode").value("PRO"))
                .andExpect(jsonPath("$.nominal").value(1))
                .andExpect(jsonPath("$.name").value("TestName"))
                .andExpect(jsonPath("$.value").value(20.0));
    }

    @Test
    void shouldCreateNewCurrency() throws Exception {

        CurrencyDto currencyDto = CurrencyDto.builder()
                .id(2L)
                .numcode("777")
                .charcode("TES")
                .nominal(1)
                .name("Supertest")
                .value(30.0)
                .build();

        String jsonCurrency = new ObjectMapper().writeValueAsString(currencyDto);

        mockMvc.perform(post("/api/currencies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCurrency))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/currencies/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numcode").value("777"))
                .andExpect(jsonPath("$.charcode").value("TES"))
                .andExpect(jsonPath("$.nominal").value(1))
                .andExpect(jsonPath("$.name").value("Supertest"))
                .andExpect(jsonPath("$.value").value(30.0));
    }

    @Test
    void testUpdate() throws Exception {

        CurrencyDto currencyDto = CurrencyDto.builder()
                .id(2L)
                .numcode("999")
                .charcode("RRR")
                .nominal(1)
                .name("TestUpd")
                .value(10.0)
                .build();
        String jsonCurrency = new ObjectMapper().writeValueAsString(currencyDto);

        mockMvc.perform(put("/api/currencies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCurrency))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/currencies/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numcode").value("999"))
                .andExpect(jsonPath("$.charcode").value("RRR"))
                .andExpect(jsonPath("$.nominal").value(1))
                .andExpect(jsonPath("$.name").value("TestUpd"))
                .andExpect(jsonPath("$.value").value(10.0));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/currencies/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/currencies/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

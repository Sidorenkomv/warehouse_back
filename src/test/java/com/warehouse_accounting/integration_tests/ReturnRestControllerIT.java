package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ReturnRestController;
import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.ReturnDto;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.LegalDetailService;
import com.warehouse_accounting.services.interfaces.ReturnService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-returns-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ReturnRestControllerIT {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    ReturnRestController returnRestController;
    @Autowired
    ReturnService returnService;
    @Autowired
    ContractorService contractorService;
    @Autowired
    LegalDetailService legalDetailService;
    @Autowired
    ContractService contractService;

    @Test
    void testReturnExistence(){
        assertThat(returnRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/returns"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/returns/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.comment").value("comment1"))
                .andExpect(jsonPath("$.dataTime").value("2000-01-01T00:00:00"));
    }

    @Test
    void create() throws Exception {
        ReturnDto returnDtoTest = returnService.getById(1L);
        ContractorDto contractorDto = contractorService.getById(1L);
        ContractDto contractDto = contractService.getById(1L);
        contractDto.setContractor(contractorDto);
        returnDtoTest.setContractDto(contractDto);
        returnDtoTest.setContractorDto(contractorDto);
        returnDtoTest.setId(3L);
        returnDtoTest.setComment("CREATE TEST");
        returnDtoTest.setDataTime(LocalDateTime.of(2000, 3, 3, 0, 0));

        mockMvc.perform(post("/api/returns")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDtoTest)))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/returns/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.comment").value("CREATE TEST"))
                .andExpect(jsonPath("$.dataTime").value("2000-03-03T00:00:00"));
    }

    @Test
    void update() throws Exception {
        ReturnDto returnDtoTest = returnService.getById(1L);
        ContractorDto contractorDto = contractorService.getById(1L);
        ContractDto contractDto = contractService.getById(1L);
        contractDto.setContractor(contractorDto);
        returnDtoTest.setContractDto(contractDto);
        returnDtoTest.setContractorDto(contractorDto);
        returnDtoTest.setId(2L);
        returnDtoTest.setComment("UPDATE TEST");

        mockMvc.perform(put("/api/returns")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(returnDtoTest)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/returns/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.comment").value("UPDATE TEST"));
    }

    @Test
    void deleteTest() throws Exception {
        mockMvc.perform(delete("/api/returns/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/returns/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.CountryRestController;
import com.warehouse_accounting.models.dto.CountryDto;
import com.warehouse_accounting.services.interfaces.CountryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class CountryRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CountryRestController countryRestController;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(countryRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/countries"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].shortName").value("Германия"))
                .andExpect(jsonPath("$[2].shortName").value("Россия"))
                .andExpect(jsonPath("$.size()").value(4));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/countries/4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.shortName").value("Россия"))
                .andExpect(jsonPath("$.longName").value("Российская Федерация"))
                .andExpect(jsonPath("$.code").value(643))
                .andExpect(jsonPath("$.codeOne").value("RU"))
                .andExpect(jsonPath("$.codeTwo").value("RUS"));
    }

    @Test
    void create() throws Exception {
        CountryDto countryDto = CountryDto.builder()
                .shortName("Марокко")
                .longName("Королевство Марокко")
                .code((short)504)
                .codeOne("MA")
                .codeTwo("MAR")
                .build();

        this.mockMvc.perform(post("/api/countries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(countryDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/countries/5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("5"))
                .andExpect(jsonPath("$.shortName").value("Марокко"))
                .andExpect(jsonPath("$.longName").value("Королевство Марокко"))
                .andExpect(jsonPath("$.code").value(504))
                .andExpect(jsonPath("$.codeOne").value("MA"))
                .andExpect(jsonPath("$.codeTwo").value("MAR"));
    }

    @Test
    void update() throws Exception {
        CountryDto countryDto = countryService.getById(3L);
        countryDto.setCodeOne("ER");
        countryDto.setCodeTwo("DF");

        this.mockMvc.perform(put("/api/countries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(countryDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/countries/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.shortName").value("США"))
                .andExpect(jsonPath("$.longName").value("Соединённые Штаты Америки"))
                .andExpect(jsonPath("$.codeOne").value("ER"))
                .andExpect(jsonPath("$.codeTwo").value("DF"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/countries/4"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/countries/4"))
                .andExpect(status().isNotFound());
    }
}
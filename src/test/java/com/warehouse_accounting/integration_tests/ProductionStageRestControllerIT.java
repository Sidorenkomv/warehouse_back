package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ProductionStageRestController;
import com.warehouse_accounting.models.dto.ProductionStageDto;
import com.warehouse_accounting.services.interfaces.ProductionStageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class ProductionStageRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductionStageRestController controller;

    @Autowired
    private ProductionStageService productionStageService;

    @Test
    void testExistence(){
        assertThat(controller).isNotNull();
    }
    @Test
    void getAll() throws Exception{
        this.mockMvc.perform(get("/api/production_stage"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {

        mockMvc.perform(get("/api/production_stage/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }
    @Test
    void create() throws Exception{
        ProductionStageDto productionStageDto = ProductionStageDto.builder()
                .id(2L)
                .name("CREATE TEST")
                .description("CREATE TEST")
                .generalAccess(true)
                .ownerEmployeeId(1L)
                .ownerDepartmentId(1L)
                .editorEmployeeId(1L)
                .build();

        mockMvc.perform(post("/api/production_stage")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(productionStageDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/production_stage/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("CREATE TEST"))
                .andExpect(jsonPath("$.description").value("CREATE TEST"));
    }

    @Test
    void update() throws Exception {
        ProductionStageDto productionStageDto = ProductionStageDto.builder()
                .id(1L)
                .name("UPDATE TEST")
                .description("UPDATE TEST")
                .generalAccess(false)
                .ownerEmployeeId(1L)
                .ownerDepartmentId(1L)
                .editorEmployeeId(1L)
                .build();

        mockMvc.perform(put("/api/production_stage")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(productionStageDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/production_stage/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("UPDATE TEST"))
                .andExpect(jsonPath("$.description").value("UPDATE TEST"));
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/production_stage/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/production_stage/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

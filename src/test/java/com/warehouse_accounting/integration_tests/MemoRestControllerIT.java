package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.models.dto.MemoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = {"classpath:init-memo.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class MemoRestControllerIT {

    private static MemoDto memoDto;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAll() throws Exception{
        mockMvc.perform(get("/api/memos"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception{
        mockMvc.perform(get("/api/memos/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.content").value("test content"))
                .andExpect(jsonPath("$.createDate").value("1999-01-08T00:00:00"))
                .andExpect(jsonPath("$.contractorId").value("1"))
                .andExpect(jsonPath("$.employeeWhoCreatedId").value("1"))
                .andExpect(jsonPath("$.employeeWhoEditedId").value("1"));
    }

    @Test
    void create() throws Exception{
        MemoDto memoDto2 = MemoDto.builder()
                .id(2L)
                .createDate(LocalDateTime.of(2022, 4, 22, 2, 57))
                .content("CREATE TEST")
                .contractorId(1L)
                .employeeWhoCreatedName("firstName")
                .employeeWhoCreatedId(1L)
                .employeeWhoEditedName("firstName")
                .employeeWhoEditedId(1L)
                .build();

        mockMvc.perform(post("/api/memos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(memoDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/memos/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.content").value("CREATE TEST"))
                .andExpect(jsonPath("$.contractorId").value("1"))
                .andExpect(jsonPath("$.employeeWhoCreatedId").value("1"))
                .andExpect(jsonPath("$.employeeWhoEditedId").value("1"));
    }

    @Test
    void update() throws Exception{
        memoDto = MemoDto.builder()
                .id(1L)
                .createDate(LocalDateTime.of(2021, 4, 22, 2, 57))
                .content("UPDATE TEST")
                .contractorId(1L)
                .employeeWhoCreatedName("firstName")
                .employeeWhoCreatedId(1L)
                .employeeWhoEditedName("firstName")
                .employeeWhoEditedId(1L)
                .build();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/memos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(memoDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/memos/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.content").value("UPDATE TEST"))
                .andExpect(jsonPath("$.createDate").value("2021-04-22T02:57:00"))
                .andExpect(jsonPath("$.contractorId").value("1"))
                .andExpect(jsonPath("$.employeeWhoCreatedId").value("1"))
                .andExpect(jsonPath("$.employeeWhoEditedId").value("1"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/memos/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/memos/1"))
                .andExpect(status().isNotFound());
    }

}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.RegionRestController;
import com.warehouse_accounting.models.dto.RegionDto;
import com.warehouse_accounting.services.interfaces.RegionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class RegionRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RegionRestController regionRestController;

    @Autowired
    private RegionService regionService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(regionRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/regions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Адыгея"))
                .andExpect(jsonPath("$[5].name").value("Астраханская"))
                .andExpect(jsonPath("$.size()").value(86));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/regions/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("Башкортостан"))
                .andExpect(jsonPath("$.socr").value("Респ"))
                .andExpect(jsonPath("$.code").value("0200000000000"))
                .andExpect(jsonPath("$.ocatd").value("80000000000"));

        this.mockMvc.perform(get("/api/regions/99999999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getByCode() throws Exception {
        this.mockMvc.perform(get("/api/regions/code/4210000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Кемеровская область - Кузбасс"));

        this.mockMvc.perform(get("/api/regions/code/4210000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Кемеровская область - Кузбасс"));

        this.mockMvc.perform(get("/api/regions/code/0099999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception {
        RegionDto regionDto = RegionDto.builder()
                .id(1L)
                .name("Новый регион")
                .socr("д")
                .code("24")
                .ocatd("52")
                .build();

        this.mockMvc.perform(post("/api/regions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(regionDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/regions/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Новый регион"))
                .andExpect(jsonPath("$.socr").value("д"))
                .andExpect(jsonPath("$.code").value("24"))
                .andExpect(jsonPath("$.ocatd").value("52"));
    }

    @Test
    void update() throws Exception {
        RegionDto regionDto = regionService.getById(2L);
        regionDto.setName("Новый регион");
        regionDto.setCode("новый код");

        this.mockMvc.perform(put("/api/regions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(regionDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/regions/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("Новый регион"))
                .andExpect(jsonPath("$.code").value("новый код"))
                .andExpect(jsonPath("$.ocatd").value("80000000000"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/regions/4"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/regions/4"))
                .andExpect(status().isNotFound());
    }
}
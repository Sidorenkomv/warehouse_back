package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.CommissionReportsRestController;
import com.warehouse_accounting.models.dto.CommissionReportsDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class CommissionReportsRestControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CommissionReportsRestController commissionRestController;

    @Test
    void testExistence() {
        Assertions.assertThat(commissionRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/commission_reports"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/commission_reports/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    void testCreate() throws Exception {

        CommissionReportsDto commissionReportsDto = CommissionReportsDto.builder()
                .id(2L)
                .sum(BigDecimal.valueOf(2))
                .isSent(true)
                .build();

        String jsonCommision = new ObjectMapper().writeValueAsString(commissionReportsDto);

        mockMvc.perform(post("/api/commission_reports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCommision))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/commission_reports/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(2.0)))
                .andExpect(jsonPath("$.isSent").value(true));
    }

    @Test
    void testUpdate() throws Exception {

        CommissionReportsDto commissionReportsDto = CommissionReportsDto.builder()
                .id(2L)
                .sum(BigDecimal.valueOf(100))
                .isSent(false)
                .build();

        String jsonCommision = new ObjectMapper().writeValueAsString(commissionReportsDto);

        mockMvc.perform(post("/api/commission_reports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCommision))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/commission_reports/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(100.0)))
                .andExpect(jsonPath("$.isSent").value(false));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/commission_reports/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/commission_reports/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.LanguageRestController;
import com.warehouse_accounting.models.dto.LanguageDto;
import com.warehouse_accounting.services.interfaces.LanguageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;



@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-language-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class LanguageRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private LanguageRestController languageRestController;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void testExistence() {
        assertThat(languageRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/language"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/language/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.language").value("Test"));
    }

/*    @Test
    void testCreate() throws Exception {
        LanguageDto languageDto = LanguageDto.builder()
                .id(2L)
                .language("Italian")
                .build();

        String jsonLanguage = new ObjectMapper().writeValueAsString(languageDto);

        mockMvc.perform(post("/api/language")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonLanguage))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/language/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.language").value("Italian"));

    }*/

    @Test
    void testUpdate() throws Exception {
        LanguageDto languageDto = languageService.getById(1L);
        languageDto.setLanguage("Русский");

        mockMvc.perform(get("/api/language/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/language")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(languageDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/language/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(languageDto)));
    }


  /*  @Test
    void testDelete() throws Exception {

        mockMvc.perform(get("/api/language/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(delete("/api/language/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/language/3"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }*/
}

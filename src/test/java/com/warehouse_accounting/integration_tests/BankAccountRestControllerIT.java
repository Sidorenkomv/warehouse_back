package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.BankAccountRestController;
import com.warehouse_accounting.models.dto.BankAccountDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class BankAccountRestControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BankAccountRestController bankAccountRestController;

    @Test
    void testExistence() {
        assertThat(bankAccountRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/bank_accounts"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {

        mockMvc.perform(get("/api/bank_accounts/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.address.fullAddress").value("Russia"));
    }

    @Test
    void testCreate() throws Exception {
        BankAccountDto bankAccountDto = BankAccountDto.builder()
                .id(2L)
                .bank("Bank")
//                .address(AddressDto.builder().fullAddress("Address").build())
                .build();

        String jsonBankAccount = new ObjectMapper().writeValueAsString(bankAccountDto);

        mockMvc.perform(post("/api/bank_accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonBankAccount))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/bank_accounts/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bank").value("Bank"))
                .andExpect(jsonPath("$.address.fullAddress").value("Address"));
    }

    @Test
    void update() throws Exception {
        BankAccountDto bankAccountDto = BankAccountDto.builder()
                .id(1L)
                .bank("BankVTB")
//                .address(AddressDto.builder().fullAddress("Russia").build())
                .build();

        String jsonBankAccount = new ObjectMapper().writeValueAsString(bankAccountDto);

        mockMvc.perform(put("/api/bank_accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonBankAccount))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/bank_accounts/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.bank").value("BankVTB"))
                .andExpect(jsonPath("$.address.fullAddress").value("Russia"));

    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/bank_accounts/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/bank_accounts/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
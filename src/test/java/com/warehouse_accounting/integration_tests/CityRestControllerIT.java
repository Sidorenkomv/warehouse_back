package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.CityRestController;
import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.models.dto.RegionDto;
import com.warehouse_accounting.services.interfaces.CityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class CityRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CityRestController cityRestController;

    @Autowired
    private CityService cityService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(cityRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/cities"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Адыгейск"))
                .andExpect(jsonPath("$[3].name").value("Кошехабльский р-н, Блечепсин"));

        this.mockMvc.perform(get("/api/cities?regionCode=0100000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Адыгейск"))
                .andExpect(jsonPath("$[1].name").value("Адыгейск г, Гатлукай"))
                .andExpect(jsonPath("$[2].name").value("Адыгейск г, Псекупс"));
    }

    @Test
    void getSlice() throws Exception {
        this.mockMvc.perform(get("/api/cities/slice?offset=2&limit=10&name=Майкоп"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Майкопский р-н, Даховская"))
                .andExpect(jsonPath("$.size()").value(10));

        this.mockMvc.perform(get("/api/cities/slice?offset=2&limit=10&name=Майкоп&regionCode="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Майкопский р-н, Даховская"))
                .andExpect(jsonPath("$.size()").value(10));

        this.mockMvc.perform(get("/api/cities/slice?offset=2&limit=10&name=Неизвестный"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        this.mockMvc.perform(get("/api/cities/slice?offset=2&limit=10&name=Майкоп&regionCode=0200000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));
    }
    @Test
    void getCount() throws Exception {
        this.mockMvc.perform(get("/api/cities/count?name="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("100"));

        this.mockMvc.perform(get("/api/cities/count?name=Майкоп"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("46"));

        this.mockMvc.perform(get("/api/cities/count?name=Майкоп&regionCode=0100000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("46"));

        this.mockMvc.perform(get("/api/cities/count?name=Майкоп&regionCode=0200000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("0"));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/cities/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.name").value("Майкоп г, Гавердовский"))
                .andExpect(jsonPath("$.socr").value("х"))
                .andExpect(jsonPath("$.code").value("0100000100300"));

        this.mockMvc.perform(get("/api/cities/9999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getByCode() throws Exception {
        this.mockMvc.perform(get("/api/cities/code/0100000100300"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.name").value("Майкоп г, Гавердовский"))
                .andExpect(jsonPath("$.socr").value("х"))
                .andExpect(jsonPath("$.code").value("0100000100300"));

        this.mockMvc.perform(get("/api/cities/code/009999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception {
        RegionDto regionDto = RegionDto.builder()
                .id(1L)
                .name("Новый город")
                .socr("с")
                .code("49")
                .ocatd("38")
                .build();

        this.mockMvc.perform(post("/api/cities")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(regionDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/cities/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Новый город"))
                .andExpect(jsonPath("$.socr").value("с"))
                .andExpect(jsonPath("$.code").value("49"))
                .andExpect(jsonPath("$.ocatd").value("38"));
    }

    @Test
    void update() throws Exception {
        CityDto cityDto = cityService.getById(4L);
        cityDto.setName("Обновлённый город");
        cityDto.setCode("Обновлённый код");

        this.mockMvc.perform(put("/api/cities")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cityDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/cities/4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.name").value("Обновлённый город"))
                .andExpect(jsonPath("$.code").value("Обновлённый код"))
                .andExpect(jsonPath("$.ocatd").value("79401000004"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/cities/3"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/cities/3"))
                .andExpect(status().isNotFound());
    }
}
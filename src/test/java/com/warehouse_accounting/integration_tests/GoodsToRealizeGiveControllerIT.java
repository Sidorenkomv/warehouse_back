package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.GoodsToRealizeGiveController;
import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.UnitDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGiveService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.UnitService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class GoodsToRealizeGiveControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GoodsToRealizeGiveController goodsToRealizeGiveController;

    @Autowired
    private GoodsToRealizeGiveService goodsToRealizeGiveService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private UnitService unitService;

    private GoodsToRealizeGiveDto goodsToRealizeGiveDto;

    @Test
    void testExist() {
        assertThat(goodsToRealizeGiveController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {

        mockMvc.perform(get("/api/goodstorealizegive"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {

        mockMvc.perform(get("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.productDtoId").value("2"))
                .andExpect(jsonPath("$.unitId").value("3"));
    }

    @Test
    void testCreate() throws Exception {
        ProductDto productDto = productService.getById(1L);
        UnitDto unitDto = unitService.getById(3L);

        GoodsToRealizeGiveDto goodsToRealizeGiveDto = GoodsToRealizeGiveDto.builder()
                .id(2L)
                .productDtoId(productDto.getId())
                .productDtoName(productDto.getName())
                .unitId(unitDto.getId())
                .unitName(unitDto.getShortName())
                .giveGoods(1)
                .quantity(2)
                .amount(3)
                .arrive(4)
                .remains(5)
                .build();

        String jsonGoodsToRealizeGive = new ObjectMapper().writeValueAsString(goodsToRealizeGiveDto);
        System.out.println(jsonGoodsToRealizeGive);
        mockMvc.perform(post("/api/goodstorealizegive")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonGoodsToRealizeGive))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizegive/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.unitId").value("3"))
                .andExpect(jsonPath("$.arrive").value("4"));
    }

    @Test
    void testUpdate() throws Exception {
        GoodsToRealizeGiveDto goodsToRealizeGiveDto = goodsToRealizeGiveService.getById(1L);

        goodsToRealizeGiveDto.setGiveGoods(999);
        goodsToRealizeGiveDto.setArrive(999);

        mockMvc.perform(get("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.giveGoods").value("5"))
                .andExpect(jsonPath("$.arrive").value("11"));

        mockMvc.perform(MockMvcRequestBuilders.put("/api/goodstorealizegive")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(goodsToRealizeGiveDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(goodsToRealizeGiveDto)));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(get("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(delete("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizegive/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

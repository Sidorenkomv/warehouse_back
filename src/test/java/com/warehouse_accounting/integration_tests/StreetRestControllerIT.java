package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.StreetRestController;
import com.warehouse_accounting.models.dto.StreetDto;
import com.warehouse_accounting.services.interfaces.StreetService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class StreetRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private StreetRestController streetRestController;

    @Autowired
    private StreetService streetService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(streetRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/streets"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Абадзехская"))
                .andExpect(jsonPath("$[16].name").value("Береговая"))
                .andExpect(jsonPath("$.size()").value(99));

        this.mockMvc.perform(get("/api/streets?regionCityCode=01000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Абадзехская"))
                .andExpect(jsonPath("$[1].name").value("Абрикосовая"))
                .andExpect(jsonPath("$.size()").value(99));
    }

    @Test
    void getSlice() throws Exception {
        this.mockMvc.perform(get("/api/streets/slice?offset=5&limit=16&name="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Комсомольская"))
                .andExpect(jsonPath("$.size()").value(16));

        this.mockMvc.perform(get("/api/streets/slice?offset=0&limit=10&name=Коммунаров"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Коммунаров"))
                .andExpect(jsonPath("$.size()").value(1));

        this.mockMvc.perform(get("/api/streets/slice?offset=0&limit=10&name=Коммунаров&regionCityCode=01000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Коммунаров"))
                .andExpect(jsonPath("$.size()").value(1));

        this.mockMvc.perform(get("/api/streets/slice?offset=5&limit=23&name=Неизвестный"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        this.mockMvc.perform(get("/api/streets/slice?offset=0&limit=10&name=Коммунаров&regionCityCode=02000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));
    }

    @Test
    void getCount() throws Exception {
        this.mockMvc.perform(get("/api/streets/count?name="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(99));
        this.mockMvc.perform(get("/api/streets/count?name=&regionCityCode=02000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(0));

        this.mockMvc.perform(get("/api/streets/count?name=Костикова"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(1));

        this.mockMvc.perform(get("/api/streets/count?name=Костикова&regionCityCode=01000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(1));

        this.mockMvc.perform(get("/api/streets/count?name=Костикова&regionCityCode=02000001000000000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(0));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/streets/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("Абрикосовая"))
                .andExpect(jsonPath("$.socr").value("ул"))
                .andExpect(jsonPath("$.code").value("01000001000000200"))
                .andExpect(jsonPath("$.index").value("385013"))
                .andExpect(jsonPath("$.gninmb").value("0100"))
                .andExpect(jsonPath("$.ocatd").value("79401000000"));

        this.mockMvc.perform(get("/api/streets/9999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception {
        StreetDto streetDto = StreetDto.builder()
                .id(1L)
                .name("Новая площадь")
                .socr("пл")
                .code("33")
                .index("45")
                .gninmb("432")
                .ocatd("99")
                .build();

        this.mockMvc.perform(post("/api/streets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(streetDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/streets/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Новая площадь"))
                .andExpect(jsonPath("$.socr").value("пл"))
                .andExpect(jsonPath("$.code").value("33"))
                .andExpect(jsonPath("$.index").value("45"))
                .andExpect(jsonPath("$.gninmb").value("432"))
                .andExpect(jsonPath("$.ocatd").value("99"));
    }

    @Test
    void update() throws Exception {
        StreetDto streetDto = streetService.getById(3L);
        streetDto.setName("Обновлённая улица");
        streetDto.setCode("Обновлённый код");

        this.mockMvc.perform(put("/api/streets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(streetDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/streets/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.name").value("Обновлённая улица"))
                .andExpect(jsonPath("$.code").value("Обновлённый код"))
                .andExpect(jsonPath("$.ocatd").value("79401000000"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/streets/5"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/streets/5"))
                .andExpect(status().isNotFound());
    }
}
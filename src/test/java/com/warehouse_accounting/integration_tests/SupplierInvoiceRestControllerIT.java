package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.SupplierInvoiceRestController;
import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class SupplierInvoiceRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SupplierInvoiceRestController supplierInvoiceRestController;

    @Test
    void testExistence() {
        assertThat(supplierInvoiceRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/supplier_invoices"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/supplier_invoices/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.organization").value("Robin Good Production"))
                .andExpect(jsonPath("$.invoiceNumber").value("1"));
    }

    @Test
    void testCreate() throws Exception {
        SupplierInvoiceDto requiredSupplierInvoice = SupplierInvoiceDto.builder()
                .organization("Robin Good Production")
                .invoiceNumber("1").build();
        String jsonSupplierInvoice = new ObjectMapper().writeValueAsString(requiredSupplierInvoice);

        mockMvc.perform(post("/api/supplier_invoices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSupplierInvoice))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_invoices/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.organization").value("Robin Good Production"))
                .andExpect(jsonPath("$.invoiceNumber").value("1"));
    }

    @Test
    void testUpdate() throws Exception {
        SupplierInvoiceDto requiredSupplierInvoice = SupplierInvoiceDto.builder()
                .id(2L)
                .organization("Robin Good Production New")
                .invoiceNumber("1").build();
        String jsonSupplierInvoice = new ObjectMapper().writeValueAsString(requiredSupplierInvoice);

        mockMvc.perform(put("/api/supplier_invoices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonSupplierInvoice))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_invoices/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.organization").value("Robin Good Production New"))
                .andExpect(jsonPath("$.invoiceNumber").value("1"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/supplier_invoices/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_invoices/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

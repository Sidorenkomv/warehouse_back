package com.warehouse_accounting.integration_tests;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.BonusTransactionRestController;
import com.warehouse_accounting.models.BonusTransaction;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.models.dto.RoleDto;
import com.warehouse_accounting.util.ConverterDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-bonus_transaction-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class BonusTransactionRestControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    BonusTransactionRestController bonusTransactionRestController;

    DepartmentDto departmentDto = DepartmentDto.builder()
            .name("none")
            .sortNumber("12345")
            .build();

    Role admin = Role.builder()
            .id(1L)
            .name("testAdmin")
            .sortNumber("adminSortNumber")
            .build();

    Set<RoleDto> roles = new HashSet<>(List.of(ConverterDto.convertToDto(admin)));

    PositionDto positionDto = PositionDto.builder()
            .name("none")
            .sortNumber("12345")
            .build();

    ImageDto imageDto = ImageDto.builder()
            .imageUrl("none")
            .sortNumber("12345")
            .build();

    EmployeeDto employeeDto = EmployeeDto.builder()
            .id(1L)
            .lastName("test2")
            .firstName("test2")
            .middleName("middleName")
            .sortNumber("sortNumber")
            .phone("phone")
            .inn("inn")
            .description("description")
            .email("email")
            .password("password2")
            .position(positionDto)
            .department(departmentDto)
            .roles(roles)
            .image(imageDto)
            .tariff(new HashSet<>())
            .build();

    @Test
    void testExistence() {
        assertThat(bonusTransactionRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception{
        mockMvc.perform(get("/api/bonus_transactions"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception{
        mockMvc.perform(get("/api/bonus_transactions/6"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("6"))
                .andExpect(jsonPath("$.created").value("2021-11-11T02:00:00"))
                .andExpect(jsonPath("$.transactionType").value("EARNING"))
                .andExpect(jsonPath("$.bonusValue").value("100"))
                .andExpect(jsonPath("$.transactionStatus").value("COMPLETED"))
                .andExpect(jsonPath("$.executionDate").value("2021-11-11T03:00:00"))
                .andExpect(jsonPath("$.bonusProgram").value("BEGINER"))
                .andExpect(jsonPath("$.comment").value("FOR CUPCAKES"));
    }

    @Test
    void testCreate() throws  Exception{
        BonusTransactionDto dto = BonusTransactionDto.builder()
                .id(3L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        String jsonBonusTransaction = new ObjectMapper().writeValueAsString(dto);

        mockMvc.perform(post("/api/bonus_transactions/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBonusTransaction))
                .andExpect(status().isOk());
    }

    @Test
    void TestUpdate() throws Exception{
        BonusTransactionDto dto = BonusTransactionDto.builder()
                .id(9L)
                .created(LocalDate.now())
                .transactionType(BonusTransaction.TransactionType.EARNING)
                .bonusValue(5L)
                .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                .executionDate(LocalDate.now())
                .bonusProgramDto(BonusProgramDto.builder().build())
                .contractorDto((ContractorDto.builder().build()))
                .comment("comment")
                .ownerDto(EmployeeDto.builder().build())
                .ownerChangedDto(EmployeeDto.builder().build())
                .dateChange(LocalDate.now())
                .build();
        String jsonBonusTransaction = new ObjectMapper().writeValueAsString(dto);

        mockMvc.perform(put("/api/bonus_transactions/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBonusTransaction))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/bonus_transactions/9"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionType").value("EARNING"))
                .andExpect(jsonPath("$.bonusValue").value("5"))
                .andExpect(jsonPath("$.transactionStatus").value("COMPLETED"))
                .andExpect(jsonPath("$.bonusProgram").value("beginer"))
                .andExpect(jsonPath("$.comment").value("First Transaction!"));
    }

    @Test
    void  testDelete() throws  Exception{
        mockMvc.perform(delete("/api/bonus_transactions/9"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/bonus_transactions/9"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.BuildingRestController;
import com.warehouse_accounting.models.dto.BuildingDto;
import com.warehouse_accounting.models.dto.RegionDto;
import com.warehouse_accounting.services.interfaces.BuildingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class BuildingRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BuildingRestController buildingRestController;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(buildingRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/buildings"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("1"))
                .andExpect(jsonPath("$[3].name").value("1"));

        this.mockMvc.perform(get("/api/buildings?regionCityStreetCode=0100000100000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("1"))
                .andExpect(jsonPath("$[1].name").value("1"))
                .andExpect(jsonPath("$[2].name").value("1"))
                .andExpect(jsonPath("$[3].name").value("1"))
                .andExpect(jsonPath("$[4].name").value("1"));
    }

    @Test
    void getSlice() throws Exception {
        this.mockMvc.perform(get("/api/buildings/slice?offset=2&limit=10&name=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("128"))
                .andExpect(jsonPath("$[1].name").value("129"))
                .andExpect(jsonPath("$[2].name").value("129А"))
                .andExpect(jsonPath("$[3].name").value("132"))
                .andExpect(jsonPath("$.size()").value(10));

        this.mockMvc.perform(get("/api/buildings/slice?offset=2&limit=10&name=2&regionCityStreetCode="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("128"))
                .andExpect(jsonPath("$[1].name").value("129"))
                .andExpect(jsonPath("$[2].name").value("129А"))
                .andExpect(jsonPath("$[3].name").value("132"))
                .andExpect(jsonPath("$.size()").value(10));

        this.mockMvc.perform(get("/api/buildings/slice?offset=2&limit=10&name=2&regionCityStreetCode=0230000100000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));

        this.mockMvc.perform(get("/api/buildings/slice?offset=2&limit=10&name=Неизвестный"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(0));
    }
    @Test
    void getCount() throws Exception {
        this.mockMvc.perform(get("/api/buildings/count?name="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("789"));

        this.mockMvc.perform(get("/api/buildings/count?name=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("217"));

        this.mockMvc.perform(get("/api/buildings/count?name=2&regionCityStreetCode="))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("217"));

        this.mockMvc.perform(get("/api/buildings/count?name=2&regionCityStreetCode=0230000100000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("0"));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/buildings/31"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("31"))
                .andExpect(jsonPath("$.name").value("4Бстр17"))
                .andExpect(jsonPath("$.socr").value("ДОМ"))
                .andExpect(jsonPath("$.code").value("0100000100000030006"))
                .andExpect(jsonPath("$.index").value("385006"));

        this.mockMvc.perform(get("/api/buildings/9999999999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception {
        BuildingDto buildingDto = BuildingDto.builder()
                .id(1L)
                .name("Новое строение")
                .socr("Новый socr")
                .code("Новый код")
                .index("Новый индекс")
                .build();

        this.mockMvc.perform(post("/api/buildings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildingDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/buildings/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Новое строение"))
                .andExpect(jsonPath("$.socr").value("Новый socr"))
                .andExpect(jsonPath("$.code").value("Новый код"))
                .andExpect(jsonPath("$.index").value("Новый индекс"));
    }

    @Test
    void update() throws Exception {
        BuildingDto buildingDto = buildingService.getById(31L);
        buildingDto.setName("Обновлённое строение");
        buildingDto.setSocr("Обновлённое socr");
        buildingDto.setCode("Обновлённый код");

        this.mockMvc.perform(put("/api/buildings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildingDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/buildings/31"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("31"))
                .andExpect(jsonPath("$.name").value("Обновлённое строение"))
                .andExpect(jsonPath("$.socr").value("Обновлённое socr"))
                .andExpect(jsonPath("$.code").value("Обновлённый код"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/buildings/6"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/buildings/6"))
                .andExpect(status().isNotFound());
    }
}
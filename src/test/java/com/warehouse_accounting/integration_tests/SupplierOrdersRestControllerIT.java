package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.SupplierOrdersRestController;
import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class SupplierOrdersRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SupplierOrdersRestController supplierOrdersRestController;

    @Test
    void testExistence() {
        assertThat(supplierOrdersRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/supplier_orders"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/supplier_orders/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.companyAccount").value("123123"))
                .andExpect(jsonPath("$.notPaid").value("200000"));
    }

    @Test
    void testCreate() throws Exception {
        SupplierOrdersDto requiredSupplierOrders = SupplierOrdersDto.builder()
                .id(4L)
                .companyId(1L)
                .contrAgentId(1L)
                .projectId(1L)
                .contractId(1L)
                .departmentId(1L)
                .employeeId(1L)
                .updatedFromEmployeeId(1L)
                .warehouseId(1L)
                .number("10600")
                .companyAccount(123123L)
                .notPaid(200000L)
                .build();

        String jsonSupplierOrders = new ObjectMapper().writeValueAsString(requiredSupplierOrders);

        mockMvc.perform(post("/api/supplier_orders/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSupplierOrders))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_orders/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("10600"))
                .andExpect(jsonPath("$.companyAccount").value("123123"))
                .andExpect(jsonPath("$.notPaid").value("200000"));
    }

    @Test
    void testUpdate() throws Exception {
        SupplierOrdersDto requiredSupplierOrders = SupplierOrdersDto.builder()
                .id(4L)
                .companyId(1L)
                .contrAgentId(1L)
                .projectId(1L)
                .contractId(1L)
                .departmentId(1L)
                .employeeId(1L)
                .updatedFromEmployeeId(1L)
                .warehouseId(1L)
                .number("UPDATE TEST")
                .companyAccount(299L)
                .notPaid(399L)
                .build();
        String jsonSupplierOrders = new ObjectMapper().writeValueAsString(requiredSupplierOrders);

        mockMvc.perform(put("/api/supplier_orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonSupplierOrders))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_orders/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("UPDATE TEST"))
                .andExpect(jsonPath("$.companyAccount").value("299"))
                .andExpect(jsonPath("$.notPaid").value("399"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/supplier_orders/4"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/supplier_orders/4"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.GoodsToRealizeGetController;
import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.UnitDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGetService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.UnitService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class GoodsToRealizeGetControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GoodsToRealizeGetController goodsToRealizeGetController;

    @Autowired
    private GoodsToRealizeGetService goodsToRealizeGetService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private UnitService unitService;

    @Test
    void testExist() {
        assertThat(goodsToRealizeGetController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {

        mockMvc.perform(get("/api/goodstorealizeget"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {

        mockMvc.perform(get("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.productDtoId").value("1"))
                .andExpect(jsonPath("$.unitId").value("2"));
    }

    @Test
    void testCreate() throws Exception {
        ProductDto productDto = productService.getById(1L);
        UnitDto unitDto = unitService.getById(3L);

        GoodsToRealizeGetDto goodsToRealizeGetDto = GoodsToRealizeGetDto.builder()
                .id(2L)
                .productDtoId(productDto.getId())
                .productDtoName(productDto.getName())
                .unitId(unitDto.getId())
                .unitName(unitDto.getShortName())
                .getGoods(1)
                .quantity(2)
                .amount(3)
                .arrive(4)
                .remains(5)
                .quantity_report(6)
                .amount_report(7)
                .quantity_Noreport(8)
                .amount_Noreport(9)
                .build();

        String jsonGoodsToRealizeGet = new ObjectMapper().writeValueAsString(goodsToRealizeGetDto);
        System.out.println(jsonGoodsToRealizeGet);
        mockMvc.perform(post("/api/goodstorealizeget")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonGoodsToRealizeGet))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizeget/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.unitId").value("3"))
                .andExpect(jsonPath("$.quantity_report").value("9"));
    }

    @Test
    void testUpdate() throws Exception {
        GoodsToRealizeGetDto goodsToRealizeGetDto = goodsToRealizeGetService.getById(1L);

        goodsToRealizeGetDto.setGetGoods(999);
        goodsToRealizeGetDto.setArrive(999);

        mockMvc.perform(get("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.getGoods").value("5"))
                .andExpect(jsonPath("$.arrive").value("11"));

        mockMvc.perform(MockMvcRequestBuilders.put("/api/goodstorealizeget")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(goodsToRealizeGetDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.getGoods").value("999"))
                .andExpect(jsonPath("$.arrive").value("999"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(get("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(delete("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/goodstorealizeget/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

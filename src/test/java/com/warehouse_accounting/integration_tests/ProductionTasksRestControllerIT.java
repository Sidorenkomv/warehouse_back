package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ProductionTasksController;
import com.warehouse_accounting.models.dto.ProductionTasksDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class ProductionTasksRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductionTasksController controller;

    @Test
    void testExistence(){
        assertThat(controller).isNotNull();
    }
    @Test
    void getAll() throws Exception{
        this.mockMvc.perform(get("/api/production_tasks"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {

        mockMvc.perform(get("/api/production_tasks/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.organization").value("ООО Рога и Мохито"))
                .andExpect(jsonPath("$.description").value("Задача для производства"));
    }
    @Test
    void create() throws Exception{
        ProductionTasksDto productionTasksDto = ProductionTasksDto.builder()
                .id(2L)
                .taskId(111L)
                .organization("CREATE TEST")
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .isAccessed(true)
                .ownerDepartmentId(1L)
                .ownerEmployeeId(1L)
                .description("CREATE TEST")
                .editEmployeeId(1L)
                .build();

        mockMvc.perform(post("/api/production_tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(productionTasksDto)))
                .andDo(print());

        mockMvc.perform(get("/api/production_tasks/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.organization").value("CREATE TEST"))
                .andExpect(jsonPath("$.description").value("CREATE TEST"));
    }

    @Test
    void update() throws Exception {

        ProductionTasksDto productionTasksDto = ProductionTasksDto.builder()
                .id(1L)
                .taskId(123L)
                .organization("UPDATE TEST")
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .isAccessed(true)
                .ownerDepartmentId(1L)
                .ownerEmployeeId(1L)
                .description("UPDATE TEST")
                .editEmployeeId(1L)
                .build();

        mockMvc.perform(put("/api/production_tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(productionTasksDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/production_tasks/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.organization").value("UPDATE TEST"))
                .andExpect(jsonPath("$.description").value("UPDATE TEST"));
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/production_tasks/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/production_tasks/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

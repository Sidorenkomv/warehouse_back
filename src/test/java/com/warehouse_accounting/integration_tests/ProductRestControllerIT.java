package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ProductRestController;
import com.warehouse_accounting.models.dto.ProductDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ProductRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRestController productRestController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testExistence() {
        assertThat(productRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/products"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/products/1"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {

        ProductDto productDto2 = ProductDto.builder()
                .id(4L)
                .name("CREATE TEST")
                .archive(false)
                .volume(BigDecimal.valueOf(20L))
                .weight(BigDecimal.valueOf(20L))
                .purchasePrice(BigDecimal.valueOf(20L))
                .description("CREATE TEST")
                .unitDto(null)
                .productPricesDto(new ArrayList<>())
                .attributeOfCalculationObjectDto(null)
                .taxSystemDto(null)
                .contractorDto(null)
                .imagesDto(new ArrayList<>())
                .build();

        mockMvc.perform(post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/products/4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.name").value("CREATE TEST"))
                .andExpect(jsonPath("$.description").value("CREATE TEST"));
    }

    @Test
    void update() throws Exception {

        ProductDto productDto3 = ProductDto.builder()
                .id(1L)
                .name("UPDATE TEST")
                .description("UPDATE TEST")
                .volume(BigDecimal.valueOf(35L))
                .weight(BigDecimal.valueOf(40L))
                .purchasePrice(BigDecimal.valueOf(45L))
                .archive(true)
                .imagesDto(new ArrayList<>())
                .productPricesDto(new ArrayList<>())
                .build();

        mockMvc.perform(put("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productDto3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/products/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("UPDATE TEST"))
                .andExpect(jsonPath("$.description").value("UPDATE TEST"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/products/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/products/1"))
                .andExpect(status().isNotFound());
    }
}
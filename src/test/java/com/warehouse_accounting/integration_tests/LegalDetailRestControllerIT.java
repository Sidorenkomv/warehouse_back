package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.LegalDetailRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import com.warehouse_accounting.services.interfaces.LegalDetailService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class LegalDetailRestControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LegalDetailRestController legalDetailRestController;

    @Autowired
    private LegalDetailService legalDetailService;

    @Autowired
    private ObjectMapper mapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(legalDetailRestController);
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/legal_details"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].fullName").value("legalDetail1"))
                .andExpect(jsonPath("$[1].fullName").value("legalDetail2"))
                .andExpect(jsonPath("$[2].fullName").value("legalDetail3"));
    }

    @Test
    void testGetById() throws Exception {
        String jsonCurrency = new ObjectMapper().writeValueAsString(legalDetailService.getById(2L));

        mockMvc.perform(get("/api/legal_details/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonCurrency));
    }

    @Test
    void testCreate() throws Exception {
        LegalDetailDto legalDetailDto = LegalDetailDto.builder()
                .id(4L)
                .fullName("legalDetailCreate")
                .firstName("firstNameCreate")
                .middleName("middleNameCreate")
                .lastName("lastNameCreate")
                .address(AddressDto.builder()
                        .postCode("createAddress")
                        .build())
                .inn("inn7743013902")
                .kpp("kpp111111111")
                .okpo("okpo11111")
                .ogrn("ogrn11111")
                .numberOfTheCertificate("numberOfTheCertificateCreate")
                .dateOfTheCertificate(LocalDate.now())
                .build();

        mockMvc.perform(post("/api/legal_details")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(legalDetailDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/legal_details/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fullName").value("legalDetailCreate"))
                .andExpect(jsonPath("$.firstName").value("firstNameCreate"))
                .andExpect(jsonPath("$.address.postCode").value("createAddress"))
                .andExpect(jsonPath("$.ogrn").value("ogrn11111"))
                .andExpect(jsonPath("$.numberOfTheCertificate").value("numberOfTheCertificateCreate"));
    }

    @Test
    void testUpdate() throws Exception {
        LegalDetailDto legalDetailDto = legalDetailService.getById(2L);

        Long oldAddressId = legalDetailDto.getAddress().getId();

        legalDetailDto.setFirstName("newFirstName");
        legalDetailDto.setAddress(AddressDto.builder()
                .postCode("updateAddress")
                .build());

        mockMvc.perform(put("/api/legal_details")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(legalDetailDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/legal_details/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("newFirstName"))
                .andExpect(jsonPath("$.address.postCode").value("updateAddress"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void testDelete() throws Exception {
        LegalDetailDto legalDetailDto = legalDetailService.getById(3L);

        Long oldAddressId = legalDetailDto.getAddress().getId();

        mockMvc.perform(delete("/api/legal_details/3"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/legal_details/3"))
                .andDo(print())
                .andExpect(status().isNotFound());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void testFilter() throws Exception {
        List<LegalDetailDto> collectBetween = legalDetailService.getAll().stream()
                .filter(dto -> dto.getId().equals(2L))
                .collect(Collectors.toList());

        mockMvc.perform(get("/api/legal_details/filter?fullName=legalDetail2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(collectBetween)));
    }
}

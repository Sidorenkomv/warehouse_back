package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.StartScreenRestController;
import com.warehouse_accounting.models.dto.StartScreenDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = {"classpath:init-start-screen.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class StartScreenRestControllerIT {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private StartScreenRestController startScreenRestController;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExist() {
        assertThat(startScreenRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/start_screen"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/start_screen/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.startScreen").value("start_screen1"));
    }

    @Test
    void testDeleteByID() throws Exception {
        mockMvc.perform(delete("/api/start_screen/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/start_screen/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate() throws Exception {
        StartScreenDto startScreenDto = StartScreenDto.builder()
                .id(1L)
                .startScreen("start_screen/1")
                .build();
        mockMvc.perform(post("/api/start_screen")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(startScreenDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/start_screen/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.startScreen").value("start_screen/1"));

    }

    @Test
    void testUpdate() throws Exception {
        StartScreenDto startScreenDto1 = StartScreenDto.builder()
                .id(1L)
                .startScreen("new_start_screen1")
                .build();
        mockMvc.perform(put("/api/start_screen")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(startScreenDto1)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/start_screen/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(startScreenDto1)));
    }
}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.RetailShiftRestController;
import com.warehouse_accounting.models.dto.*;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class RetailShiftRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RetailShiftRestController retailShiftRestController;

    @Autowired
    private PointOfSalesService pointOfSalesService;

    @Test
    @SneakyThrows
    void testExistence() {
        assertThat(retailShiftRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void testGetAll() {
        mockMvc.perform(get("/api/retail_shift"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void testGetById() {
        mockMvc.perform(get("/api/retail_shift/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void testCreate() {

        RetailShiftDto retailShiftDto = RetailShiftDto.builder()
                .id(3L)
                .pointOfSales(pointOfSalesService.getById(1L))
                .description("comment")
                .build();

        mockMvc.perform(post("/api/retail_shift")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(retailShiftDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/retail_shift/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("comment"));

    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void testUpdate() {
        RetailShiftDto retailShiftDto = RetailShiftDto.builder()
                .id(2L)
                .pointOfSales(pointOfSalesService.getById(1L))
                .description("new comment")
                .sale(BigDecimal.valueOf(90))
                .build();

        String retailShiftJson = new ObjectMapper().writeValueAsString(retailShiftDto);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/retail_shift")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(retailShiftJson))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/retail_shift/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("new comment"))
                .andExpect(jsonPath("$.sale").value(90));
    }

    @DisplayName("delete method test")
    @Test
    @SneakyThrows
    void testDelete() {
        mockMvc.perform(delete("/api/retail_shift/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/retail_shift/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}


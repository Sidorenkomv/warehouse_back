package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ContactFaceRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.ContactFaceDto;
import com.warehouse_accounting.services.interfaces.ContactFaceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class ContactFaceRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContactFaceRestController contactFaceRestController;

    @Autowired
    private ContactFaceService contactFaceService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void TestExist() {
        Assertions.assertNotNull(contactFaceRestController);
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/contact_face"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("contactFace1"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("contactFace2"));
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/contact_face/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("contactFace2"))
                .andExpect(jsonPath("$.address.postCode").value("postCodeContactFace2"))
                .andExpect(jsonPath("$.position").value("position2"))
                .andExpect(jsonPath("$.comment").value("comment2"));
    }

    @Test
    void create() throws Exception {
        ContactFaceDto contactFaceDto = ContactFaceDto.builder()
                .id(4L)
                .name("contactFace4")
                .description("description4")
                .phone("phone4")
                .email("email4")
                .externalCode("externalCode4")
                .address(AddressDto.builder()
                        .postCode("postCodeContactFace4")
                        .build())
                .position("position4")
                .linkContractor("linkContractor4")
                .comment("comment4")
                .build();

        mockMvc.perform(post("/api/contact_face")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(contactFaceDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/contact_face/4"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("contactFace4"))
                .andExpect(jsonPath("$.address.postCode").value("postCodeContactFace4"))
                .andExpect(jsonPath("$.position").value("position4"))
                .andExpect(jsonPath("$.comment").value("comment4"));
    }

    @Test
    void update() throws Exception {
        ContactFaceDto contactFaceDto = contactFaceService.getById(2L);

        Long oldAddressId = contactFaceDto.getAddress().getId();

        contactFaceDto.setExternalCode("externalCodeUpdate");
        contactFaceDto.setAddress(AddressDto.builder()
                .postCode("updateAddress")
                .build());
        contactFaceDto.setPosition("positionUpdate");

        mockMvc.perform(MockMvcRequestBuilders.put("/api/contact_face")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(contactFaceDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/contact_face/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("description2"))
                .andExpect(jsonPath("$.externalCode").value("externalCodeUpdate"))
                .andExpect(jsonPath("$.address.postCode").value("updateAddress"))
                .andExpect(jsonPath("$.position").value("positionUpdate"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void delete() throws Exception{
        ContactFaceDto contactFaceDto = contactFaceService.getById(2L);

        Long oldAddressId = contactFaceDto.getAddress().getId();

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/contact_face/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/contact_face/2"))
                .andDo(print())
                .andExpect(status().isNotFound());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.WarehouseRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class WarehouseRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WarehouseRestController warehouseRestController;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        Assertions.assertNotNull(warehouseRestController);
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/warehouses"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("Вспомогательный"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].name").value("Основной"));
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/warehouses/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("Основной"))
                .andExpect(jsonPath("$.address.postCode").value("312455"));
    }

    @Test
    void testCreate() throws Exception {
        WarehouseDto warehouse = WarehouseDto.builder()
                .id(4L)
                .name("warehouse4")
                .sortNumber("4")
                .address(AddressDto.builder()
                        .postCode("postCodeWarehouseCreate")
                        .build())
                .comment("Маленький склад")
                .build();

        mockMvc.perform(post("/api/warehouses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(warehouse)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/warehouses/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.name").value("warehouse4"))
                .andExpect(jsonPath("$.sortNumber").value("4"))
                .andExpect(jsonPath("$.comment").value("Маленький склад"))
                .andExpect(jsonPath("$.address.postCode").value("postCodeWarehouseCreate"));
    }

    @Test
    void testUpdate() throws Exception {
        WarehouseDto warehouse = warehouseService.getById(2L);

        Long oldAddressId = warehouse.getAddress().getId();

        warehouse.setName("warehouseUpdate");
        warehouse.setAddress(AddressDto.builder()
                .postCode("updateAddress")
                .build());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/warehouses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(warehouse)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/warehouses/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("warehouseUpdate"))
                .andExpect(jsonPath("$.address.postCode").value("updateAddress"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void testDelete() throws Exception {
        WarehouseDto warehouse = warehouseService.getById(3L);

        Long oldAddressId = warehouse.getAddress().getId();

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/warehouses/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/warehouses/3"))
                .andDo(print())
                .andExpect(status().isNotFound());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/addresses/" + oldAddressId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
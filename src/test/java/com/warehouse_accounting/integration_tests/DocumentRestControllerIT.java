package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.repositories.DocumentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


//перед запуском необходимо очистить таблицу documents и закомментировать класс DataInitializer
@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
//@Sql(value = "classpath:init-documents-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class DocumentRestControllerIT {

    private static DocumentDto documentDto1;
    private static DocumentDto documentDto2;

    private static List<DocumentDto> documentDtoList = new ArrayList<>();

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        documentDto2 = DocumentDto.builder()
                .id(7L)
                .type("CREATE TEST")
                .docNumber("CREATE TEST")
                .date(LocalDateTime.of(2016, 7, 3, 4, 2, 0))
                .sum(BigDecimal.valueOf(20000))
                .isSharedAccess(false)
                .sent(true)
                .print(true)
                .comments("CREATE TEST")
                .companyId(1L)
                .contrAgentId(1L)
                .contractId(1L)
                .departmentId(1L)
                .employeeId(1L)
                .projectId(1L)
                .salesChannelId(1L)
                .updatedFromEmployeeId(2L)
                .warehouseFromId(1L)
                .warehouseToId(2L)
                .tasks(new ArrayList<>())
                .files(new ArrayList<>())
                .build();

        documentDtoList.add(documentDto1);
    }

    @AfterEach
    void clean() {
        documentRepository.deleteAll();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/documents"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/documents/1"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.docNumber").value("Оп-1"))
                .andExpect(jsonPath("$.warehouseFromName").value("Вспомогательный"));
    }

    @Test
    void create() throws Exception {
        documentDto2 = DocumentDto.builder()
                .id(7L)
                .type("CREATE TEST")
                .docNumber("CREATE TEST")
                .date(LocalDateTime.of(2016, 7, 3, 4, 2, 0))
                .sum(BigDecimal.valueOf(20000))
                .isSharedAccess(false)
                .sent(true)
                .print(true)
                .comments("CREATE TEST")
                .companyId(1L)
                .contrAgentId(1L)
                .contractId(1L)
                .departmentId(1L)
                .employeeId(1L)
                .projectId(1L)
                .salesChannelId(1L)
                .updatedFromEmployeeId(2L)
                .warehouseFromId(1L)
                .warehouseToId(2L)
                .tasks(new ArrayList<>())
                .files(new ArrayList<>())
                .build();

        mockMvc.perform(post("/api/documents")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(documentDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/documents/7"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("7"))
                .andExpect(jsonPath("$.type").value("CREATE TEST"))
                .andExpect(jsonPath("$.docNumber").value("CREATE TEST"))
                .andExpect(jsonPath("$.comments").value("CREATE TEST"));
    }

    @Test
    void update() throws Exception {
        documentDto1 = DocumentDto.builder()
                .id(7L)
                .type("UPDATE TEST")
                .docNumber("UPDATE TEST")
                .date(LocalDateTime.of(2011, 12, 5, 3, 0, 1))
                .sum(BigDecimal.valueOf(35000))
                .isSharedAccess(false)
                .sent(false)
                .print(true)
                .comments("UPDATE TEST")
                .companyId(1L)
                .contrAgentId(1L)
                .contractId(1L)
                .departmentId(1L)
                .employeeId(1L)
                .projectId(1L)
                .salesChannelId(1L)
                .updatedFromEmployeeId(2L)
                .warehouseFromId(1L)
                .warehouseToId(1L)
                .tasks(new ArrayList<>())
                .files(new ArrayList<>())
                .build();

        mockMvc.perform(post("/api/documents")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(documentDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(put("/api/documents")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(documentDto1)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/documents/7"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("7"))
                .andExpect(jsonPath("$.type").value("UPDATE TEST"))
                .andExpect(jsonPath("$.docNumber").value("UPDATE TEST"))
                .andExpect(jsonPath("$.comments").value("UPDATE TEST"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/documents/2"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/documents/2"))
                .andExpect(status().isNotFound());
    }
}

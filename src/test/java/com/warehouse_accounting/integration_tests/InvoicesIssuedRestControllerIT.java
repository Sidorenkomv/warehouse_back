package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.InvoicesIssuedRestController;
import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.services.interfaces.InvoicesIssuedService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class InvoicesIssuedRestControllerIT {

    @Autowired
    private InvoicesIssuedRestController invoicesIssuedRestController;

    @Autowired
    private InvoicesIssuedService invoicesIssuedService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void isExist(){
        assertThat(invoicesIssuedRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception{
        mockMvc.perform(get("/api/invoices_issued"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception{
        mockMvc.perform(get("/api/invoices_issued/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.data").value(LocalDateTime.of(2002,11,12, 11, 11, 55).toString()))
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(100.0)))
                .andExpect(jsonPath("$.sent").value(true))
                .andExpect(jsonPath("$.printed").value(true))
                .andExpect(jsonPath("$.companyId").value(1L))
                .andExpect(jsonPath("$.contractorId").value(1L))
                .andExpect(jsonPath("$.comment").value("Изменено"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception{
        InvoicesIssuedDto invoicesIssuedDto2 = InvoicesIssuedDto.builder()
                .id(2L)
                .data(LocalDateTime.of(2002,11,12, 11, 11, 55))
                .sum(BigDecimal.valueOf(100))
                .sent(true)
                .printed(true)
                .companyId(1L)
                .contractorId(1L)
                .comment("Сделано")
                .build();
        InvoicesIssuedDto invoicesIssuedDto3 = InvoicesIssuedDto.builder()
                .id(3L)
                .data(LocalDateTime.of(2002,11,12, 11, 11, 55))
                .sum(BigDecimal.valueOf(100))
                .sent(true)
                .printed(true)
                .companyId(1L)
                .contractorId(1L)
                .comment("Сделано")
                .build();

        mockMvc.perform(post("/api/invoices_issued")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoicesIssuedDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/invoices_issued")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoicesIssuedDto3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/invoices_issued/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.data").value(LocalDateTime.of(2002,11,12, 11, 11, 55).toString()))
                .andExpect(jsonPath("$.sum").value(BigDecimal.valueOf(100.0)))
                .andExpect(jsonPath("$.sent").value(true))
                .andExpect(jsonPath("$.printed").value(true))
                .andExpect(jsonPath("$.companyId").value(1L))
                .andExpect(jsonPath("$.contractorId").value(1L))
                .andExpect(jsonPath("$.comment").value("Сделано"))
                .andExpect(status().isOk());

    }

    @Test
    void update() throws Exception{
        InvoicesIssuedDto invoicesIssuedDtoUpdateTest = invoicesIssuedService.getById(2L);
        invoicesIssuedDtoUpdateTest.setComment("Изменено");

        mockMvc.perform(get("/api/invoices_issued/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/invoices_issued")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoicesIssuedDtoUpdateTest)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/invoices_issued/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(invoicesIssuedDtoUpdateTest)));
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/invoices_issued/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/invoices_issued/3"))
                .andExpect(status().isNotFound());
    }
}


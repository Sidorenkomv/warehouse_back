package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.UnitsOfMeasureRestController;
import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
//@Sql(value = "classpath:init-units-of-measure-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class UnitsOfMeasureRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UnitsOfMeasureRestController unitsOfMeasureRestController;

    @Test
    void shouldPassIfUnitsOfMeasureRestControllerExists() {
        assertThat(unitsOfMeasureRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception{
        mockMvc.perform(get("/api/unitsOfMeasure"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void shouldRetrieveUnitOfMeasureById() throws Exception{
        mockMvc.perform(get("/api/unitsOfMeasure/1"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value("1"))
            .andExpect(jsonPath("$.type").value("Системный"))
            .andExpect(jsonPath("$.name").value("мм"))
            .andExpect(jsonPath("$.fullName").value("Миллиметр"))
            .andExpect(jsonPath("$.code").value("003"));
    }

    @Test
    void shouldCreateNewUnitOfMeasure() throws Exception {
        UnitsOfMeasureDto testUnitOfMeasure = UnitsOfMeasureDto.builder()
            .type("Системный")
            .name("миля")
            .fullName("Морская миля")
            .code("33")
            .build();

        String jsonUnitsOfMeasure = new ObjectMapper().writeValueAsString(testUnitOfMeasure);

        mockMvc.perform(post("/api/unitsOfMeasure")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUnitsOfMeasure))
            .andExpect(status().isOk());

        mockMvc.perform(get("/api/unitsOfMeasure"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/unitsOfMeasure/60"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.type").value("Системный"))
            .andExpect(jsonPath("$.name").value("миля"))
            .andExpect(jsonPath("$.fullName").value("Морская миля"))
            .andExpect(jsonPath("$.code").value("33"));
    }

    @Test
    void testUpdate() throws Exception {

        UnitsOfMeasureDto unitsOfMeasureDto = UnitsOfMeasureDto.builder()
            .id(2L)
            .type("Системный")
            .name("миля")
            .fullName("Морская миля")
            .code("333")
            .build();
        String jsonUnitsOfMeasure = new ObjectMapper().writeValueAsString(unitsOfMeasureDto);

        mockMvc.perform(put("/api/unitsOfMeasure")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUnitsOfMeasure))
            .andDo(print())
            .andExpect(status().isOk());

        mockMvc.perform(get("/api/unitsOfMeasure/2"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value("миля"))
            .andExpect(jsonPath("$.fullName").value("Морская миля"))
            .andExpect(jsonPath("$.code").value("333"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/unitsOfMeasure/1"))
            .andDo(print())
            .andExpect(status().isOk());

        mockMvc.perform(get("/api/unitsOfMeasure/1"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }
}





























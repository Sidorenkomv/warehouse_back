package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PurchaseForecastRestController;
import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.services.interfaces.PurchaseForecastService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class PurchaseForecastRestControllerIT {

    @Autowired
    private PurchaseForecastService purchaseForecastService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PurchaseForecastRestController purchaseForecastRestController;

    @Test
    void isExist() {
        assertThat(purchaseForecastRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/purchase_forecast"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/purchase_forecast/1"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.reservedDays").value("100"))
                .andExpect(jsonPath("$.reservedProduct").value("500"))
                .andExpect(jsonPath("$.ordered").value(false))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        PurchaseForecastDto purchaseForecastDtoCreateTest = PurchaseForecastDto.builder()
                .id(2L)
                .reservedDays(2L)
                .reservedProduct(5L)
                .ordered(false)
                .build();

        PurchaseForecastDto purchaseForecastDtoCreateTest2 = PurchaseForecastDto.builder()
                .id(3L)
                .reservedDays(2L)
                .reservedProduct(5L)
                .ordered(false)
                .build();

        mockMvc.perform(post("/api/purchase_forecast")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseForecastDtoCreateTest)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/purchase_forecast")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseForecastDtoCreateTest2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_forecast/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.reservedDays").value("2"))
                .andExpect(jsonPath("$.reservedProduct").value("5"))
                .andExpect(jsonPath("$.ordered").value(false))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        PurchaseForecastDto purchaseForecastDtoUpdateTest = purchaseForecastService.getById(3L);
        purchaseForecastDtoUpdateTest.setReservedDays(200L);

        mockMvc.perform(get("/api/purchase_forecast/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/purchase_forecast")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseForecastDtoUpdateTest)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_forecast/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(purchaseForecastDtoUpdateTest)));
    }

    @Test
    void delete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/purchase_forecast/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_forecast/2"))
                .andExpect(status().isNotFound());
    }
}

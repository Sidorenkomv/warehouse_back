package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.FeedRestController;
import com.warehouse_accounting.models.dto.FeedDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-feeds-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class FeedRestControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FeedRestController feedRestController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExistence() {
        assertThat(feedRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/feed"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/feed/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.feedHead").value("Заголовок новостей 1"))
                .andExpect(jsonPath("$.feedBody").value("Новости тест тело новостей 1"))
                .andExpect(jsonPath("$.feedDate").value("2022-01-20T13:33:36.667+00:00"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/feed/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/feed/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception{

        Calendar calendar = Calendar.getInstance();
        Date data = calendar.getTime();
        calendar.add(Calendar.HOUR, -3); // добовляем разницу между часовыми поясами
        SimpleDateFormat formater1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss.SSS");
        String dataTestString = formater1.format(calendar.getTime()) + "T" + formater2.format(calendar.getTime()) +"+00:00";
        FeedDto feedDto = FeedDto.builder()
                .feedDate(data)
                .feedBody("Тело новостей №3")
                .feedHead("Заголовок новостей №3")
                .build();
        String jsonTest = objectMapper.writeValueAsString(feedDto);

        this.mockMvc.perform(post("/api/feed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonTest))
                .andExpect(status().isOk());



        this.mockMvc.perform(get("/api/feed/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(3L))
                .andExpect(jsonPath("$.feedDate").value(dataTestString))
                .andExpect(jsonPath("$.feedBody").value("Тело новостей №3"))
                .andExpect(jsonPath("$.feedHead").value("Заголовок новостей №3"));
    }

    @Test
    void update() throws Exception{
        Calendar calendar = Calendar.getInstance();
        Date data = calendar.getTime();
        calendar.add(Calendar.HOUR, -3); // добовляем разницу между часовыми поясами
        SimpleDateFormat formater1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formater2 = new SimpleDateFormat("HH:mm:ss.SSS");
        String dataTestString = formater1.format(calendar.getTime()) + "T" + formater2.format(calendar.getTime()) +"+00:00";
        FeedDto feedDto = FeedDto.builder()
                .id(2L)
                .feedHead("Заголовок новости 2 апдейт")
                .feedBody("Тело новости 2 апдейт")
                .feedDate(data)
                .build();
        String jsonTest = objectMapper.writeValueAsString(feedDto);


        this.mockMvc.perform(put("/api/feed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonTest))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/feed/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.feedDate").value(dataTestString))
                .andExpect(jsonPath("$.feedBody").value("Тело новости 2 апдейт"))
                .andExpect(jsonPath("$.feedHead").value("Заголовок новости 2 апдейт"));

    }

}

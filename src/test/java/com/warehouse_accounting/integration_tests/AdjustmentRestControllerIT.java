package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.AdjustmentRestController;
import com.warehouse_accounting.models.TypeOfAdjustment;
import com.warehouse_accounting.models.dto.AdjustmentDto;
import com.warehouse_accounting.services.interfaces.AdjustmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-adjustments-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class AdjustmentRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AdjustmentRestController adjustmentRestController;

    @Autowired
    private AdjustmentService adjustmentService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void TestExist() {
        assertThat(adjustmentRestController).isNotNull();
    }

    @Test
    void testCreate() throws Exception {
        AdjustmentDto adjustmentDto = AdjustmentDto.builder()
                .id(1L)
                .number("CREATE TEST")
                .companyId(null)
                .contractorId(null)
                .type(TypeOfAdjustment.CASHBALANCE)
                .currentBalance(BigDecimal.valueOf(2000.00))
                .totalBalance(BigDecimal.valueOf(1000.00))
                .adjustmentAmount(BigDecimal.valueOf(1000.00))
                .comment("CREATE TEST")
                .build();

        String jsonAdjustment = new ObjectMapper().writeValueAsString(adjustmentDto);
        mockMvc.perform(post("/api/adjustments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonAdjustment))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/adjustments/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("CREATE TEST"))
                .andExpect(jsonPath("$.comment").value("CREATE TEST"))
                .andExpect(jsonPath("$.type").value("CASHBALANCE"))
                .andExpect(jsonPath("$.adjustmentAmount").value(BigDecimal.valueOf(1000.00)));
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/adjustments/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.comment").value("Test comment"));
    }

    @Test
    void testUpdate() throws Exception {

        AdjustmentDto adjustmentDto = adjustmentService.getById(1L);
        adjustmentDto.setNumber("UPDATE TEST");
        adjustmentDto.setComment("UPDATE TEST");
        adjustmentDto.setCompanyId(1L);
        adjustmentDto.setContractorName("Best_Contractor");
        adjustmentDto.setContractorId(1L);


        mockMvc.perform(put("/api/adjustments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(adjustmentDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/adjustments/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(adjustmentDto)));
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/adjustments"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testDelete() throws Exception {

        mockMvc.perform(delete("/api/adjustments/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/adjustments/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

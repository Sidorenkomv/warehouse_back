package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.AddressRestController;
import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.BuildingDto;
import com.warehouse_accounting.services.interfaces.AddressService;
import com.warehouse_accounting.services.interfaces.BuildingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@Transactional
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class AddressRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AddressRestController addressRestController;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BuildingService buildingService;

    @Test
    void testExistence() {
        Assertions.assertNotNull(addressRestController);
    }

    @Test
    void getAll() throws Exception {
        this.mockMvc.perform(get("/api/addresses"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].postCode").value("511312"))
                .andExpect(jsonPath("$[2].postCode").value("775566"));
    }

    @Test
    void getById() throws Exception {
        this.mockMvc.perform(get("/api/addresses/4"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.fullAddress").value("new Address"));

        this.mockMvc.perform(get("/api/addresses/9999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void create() throws Exception {
        buildingService.create(BuildingDto.builder()
                .name("Дом 1").socr("Дом 1").code("Дом 1").index("Дом 1")
                .build());

        AddressDto addressDto = AddressDto.builder()
                .id(14L)
                .postCode("newPostCode")
                .buildingId(1L)
                .office("newOffice")
                .fullAddress("newFullAddress")
                .other("newOther")
                .comment("newComment")
                .build();

        this.mockMvc.perform(post("/api/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/addresses/14"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("14"))
                .andExpect(jsonPath("$.postCode").value("newPostCode"))
                .andExpect(jsonPath("$.buildingId").value("1"))
                .andExpect(jsonPath("$.office").value("newOffice"))
                .andExpect(jsonPath("$.fullAddress").value("newFullAddress"))
                .andExpect(jsonPath("$.other").value("newOther"))
                .andExpect(jsonPath("$.comment").value("newComment"));
    }

    @Test
    void update() throws Exception {
        AddressDto addressDto = addressService.getById(2L);
        addressDto.setPostCode("updatePostCode");
        addressDto.setComment("updateComment");

        this.mockMvc.perform(put("/api/addresses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(addressDto)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/addresses/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.postCode").value("updatePostCode"))
                .andExpect(jsonPath("$.comment").value("updateComment"));
    }

//Этот тест не логичен и вызывает ошибку, так как удаление адреса будет при любых обстоятельствах
//влечь за собой удаление другого объекта к которому этот адрес привязан потому что отношения OneToOne
//    @Test
//    void deleteById() throws Exception {
//        mockMvc.perform(delete("/api/addresses/4"))
//                .andExpect(status().isOk());
//
//        mockMvc.perform(get("/api/addresses/4"))
//                .andExpect(status().isNotFound());
//    }
}
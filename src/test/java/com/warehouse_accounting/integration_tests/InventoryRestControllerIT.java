package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.warehouse_accounting.controllers.rest.InventoryRestController;
import com.warehouse_accounting.models.dto.InventoryDto;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;



import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class InventoryRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InventoryRestController inventoryRestController;

    @Test
    @SneakyThrows
    void testExistence() {
        assertThat(inventoryRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void testGetAll() {
        mockMvc.perform(get("/api/inventory"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void testGetById() {
        mockMvc.perform(get("/api/inventory/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void testCreate() {

        InventoryDto inventoryDto = InventoryDto.builder()
                .id(2L)
                .moved(true)
                .printed(true)
                .comment("Комментарий2")
                .build();

        String jsonInventory = new ObjectMapper().writeValueAsString(inventoryDto);

        mockMvc.perform(post("/api/inventory")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonInventory))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/inventory/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.moved").value(true))
                .andExpect(jsonPath("$.printed").value(true))
                .andExpect(jsonPath("$.comment").value("Комментарий2"));

    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void testUpdate() {

        InventoryDto inventoryDto = InventoryDto.builder()
                .id(2L)
                .moved(false)
                .printed(false)
                .comment("Комментарий3")
                .build();

        String jsonInventory = new ObjectMapper().writeValueAsString(inventoryDto);

        mockMvc.perform(post("/api/inventory")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonInventory))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/inventory/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.moved").value(false))
                .andExpect(jsonPath("$.printed").value(false))
                .andExpect(jsonPath("$.comment").value("Комментарий3"));
    }

    @DisplayName("delete method test")
    @Test
    @SneakyThrows
    void testDelete() {
        mockMvc.perform(delete("/api/inventory/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/inventory/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}

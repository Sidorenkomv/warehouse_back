package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.InternalOrderRestController;
import com.warehouse_accounting.models.dto.InternalOrderDto;
import com.warehouse_accounting.repositories.InternalOrderRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-internal_orders.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class InternalOrderRestControllerIT {

    private static InternalOrderDto internalOrderDto1;

    private static final List<InternalOrderDto> internalOrderDtos = new ArrayList<>();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InternalOrderRestController internalOrderRestController;

    @Autowired
    private InternalOrderRepository internalOrderRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String url = "/api/internal_orders";

    @BeforeEach
    void setUp() {
        internalOrderDto1 = InternalOrderDto.builder()
                .id(10L)
                .number("doc_number1")
                .localDateTime(LocalDateTime.of(2016, 11, 11, 3, 7, 0))
                .sum(BigDecimal.valueOf(13000L))
                .shipped(BigDecimal.valueOf(2000L))
                .isSharedAccess(true)
                .sent(false)
                .print(false)
                .comment("comment11")
                .build();

        internalOrderDtos.add(internalOrderDto1);
    }

    @AfterEach
    void clean() {
        internalOrderRepository.deleteAll();
    }

    @Test
    void testExistence() {
        assertThat(internalOrderRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void getAll() {
        mockMvc.perform(get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void getById() {
        mockMvc.perform(get(url + "/10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("10"));
    }


    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void testCreate() {
        InternalOrderDto internalOrderDto2 = InternalOrderDto.builder()
                .id(20L)
                .number("num2")
                .localDateTime(LocalDateTime.now())
                .sum(BigDecimal.valueOf(60000L))
                .shipped(BigDecimal.ZERO)
                .isSharedAccess(true)
                .sent(true)
                .print(false)
                .comment("comment1")
                .build();

        String json = objectMapper.writeValueAsString(internalOrderDto2);

        mockMvc.perform(post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void update() {
        internalOrderDto1 = InternalOrderDto.builder()
                .id(10L)
                .number("doc_number21")
                .shipped(BigDecimal.ZERO)
                .isSharedAccess(false)
                .comment("comment1241")
                .build();

        String json = objectMapper.writeValueAsString(internalOrderDto1);

        mockMvc.perform(MockMvcRequestBuilders.put(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get(url + "/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value("doc_number21"))
                .andExpect(jsonPath("$.shipped").value(BigDecimal.valueOf(0.0)))
                .andExpect(jsonPath("$.isSharedAccess").value(false))
                .andExpect(jsonPath("$.comment").value("comment1241"));
    }

    @DisplayName("deleteById method test")
    @Test
    @SneakyThrows
    void deleteById() throws Exception {
        mockMvc.perform(delete(url + "/10"))
                .andExpect(status().isOk());

        mockMvc.perform(get(url + "/10"))
                .andExpect(status().isNotFound());
    }
}

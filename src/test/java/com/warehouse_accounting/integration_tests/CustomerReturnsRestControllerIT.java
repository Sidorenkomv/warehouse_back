package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.CustomerReturnsRestController;
import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.ContractorGroupDto;
import com.warehouse_accounting.models.dto.CustomerReturnsDto;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-customer-returns-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class CustomerReturnsRestControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CustomerReturnsRestController customerReturnsRestController;
    @Autowired
    private ObjectMapper mapper;
    private static CustomerReturnsDto customerReturnsDto;


    @Test
    void testExistence() {
        assertThat(customerReturnsRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/customerreturns"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/customerreturns/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.comment").value("Возврат оформлен"));
    }

    @Test
    void testCreate() throws Exception {
        customerReturnsDto = this.createDto(4L, "CREATE TEST");

        mockMvc.perform(post("/api/customerreturns")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(customerReturnsDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/customerreturns/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("4"))
                .andExpect(jsonPath("$.comment").value("CREATE TEST"));
    }

    @Test
    void testUpdate() throws Exception {
        customerReturnsDto = this.createDto(1L, "UPDATE TEST");

        mockMvc.perform(put("/api/customerreturns")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(customerReturnsDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/customerreturns/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.comment").value("UPDATE TEST"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/customerreturns/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/customerreturns/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    private CustomerReturnsDto createDto(Long id, String message) {
        return CustomerReturnsDto.builder()
                .id(id)
                .date(LocalDateTime.now().plusMonths(5))
                .warehouseDto(WarehouseDto.builder().id(1L).build())
                .contractDto(ContractDto.builder()
                        .id(1L)
                        .contractor(ContractorDto.builder()
                                .id(1L)
                                .legalDetail(LegalDetailDto.builder()
                                        .id(1L)
                                        .build())
                                .contractorGroup(ContractorGroupDto.builder()
                                        .id(1L)
                                        .build())
                                .build())
                        .build())
                .contractorDto(ContractorDto.builder()
                        .id(1L)
                        .legalDetail(LegalDetailDto.builder()
                                .id(1L)
                                .build())
                        .contractorGroup(ContractorGroupDto.builder()
                                .id(1L)
                                .build())
                        .build())
                .companyDto(CompanyDto.builder().id(1L).build())
                .productDtos(new ArrayList<>())
                .taskDtos(new ArrayList<>())
                .fileDtos(new ArrayList<>())
                .sum(BigDecimal.valueOf(0))
                .isPaid(false)
                .isSend(false)
                .comment(message)
                .build();
    }
}

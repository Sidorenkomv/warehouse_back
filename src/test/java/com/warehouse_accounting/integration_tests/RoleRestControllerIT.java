package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.RoleRestController;
import com.warehouse_accounting.models.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
//@Sql(value = "classpath:init-role-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class RoleRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RoleRestController roleRestController;

    @Test
    void testExistence() {
        assertThat(roleRestController).isNotNull();
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/roles"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetById() throws Exception {
        mockMvc.perform(get("/api/roles/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("admin"))
                .andExpect(jsonPath("$.sortNumber").value("sort_admin"));
    }

    @Test
    void testCreate() throws Exception {
        Role requiredRole = Role.builder()
                .name("admin")
                .build();
        String jsonRole = new ObjectMapper().writeValueAsString(requiredRole);

        mockMvc.perform(post("/api/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRole))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/roles/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("admin"));
    }

    @Test
    void testUpdate() throws Exception {
        Role requiredRole = Role.builder()
                .id((long) 1)
                .name("admin")
                .build();
        String jsonRole = new ObjectMapper().writeValueAsString(requiredRole);

        mockMvc.perform(put("/api/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRole))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/roles/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("admin"));
    }

    @Test
    void testDelete() throws Exception {
        mockMvc.perform(delete("/api/roles/2"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/roles/2"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

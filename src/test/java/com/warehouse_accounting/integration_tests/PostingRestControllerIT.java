package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PostingRestController;
import com.warehouse_accounting.models.dto.PostingDto;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class PostingRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PostingRestController postingRestController;

    @Test
    @SneakyThrows
    void testExistence() {
        assertThat(postingRestController).isNotNull();
    }

    @DisplayName("getAll method test")
    @Test
    @SneakyThrows
    void testGetAll() {
        mockMvc.perform(get("/api/posting"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @DisplayName("getById method test")
    @Test
    @SneakyThrows
    void testGetById() {
        mockMvc.perform(get("/api/posting/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @DisplayName("create method test")
    @Test
    @SneakyThrows
    void testCreate() {

        PostingDto postingDto = PostingDto.builder()
                .id(2L)
                .moved(true)
                .printed(true)
                .comment("Покупай")
                .build();





        String jsonPosting = new ObjectMapper().writeValueAsString(postingDto);

        mockMvc.perform(post("/api/posting")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonPosting))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/posting/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.moved").value(true))
                .andExpect(jsonPath("$.printed").value(true))
                .andExpect(jsonPath("$.comment").value("Покупай"));

    }

    @DisplayName("update method test")
    @Test
    @SneakyThrows
    void testUpdate() {

       PostingDto postingDto = PostingDto.builder()
                .id(2L)
                .moved(false)
                .printed(false)
                .comment("Бери")
                .build();

        String jsonPosting = new ObjectMapper().writeValueAsString(postingDto);

        mockMvc.perform(post("/api/posting")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonPosting))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/posting/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.moved").value(false))
                .andExpect(jsonPath("$.printed").value(false))
                .andExpect(jsonPath("$.comment").value("Бери"));
    }

    @DisplayName("delete method test")
    @Test
    @SneakyThrows
    void testDelete() {
        mockMvc.perform(delete("/api/posting/1"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/posting/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}
package com.warehouse_accounting.integration_tests;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PositionRestController;
import com.warehouse_accounting.models.Position;
import com.warehouse_accounting.repositories.PositionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class PositionRestControllerIT {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PositionRestController controller;
    @Autowired
    private PositionRepository repository;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExist() {
        assertThat(controller).isNotNull();
    }

    Position position = new Position(1L, "ТестИмя", "НомерСорт");
    Position position1 = new Position(2L, "ТестИмя2", "НомерСорт");
    Position position2 = new Position(3L, "ТестИмя3", "НомерСорт");
    Position position3 = new Position(4L, "ТестИмя4", "НомерСорт");

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/positions"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Arrays.asList(position, position1, position2, position3))));
    }

    @Test
    void create() throws Exception {
        mockMvc.perform(post("/api/positions", position)
                        .content(objectMapper.writeValueAsString(position))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/positions", position1)
                        .content(objectMapper.writeValueAsString(position1))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/positions", position2)
                        .content(objectMapper.writeValueAsString(position2))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/positions", position3)
                        .content(objectMapper.writeValueAsString(position3))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/positions/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("ТестИмя2"))
                .andExpect(jsonPath("$.sortNumber").value("НомерСорт"));
    }

    @Test
    void update() throws Exception {
        Position position = new Position(1L, "новоеИмя", "НомерСорт");

        mockMvc.perform(put("/api/positions/", position)
                        .content(objectMapper.writeValueAsString(position))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/positions/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("новоеИмя"))
                .andExpect(jsonPath("$.sortNumber").value("НомерСорт"));
    }

    @Test
    void deleteByID() throws Exception {
        mockMvc.perform(delete("/api/positions/4"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/positions/4"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testSearch() throws Exception {
        String name = "ТестИмя3";
        mockMvc.perform(get("/api/positions/search/{name}", name))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].name").value("ТестИмя3"));
    }
}

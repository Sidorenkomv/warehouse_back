package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.SalesChannelRestController;
import com.warehouse_accounting.models.dto.SalesChannelDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class SalesChannelRestControllerIT {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SalesChannelRestController salesChannelRestController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testExistence() {
        assertThat(salesChannelRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/sales_channels"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/sales_channels/1"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {

        SalesChannelDto salesChannelDto2 = new SalesChannelDto.Builder()
                .id(2L)
                .name("SalesChannel2")
                .type("someType")
                .description("Description")
                .generalAccessC("someAccess")
                .ownerDepartment("Martha")
                .ownerEmployee("David")
                .whenChanged("06/06/2018")
                .whoChanged("Jessica")
                .build();

        mockMvc.perform(post("/api/sales_channels")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(salesChannelDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/sales_channels/2"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(salesChannelDto2)))
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("SalesChannel2"))
                .andExpect(jsonPath("$.type").value("someType"));

    }

    @Test
    void update() throws Exception {

        SalesChannelDto salesChannelDto3 = new SalesChannelDto.Builder()
                .id(1L)
                .name("SalesChannel3")
                .type("differentType")
                .description("Description")
                .generalAccessC("someAccess")
                .ownerDepartment("Mary")
                .ownerEmployee("Kate")
                .whenChanged("25/06/2018")
                .whoChanged("Kennedy")
                .build();

        mockMvc.perform(put("/api/sales_channels")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(salesChannelDto3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/sales_channels/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(salesChannelDto3)))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("SalesChannel3"))
                .andExpect(jsonPath("$.type").value("differentType"));;
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/sales_channels/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/sales_channels/1"))
                .andExpect(status().isNotFound());
    }

}
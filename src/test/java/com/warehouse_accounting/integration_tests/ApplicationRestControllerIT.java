package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.ApplicationRestController;
import com.warehouse_accounting.models.dto.ApplicationDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
class ApplicationRestControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ApplicationRestController controller;

    @Test
    void testExistence() throws Exception { assertThat(controller).isNotNull(); }

    @Test
    void create() throws Exception {
        ApplicationDto applicationDto = ApplicationDto.builder()
                .id(3L)
                .name("CREATE TEST")
                .description("поставщиком компьютерного оборудования. " +
                        "Загрузка цен, остатков, описания и фото товара из каталога поставщика. " +
                        "Установка цен закупки.")
                .build();

        mockMvc.perform(post("/api/applications")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(applicationDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/applications/3"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(applicationDto)))
                .andExpect(jsonPath("$.id").value("3"))
                .andExpect(jsonPath("$.name").value("CREATE TEST"));
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/applications"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/applications/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Аналитика BI"))
                .andExpect(jsonPath("$.logoId").value("2"));
    }

    @Test
    void update() throws Exception {
        ApplicationDto applicationDto = ApplicationDto.builder()
                .id(2L)
                .name("UPDATE TEST")
                .description("поставщиком компьютерного оборудования. " +
                        "Загрузка цен, остатков, описания и фото товара из каталога поставщика. " +
                        "Установка цен закупки.")
                .build();

        mockMvc.perform(put("/api/applications")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(applicationDto)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/applications/2"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(applicationDto)))
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.name").value("UPDATE TEST"));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/applications/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/applications/1"))
                .andExpect(status().isNotFound());
    }
}

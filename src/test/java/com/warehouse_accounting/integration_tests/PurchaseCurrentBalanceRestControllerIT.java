package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PurchaseCurrentBalanceRestController;
import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.services.interfaces.PurchaseCurrentBalanceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class PurchaseCurrentBalanceRestControllerIT {
    @Autowired
    private PurchaseCurrentBalanceService purchaseCurrentBalanceService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PurchaseCurrentBalanceRestController purchaseCurrentBalanceRestController;

    @Test
    void isExist() {
        assertThat(purchaseCurrentBalanceRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception{
        mockMvc.perform(get("/api/purchase_current_balance"))
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception{
        mockMvc.perform(get("/api/purchase_current_balance/1"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.remainder").value("100"))
                .andExpect(jsonPath("$.productReserved").value("100"))
                .andExpect(jsonPath("$.productsAwaiting").value("200"))
                .andExpect(jsonPath("$.productAvailableForOrder").value("100"))
                .andExpect(jsonPath("$.daysStoreOnTheWarehouse").value("5"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception{
        PurchaseCurrentBalanceDto purchaseCurrentBalanceDtoCreateTest2 = PurchaseCurrentBalanceDto.builder()
                .id(2L)
                .remainder(200L)
                .productReserved(100L)
                .productsAwaiting(250L)
                .productAvailableForOrder(100L)
                .daysStoreOnTheWarehouse(5L)
                .build();

        PurchaseCurrentBalanceDto purchaseCurrentBalanceDtoCreateTest3 = PurchaseCurrentBalanceDto.builder()
                .id(3L)
                .remainder(200L)
                .productReserved(100L)
                .productsAwaiting(250L)
                .productAvailableForOrder(100L)
                .daysStoreOnTheWarehouse(5L)
                .build();

        mockMvc.perform(post("/api/purchase_current_balance")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(purchaseCurrentBalanceDtoCreateTest2)))
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/purchase_current_balance")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(purchaseCurrentBalanceDtoCreateTest3)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_current_balance/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.remainder").value("200"))
                .andExpect(jsonPath("$.productReserved").value("100"))
                .andExpect(jsonPath("$.productsAwaiting").value("250"))
                .andExpect(jsonPath("$.productAvailableForOrder").value("100"))
                .andExpect(jsonPath("$.daysStoreOnTheWarehouse").value("5"))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception{
        PurchaseCurrentBalanceDto purchaseCurrentBalanceDtoUpdateTest = purchaseCurrentBalanceService.getById(3L);
        purchaseCurrentBalanceDtoUpdateTest.setProductAvailableForOrder(150L);

        mockMvc.perform(get("/api/purchase_current_balance/3"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/purchase_current_balance")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(purchaseCurrentBalanceDtoUpdateTest)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_current_balance/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(purchaseCurrentBalanceDtoUpdateTest)));
    }

    @Test
    void delete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/purchase_current_balance/2"))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/purchase_current_balance/2"))
                .andExpect(status().isNotFound());
    }

}

package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.PriceListRestController;
import com.warehouse_accounting.models.dto.PriceListDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@Sql(value = "classpath:init-price_lists-table.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class PriceListRestControllerIT {

    private static PriceListDto priceListDto1;

    private static List<PriceListDto> priceListDtoList;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PriceListRestController priceListRestController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        priceListDto1 = PriceListDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.of(2021, 11, 11, 2, 0, 0))
                .moved(true)
                .printed(true)
                .comment("test")
                .build();

        priceListDtoList = List.of(priceListDto1);
    }

    @Test
    void testExistence() {
        assertThat(priceListRestController).isNotNull();
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(get("/api/price_list"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(priceListDtoList)));
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/price_list/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(priceListDto1)));
    }

    @Test
    void create() throws Exception {
        PriceListDto priceListDto2 = PriceListDto.builder()
                .id(2L)
                .dateOfCreation(LocalDateTime.of(2019, 6, 3, 3, 4, 0))
                .moved(true)
                .printed(true)
                .comment("comment")
                .build();

        mockMvc.perform(post("/api/price_list")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(priceListDto2)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/price_list/2"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(priceListDto2)));
    }

    @Test
    void update() throws Exception {
        priceListDto1 = PriceListDto.builder()
                .id(1L)
                .dateOfCreation(LocalDateTime.of(2016, 3, 14, 7, 0, 0))
                .moved(false)
                .printed(true)
                .comment("comment-test")
                .build();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/price_list")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(priceListDto1)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/price_list/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(priceListDto1)));
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/price_list/1"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/price_list/1"))
                .andExpect(status().isNotFound());
    }
}

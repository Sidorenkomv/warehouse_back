package com.warehouse_accounting.integration_tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse_accounting.controllers.rest.EmployeeRestController;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.repositories.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Collections;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class EmployeeRestControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeRestController employeeRestController;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testExist() {
        assertThat(employeeRestController).isNotNull();
    }


    @Test
    void  getAll() throws Exception {
        mockMvc.perform(get("/api/employees"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getById() throws Exception {
        mockMvc.perform(get("/api/employees/1"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.password").value("password"))
                .andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        EmployeeDto employeeDto = EmployeeDto.builder()
                .id(4L)
                .lastName("test2")
                .firstName("test2")
                .middleName("middleName")
                .sortNumber("sortNumber")
                .phone("phone")
                .inn("inn")
                .description("description")
                .email("email")
                .password("password2")
                .position(null)
                .department(null)
                .roles(Collections.emptySet())
                .image(null)
                .tariff(new HashSet<>())
                .build();

        EmployeeDto employeeDto2 = EmployeeDto.builder()
                .id(3L)
                .lastName("test3")
                .firstName("test3")
                .middleName("middleName")
                .sortNumber("sortNumber")
                .phone("phone")
                .inn("inn")
                .description("description")
                .email("email2")
                .password("password3")
                .position(null)
                .department(null)
                .roles(Collections.emptySet())
                .image(null)
                .tariff(new HashSet<>())
                .build();

        mockMvc.perform(post("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employeeDto)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employeeDto2)))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/employees/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.lastName").value("admin"))
                .andExpect(jsonPath("$.firstName").value("admin"))
                .andExpect(jsonPath("$.middleName").value("admin"))
                .andExpect(jsonPath("$.sortNumber").value("sortNumber"))
                .andExpect(jsonPath("$.phone").value("phone"))
                .andExpect(jsonPath("$.inn").value("inn"))
                .andExpect(jsonPath("$.description").value("description"))
                .andExpect(jsonPath("$.email").value("admin"))
                .andExpect(jsonPath("$.password").value("{noop}admin"))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        EmployeeDto employeeDto2 = EmployeeDto.builder()
                .id(2L)
                .lastName("test1_new")
                .firstName("test1_new")
                .middleName("middleName")
                .sortNumber("sortNumber")
                .phone("phone")
                .inn("inn")
                .description("description")
                .email("email")
                .password("password")
                .position(null)
                .department(null)
                .roles(Collections.emptySet())
                .image(null)
                .tariff(new HashSet<>())
                .build();
        String jsonEmployee2 = new ObjectMapper().writeValueAsString(employeeDto2);

        mockMvc.perform(put("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonEmployee2))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/employees/2"))
                .andDo(print())
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.lastName").value("test1_new"))
                .andExpect(jsonPath("$.firstName").value("test1_new"))
                .andExpect(jsonPath("$.middleName").value("middleName"))
                .andExpect(jsonPath("$.sortNumber").value("sortNumber"))
                .andExpect(jsonPath("$.phone").value("phone"))
                .andExpect(jsonPath("$.inn").value("inn"))
                .andExpect(jsonPath("$.description").value("description"))
                .andExpect(jsonPath("$.email").value("email"))
                .andExpect(jsonPath("$.password").value("password"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteById() throws Exception {
        mockMvc.perform(delete("/api/employees/3"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/employees/3"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}



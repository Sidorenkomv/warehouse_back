INSERT INTO customer_returns (id,  warehouse_id, contract_id, contractor_id, company_id, sum, comment)
VALUES (1, 1, 1, 1, 1, 10000, 'Возврат оформлен');

INSERT INTO customer_returns (id,  warehouse_id, contract_id, contractor_id, company_id, sum, comment)
VALUES (2, 2, 1, 1, 1, 50000, 'Возврат не оформлен');

INSERT INTO customer_returns (id,  warehouse_id, contract_id, contractor_id, company_id, sum, comment)
VALUES (3, 3, 1, 1, 1, 70000, 'Возврат оформлен, ожидается возврат средств');
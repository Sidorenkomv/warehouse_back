DELETE FROM measures;

insert into measures(id, type, name, full_name, code)
values (1, 'Системный', 'mm', 'Millimeter', '32'), (2, 'Системный', 'cm', 'Centimetre', '32');

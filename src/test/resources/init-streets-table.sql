delete from streets;

INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (1, 'Байкаринская', 'ул', '02000001000000100', '450019', '0274', '0275', '80401380000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (2, 'Рабкоровский', 'пер', '02000001000000200', '450006', '0278', '', '80401390000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (3, 'Воронежская', 'ул', '02000001000000300', '450003', '0274', '0275', '80401380000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (4, '2-й Благоварский', 'пер', '02000001000000400', '450019', '0274', '0275', '80401380000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (5, 'Благоварский 2-й', 'пер', '02000001000000401', '450019', '0274', '0275', '80401380000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (6, 'Молодежный', 'б-р', '02000001000000500', '450071', '0276', '', '80401384000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (7, 'Мусы Гареева', 'ул', '02000001000000600', '450098', '0276', '', '80401384000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (8, 'Зеленогорская', 'ул', '02000001000000700', '450104', '0276', '', '80401384000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (9, 'Подводника Родионова', 'ул', '02000001000000800', '450078', '0278', '', '80401390000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (10, '2-я Кооперативная', 'ул', '02000001000000900', '450018', '0274', '', '80401375000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (11, 'Кооперативная 2-я', 'ул', '02000001000000901', '450018', '0274', '', '80401375000');
INSERT INTO streets (id, name, socr, code, index, gninmb, uno, ocatd) VALUES (12, 'Тенистый', 'пер', '02000001000001000', '450004', '0274', '', '80401375000');

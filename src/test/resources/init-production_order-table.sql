
insert into projects(name, code, description)
values ('name for test', 'code for test', 'description for test');

insert into warehouses(comment, name, sort_number)
values ('comment for test', 'name for test', '4');

insert into technological_maps(name)
values ('name for test');

insert into companies(name)
values ('company name for test');

insert into production_orderes (id, plan_date, volume_of_production, technological_map_id)
VALUES (1, '2000-01-01', 1000, 1);

insert INTO documents (company_id)
VALUES (1);



delete from units;

INSERT INTO units (`id`, `short_name`, `full_name`, `sort_number`) VALUES
(1, 'shortName1', 'fullName1', 'sortNumber1'),
(2, 'shortName2', 'fullName2', 'sortNumber2'),
(3, 'shortName3', 'fullName3', 'sortNumber3'),
(4, 'shortName4', 'fullName4', 'sortNumber4');
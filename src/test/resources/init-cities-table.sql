delete from cities;

INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (1, 'Уфа', 'г', '0200000100000', '', '0200', '', '80401000000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (2, 'Участка Нагаевского лесничества', 'п', '0200000100100', '', '0276', '', '80401944004', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (3, 'Аэродром за Белой', 'п', '0200000100299', '', '0274', '0275', '80401380000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (4, 'Некрасовский', 'п', '0200000100399', '', '0274', '0275', '80401380000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (5, 'Земмашина', 'п', '0200000100499', '', '0274', '0275', '80401380000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (6, 'Никольский', 'п', '0200000100500', '', '0277', '', '80401955004', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (7, 'п/л Черемушки', 'нп', '0200000100699', '', '0274', '', '80401375000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (8, 'Кузнецовский свх', 'х', '0200000100899', '', '0274', '0275', '80401375000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (9, 'Королево', 'д', '0200000100900', '', '0274', '', '80401923004', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (10, 'УЕ-394/3', 'казарма', '0200000101051', '', '0273', '', '80401370000', '0');
INSERT INTO cities (id, name, socr, code, index, gninmb, uno, ocatd, status) VALUES (11, 'Степановский', 'п', '0200000101199', '', '0277', '', '80401385000', '0');
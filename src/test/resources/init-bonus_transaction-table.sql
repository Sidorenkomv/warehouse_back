delete from bonus_transactions;

insert into images(image_url, sort_number) values ('none', 'none');

insert into roles(name) values ('none');

insert into positions(name, sort_number) values ('none', 'none');

insert into departments(name, sort_number) values ('none', 'none');

insert into employees(email, department_id, position_id, role_id, image_id) values (concat('', round(rand()*10000000000, 0)), 1, 1, 1, 1);

insert into bonus_transactions(id, created, transaction_type, bonus_value, transaction_status, execution_date,
                               bonus_program, comment, owner_id )
values(6, '2021-11-11 02:00:00', 'EARNING', 100 ,'COMPLETED', '2021-11-11 03:00:00', 'BEGINER','FOR CUPCAKES', 1) ,
       (9, '2021-11-11 02:20:00', 'EARNING', 500 ,'COMPLETED', '2021-11-11 03:20:00', 'VIP','FOR DELIVERY', 1);

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.services.interfaces.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cities")
@Api(tags = "City Rest Controller")
@Tag(name = "City Rest Controller", description = "CRUD операции с City")
public class CityRestController {

    private final CityService cityService;

    public CityRestController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех City, по коду региона",
            response = CityDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )

    public ResponseEntity<List<CityDto>> getAll(
            @ApiParam(name = "regionCode", value = "region для фильтра City")
            @RequestParam(name = "regionCode", defaultValue = "") String regionCode
    ) {
        return ResponseEntity.ok(cityService.getAll(regionCode));
    }

    @GetMapping("/slice")
    @ApiOperation(
            value = "getSlice",
            notes = "Получение списка City по имени и коду региона начиная с заданного смещения и указанной длины ",
            response = CityDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<CityDto>> getSlice(
            @ApiParam(name = "offset", value = "смещение для списка City", required = true)
            @RequestParam(name = "offset", required = true) int offset,
            @ApiParam(name = "limit", value = "ограничение размера для списка City", required = true)
            @RequestParam(name = "limit", required = true) int limit,
            @ApiParam(name = "name", value = "фильтр name для списка City")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCode", value = "фильтр name для списка City")
            @RequestParam(name = "regionCode", defaultValue = "") String regionCode
    ){
        return ResponseEntity.ok(cityService.getSlice(offset, limit, name, regionCode));
    }

    @GetMapping("/count")
    @ApiOperation(
            value = "getCount",
            notes = "Получение размера списка City с применением фильтра name и кода региона",
            response = int.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение размера City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Integer> getCount(
            @ApiParam(name = "name", value = "фильтр name для получения размера списка City")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCode", value = "фильтр name для получения размера списка City")
            @RequestParam(name = "regionCode", defaultValue = "") String regionCode
    ) {
        return ResponseEntity.ok(cityService.getCount(name, regionCode));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение City по id", response = CityDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<CityDto> getById(@ApiParam(name = "id", value = "id для получения City", required = true)
                                           @PathVariable("id") Long id) {
        return ResponseEntity.ok(cityService.getById(id));
    }

    @GetMapping("/code/{code}")
    @ApiOperation(value = "getByCode", notes = "Получение City по коду", response = CityDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<CityDto> getByCode(@ApiParam(name = "code", value = "код для получения City", required = true)
                                               @PathVariable("code") String code) {
        return ResponseEntity.ok(cityService.getByCode(code));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового City")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(
            @ApiParam(name = "CityDto", value = "CityDto для создания City", required = true)
            @RequestBody CityDto cityDto
    ) {
        cityService.create(cityDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление City")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )

    public ResponseEntity<?> update(@ApiParam(name = "CityDto", value = "CityDto для обновления City", required = true)
                                    @RequestBody CityDto cityDto) {
        cityService.update(cityDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление City по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление City"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )

    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого City", required = true)
                                        @PathVariable("id") Long id) {
        cityService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.StreetDto;
import com.warehouse_accounting.services.interfaces.StreetService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/streets")
@Api(tags = "Street Rest Controller")
@Tag(name = "Street Rest Controller", description = "CRUD операции с Street")
public class StreetRestController {

    private final StreetService streetService;

    public StreetRestController(StreetService streetService) {
        this.streetService = streetService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех Street по коду региона и города",
            response = StreetDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<StreetDto>> getAll(
            @ApiParam(name = "regionCityCode", value = "код region+city фильтр для получения Street")
            @RequestParam(name = "regionCityCode", defaultValue = "") String regionCityCode
    ) {
        return ResponseEntity.ok(streetService.getAll(regionCityCode));
    }

    @GetMapping("/slice")
    @ApiOperation(
            value = "getSlice",
            notes = "Получение списка Street по имени и коду region+city начиная с заданного смещения и указанной длины ",
            response = StreetDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<StreetDto>> getSlice(
            @ApiParam(name = "offset", value = "смещение для списка Street", required = true)
            @RequestParam(name = "offset", required = true) int offset,
            @ApiParam(name = "limit", value = "ограничение размера для списка Street", required = true)
            @RequestParam(name = "limit", required = true) int limit,
            @ApiParam(name = "name", value = "фильтр name для списка Street")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCityCode", value = "код region+city фильтр для получения списка Street")
            @RequestParam(name = "regionCityCode", defaultValue = "") String regionCityCode
    ){
        return ResponseEntity.ok(streetService.getSlice(offset, limit, name, regionCityCode));
    }

    @GetMapping("/count")
    @ApiOperation(
            value = "getCount",
            notes = "Получение размера списка Street с применением фильтра name",
            response = int.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение размера Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Integer> getCount(
            @ApiParam(name = "name", value = "фильтр name для получения размера списка Street")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCityCode", value = "код region+city фильтр для получения размера списка Street")
            @RequestParam(name = "regionCityCode", defaultValue = "") String regionCityCode
    ) {
        return ResponseEntity.ok(streetService.getCount(name, regionCityCode));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Street по id", response = StreetDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<StreetDto> getById(@ApiParam(name = "id", value = "id для получения Street", required = true)
                                             @PathVariable("id") Long id) {
        return ResponseEntity.ok(streetService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Street")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "StreetDto", value = "StreetDto для создания Street", required = true)
                                    @RequestBody StreetDto streetDto) {
        streetService.create(streetDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление Street")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "StreetDto", value = "StreetDto для обновления Street", required = true)
                                    @RequestBody StreetDto streetDto) {
        streetService.update(streetDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Street по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Street"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Street", required = true)
                                        @PathVariable("id") Long id) {
        streetService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.RetailShiftDto;
import com.warehouse_accounting.services.interfaces.RetailShiftService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/retail_shift")
@Api(tags = "RetailShift Rest Controller")
@Tag(name = "RetailShift Rest Controller", description = "CRUD операции с RetailShift")
public class RetailShiftRestController {

    private final RetailShiftService retailShiftService;

    @Autowired
    public RetailShiftRestController(RetailShiftService retailShiftService) {
        this.retailShiftService = retailShiftService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех RetailShift",
            response = RetailShiftDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка RetailShift"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<RetailShiftDto>> getAll() {
        return ResponseEntity.ok(retailShiftService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "getById",
            notes = "Получение RetailShift по id",
            response = RetailShiftDto.class
    )

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение RetailShift"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<RetailShiftDto> getById(@ApiParam(name = "id", value = "id для получения RetailShift",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(retailShiftService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового RetailShift")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание RetailShift"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "RetailShiftDto", value = "RetailShiftDto для создания RetailShift",
            required = true) @RequestBody RetailShiftDto retailShiftDto) {
        retailShiftService.create(retailShiftDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление RetailShift")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление RetailShift"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "RetailShiftDto", value = "RetailShiftDto для обновления RetailShift",
            required = true) @RequestBody RetailShiftDto retailShiftDto) {
        retailShiftService.update(retailShiftDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление RetailShift по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление RetailShift"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого RetailShift",
            required = true) @PathVariable("id") Long id) {

        retailShiftService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}


package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PriceListDto;
import com.warehouse_accounting.services.interfaces.PriceListService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/price_list")
@Api(tags = "PriceList Rest Controller")
@Tag(name = "PriceList Rest Controller", description = "CRUD операции с PriceList")
public class PriceListRestController {

    private final PriceListService priceListService;

    public PriceListRestController(PriceListService priceListService) {
        this.priceListService = priceListService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех PriceList",
            response = PriceListDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка PriceList"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")
    })
    public ResponseEntity<List<PriceListDto>> getAll() {
        return ResponseEntity.ok(priceListService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение PriceList по id", response = PriceListDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение PriceList"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<PriceListDto> getById(@ApiParam(name = "id", value = "id для получения PriceList", required = true)
                                                @PathVariable("id") Long id) {
        return ResponseEntity.ok(priceListService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового PriceList")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание PriceList"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "PriceListDto", value = "PriceListDto для создания PriceList", required = true)
                                    @RequestBody PriceListDto priceListDto) {
        priceListService.create(priceListDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление PriceList")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление PriceList"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "PriceListDto", value = "PriceListDto для обновления PriceList", required = true)
                                    @RequestBody PriceListDto priceListDto) {
        priceListService.update(priceListDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление PriceList по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление PriceList"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого PriceList", required = true)
                                        @PathVariable("id") Long id) {
        priceListService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

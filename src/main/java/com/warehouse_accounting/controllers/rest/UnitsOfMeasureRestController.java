package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.controllers.rest.controllersInterfaces.UnitsOfMeasureRestControllerInt;
import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import com.warehouse_accounting.services.interfaces.UnitsOfMeasureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/unitsOfMeasure")
public class UnitsOfMeasureRestController implements UnitsOfMeasureRestControllerInt {

    private final UnitsOfMeasureService unitsOfMeasureService;

    public UnitsOfMeasureRestController(UnitsOfMeasureService unitsOfMeasureService) {
        this.unitsOfMeasureService = unitsOfMeasureService;
    }

    public ResponseEntity<List<UnitsOfMeasureDto>> getAll() {
        return ResponseEntity.ok(unitsOfMeasureService.getAll());
    }

    public ResponseEntity<UnitsOfMeasureDto> getById(Long id) {
        return ResponseEntity.ok(unitsOfMeasureService.getById(id));
    }

    public ResponseEntity<?> create(UnitsOfMeasureDto measureDto) {
        unitsOfMeasureService.create(measureDto);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> update(UnitsOfMeasureDto measureDto) {
        unitsOfMeasureService.update(measureDto);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> deleteById(Long id) {
        unitsOfMeasureService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
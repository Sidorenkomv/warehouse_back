
package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PaymentDto;
import com.warehouse_accounting.models.dto.PointOfSalesDto;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/retail")
@Api(tags = "Retail Rest Controller")
@Tag(name = "Retail Rest Controller", description = "CRUD операции с объектами")
public class RetailRestController {


    private final PointOfSalesService pointOfSalesService;

    public RetailRestController(PointOfSalesService pointOfSalesService) {
        this.pointOfSalesService = pointOfSalesService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все параметры рассчитываемого объекта", notes = "Возвращает список PointOfSalesDto",
            response = PointOfSalesDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа PointOfSalesDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")
    })
    public ResponseEntity<List<PointOfSalesDto>> getAll() {
        return ResponseEntity.ok(pointOfSalesService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает параметр рассчитываемого объекта с выбранным id", notes = "Возвращает PointOfSalesDto",
            response = PointOfSalesDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение PointOfSalesDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")
    })
    public ResponseEntity<PointOfSalesDto> getById(@ApiParam(name = "id", value = "Id нужного PointOfSalesDto", required = true)
                                                   @PathVariable("id") Long id) {
        return ResponseEntity.ok(pointOfSalesService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание PointOfSalesDto",
                    response = PaymentDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "PointOfSalesDto", value = "Объект PointOfSalesDto для создания",
            required = true) @RequestBody PointOfSalesDto pointOfSalesDto) {
        pointOfSalesService.create(pointOfSalesDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление PointOfSalesDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "PointOfSalesDto", value = "Объект PointOfSalesDto для обновления",
            required = true) @RequestBody PointOfSalesDto pointOfSalesDto) {
        pointOfSalesService.update(pointOfSalesDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Обновляет параметр рассчитываемого объекта c выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление PointOfSalesDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id PointOfSalesDto для удаления", required = true)
                                        @PathVariable Long id) {
        pointOfSalesService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}


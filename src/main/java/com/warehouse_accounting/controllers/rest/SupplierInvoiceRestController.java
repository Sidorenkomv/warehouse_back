package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import com.warehouse_accounting.services.interfaces.SupplierInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/supplier_invoices")
@Api(tags = "Supplier Invoice REST Controller")
@Tag(name = "Supplier Invoice REST Controller", description = "CRUD for Supplier Invoice")
public class SupplierInvoiceRestController {
    private final SupplierInvoiceService supplierInvoiceService;

    public SupplierInvoiceRestController(SupplierInvoiceService supplierInvoiceService) {
        this.supplierInvoiceService = supplierInvoiceService;
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех SupplierInvoiceDto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка SupplierInvoiceDto"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<SupplierInvoiceDto>> getAll() {
        return ResponseEntity.ok(supplierInvoiceService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение SupplierInvoice по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение SupplierInvoice"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<SupplierInvoiceDto> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(supplierInvoiceService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание SupplierInvoice")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное сохранение SupplierInvoice"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@RequestBody SupplierInvoiceDto supplierInvoiceDto) {
        supplierInvoiceService.create(supplierInvoiceDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение SupplierInvoice")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение SupplierInvoice"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@RequestBody SupplierInvoiceDto supplierInvoiceDto) {
        supplierInvoiceService.update(supplierInvoiceDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление SupplierInvoice")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление SupplierInvoice"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        supplierInvoiceService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

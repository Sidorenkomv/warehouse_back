package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.AdditionalFieldDto;
import com.warehouse_accounting.repositories.AdditionalFieldRepository;
import com.warehouse_accounting.services.interfaces.AdditionalFieldService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/additional_field")
@Api(tags = "AdditionalField Rest Controller")
@Tag(name = "AdditionalField Rest Controller", description = "CRUD операции с объектами")
public class AdditionalFieldController {

    private final AdditionalFieldService service;
    private final CheckEntityService checkEntityService;
    private final AdditionalFieldRepository repository;

    public AdditionalFieldController(AdditionalFieldService service,
                                     CheckEntityService checkEntityService,
                                     AdditionalFieldRepository repository) {
        this.service = service;
        this.checkEntityService = checkEntityService;
        this.repository = repository;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех AdditionalField",
            response = AdditionalFieldDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка AdditionalField"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<AdditionalFieldDto>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }



    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение AdditionalField по id", response = AdditionalFieldDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение AdditionalField"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<AdditionalFieldDto> getById(
            @ApiParam(name = "id", value = "id для получения AdditionalField", required = true)
            @PathVariable("id") Long id) {
        checkEntityService.checkExist(id, repository, "AdditionalField");
        return ResponseEntity.ok(service.getById(id));

    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового AdditionalField")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание AdditionalField"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<AdditionalFieldDto> create(@ApiParam(name = "AdditionalFieldDto",
            value = "AdditionalFieldDto для создания AdditionalField",
            required = true) @RequestBody AdditionalFieldDto dto) {
        System.out.println(dto);

        return ResponseEntity.ok(service.create(dto));
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление AdditionalFieldDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление AdditionalFieldDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<AdditionalFieldDto> update(@ApiParam(name = "AdditionalFieldDto",
            value = "AdditionalFieldDto для обновления AdditionalField",
            required = true) @RequestBody AdditionalFieldDto dto) {
        checkEntityService.checkExist(dto.getId(), repository, "AdditionalFieldDto");

        return ResponseEntity.ok(service.update(dto));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление AdditionalFieldDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление AdditionalFieldDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого AdditionalFieldDto",
            required = true) @PathVariable("id") Long id) {
        checkEntityService.checkExist(id, repository, "AdditionalField");
        service.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.CustomerOrderDto;
import com.warehouse_accounting.services.interfaces.CustomerOrderService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/customer_orders")
@Api(tags = "CustomerOrder Rest Controller")
@Tag(name = "CustomerOrder Rest Controller", description = "CRUD операции с объектами")
public class CustomerOrderRestController {

    private final CustomerOrderService customerOrderService;

    @Autowired
    public CustomerOrderRestController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все параметры рассчитываемого объекта",
            notes = "Возвращает список CustomerOrderDto",
            response = CustomerOrderDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа CustomerOrderDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<CustomerOrderDto>> getAll() {
        return ResponseEntity.ok(customerOrderService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает параметр рассчитываемого объекта с выбранным id",
            notes = "Возвращает CustomerOrderDto",
            response = CustomerOrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение CustomerOrderDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<CustomerOrderDto> getById(@ApiParam(name = "id", value = "Id нужного CustomerOrderDto", required = true)
                                                    @PathVariable("id") Long id) {
        return ResponseEntity.ok(customerOrderService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание CustomerOrderDto",
                    response = CustomerOrderDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "CustomerOrderDto", value = "Объект CustomerOrderDto для создания",
            required = true) @RequestBody CustomerOrderDto customerOrderDto) {
        customerOrderService.create(customerOrderDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление CustomerOrderDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "CustomerOrderDto", value = "Объект CustomerOrderDto для обновления",
            required = true) @RequestBody CustomerOrderDto customerOrderDto) {
        customerOrderService.update(customerOrderDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет параметр рассчитываемого объекта c выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление CustomerOrderDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id CustomerOrderDto для удаления", required = true)
                                        @PathVariable Long id) {
        customerOrderService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

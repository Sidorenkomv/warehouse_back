package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PostingDto;
import com.warehouse_accounting.services.interfaces.PostingService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/posting")
@Api(tags = "Posting Rest Controller")
@Tag(name = "Posting Rest Controller", description = "CRUD операции с объектами")
public class PostingRestController {

    private final PostingService postingService;

    public PostingRestController(PostingService postingService) {
        this.postingService = postingService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список PostingDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка PostingDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<PostingDto>> getAll() {
        return ResponseEntity.ok(postingService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает PostingDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение PostingDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<PostingDto> getById(@ApiParam(name = "id", value = "id для получения PostingDto",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(postingService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает Posting")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Posting"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "PostingDto", value = "объект PostingDto для создания",
            required = true) @RequestBody PostingDto postingDto) {
        postingService.create(postingDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет Posting")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Posting"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "PostingDto", value = "объект PostingDto для обновления",
            required = true) @RequestBody PostingDto postingDto) {
        postingService.update(postingDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет Posting по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Posting"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id для удаления Posting",
            required = true) @PathVariable("id") Long id) {
        postingService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.TasksDto;
import com.warehouse_accounting.services.interfaces.TasksService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks_employee")
@Api(tags = "Task Rest Controller")
@Tag(name = "Task Rest Controller", description = "API for doing some CRUD with Task")
public class TasksRestController {
    private final TasksService tasksService;

    public TasksRestController(TasksService tasksService) {
        this.tasksService = tasksService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "return List<TasksDto>",
            response = TasksDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Tasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<TasksDto>> getAll() {
        return ResponseEntity.ok(tasksService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "getById",
            notes = "return Tasks",
            response = TasksDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Tasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<TasksDto> getById(
            @ApiParam(name = "id", value = "id для получения Task", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(tasksService.getById(id));
    }

    @PostMapping
    @ApiOperation(
            value = "create",
            notes = "Create Tasks")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Tasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "TaskDto", value = "TaskDto for create Task", required = true)
            @RequestBody TasksDto taskDto) {
        tasksService.create(taskDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(
            value = "update",
            notes = "Update Tasks")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное изменение Tasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "TaskDto", value = "TaskDto for update Task", required = true)
            @RequestBody TasksDto taskDto) {
        tasksService.update(taskDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "deleteById",
            notes = "Deleting a Task by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Tasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Tasks", required = true)
            @PathVariable("id") Long id) {
        tasksService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.models.dto.TypeOfContractorDto;
import com.warehouse_accounting.services.interfaces.BonusProgramService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/bonus_program")
@Api(tags = "BonusProgram Rest")
@Tag(name = "BonusProgram Rest", description = "CRUD with BonusProgram")
public class BonusProgramRestController {
    private BonusProgramService bonusProgramService;

    @Autowired
    public BonusProgramRestController(BonusProgramService bonusProgramService) {
        this.bonusProgramService = bonusProgramService;
    }


    @GetMapping
    @ApiOperation(value = "Все Бонусные программы", notes = "return List<BonusProgramDto>", response = BonusProgramDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List<BonusProgramDto> успешно получен", response = BonusProgramDto.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<BonusProgramDto>> getAll() {
        return ResponseEntity.ok(bonusProgramService.getAll());
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Возвращает объект BonusProgramDto", notes = "Возвращает объект BonusProgramDto по его ID", response = BonusProgramDto.class)
    @io.swagger.v3.oas.annotations.responses.ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<BonusProgramDto> getById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") long id) {
        return ResponseEntity.ok(bonusProgramService.getById(id));

    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Удаляет объект BonusProgramDto", notes = "Удаляет объект BonusProgramDto по его ID", response = BonusProgramDto.class)
    @io.swagger.v3.oas.annotations.responses.ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> deleteById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") long id) {
        bonusProgramService.deleteById(id);
        return ResponseEntity.ok().build();

    }

    @PostMapping
    @ApiOperation(value = "Создает объект BonusProgramDto", notes = "Создает объект BonusProgramDto", response = BonusProgramDto.class)
    @io.swagger.v3.oas.annotations.responses.ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> create(@RequestBody BonusProgramDto bonusProgramDto) {
        bonusProgramService.create(bonusProgramDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Изменяет объект BonusProgramDto", notes = "Изменяет объект BonusProgramDto", response = BonusProgramDto.class)
    @io.swagger.v3.oas.annotations.responses.ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> update(@ApiParam(name = "BonusProgramDto", value = "Объект BonusProgramDto для обновления",
            required = true) @RequestBody BonusProgramDto bonusProgramDto) {
        System.out.println(bonusProgramDto);
        bonusProgramService.update(bonusProgramDto);
        return ResponseEntity.ok().build();
    }


}

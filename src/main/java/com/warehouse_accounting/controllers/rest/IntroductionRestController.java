package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.IntroductionDto;
import com.warehouse_accounting.services.interfaces.IntroductionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/introduction")
@Api(tags = "Introduction Rest Controller")
@Tag(name = "Introduction Controller", description = "API для проведения CRUD операций с объектами Introduction")
public class IntroductionRestController {




    private final IntroductionService introductionService;

    @Autowired
    public IntroductionRestController(IntroductionService introductionService) {
        this.introductionService = introductionService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает из базы данных список всех розниц/внесений",
            notes = "Возвращает список IntroductionDto",
            response = IntroductionDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Introduction"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<IntroductionDto>> getAll() {
        return ResponseEntity.ok(introductionService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает из базы данных розниц/внесений с указанным id",
            notes = "Возвращает IntroductionDto",
            response = IntroductionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение IntroduceDto по id"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<IntroductionDto> getById(
            @ApiParam(name = "id", value = "id для получения нужного IntroduceDto", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(introductionService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создаем новую розницу/внесение",
            notes = "Созддает новый Introduction")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Introduction"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "Introduction", value = "бъект IntroductionDto для создания Introduction", required = true)
            @RequestBody IntroductionDto introductionDto) {
        introductionService.create(introductionDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping()
    @ApiOperation(value = "Редактирует розница/внесения",
            notes = "Редактирует Introduction")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование Introduction"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "IntroductionDto", value = "Объект IntroductionDto для редактирования Introduction", required = true)
            @RequestBody IntroductionDto introductionDto) {
        introductionService.update(introductionDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет из базы данных розницу/внесения с указанным id",
            notes = "Удаляет Introduction")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Introduction"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Introduction", required = true)
            @PathVariable("id") Long id) {
        introductionService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.models.dto.IpNetworkDto;
import com.warehouse_accounting.services.interfaces.IpNetworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/ip_network")
@Api(tags = "IpNetwork Rest Controller")
@Tag(name = "IpNetwork Rest Controller", description = "CRUD операции с объектами")
public class IpNetworkRestController {
    private final IpNetworkService ipNetworkService;

    public IpNetworkRestController(IpNetworkService ipNetworkService) {
        this.ipNetworkService = ipNetworkService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все IP-сети", notes = "Возвращает список IpNetworkDto",
            response = IpNetworkDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа IpNetworkDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<IpNetworkDto>> getAll() {
        return ResponseEntity.ok(ipNetworkService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает IP-сеть с выбранным id", notes = "Возвращает IpNetworkDto",
            response = IpNetworkDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение IpNetworkDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<IpNetworkDto> getById(@ApiParam(name = "id", value = "Id нужного IpNetworkDto", required = true)
                                            @PathVariable("id") Long id) {
        return ResponseEntity.ok(ipNetworkService.getById(id));
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданную IP-сеть")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление IpNetworkDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "IpNetworkDto", value = "Объект IpNetworkDto для обновления",
            required = true) @RequestBody IpNetworkDto ipNetworkDto) {
        ipNetworkService.update(ipNetworkDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    @ApiOperation(value = "Создает переданную IP-сеть")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание IpNetworkDto",
                    response = ImageDto.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "IpNetworkDto", value = "Объект IpNetworkDto для создания",
            required = true) @RequestBody IpNetworkDto ipNetworkDto) {
        ipNetworkService.create(ipNetworkDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет IP-сеть с выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление IpNetworkDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id IpNetworkDto для удаления", required = true)
                                        @PathVariable("id") Long id) {
        ipNetworkService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}

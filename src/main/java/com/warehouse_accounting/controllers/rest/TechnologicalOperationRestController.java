package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.TechnologicalOperationDto;
import com.warehouse_accounting.services.interfaces.TechnologicalOperationService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/technological_operations")
@Api(tags = "TechnologicalOperation Rest Controller")
@Tag(name = "TechnologicalOperation Rest Controller", description = "API для работы с TechnologicalOperation")
public class TechnologicalOperationRestController {

    private final TechnologicalOperationService technologicalOperationService;

    public TechnologicalOperationRestController(TechnologicalOperationService technologicalOperationService) {
        this.technologicalOperationService = technologicalOperationService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех TechnologicalOperations",
            response = TechnologicalOperationDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно получены все TechnologicalOperations"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<List<TechnologicalOperationDto>> getAll() {
        return ResponseEntity.ok(technologicalOperationService.getAll());
    }

    @GetMapping("{id}")
    @ApiOperation(
            value = "getById",
            notes = "Получение TechnologicalOperations по id",
            response = TechnologicalOperationDto.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение TechnologicalOperation по id"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<TechnologicalOperationDto> getById(@ApiParam(name = "id", value = "id для получения TechnologicalOperation", required = true)
                                                             @PathVariable("id") Long id) {
        return ResponseEntity.ok(technologicalOperationService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание новой TechnologicalOperation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание TechnologicalOperation"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "TechnologicalOperationDto", value = "TechnologicalOperationDto для создания TechnologicalOperation", required = true)
                                    @RequestBody TechnologicalOperationDto technologicalOperationDto) {
        technologicalOperationService.create(technologicalOperationDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Редактирование TechnologicalOperation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование TechnologicalOperation"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "TechnologicalOperationDto", value = "TechnologicalOperationDto для редактирования TechnologicalOperation", required = true)
                                    @RequestBody TechnologicalOperationDto technologicalOperationDto) {
        technologicalOperationService.update(technologicalOperationDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "delete", notes = "Удаление TechnologicalOperation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление TechnologicalOperation"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "id удаляемого TechnologicalOperation")
                                    @PathVariable("id") Long id) {
        technologicalOperationService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.TechnologicalMapDto;
import com.warehouse_accounting.services.interfaces.TechnologicalMapService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class implements API for working with {@link com.warehouse_accounting.models.TechnologicalMap}
 *
 * @author pavelsmirnov
 * @version 0.1
 * Created 30.03.2021
 */
@RestController
@RequestMapping("/api/technological_map")
@Api(tags = "TechnologicalMap Rest Controller")
@Tag(name = "TechnologicalMap Rest Controller", description = "API for doing some CRUD with TechnologicalMap")
public class TechnologicalMapRestController {
    private final TechnologicalMapService technologicalMapService;

    public TechnologicalMapRestController(TechnologicalMapService technologicalMapService) {
        this.technologicalMapService = technologicalMapService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "return List<TechnologicalMapDto>",
            response = TechnologicalMapDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<TechnologicalMapDto>> getAll() {
        return ResponseEntity.ok(technologicalMapService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "getById",
            notes = "return TechnologicalMap",
            response = TechnologicalMapDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<TechnologicalMapDto> getById(
            @ApiParam(name = "id", value = "id для получения TechnologicalMap", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(technologicalMapService.getById(id));
    }

    @PostMapping
    @ApiOperation(
            value = "create",
            notes = "Create TechnologicalMap")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "TechnologicalMapDto", value = "TechnologicalMapDto for create TechnologicalMap", required = true)
            @RequestBody TechnologicalMapDto technologicalMapDto) {
        technologicalMapService.create(technologicalMapDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(
            value = "update",
            notes = "Update TechnologicalMap")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное изменение TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "TechnologicalMapDto", value = "TechnologicalMapDto for update TechnologicalMap", required = true)
            @RequestBody TechnologicalMapDto technologicalMapDto) {
        technologicalMapService.update(technologicalMapDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "deleteById",
            notes = "Deleting a TechnologicalMap by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого TechnologicalMap", required = true)
            @PathVariable("id") Long id) {
        technologicalMapService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/isDeleted/{id}")
    @ApiOperation(
            value = "isDeleted",
            notes = "Mark for deleting a TechnologicalMap by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешная маркировка на удаление TechnologicalMap"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> isDeletedById(
            @ApiParam(name = "id", value = "id удаляемого TechnologicalMap", required = true)
            @PathVariable("id") Long id) {
        technologicalMapService.isDeletedById(id);
        return ResponseEntity.ok().build();
    }
}

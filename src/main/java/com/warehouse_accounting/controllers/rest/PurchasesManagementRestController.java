package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.services.interfaces.PurchasesManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/purchases_management")
@Tag(name = "PurchasesManagement Rest Controller", description = "CRUD операции с управлением закупками")
@Api(tags = "PurchasesManagement Rest Controller")
@RequiredArgsConstructor
public class PurchasesManagementRestController {

    private final PurchasesManagementService purchasesManagementService;

    @GetMapping()
    @ApiOperation(value = "getAll", notes = "Получение списка всех закупок")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка закупок"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<List<PurchasesManagementDto>> getAll() {
        return ResponseEntity.ok(purchasesManagementService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение закупки по её id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение закупки"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<PurchasesManagementDto> getById(@ApiParam(name = "id", type = "Long", value = "Переданый в URL id, по которому нужно найти закупку")
                                                          @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(purchasesManagementService.getById(id));
    }

    @PostMapping()
    @ApiOperation(value = "create", notes = "Создание новой закупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание закупки"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> create(@ApiParam(name = "purchaseManagementDto", value = "Dto закупки, которые надо создать")
                                    @RequestBody PurchasesManagementDto purchasesManagementDto) {
        purchasesManagementService.create(purchasesManagementDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping()
    @ApiOperation(value = "update", notes = "Изменение закупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение закупки"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> update(@ApiParam(name = "purchasesManagementDto", value = "Dto закупки, которую надо обновить")
                                    @RequestBody PurchasesManagementDto purchasesManagementDto) {
        purchasesManagementService.update(purchasesManagementDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "delete", notes = "Удаление закупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление закупки"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Переданный id, по которому необходимо удалить закупку", type = "Long")
                                    @PathVariable(name = "id") Long id) {
        purchasesManagementService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

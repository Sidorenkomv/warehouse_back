package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ProductionStageDto;
import com.warehouse_accounting.services.interfaces.ProductionStageService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/production_stage")
@Api(tags = "ProductionStage Rest Controller")
@Tag(name = "ProductionStage Rest Controller", description = "CRUD операции с объектами.")
public class ProductionStageRestController {

    private final ProductionStageService productionStageService;

    public ProductionStageRestController(ProductionStageService productionStageService) {
        this.productionStageService = productionStageService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все параметры рассчитываемого объекта не архивные", notes = "Возвращает список ProductionStageDto",
            response = ProductionStageDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно получен лист ProductionStageDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<List<ProductionStageDto>> getAll() {
        return ResponseEntity.ok(productionStageService.getAll(false));
    }

    @GetMapping("/archived")
    @ApiOperation(value = "Возвращает все параметры рассчитываемого объекта архивные", notes = "Возвращает список ProductionStageDto",
            response = ProductionStageDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно получен лист ProductionStageDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<List<ProductionStageDto>> getAllArchived() {
        return ResponseEntity.ok(productionStageService.getAll(true));
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает параметр рассчитываемого объекта с выбранным id", notes = "Возвращает ProductionStageDto",
            response = ProductionStageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно получен ProductionStageDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<ProductionStageDto> getById(@ApiParam(name = "id", value = "Id нужного ProductionStageDto", required = true)
                                                      @PathVariable("id") Long id) {
        return ResponseEntity.ok(productionStageService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно создан объект ProductionStage",
                    response = ProductionStageDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<?> create(@ApiParam(name = "ProductionStageDto", value = "Объект ProductionStageDto для создания",
            required = true) @RequestBody ProductionStageDto stageDto) {
        productionStageService.create(stageDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно обновлён обьект ProductionStage",
                    response = ProductionStageDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<?> update(@ApiParam(name = "ProductionStageDto", value = "Объект ProductionStageDto для создания",
            required = true) @RequestBody ProductionStageDto stageDto) {
        productionStageService.update(stageDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет параметр рассчитываемого объекта с выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ProductionStageDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной опреции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id нужного ProductionStageDto",
            required = true) @PathVariable("id") Long id) {
        productionStageService.delete(id);
        return ResponseEntity.ok().build();
    }

}

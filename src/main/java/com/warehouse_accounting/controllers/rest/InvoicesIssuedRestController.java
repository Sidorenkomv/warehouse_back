package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.services.interfaces.InvoicesIssuedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoices_issued")
@Api(tags = "InvoicesIssued Rest Controller")
@Tag(name = "InvoiceIssued Rest Controller", description = "API для работы с накладными")
public class InvoicesIssuedRestController {

    private final InvoicesIssuedService invoicesIssuedService;

    public InvoicesIssuedRestController(InvoicesIssuedService invoicesIssuedService) {
        this.invoicesIssuedService = invoicesIssuedService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает из базы данных список всех накладных", notes = "Возвращает из базы данных список всех накладных", response = InvoicesIssuedDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список накладных успешно получен"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<List<InvoicesIssuedDto>> getAll() {
        return ResponseEntity.ok(invoicesIssuedService.getAll());
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно получена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<InvoicesIssuedDto> getById(@ApiParam(name = "id", value = "Значение поля id объекта, который требуется получить", example = "1", required = true)
                                                     @PathVariable Long id) {
        return ResponseEntity.ok(invoicesIssuedService.getById(id));
    }

    @PostMapping
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно создана"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> create(@ApiParam(name = "invoicesIssuedDto", value = "Накладная (Объект invoicesIssuedDto), которую требуется сохранить в базе данных.")
                                    @RequestBody InvoicesIssuedDto invoicesIssuedDto) {
        invoicesIssuedService.create(invoicesIssuedDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<?> update(@ApiParam(name = "invoicesIssuedDto", value = "Накладная (Объект invoicesIssuedDto), которую требуется обновить в базе данных.")
                                    @RequestBody InvoicesIssuedDto invoicesIssuedDto) {
        invoicesIssuedService.update(invoicesIssuedDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно удалена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> delete(
            @ApiParam(name = "id", value = "Значение поля id объекта, который требуется удалить", example = "1", required = true)
            @PathVariable Long id) {
        invoicesIssuedService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

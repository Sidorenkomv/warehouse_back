package com.warehouse_accounting.controllers.rest.controllersInterfaces;

import com.warehouse_accounting.models.dto.SalesChannelDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Api(tags = "SalesChannel Rest Controller")
@Tag(name = "SalesChannel Rest Controller",
    description = "CRUD операции с объектами")
public interface SalesChannelRestControllerInt {

    @GetMapping
    @ApiOperation(value = "Возвращает список объектов", notes = "Возвращает список SalesChannelDto",
        response = SalesChannelDto.class, responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
        @ApiResponse(responseCode = "404", description = "Контроллер не найден"),
        @ApiResponse(responseCode = "403", description = "Операция запрещена"),
        @ApiResponse(responseCode = "401", description = "Нет доступа к операции")})
    ResponseEntity<List<SalesChannelDto>> getAll();

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает объект SalesChannelDto",
        notes = "Возвращает объект SalesChannelDto по его ID", response = SalesChannelDto.class)
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
        @ApiResponse(responseCode = "404", description = "Контроллер не найден"),
        @ApiResponse(responseCode = "403", description = "Операция запрещена"),
        @ApiResponse(responseCode = "401", description = "Нет доступа к операции")})
    ResponseEntity<SalesChannelDto> getById(@ApiParam(name = "id",
        value = "Id  нужного SalesChannelDto",
        required = true)@PathVariable("id") Long id);

    @PostMapping
    @ApiOperation(value = "Создает объект SalesChannel")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
        @ApiResponse(responseCode = "201", description = "Объект успешно создался"),
        @ApiResponse(responseCode = "404", description = "Контроллер не найден"),
        @ApiResponse(responseCode = "403", description = "Операция запрещена"),
        @ApiResponse(responseCode = "401", description = "Нет доступа к операции")})
    ResponseEntity<?> create(@ApiParam(name = "SalesChannelDto",
        value = "Объект SalesChannelDto для создания",
        required = true)@RequestBody SalesChannelDto salesChannelDto);

    @PutMapping
    @ApiOperation(value = "Обновляет объект SalesChannelDto")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
        @ApiResponse(responseCode = "201", description = "Объект успешно обновился"),
        @ApiResponse(responseCode = "404", description = "Контроллер не найден"),
        @ApiResponse(responseCode = "403", description = "Операция запрещена"),
        @ApiResponse(responseCode = "401", description = "Нет доступа к операции")})
    ResponseEntity<?> update(@ApiParam(name = "SalesChannelDto",
        value = "Объект SalesChannelDto для обновления",
        required = true) @RequestBody SalesChannelDto salesChannelDto);

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет объект SalesChannelDto с выбранным id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
        @ApiResponse(responseCode = "204", description = "Cервер успешно обработал запрос, но в ответе были переданы только заголовки без тела сообщения"),
        @ApiResponse(responseCode = "404", description = "Контроллер не найден"),
        @ApiResponse(responseCode = "403", description = "Операция запрещена"),
        @ApiResponse(responseCode = "401", description = "Нет доступа к операции")})
    ResponseEntity<?> deleteById(@ApiParam(name = "id",
        value = "Id SalesChannelDto для удаления",
        required = true) @PathVariable("id") Long id);
}

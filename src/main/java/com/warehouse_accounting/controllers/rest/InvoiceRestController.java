package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.InvoiceDto;
import com.warehouse_accounting.services.interfaces.InvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoices")
@Api(tags = "Invoice Rest Controller")
@Tag(name = "Invoice Rest Controller", description = "API для работы с накладными")
public class InvoiceRestController {

    private final InvoiceService invoiceService;

    public InvoiceRestController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает из базы данных список всех накладных", notes = "Возвращает из базы данных список всех накладных", response = InvoiceDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список накладных успешно получен"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<List<InvoiceDto>> getAll() {
        return ResponseEntity.ok(invoiceService.getAll());
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно получена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<InvoiceDto> getById(
            @ApiParam(name = "id", value = "Значение поля id объекта, который требуется получить", example = "1", required = true)
            @PathVariable Long id) {
        return ResponseEntity.ok(invoiceService.getById(id));
    }

    @PostMapping
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно создана"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<Void> create(
            @ApiParam(name = "invoiceDto", value = "Накладная (Объект InvoiceDto), которую требуется сохранить в базе данных.")
            @RequestBody InvoiceDto invoiceDto) {
        invoiceService.create(invoiceDto);
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).build();
    }

    @PutMapping
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно обновлена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<Void> update(
            @ApiParam(name = "invoiceDto", value = "Накладная (Объект InvoiceDto), которую требуется обновить в базе данных.")
            @RequestBody InvoiceDto invoiceDto) {
        invoiceService.create(invoiceDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Накладная успешно удалена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<Void> delete(
            @ApiParam(name = "id", value = "Значение поля id объекта, который требуется удалить", example = "1", required = true)
            @PathVariable Long id) {
        invoiceService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

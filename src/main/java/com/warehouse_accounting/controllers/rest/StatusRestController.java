package com.warehouse_accounting.controllers.rest;


import com.warehouse_accounting.models.dto.StatusDto;
import com.warehouse_accounting.services.interfaces.StatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("api/statuses")
@Api(tags = "Status Rest Controller")
@Tag(name = "Status Rest Controller", description = "API for doing some CRUD with Status")
public class StatusRestController {

    private final StatusService statusService;

    public StatusRestController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping("/getAllByNameOfClass/{nameOfClass}")
    @ApiOperation(
            value = "getAllByNameOfClass",
            notes = "return List<StatusDto>",
            response = StatusDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка getAllByNameOfClass Statuses"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<StatusDto>> getAllByNameOfClass(
            @ApiParam(name = "nameOfClass", value = "nameOfClass для получения Status", required = true)
            @PathVariable("nameOfClass") String nameOfClass) {
        return ResponseEntity.ok(statusService.getAllByNameOfClass(nameOfClass));
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "return List<StatusDto>",
            response = StatusDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Statuses"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<StatusDto>> getAll() {
        return ResponseEntity.ok(statusService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "getById",
            notes = "return Status",
            response = StatusDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Status"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<StatusDto> getById(
            @ApiParam(name = "id", value = "id для получения Status", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(statusService.getById(id));
    }

    @PostMapping
    @ApiOperation(
            value = "create",
            notes = "Create Status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Status"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "StatusDto", value = "StatusDto for create Status", required = true)
            @RequestBody StatusDto statusDtoDto) {
        statusService.create(statusDtoDto);
        return ResponseEntity.ok().build();
    }


    @PutMapping
    @ApiOperation(
            value = "update",
            notes = "Update Status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное изменение Status"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "StatusDto", value = "StatusDto for update Status", required = true)
            @RequestBody StatusDto statusDtoDto) {
        statusService.update(statusDtoDto);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "deleteById",
            notes = "Deleting a Status by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Status"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Status", required = true)
            @PathVariable("id") Long id) {
        statusService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

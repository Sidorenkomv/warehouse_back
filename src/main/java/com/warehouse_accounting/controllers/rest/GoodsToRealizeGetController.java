package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGetService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/goodstorealizeget")
@Api(tags = "Goods To Realize GET Rest Controller")
@Tag(name = "Goods To Realize GET Controller", description = "API для работы с Goods To Realize GET")
public class GoodsToRealizeGetController {


    private final GoodsToRealizeGetService goodsToRealizeGetService;

    public GoodsToRealizeGetController(GoodsToRealizeGetService goodsToRealizeGetService) {
        this.goodsToRealizeGetService = goodsToRealizeGetService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех Goods To Realize GET",
            response = GoodsToRealizeGetDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Goods To Realize GET"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<GoodsToRealizeGetDto>> getAll() {
        return ResponseEntity.ok(goodsToRealizeGetService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Goods To Realize GET по id", response = GoodsToRealizeGetDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Goods To Realize GET"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<GoodsToRealizeGetDto> getById(@ApiParam(name = "id", value = "id для получения Goods To Realize GET", required = true)
                                                        @PathVariable("id") Long id) {
        return ResponseEntity.ok(goodsToRealizeGetService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Goods To Realize GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Goods To Realize GET"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "Goods To Realize GET", value = " для создания Goods To Realize GET", required = true)
                                    @RequestBody GoodsToRealizeGetDto goodsToRealizeGetDto) {
        goodsToRealizeGetService.create(goodsToRealizeGetDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Goods To Realize GET")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Goods To Realize GET"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "Goods To Realize GET", value = "для обновления Goods To Realize GET", required = true)
                                    @RequestBody GoodsToRealizeGetDto goodsToRealizeGetDto) {
        goodsToRealizeGetService.update(goodsToRealizeGetDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Goods To Realize GET по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Goods To Realize GET"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Goods To Realize GET", required = true)
                                        @PathVariable("id") Long id) {
        goodsToRealizeGetService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}


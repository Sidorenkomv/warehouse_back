package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ScriptDto;
import com.warehouse_accounting.services.interfaces.ScriptService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/scripts")
@Api(tags = "Script Rest Controller")
@Tag(name = "Script Rest Controller", description = "API для работы с Сценарии")
public class ScriptController {

    private final ScriptService scriptService;


    public ScriptController(ScriptService scriptService) {
        this.scriptService = scriptService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех сценариев",
            response = ScriptDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешно получены все сценарии"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<List<ScriptDto>> getAll() {
        return ResponseEntity.ok(scriptService.getAll());
    }

    @GetMapping("{id}")
    @ApiOperation(
            value = "getById",
            notes = "Получение сценария по id",
            response = ScriptDto.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение сценария по id"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<ScriptDto> getById(@ApiParam(name = "id", value = "id сценария", required = true)
                                             @PathVariable("id") Long id) {
        return ResponseEntity.ok(scriptService.getById(id));
    }


    @PostMapping
    @ApiOperation(value = "create", notes = "Создание сценария")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание сценария"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "ScriptDto", value = "ScriptDto для создания сценария", required = true)
                                    @RequestBody ScriptDto scriptDto) {
        scriptService.create(scriptDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Редактирование сценариев")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование сценария"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "ScriptDto", value = "ScriptDto для редактирования сценария", required = true)
                                    @RequestBody ScriptDto scriptDto) {
        scriptService.update(scriptDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "delete", notes = "Удаление сценариев")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление сценария"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")}
    )
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "id удаляемого сценария")
                                    @PathVariable("id") Long id) {
        scriptService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

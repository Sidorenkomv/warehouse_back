package com.warehouse_accounting.controllers.rest;


import com.warehouse_accounting.models.dto.InventoryDto;
import com.warehouse_accounting.services.interfaces.InventoryService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
@Api(tags = "Inventory Rest Controller")
@Tag(name = "Inventory Rest Controller", description = "CRUD операции с объектами")

public class InventoryRestController {
    private final InventoryService inventoryService;

    public InventoryRestController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список InventoryDto")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Успешное получение списка inventoryDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<InventoryDto>> getAll() {
        return ResponseEntity.ok(inventoryService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает InventoryDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение InventoryDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<InventoryDto> getById(@ApiParam(name = "id", value = "id для получения InventoryDto",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(inventoryService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает Inventory")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Inventory"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "InventoryDto", value = "объект InventoryDto для создания",
            required = true) @RequestBody InventoryDto inventoryDto) {
        inventoryService.create(inventoryDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет Inventory")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Inventory"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "InventoryDto", value = "объект InventoryDto для обновления",
            required = true) @RequestBody InventoryDto inventoryDto) {
        inventoryService.update(inventoryDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет Inventory по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Inventory"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id для удаления Inventory",
            required = true) @PathVariable("id") Long id) {
        inventoryService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

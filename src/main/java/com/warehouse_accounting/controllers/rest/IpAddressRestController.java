package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.services.interfaces.IpAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/ip_address")
@Api(tags = "IpAddress Rest Controller")
@Tag(name = "IpAddress Rest Controller", description = "CRUD операции с объектами")
public class IpAddressRestController {

    private final IpAddressService ipAddressService;

    public IpAddressRestController(IpAddressService ipAddressService) {
        this.ipAddressService = ipAddressService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все IP-адреса", notes = "Возвращает список IpAddressDto",
            response = IpAddressDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа IpAddressDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<IpAddressDto>> getAll() {
        return ResponseEntity.ok(ipAddressService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает IP-адрес с выбранным id", notes = "Возвращает IpAddressDto",
            response = IpAddressDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение IpAddressDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<IpAddressDto> getById(@ApiParam(name = "id", value = "Id нужного IpAddressDto", required = true)
                                                @PathVariable("id") Long id) {
        return ResponseEntity.ok(ipAddressService.getById(id));
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданный IP-адрес")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление IpAddressDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "IpAddressDto", value = "Объект IpAddressDto для обновления",
            required = true) @RequestBody IpAddressDto ipAddressDto) {
        ipAddressService.update(ipAddressDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    @ApiOperation(value = "Создает переданный IP-адрес")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание IpAddressDto",
                    response = ImageDto.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "IpAddressDto", value = "Объект IpNetworkDto для создания",
            required = true) @RequestBody IpAddressDto ipAddressDto) {
        ipAddressService.create(ipAddressDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет IP-адрес с выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление IpAddressDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id IpAddressDto для удаления", required = true)
                                        @PathVariable("id") Long id) {
        ipAddressService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}

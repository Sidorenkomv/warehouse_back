package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.RecycleBin;
import com.warehouse_accounting.models.dto.RecycleBinDto;
import com.warehouse_accounting.services.interfaces.RecycleBinService;
import com.warehouse_accounting.services.utilServices.ExportPdfSpreadsheetService;
import com.warehouse_accounting.services.utilServices.ExportXlsxSpreadsheetService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/api/recycle_bin")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class RecycleBinRestController {
    private final RecycleBinService recycleBinService;
    private final ExportXlsxSpreadsheetService exportXlsxSpreadsheetService;
    private final ExportPdfSpreadsheetService exportPdfSpreadsheetService;

    @GetMapping
    ResponseEntity<List<RecycleBinDto>> getAll() {
        return ResponseEntity.ok(recycleBinService.getAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<RecycleBinDto> getById(@PathVariable Long id) {
        RecycleBinDto recycleBin = recycleBinService.getById(id);
        return ResponseEntity.of(Optional.ofNullable(recycleBin));
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody RecycleBinDto recycleBinDto) {
        recycleBinService.create(recycleBinDto);
        return ResponseEntity.ok().build();
    }
    @PutMapping
    public ResponseEntity<Void> update(@RequestBody RecycleBinDto recycleBinDto) {
        recycleBinService.update(recycleBinDto);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
        recycleBinService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/export/xlsx")
    @ApiOperation(value = "Скачать таблицу со списком RecycleBinDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение таблицы xlsx RecycleBinDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Resource> getExcel() {
        String filename = "someRecycleBin.xlsx";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(exportXlsxSpreadsheetService.getWorkbook(RecycleBinDto.class));
    }

   @GetMapping("/export/pdf")
    @ApiOperation(value = "Скачать таблицу со списком RecycleBinDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение таблицы со списком RecycleBinDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Resource> getPDF() throws IOException {
        String filename = "someRecycleBin.pdf";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(exportPdfSpreadsheetService.getPdf(RecycleBinDto.class));
    }


    //не работает
    @GetMapping ("/terms-conditions/application/pdf")
    @ApiOperation(value = "Открыть в браузере таблицу со списком RecycleBinDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное открытие таблицы в браузере со списком RecycleBinDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Resource> getTermsConditions() throws IOException {
            String filename = "someRecycleBin.pdf";
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content Disposition", "inline; filename=" +filename);
            return  ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.parseMediaType("application/pdf"))
                    .body(exportPdfSpreadsheetService.getPdf(RecycleBinDto.class));
    }
}
package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import com.warehouse_accounting.services.interfaces.SupplierOrdersService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/supplier_orders")
@Api(tags = "Supplier Orders Rest Controller")
@Tag(name = "Supplier Orders Rest Controller", description = "CRUD для заказов поставищкам")
public class SupplierOrdersRestController {
    private final SupplierOrdersService supplierOrdersService;

    @Autowired
    public SupplierOrdersRestController(SupplierOrdersService supplierOrdersService) {
        this.supplierOrdersService = supplierOrdersService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все заказы поставщикам", notes = "Возвращает список SupplierOrdersDto",
            response = SupplierOrdersDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа SupplierOrdersDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<SupplierOrdersDto>> getAll() {
        return ResponseEntity.ok(supplierOrdersService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает заказ поставщикам с выбранным id", notes = "Возвращает SupplierOrdersDto",
            response = SupplierOrdersDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение SupplierOrdersDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<SupplierOrdersDto> getById(@ApiParam(name = "id", value = "Id нужного SupplierOrdersDto", required = true)
                                                     @PathVariable("id") Long id) {
        return ResponseEntity.ok(supplierOrdersService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает заказ поставищкам")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание SupplierOrdersDto",
                    response = SupplierOrdersDto.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "SupplierOrdersDto", value = "Объект SupplierOrdersDto для создания",
            required = true) @RequestBody SupplierOrdersDto supplierOrdersDto) {
        supplierOrdersService.create(supplierOrdersDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет заказ поставщикам")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление SupplierOrdersDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "SupplierOrdersDto", value = "Объект SupplierOrdersDto для обновления",
            required = true) @RequestBody SupplierOrdersDto supplierOrdersDto) {
        supplierOrdersService.update(supplierOrdersDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет заказ поставищкам с выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление SupplierOrdersDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id SupplierOrdersDto для удаления", required = true)
                                        @PathVariable("id") Long id) {
        supplierOrdersService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

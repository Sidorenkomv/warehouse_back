package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PaymentDto;
import com.warehouse_accounting.models.dto.SubscriptionDto;
import com.warehouse_accounting.services.interfaces.SubscriptionService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/subscription")
@Api(tags = "Payment Rest Controller")
@Tag(name = "Payment Rest Controller", description = "CRUD операции с объектами")
public class SubscriptionRestController {

    private final SubscriptionService subscriptionService;

    public SubscriptionRestController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все параметры рассчитываемого объекта", notes = "Возвращает список Subscription",
            response = SubscriptionDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа SubscriptionDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<SubscriptionDto>> getAll() {
        return ResponseEntity.ok(subscriptionService.getAll());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает параметр рассчитываемого объекта с выбранным id", notes = "Возвращает SubscriptionDto",
            response = SubscriptionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение SubscriptionDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<SubscriptionDto> getById(@ApiParam(name = "id", value = "Id нужного SubscriptionDto", required = true)
                                                   @PathVariable("id") Long id) {
        return ResponseEntity.ok(subscriptionService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание SubscriptionDto",
                    response = PaymentDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "SubscriptionDto", value = "Объект SubscriptionDto для создания",
            required = true) @RequestBody SubscriptionDto subscriptionDto) {
        subscriptionService.create(subscriptionDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет переданный параметр рассчитываемого объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление SubscriptionDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "SubscriptionDto", value = "Объект SubscriptionDto для обновления",
            required = true) @RequestBody SubscriptionDto subscriptionDto) {
        subscriptionService.update(subscriptionDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Обновляет параметр рассчитываемого объекта c выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление SubscriptionDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id SubscriptionDto для удаления", required = true)
                                        @PathVariable Long id) {
        subscriptionService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

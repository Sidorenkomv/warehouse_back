package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.ProductPrice;
import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.models.dto.ProductPriceForPriceListDto;
import com.warehouse_accounting.services.interfaces.ProductPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product_price")
@Api(tags = "ProductPrice Rest Controller")
@Tag(name = "ProductPrice Rest Controller", description = "CRUD операции с объектами")
@RequiredArgsConstructor
public class ProductPriceRestController {

    private final ProductPriceService productPriceService;

    @GetMapping()
    @ApiOperation(value = "Возвращает список объектов ProductPriceForPriceListDto", notes = "Возвращает List ProductPriceForPriceListDto", response = ProductPriceForPriceListDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ProductPriceForPriceListDto>> getAll() {
        return ResponseEntity.ok(productPriceService.getAll());
    }

    @GetMapping(value = "/{id}/product")
    @ApiOperation(value = "Возвращает объект ProductPriceDto", notes = "Возвращает объект ProductPriceDto по его ID", response = ProductPriceForPriceListDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductPriceDto> getProductPriceById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") long id) {
        return ResponseEntity.ok(productPriceService.getProductPriceById(id));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Возвращает объект ProductPriceForPriceListDto", notes = "Возвращает объект ProductPriceForPriceListDto по его ID", response = ProductPriceForPriceListDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductPriceForPriceListDto> getById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") long id) {
        return ResponseEntity.ok(productPriceService.getById(id));
    }

    @GetMapping(value = "/{id}/list")
    @ApiOperation(value = "Возвращает список объектов ProductPriceForPriceList для данного Product", notes = "Возвращает List ProductPriceForPriceList по ID данного Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос успешно выполнен"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ProductPriceForPriceListDto>> getListProductPriceById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") long id) {
        return ResponseEntity.ok(productPriceService.getListProductPriceByProductId(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Product Price List")
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Успешное создание Product Price List"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @io.swagger.annotations.ApiResponse(code = 403, message = "Операция запрещена"),
            @io.swagger.annotations.ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "List<ProductPriceDto>", value = "List<ProductPriceDto>", required = true)
                                    @RequestBody List<ProductPriceDto> productPriceDto) {
        productPriceService.create(productPriceDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление Product Price")
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Успешное обновление Product Price"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @io.swagger.annotations.ApiResponse(code = 403, message = "Операция запрещена"),
            @io.swagger.annotations.ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductPrice> update(@ApiParam(name = "ProductPriceDto", value = "ProductPriceDto", required = true)
                                               @RequestBody ProductPriceDto productPriceDto) {
        productPriceService.update(productPriceDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Product Price по id")
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Успешное удаление Product Price"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @io.swagger.annotations.ApiResponse(code = 403, message = "Операция запрещена"),
            @io.swagger.annotations.ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductPrice> deleteById(@ApiParam(name = "id", value = "id удаляемого Product Price", required = true)
                                                   @PathVariable("id") Long id) {
        productPriceService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}




package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.TaskDto;
import com.warehouse_accounting.services.interfaces.TaskService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Please use TasksRestController
 * <p>
 * --------------------------------
 * <p>
 * This class implements API for working with {@link com.warehouse_accounting.models.Task}
 *
 * @author pavelsmirnov
 * @version 0.1
 * Created 30.03.2021
 */
@Deprecated
@RestController
@RequestMapping("/api/tasks")
@Api(tags = "Task Rest Controller")
@Tag(name = "Task Rest Controller", description = "API for doing some CRUD with Task")
public class TaskRestController {
    private final TaskService taskService;

    public TaskRestController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "return List<TaskDto>",
            response = TaskDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Task"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<TaskDto>> getAll() {
        return ResponseEntity.ok(taskService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "getById",
            notes = "return Task",
            response = TaskDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Task"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<TaskDto> getById(
            @ApiParam(name = "id", value = "id для получения Task", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(taskService.getById(id));
    }

    @PostMapping
    @ApiOperation(
            value = "create",
            notes = "Create Task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Task"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "TaskDto", value = "TaskDto for create Task", required = true)
            @RequestBody TaskDto taskDto) {
        taskService.create(taskDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    @ApiOperation(
            value = "update",
            notes = "Update Task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное изменение Task"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "TaskDto", value = "TaskDto for update Task", required = true)
            @RequestBody TaskDto taskDto) {
        taskService.update(taskDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "deleteById",
            notes = "Deleting a Task by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Task"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Task", required = true)
            @PathVariable("id") Long id) {
        taskService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.StartScreenDto;
import com.warehouse_accounting.services.interfaces.StartScreenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/start_screen")
@Api(tags = "StartScreen Rest Controller")
@Tag(name = "StartScreen Rest Controller", description = "API для работы с настройками стартовой страницы")
@RequiredArgsConstructor
public class StartScreenRestController {
    private final StartScreenService startScreenService;

    @GetMapping
    @ApiOperation(value = "Возвращает список настроек стартовой страницы", notes = "Возвращает список StartScreenDto",
            response = StartScreenDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение настроек стартовой страницы"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<StartScreenDto>> getAll() {
        return ResponseEntity.ok(startScreenService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает настройки стартовой страницы по id пользователя", notes = "Возвращает StartScreenDto",
            response = StartScreenDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение настроек стартовой страницы"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<StartScreenDto> getById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(startScreenService.getById(id));
    }

    @PutMapping
    @ApiOperation(value = "Создает настройки стартовой страницы")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание настроек стартовой страницыв"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(
            @ApiParam(name = "StartScreenDto", value = "Объект StartScreenDto который нужно сохранить в программе")
            @RequestBody StartScreenDto startScreenDto) {
        startScreenService.create(startScreenDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    @ApiOperation(value = "Изменяет настройки стартовой страницы")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение настройки стартовой страницы"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(
            @ApiParam(name = "startScreenDto", value = "Объект startScreenDto который нужно изменить в программе")
            @RequestBody StartScreenDto startScreenDto) {
        startScreenService.update(startScreenDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет настройки стартовой страницы по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление настроек стартовой страницы"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "Значение поля Id объекта который хотим удалить", example = "1", required = true)
            @PathVariable("id") long id) {
        startScreenService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

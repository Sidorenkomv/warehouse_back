package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.services.interfaces.PurchaseForecastService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/purchase_forecast")
@Tag(name = "PurchaseForecast Rest Controller", description = "CRUD операции с прогнозами продаж")
@Api(tags = "PurchaseForecast Rest Controller")
@RequiredArgsConstructor
public class PurchaseForecastRestController {

    private final PurchaseForecastService purchaseForecastService;

    @GetMapping()
    @ApiOperation(value = "getAll", notes = "Получение списка прогнозов")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка прогнозов"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<List<PurchaseForecastDto>> getAll() {
        return ResponseEntity.ok(purchaseForecastService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение прогноза по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение прогноза"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<PurchaseForecastDto> getById(@ApiParam(name = "id", type = "Long", value = "Переданый в URL id, по которому нужно найти прогноз")
                                                       @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(purchaseForecastService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание прогноза")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание прогноза"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> create(@ApiParam(name = "purchaseForecastDto", value = "Dto прогноза, который нужно создать")
                                    @RequestBody PurchaseForecastDto purchaseForecastDto) {
        purchaseForecastService.create(purchaseForecastDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение прогноза")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение прогноза"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> update(@ApiParam(name = "purchaseForecastDto", value = "Dto прогноза, который нужно изменить")
                                    @RequestBody PurchaseForecastDto purchaseForecastDto) {
        purchaseForecastService.update(purchaseForecastDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "delete", notes = "Удаление прогноза по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление прогноза"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> delete(@ApiParam(name = "id", type = "Long", value = "Переданый в URL id прогноза, который надо удалить")
                                    @PathVariable(name = "id") Long id) {
        purchaseForecastService.delete(id);
        return ResponseEntity.ok().build();
    }
}

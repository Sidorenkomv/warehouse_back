package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.AcceptancesDto;
import com.warehouse_accounting.services.interfaces.AcceptancesService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/acceptances")
@Api(tags = "Acceptances Rest Controller")
@Tag(name = "Acceptances Controller", description = "API для проведения CRUD операций с объектами Acceptances")
public class AcceptancesRestController {

    private final AcceptancesService acceptancesService;

    @Autowired
    public AcceptancesRestController(AcceptancesService acceptancesService) {
        this.acceptancesService = acceptancesService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает из базы данных список всех приемок",
            notes = "Возвращает список AcceptancesDto",
            response = AcceptancesDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка AcceptancesDto"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<AcceptancesDto>> getAll() {
        return ResponseEntity.ok(acceptancesService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает из базы данных приемку с указанным id",
            notes = "Возвращает AcceptancesDto",
            response = AcceptancesDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение AcceptancesDto по id"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<AcceptancesDto> getById(
            @ApiParam(name = "id", value = "id для получения нужного AcceptancesDto", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(acceptancesService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает новую приемку",
            notes = "Создает Acceptances")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Acceptances"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "Acceptances", value = "Объект AcceptancesDto для создания Acceptances", required = true)
            @RequestBody AcceptancesDto acceptancesDto) {
        acceptancesService.create(acceptancesDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Редактирует приемку",
            notes = "Редактирует Acceptances")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование Acceptances"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "AcceptancesDto", value = "Объект AcceptancesDto для редактирования Acceptances", required = true)
            @RequestBody AcceptancesDto acceptancesDto) {
        acceptancesService.update(acceptancesDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет из базы данных приемку с указанным id",
            notes = "Удаляет Acceptances")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Acceptances"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Acceptances", required = true)
            @PathVariable("id") Long id) {
        acceptancesService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

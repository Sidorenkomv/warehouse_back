package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.util.ConverterDto;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employees")
@Api(tags = "Employee Rest Controller")
@Tag(name = "Employee Rest Controller", description = "CRUD операции с Employee")
public class EmployeeRestController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех Employee",
            response = EmployeeDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<EmployeeDto>> getAll() {
        List<Employee> employees = employeeService.findAll();
        return ResponseEntity.ok(
                employees.stream().map(ConverterDto::convertToDto).collect(Collectors.toList())
        );
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Employee по id", response = EmployeeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<EmployeeDto> getById(
            @ApiParam(name = "id", value = "id для получения Employee", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(
                ConverterDto.convertToDto(employeeService.findById(id))
        );
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Employee")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<EmployeeDto> create(
            @ApiParam(name = "EmployeeDto", value = "EmployeeDto для создания Employee", required = true)
            @RequestBody EmployeeDto employeeDto) {
        Employee employee = ConverterDto.convertToModel(employeeDto);
        Employee createdEmployee = employeeService.create(employee);
        return ResponseEntity.ok(ConverterDto.convertToDto(createdEmployee));
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление Employee")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )

    public ResponseEntity<EmployeeDto> update(
            @ApiParam(name = "EmployeeDto", value = "EmployeeDto для обновления Employee", required = true)
            @RequestBody EmployeeDto employeeDto) {
        Employee employee = ConverterDto.convertToModel(employeeDto);
        Employee updatedEmployee = employeeService.update(employee);
        return ResponseEntity.ok(ConverterDto.convertToDto(updatedEmployee));
    }


    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Employee по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<String> deleteById(
            @ApiParam(name = "id", value = "id удаляемого Employee", required = true)
            @PathVariable("id") Long id) {
        employeeService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

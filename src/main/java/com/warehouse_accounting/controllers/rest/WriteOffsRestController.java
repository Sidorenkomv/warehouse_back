package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.WriteOffsDto;
import com.warehouse_accounting.services.interfaces.WriteOffsService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/writeOffs")
@Api(tags = "WriteOffs Rest Controller")
@Tag(name = "WriteOffs Rest Controller", description = "CRUD операции с объектами")
public class WriteOffsRestController {

    private final WriteOffsService writeOffsService;

    public WriteOffsRestController(WriteOffsService writeOffsService) {
        this.writeOffsService = writeOffsService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список WriteOffsDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка WriteOffsDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<WriteOffsDto>> getAll() {
        return ResponseEntity.ok(writeOffsService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает WriteOffsDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение WriteOffsDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<WriteOffsDto> getById(@ApiParam(name = "id", value = "id для получения WriteOffsDto",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(writeOffsService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает WriteOffs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание WriteOffs"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "WriteOffsDto", value = "объект WriteOffsDto для создания",
            required = true) @RequestBody WriteOffsDto writeOffsDto) {
        writeOffsService.create(writeOffsDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет WriteOffs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление WriteOffs"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "WriteOffsDto", value = "объект WriteOffsDto для обновления",
            required = true) @RequestBody WriteOffsDto writeOffsDto) {
        writeOffsService.update(writeOffsDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет WriteOffs по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление WriteOffs"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id для удаления WriteOffs",
            required = true) @PathVariable("id") Long id) {
        writeOffsService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
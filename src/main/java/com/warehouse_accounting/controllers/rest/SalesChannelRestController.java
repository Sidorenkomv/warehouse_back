package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.controllers.rest.controllersInterfaces.SalesChannelRestControllerInt;
import com.warehouse_accounting.models.dto.SalesChannelDto;
import com.warehouse_accounting.services.interfaces.SalesChannelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("api/sales_channels")
@RestController
public class SalesChannelRestController implements SalesChannelRestControllerInt {

    private final SalesChannelService salesChannelService;

    public SalesChannelRestController(SalesChannelService salesChannelService) {
        this.salesChannelService = salesChannelService;
    }

    public ResponseEntity<List<SalesChannelDto>> getAll() {
        return ResponseEntity.ok(salesChannelService.getAll());
    }

    public ResponseEntity<SalesChannelDto> getById(Long id) {
        return ResponseEntity.ok(salesChannelService.getById(id));
    }

    public ResponseEntity<?> create(@RequestBody SalesChannelDto salesChannelDto) {
        salesChannelService.create(salesChannelDto);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> update(SalesChannelDto salesChannelDto) {
        salesChannelService.update(salesChannelDto);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> deleteById(Long id) {
        salesChannelService.deleteById(id);
        return ResponseEntity.ok().build();
    }


}

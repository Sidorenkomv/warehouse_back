package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.MovementDto;
import com.warehouse_accounting.services.interfaces.MovementService;
import com.warehouse_accounting.services.utilServices.ExportXlsxSpreadsheetService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/movements")
@Api(tags = "Movement Rest Controller")
@Tag(name = "Movement Rest Controller", description = "CRUD операции с объектами")
public class MovementRestController {

    private final MovementService service;
    private final ExportXlsxSpreadsheetService exportXlsxFileService;

    public MovementRestController(MovementService service,
                                  ExportXlsxSpreadsheetService exportXlsxFileService) {
        this.service = service;
        this.exportXlsxFileService = exportXlsxFileService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список MovementDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка MovementDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<MovementDto>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает MovementDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение MovementDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<MovementDto> getById(@ApiParam(name = "id", value = "id для получения MovementDto",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает Movement")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Movement"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "MovementDto", value = "объект MovementDto для создания",
            required = true) @RequestBody MovementDto movementDto) {
        service.create(movementDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет Movement")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Movement"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "MovementDto", value = "объект MovementDto для обновления",
            required = true) @RequestBody MovementDto movementDto) {
        service.update(movementDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет Movement по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Movement"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id для удаления Movement",
            required = true) @PathVariable("id") Long id) {
        service.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/export/xlsx")
    @ApiOperation(value = "Скачать таблицу со списком MovementDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение таблицы со списком MovementDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Resource> getExcel() {
        String filename = "some.xlsx";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(exportXlsxFileService.getWorkbook(MovementDto.class));
    }
}

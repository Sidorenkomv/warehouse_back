package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.SerialNumberDto;
import com.warehouse_accounting.services.interfaces.SerialNumberService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/serial_number")
@Api(tags = "SerialNumber Rest Controller")
@Tag(name = "SerialNumber Controller", description = "API для проведения CRUD операций с объектами SerialNumber")
public class SerialNumberRestController {
    private final SerialNumberService serialNumberService;

    public SerialNumberRestController(SerialNumberService serialNumberService) {
        this.serialNumberService = serialNumberService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список SerialNumberDto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка SerialNumberDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<SerialNumberDto>> getAll() {
        return ResponseEntity.ok(serialNumberService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает SerialNumberDto по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение SerialNumberDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<SerialNumberDto> getById(@ApiParam(name = "id", value = "id для получения SerialNumberDto",
            required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(serialNumberService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает SerialNumber")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание SerialNumber"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "SerialNumberDto", value = "объект SerialNumberDto для создания",
            required = true) @RequestBody SerialNumberDto serialNumberDto) {
        serialNumberService.create(serialNumberDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет SerialNumber")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление SerialNumber"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "SerialNumberDto", value = "объект SerialNumberDto для обновления",
            required = true) @RequestBody SerialNumberDto serialNumberDto) {
        serialNumberService.update(serialNumberDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет SerialNumber по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление SerialNumber"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id для удаления SerialNumber",
            required = true) @PathVariable("id") Long id) {
        serialNumberService.deleteById(id);
        return ResponseEntity.ok().build();
    }


}

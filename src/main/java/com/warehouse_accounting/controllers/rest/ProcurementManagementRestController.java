package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ProcurementManagementDto;
import com.warehouse_accounting.services.interfaces.ProcurementManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/procurement_management")
@Api(tags = "Procurement Management REST Controller")
@Tag(name = "Procurement Management REST Controller", description = "CRUD for Procurement Management")
public class ProcurementManagementRestController {
    private final ProcurementManagementService procurementManagementService;

    public ProcurementManagementRestController(ProcurementManagementService procurementManagementService) {
        this.procurementManagementService = procurementManagementService;
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех InvoiceReceivedDto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка ProcurementManagementDto"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ProcurementManagementDto>> getAll() {
        return ResponseEntity.ok(procurementManagementService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение ProcurementManagement по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение ProcurementManagement"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProcurementManagementDto> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(procurementManagementService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание ProcurementManagement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное сохранение ProcurementManagement"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@RequestBody ProcurementManagementDto procurementManagementDto) {
        procurementManagementService.create(procurementManagementDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение ProcurementManagement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение ProcurementManagement"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@RequestBody ProcurementManagementDto procurementManagementDto) {
        procurementManagementService.update(procurementManagementDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление ProcurementManagement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление ProcurementManagement"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        procurementManagementService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}

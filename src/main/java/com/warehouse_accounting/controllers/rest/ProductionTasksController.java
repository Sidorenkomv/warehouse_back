package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ProductionTasksDto;
import com.warehouse_accounting.services.interfaces.ProductionTasksService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/production_tasks")
@Api(tags = "ProductionTasks Rest Controller")
@Tag(name = "ProductionTasks Rest Controller", description = "CRUD операции с объектами")
public class ProductionTasksController {

    private final ProductionTasksService productionTasksService;

    public ProductionTasksController(ProductionTasksService productionTasksService) {
        this.productionTasksService = productionTasksService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех ProductionTasks",
            response = ProductionTasksDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка ProductionTasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ProductionTasksDto>> getAll() {
        return ResponseEntity.ok(productionTasksService.getAll());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение ProductionTasks по id", response = ProductionTasksDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ProductionTasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductionTasksDto> getById(
            @ApiParam(name = "id", value = "id для получения ProductionTasks", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(productionTasksService.getById(id));

    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового ProductionTasks")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ProductionTasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "ProductionTasksDto",
            value = "ProductionTasksDto для создания ProductionTasks",
            required = true) @RequestBody ProductionTasksDto productionTasksDto) {
        productionTasksService.create(productionTasksDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление ProductionTasks")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ProductionTasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "ProductionTasksDto",
            value = "ProductionTasksDto для обновления ProductionTasks",
            required = true) @RequestBody ProductionTasksDto productionTasksDto) {
        productionTasksService.update(productionTasksDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление ProductionTasks по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ProductionTasks"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого ProductionTasks",
            required = true) @PathVariable("id") Long id) {
        productionTasksService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

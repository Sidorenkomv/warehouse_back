package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.RegionDto;
import com.warehouse_accounting.services.interfaces.RegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/regions")
@Api(tags = "Region Rest Controller")
@Tag(name = "Region Rest Controller", description = "CRUD операции с Region")
public class RegionRestController {

    private final RegionService regionService;

    public RegionRestController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка Region",
            response = RegionDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )

    public ResponseEntity<List<RegionDto>> getAll() {
        return ResponseEntity.ok(regionService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Region по id", response = RegionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<RegionDto> getById(@ApiParam(name = "id", value = "id для получения Region", required = true)
                                             @PathVariable("id") Long id) {
        return ResponseEntity.ok(regionService.getById(id));
    }

    @GetMapping("/code/{code}")
    @ApiOperation(value = "getByCode", notes = "Получение Region по коду", response = RegionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<RegionDto> getByCode(@ApiParam(name = "code", value = "код для получения Region", required = true)
                                             @PathVariable("code") String code) {
        return ResponseEntity.ok(regionService.getByCode(code));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Region")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "RegionDto", value = "RegionDto для создания Region", required = true)
                                    @RequestBody RegionDto regionDto) {
        regionService.create(regionDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление Region")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "RegionDto", value = "RegionDto для обновления Region", required = true)
                                    @RequestBody RegionDto regionDto) {
        regionService.update(regionDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Region по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Region"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Region", required = true)
                                        @PathVariable("id") Long id) {
        regionService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

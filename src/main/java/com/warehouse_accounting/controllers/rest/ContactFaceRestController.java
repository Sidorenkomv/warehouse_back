package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ContactFaceDto;
import com.warehouse_accounting.services.interfaces.ContactFaceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contact_face")
@Api(tags = "ContactFace RESTController")
@Tag(name = "ContactFace RESTController", description = "controller for doing some CRUD-magic with сontact_face:)")

public class ContactFaceRestController {
    private final ContactFaceService contactFaceService;

    public ContactFaceRestController(ContactFaceService contactFaceService) {
        this.contactFaceService = contactFaceService;
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех СontactFace")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка имеющихся СontactFace"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ContactFaceDto>> getAll() {
        return ResponseEntity.ok(contactFaceService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение ContactFace по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение ContactFace"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ContactFaceDto> getById(@PathVariable("id") long id) {
        return ResponseEntity.ok(contactFaceService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Сохранение ContactFace")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное сохранение ContactFace"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@RequestBody ContactFaceDto contactFaceDto) {
        contactFaceService.create(contactFaceDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение ContactFace")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение ContactFace"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@RequestBody ContactFaceDto contactFaceDto) {
        contactFaceService.update(contactFaceDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление ContactFace")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление ContactFace"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        contactFaceService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.services.interfaces.PurchaseHistoryOfSalesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/purchase_history")
@Tag(name = "PurchaseHistoryOfSales Rest Controller", description = "CRUD операции с историями продаж")
@Api(tags = "PurchaseHistoryOfSales Rest Controller")
@RequiredArgsConstructor
public class PurchaseHistoryOfSalesRestController {

    private final PurchaseHistoryOfSalesService purchasehistoryOfSalesService;

    @GetMapping()
    public ResponseEntity<List<PurchaseHistoryOfSalesDto>> getAll() {
        return ResponseEntity.ok(purchasehistoryOfSalesService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getAll", notes = "Получение списка историй")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение историй"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<PurchaseHistoryOfSalesDto> getById(@ApiParam(name = "id", value = "Переданый в URL id, по которому нужно найти историю закупки", required = true)
                                                             @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(purchasehistoryOfSalesService.getById(id));
    }

    @PostMapping()
    @ApiOperation(value = "create", notes = "Создание новой истории продаж")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание новой истории продаж"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> create(@ApiParam(name = "purchaseHistoryOfSalesDto", value = "DTO истории, которую надо создать")
                                    @RequestBody PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto) {
        purchasehistoryOfSalesService.create(purchaseHistoryOfSalesDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение истории продаж")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение истории продаж"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> update(@ApiParam(name = "purchaseHistoryOfSalesDto", value = "DTO истории, которую надо изменить")
                                    @RequestBody PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto) {
        purchasehistoryOfSalesService.update(purchaseHistoryOfSalesDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "delete", notes = "Удаление истории продаж")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление истории продаж"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> delete(@ApiParam(name = "id", type = "Long", value = "Передайный в URL id истории, которую надо удалить")
                                    @PathVariable(name = "id") Long id) {
        purchasehistoryOfSalesService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

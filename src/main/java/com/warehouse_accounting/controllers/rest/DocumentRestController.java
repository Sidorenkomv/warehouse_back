package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.services.interfaces.DocumentService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/documents")
@Api(tags = "Documents Rest Controller")
@Tag(name = "Documents Orders Rest Controller", description = "CRUD для работы с Document")
public class DocumentRestController {

    private final DocumentService documentService;

    @Autowired
    public DocumentRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все документы", notes = "Возвращает список SupplierOrdersDto",
            response = DocumentDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа DocumentDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<DocumentDto>> getAll() {
        return ResponseEntity.ok(documentService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает документ с выбранным id", notes = "Возвращает DocumentDto",
            response = DocumentDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение DocumentDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<DocumentDto> getById(@ApiParam(name = "id", value = "Id нужного DocumentDto", required = true)
                                               @PathVariable("id") Long id) {
        return ResponseEntity.ok(documentService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает новый документ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание DocumentDto",
                    response = DocumentDto.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "DocumentDto", value = "Объект DocumentDto для создания",
            required = true) @RequestBody DocumentDto documentDto) {
        documentService.create(documentDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет документ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление DocumentDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "DocumentDto", value = "Объект DocumentDto для обновления",
            required = true) @RequestBody DocumentDto documentDto) {
        documentService.update(documentDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет документ с выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление DocumentDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id DocumentDto для удаления", required = true)
                                        @PathVariable("id") Long id) {
        documentService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

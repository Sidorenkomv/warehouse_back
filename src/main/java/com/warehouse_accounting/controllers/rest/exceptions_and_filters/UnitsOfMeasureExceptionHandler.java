package com.warehouse_accounting.controllers.rest.exceptions_and_filters;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UnitsOfMeasureExceptionHandler {

    @ExceptionHandler(NonExistingUnitException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNonExistingUnit() {
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.services.interfaces.PurchaseCurrentBalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/purchase_current_balance")
@Tag(name = "PurchaseCurrentBalance Rest Controller", description = "CRUD операции с текущими остатками")
@Api(tags = "PurchaseCurrentBalance Rest Controller")
@RequiredArgsConstructor
public class PurchaseCurrentBalanceRestController {

    private final PurchaseCurrentBalanceService purchaseCurrentBalanceService;

    @GetMapping()
    @ApiOperation(value = "getAll", notes = "Получение списка текущих остатков")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка остатков"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<List<PurchaseCurrentBalanceDto>> getAll() {
        return ResponseEntity.ok(purchaseCurrentBalanceService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение остатка по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение остатка по id"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<PurchaseCurrentBalanceDto> getById(@ApiParam(name = "id", type = "Long", value = "Переданый в URL id, по которому нужно найти текущий остаток")
                                                             @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(purchaseCurrentBalanceService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создания текущего остатка")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создания текущего остатка"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> create(@ApiParam(name = "purchaseCurrentBalanceDto", value = "Dto остатка, который нужно создать")
                                    @RequestBody PurchaseCurrentBalanceDto purchaseCurrentBalanceDto) {
        purchaseCurrentBalanceService.create(purchaseCurrentBalanceDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение текущего остатка")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение текущего остатка"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> update(@ApiParam(name = "purchaseCurrentBalanceDto", value = "Dto остатка, который нужно изменить")
                                    @RequestBody PurchaseCurrentBalanceDto purchaseCurrentBalanceDto) {
        purchaseCurrentBalanceService.update(purchaseCurrentBalanceDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "delete", notes = "Удаление текущего остатка по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление текущего остатка по id"),
            @ApiResponse(responseCode = "401", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден")
    })
    public ResponseEntity<?> delete(@ApiParam(name = "id", type = "Long", value = "Переданый в URL id, по которому нужно удалить текущий остаток")
                                    @PathVariable(name = "id") Long id) {
        purchaseCurrentBalanceService.delete(id);
        return ResponseEntity.ok().build();
    }
}

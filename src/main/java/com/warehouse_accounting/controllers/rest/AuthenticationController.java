package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.dto.JwtDtoRequest;
import com.warehouse_accounting.models.dto.JwtDtoResponse;
import com.warehouse_accounting.repositories.EmployeeRepository;
import com.warehouse_accounting.security.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

    private final JwtTokenUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public AuthenticationController(JwtTokenUtil jwtUtil, AuthenticationManager authenticationManager, EmployeeRepository employeeRepository) {
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/auth/token/")
    public ResponseEntity<JwtDtoResponse> getToken(@RequestBody JwtDtoRequest request) {
        JwtDtoResponse jwtDtoResponse = new JwtDtoResponse();

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            Employee employee = (Employee) authentication.getPrincipal();

            if (request.isRemember()) {
                jwtDtoResponse.setJwtToken(jwtUtil.createRefreshToken(employee));
            } else {
                jwtDtoResponse.setJwtToken(jwtUtil.createAccessToken(employee));
            }
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Введены неправильно имя пользователя/пароль", e);
        }
        return new ResponseEntity<>(jwtDtoResponse, HttpStatus.OK);
    }

    @GetMapping("/auth/check_token")
    public ResponseEntity<Void> checkAuthorization() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        if (userDetails == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().equals("ROLE_USER")) {
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}

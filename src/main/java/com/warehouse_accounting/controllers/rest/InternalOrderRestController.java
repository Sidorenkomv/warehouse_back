package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.InternalOrderDto;
import com.warehouse_accounting.services.interfaces.InternalOrderService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/internal_orders")
@Api(tags = "InternalOrder Rest Controller")
@Tag(name = "InternalOrder Controller", description = "API для проведения CRUD операций с объектами InternalOrder")
public class InternalOrderRestController {

    private final InternalOrderService internalOrderService;

    public InternalOrderRestController(InternalOrderService internalOrderService) {
        this.internalOrderService = internalOrderService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает из базы данных список всех внутренних заказов",
            notes = "Возвращает список InternalOrderDto",
            response = InternalOrderDto.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка InternalOrder"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<List<InternalOrderDto>> getAll() {
        return ResponseEntity.ok(internalOrderService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает из базы данных внутренний заказ с указанным id",
            notes = "Возвращает InternalOrderDto",
            response = InternalOrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение InternalOrderDto по id"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<InternalOrderDto> getById(
            @ApiParam(name = "id", value = "id для получения нужного InternalOrderDto", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(internalOrderService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает новый внутренний заказ",
            notes = "Создает InternalOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание InternalOrder"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> create(
            @ApiParam(name = "InternalOrder", value = "Объект InternalOrderDto для создания InternalOrder", required = true)
            @RequestBody InternalOrderDto internalOrderDto) {
        internalOrderService.create(internalOrderDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Редактирует внутренний заказ",
            notes = "Редактирует InternalOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование InternalOrder"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> update(
            @ApiParam(name = "InternalOrderDto", value = "Объект InternalOrderDto для редактирования InternalOrder", required = true)
            @RequestBody InternalOrderDto internalOrderDto) {
        internalOrderService.update(internalOrderDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет из базы данных внутренний заказ с указанным id",
            notes = "Удаляет InternalOrder")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление InternalOrder"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 500, message = "Ошибка сервера")})
    public ResponseEntity<?> deleteById(
            @ApiParam(name = "id", value = "id удаляемого InternalOrder", required = true)
            @PathVariable("id") Long id) {
        internalOrderService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

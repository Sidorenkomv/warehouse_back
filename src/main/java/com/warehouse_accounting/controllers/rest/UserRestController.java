package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.util.ConverterDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth/login")
@Api(tags = "User Rest Controller")
@Tag(name = "User Rest Controller")
public class UserRestController {

    private final EmployeeService employeeService;

    public UserRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/{name}")
    @ApiOperation(value = "getByEmail", notes = "Получение Employee по email", response = EmployeeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Employee"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<EmployeeDto> getByEmail(@ApiParam(name = "name", value = "name для получения Employee", required = true) @PathVariable("name") String name) {
        return ResponseEntity.ok(
                ConverterDto.convertToDto((Employee) employeeService.loadUserByUsername(name))
        );
    }
}

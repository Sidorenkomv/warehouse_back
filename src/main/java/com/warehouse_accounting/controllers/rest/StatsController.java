package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.StartScreenDto;
import com.warehouse_accounting.models.dto.StatsDto;
import com.warehouse_accounting.services.interfaces.StatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stats")
@Api(tags = "Stats Rest Controller")
@Tag(name = "Stats Rest Controller",
        description = "CRUD операции с объектами")
public class StatsController {

    private final StatsService statsService;

    @Autowired
    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает статистику для экрана показателей", notes = "Возвращает StatsDto",
            response = StartScreenDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение статистики"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<StatsDto> getStats() {
        return ResponseEntity.ok(statsService.getStatsDto());
    }
}

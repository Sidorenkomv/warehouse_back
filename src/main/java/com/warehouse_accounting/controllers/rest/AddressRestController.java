package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.services.interfaces.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/addresses")
@Api(tags = "Address RESTController")
@Tag(name = "Address RESTController", description = "controller for doing some CRUD-magic with addresses")
public class AddressRestController {
    private final AddressService addressService;

    public AddressRestController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех Address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<AddressDto>> getAll() {
        return ResponseEntity.ok(addressService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Address по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<AddressDto> getById(@PathVariable("id") long id) {
        return ResponseEntity.ok(addressService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Сохранение Address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное сохранение Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@RequestBody AddressDto addressDto) {
        addressService.create(addressDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение Address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@RequestBody AddressDto addressDto) {
        addressService.update(addressDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        addressService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/fulladdress/{fulladdress}")
    @ApiOperation(value = "getByFullAddress", notes = "Получение Address по полному адресу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение Address"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<AddressDto> getByFullAddress(@PathVariable("fulladdress") String fullAddress) {
        return ResponseEntity.ok(addressService.getByFullAddress(fullAddress));
    }
}

package com.warehouse_accounting.controllers.rest;


import com.warehouse_accounting.models.dto.DiscountTypeDto;
import com.warehouse_accounting.services.interfaces.DiscountTypeService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/discount_type")
@Api(tags = "DiscountType Rest Controller")
@Tag(name = "DiscountType Rest Controller", description = "CRUD операции с объектами")
public class DiscountTypeRestController {

    private final DiscountTypeService discountTypeService;

    public DiscountTypeRestController(DiscountTypeService discountTypeService) {
        this.discountTypeService = discountTypeService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех DiscountType",
            response = DiscountTypeDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<DiscountTypeDto>> getAll() {
        return ResponseEntity.ok(discountTypeService.getAll());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение DiscountType по id", response = DiscountTypeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<DiscountTypeDto> getById(@ApiParam(name = "id", value = "id для получения DiscountType", required = true) @PathVariable("id") Long id) {
        return ResponseEntity.ok(discountTypeService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового DiscountType")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "DiscountTypeDto", value = "DiscountTypeDto для создания DiscountType", required = true) @RequestBody DiscountTypeDto discountTypeDto) {
        discountTypeService.create(discountTypeDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление DiscountType")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "DiscountTypeDto", value = "DiscountTypeDto для обновления DiscountType", required = true) @RequestBody DiscountTypeDto discountTypeDto) {
        discountTypeService.update(discountTypeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление DiscountType по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого DiscountType", required = true) @PathVariable("id") Long id) {
        discountTypeService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

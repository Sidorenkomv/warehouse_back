package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGiveService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/goodstorealizegive")
@Api(tags = "Goods To Realize GIVE Rest Controller")
@Tag(name = "Goods To Realize GIVE Rest Controller", description = "API для работы с Goods To Realize GIVE")
public class GoodsToRealizeGiveController {
    private final GoodsToRealizeGiveService goodsToRealizeGiveService;

    public GoodsToRealizeGiveController(GoodsToRealizeGiveService goodsToRealizeGiveService) {
        this.goodsToRealizeGiveService = goodsToRealizeGiveService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех Goods To Realize GIVE",
            response = GoodsToRealizeGiveDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Goods To Realize GIVE"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<GoodsToRealizeGiveDto>> getAll() {
        return ResponseEntity.ok(goodsToRealizeGiveService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Goods To Realize GIVE по id", response = GoodsToRealizeGiveDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Goods To Realize GIVE"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<GoodsToRealizeGiveDto> getById(@ApiParam(name = "id", value = "id для получения Goods To Realize GIVE", required = true)
                                                         @PathVariable("id") Long id) {
        return ResponseEntity.ok(goodsToRealizeGiveService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Goods To Realize GIVE")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Goods To Realize GIVE"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "Goods To Realize GIVE", value = " для создания Goods To Realize GIVE", required = true)
                                    @RequestBody GoodsToRealizeGiveDto goodsToRealizeGetDto) {
        goodsToRealizeGiveService.create(goodsToRealizeGetDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Goods To Realize GIVE")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Goods To Realize GIVE"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "Goods To Realize GIVE", value = "для обновления Goods To Realize GIVE", required = true)
                                    @RequestBody GoodsToRealizeGiveDto goodsToRealizeGetDto) {
        goodsToRealizeGiveService.update(goodsToRealizeGetDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Goods To Realize GIVE по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Goods To Realize GIVE"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Goods To Realize GIVE", required = true)
                                        @PathVariable("id") Long id) {
        goodsToRealizeGiveService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}


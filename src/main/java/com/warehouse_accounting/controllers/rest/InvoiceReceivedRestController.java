package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.InvoiceReceivedDto;
import com.warehouse_accounting.services.interfaces.InvoiceReceivedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoice_received")
@Api(tags = "Invoice Received REST Controller")
@Tag(name = "Invoice Received REST Controller", description = "CRUD for Invoice Received")
public class InvoiceReceivedRestController {
    private final InvoiceReceivedService invoiceReceivedService;

    public InvoiceReceivedRestController(InvoiceReceivedService invoiceReceivedService) {
        this.invoiceReceivedService = invoiceReceivedService;
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех InvoiceReceivedDto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка InvoiceReceivedDto"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoiceReceivedDto>> getAll() {
        return ResponseEntity.ok(invoiceReceivedService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение InvoiceReceived по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение InvoiceReceived"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoiceReceivedDto> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(invoiceReceivedService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание InvoiceReceived")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное сохранение InvoiceReceived"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@RequestBody InvoiceReceivedDto invoiceReceivedDto) {
        invoiceReceivedService.create(invoiceReceivedDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение InvoiceReceived")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение InvoiceReceived"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@RequestBody InvoiceReceivedDto invoiceReceivedDto) {
        invoiceReceivedService.update(invoiceReceivedDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление InvoiceReceived")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление InvoiceReceived"),
            @ApiResponse(responseCode = "404", description = "Данный контроллер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        invoiceReceivedService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.BuildingDto;
import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.repositories.BuildingRepository;
import com.warehouse_accounting.repositories.CityRepository;
import com.warehouse_accounting.services.interfaces.BuildingService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/buildings")
@Api(tags = "Building Rest Controller")
@Tag(name = "Building Rest Controller", description = "CRUD операции с Building")
public class BuildingRestController {

    private final BuildingService buildingService;

    private final CheckEntityService checkEntityService;
    private final BuildingRepository repository;

    public BuildingRestController(
            BuildingService buildingService,
            CheckEntityService checkEntityService,
            BuildingRepository repository
    ) {
        this.buildingService = buildingService;
        this.checkEntityService = checkEntityService;
        this.repository = repository;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка Building по коду region+city+street",
            response = BuildingDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<BuildingDto>> getAll(
            @ApiParam(name = "regionCityStreetCode", value = "код region+city+street для списка Building")
            @RequestParam(name = "regionCityStreetCode", defaultValue = "") String regionCityStreetCode
    ){
        return ResponseEntity.ok(buildingService.getAll(regionCityStreetCode));
    }

    @GetMapping("/slice")
    @ApiOperation(
            value = "getSlice",
            notes = "Получение списка Building по имени и коду region+city+street начиная с заданного смещения и указанной длины ",
            response = BuildingDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<BuildingDto>> getSlice(
            @ApiParam(name = "offset", value = "смещение для списка Building", required = true)
            @RequestParam(name = "offset", required = true) int offset,
            @ApiParam(name = "limit", value = "ограничение размера для списка Building", required = true)
            @RequestParam(name = "limit", required = true) int limit,
            @ApiParam(name = "name", value = "фильтр name для списка Building")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCityStreetCode", value = "код region+city+street для списка Building")
            @RequestParam(name = "regionCityStreetCode", defaultValue = "") String regionCityStreetCode
    ){
        return ResponseEntity.ok(buildingService.getSlice(offset, limit, name, regionCityStreetCode));
    }

    @GetMapping("/count")
    @ApiOperation(
            value = "getCount",
            notes = "Получение размера списка Building с применением фильтра name и кода коду region+city+street",
            response = int.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение размера Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Integer> getCount(
            @ApiParam(name = "name", value = "фильтр name для получения размера списка Building")
            @RequestParam(name = "name", defaultValue = "") String name,
            @ApiParam(name = "regionCityStreetCode", value = "код region+city+street для списка Building")
            @RequestParam(name = "regionCityStreetCode", defaultValue = "") String regionCityStreetCode
    ) {
        return ResponseEntity.ok(buildingService.getCount(name, regionCityStreetCode));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Building по id", response = BuildingDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<BuildingDto> getById(@ApiParam(name = "id", value = "id для получения Building", required = true)
                                              @PathVariable("id") Long id) {
        checkEntityService.checkExist(id, repository, "Building");
        return ResponseEntity.ok(buildingService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Building")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "BuildingDto", value = "BuildingDto для создания Building", required = true)
                                    @RequestBody BuildingDto buildingDto) {
        buildingService.create(buildingDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление Building")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "BuildingDto", value = "BuildingDto для обновления Building", required = true)
                                    @RequestBody BuildingDto buildingDto) {
        checkEntityService.checkExist(buildingDto.getId(), repository, "Building");
        buildingService.update(buildingDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Building по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Building"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Building", required = true)
                                        @PathVariable("id") Long id) {
        checkEntityService.checkExist(id, repository, "Building");
        buildingService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

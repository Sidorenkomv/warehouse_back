package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ProductionProcessTechnologyDto;
import com.warehouse_accounting.services.interfaces.ProductionProcessTechnologyService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/production_process_technology")
@Api(tags = "ProductionProcessTechnology Rest Controller")
@Tag(name = "ProductionProcessTechnology Rest Controller", description = "CRUD операции с объектами")
public class ProductionProcessTechnologyController {

    private final ProductionProcessTechnologyService productionProcessTechnologyService;

    public ProductionProcessTechnologyController(ProductionProcessTechnologyService productionProcessTechnologyService) {
        this.productionProcessTechnologyService = productionProcessTechnologyService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех ProductionProcessTechnology",
            response = ProductionProcessTechnologyDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка ProductionProcessTechnology"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ProductionProcessTechnologyDto>> getAll() {
        return ResponseEntity.ok(productionProcessTechnologyService.getAll());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение ProductionProcessTechnology по id", response = ProductionProcessTechnologyDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ProductionProcessTechnology"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ProductionProcessTechnologyDto> getById(
            @ApiParam(name = "id", value = "id для получения ProductionProcessTechnology", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(productionProcessTechnologyService.getById(id));

    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового ProductionProcessTechnology")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ProductionProcessTechnology"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "ProductionProcessTechnologyDto",
            value = "ProductionProcessTechnologyDto для создания ProductionProcessTechnology",
            required = true) @RequestBody ProductionProcessTechnologyDto productionProcessTechnologyDto) {
        productionProcessTechnologyService.create(productionProcessTechnologyDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление ProductionProcessTechnologyType")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ProductionProcessTechnologyType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "ProductionProcessTechnologyDto",
            value = "ProductionProcessTechnologyDto для обновления ProductionProcessTechnology",
            required = true) @RequestBody ProductionProcessTechnologyDto productionProcessTechnologyDto) {
        productionProcessTechnologyService.update(productionProcessTechnologyDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление ProductionProcessTechnology по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ProductionProcessTechnology"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого ProductionProcessTechnology",
            required = true) @PathVariable("id") Long id) {
        productionProcessTechnologyService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.DiscountDto;
import com.warehouse_accounting.services.interfaces.DiscountService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/discount")
@Api(tags = "Discount Rest Controller")
@Tag(name = "Discount Rest Controller", description = "CRUD операции с объектами")
public class DiscountRestController {

    private final DiscountService discountService;

    public DiscountRestController(DiscountService discountService) {
        this.discountService = discountService;
    }

    @GetMapping
    @ApiOperation(
            value = "getAll",
            notes = "Получение списка всех Discount",
            response = DiscountDto.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Discount"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<DiscountDto>> getAll() {
        return ResponseEntity.ok(discountService.getAll());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение Discount по id", response = DiscountDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение Discount"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<DiscountDto> getById(
            @ApiParam(name = "id", value = "id для получения Discount", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(discountService.getById(id));

    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Создание нового Discount")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание Discount"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "DiscountDto", value = "DiscountDto для создания Discount",
            required = true) @RequestBody DiscountDto discountDto) {
        discountService.create(discountDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Обновление DiscountType")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление DiscountType"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "DiscountDto", value = "DiscountDto для обновления Discount",
            required = true) @RequestBody DiscountDto discountDto) {
        discountService.update(discountDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "deleteById", notes = "Удаление Discount по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Discount"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "id удаляемого Discount", required = true) @PathVariable("id") Long id) {
        discountService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

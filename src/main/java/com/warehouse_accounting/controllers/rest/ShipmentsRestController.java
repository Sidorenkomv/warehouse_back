package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.CustomerOrderDto;
import com.warehouse_accounting.models.dto.ShipmentsDto;
import com.warehouse_accounting.services.interfaces.ShipmentsService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/rest_shipments")
@Api(tags = "Shipments Rest Controller")
@Tag(name = "Shipments Rest Controller", description = "CRUD операции с объектами отгрузка")
public class ShipmentsRestController {

    private final ShipmentsService shipmentsService;

    @Autowired
    public ShipmentsRestController(ShipmentsService shipmentsService) {
        this.shipmentsService = shipmentsService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает все отгрузки",
            notes = "Возвращает список ShipmentsDto",
            response = CustomerOrderDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ShipmentsDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<ShipmentsDto>> getAll() {
        return ResponseEntity.ok(shipmentsService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает отгрузку с выбранным id",
            notes = "Возвращает ShipmentsDto",
            response = CustomerOrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ShipmentsDto"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<ShipmentsDto> getById(@ApiParam(name = "id", value = "Id нужного ShipmentsDto", required = true)
                                                @PathVariable("id") Long id) {
        return ResponseEntity.ok(shipmentsService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает отгрузку")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ShipmentsDto",
                    response = ShipmentsDto.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 402, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "ShipmentsDto", value = "Объект ShipmentsDto для создания",
            required = true) @RequestBody ShipmentsDto shipmentsDto) {
        shipmentsService.create(shipmentsDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Обновляет отгрузку")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ShipmentsDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "ShipmentsDto", value = "Объект ShipmentsDto для обновления",
            required = true) @RequestBody ShipmentsDto shipmentsDto) {

        shipmentsService.update(shipmentsDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет отгрузку c выбранным id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ShipmentsDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> deleteById(@ApiParam(name = "id", value = "Id ShipmentsDto для удаления", required = true)
                                        @PathVariable Long id) {
        shipmentsService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

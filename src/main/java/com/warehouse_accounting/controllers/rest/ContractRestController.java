package com.warehouse_accounting.controllers.rest;

import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.services.interfaces.ContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contracts")
@Api(tags = "Contract Rest Controller")
@Tag(name = "Contract Rest Controller", description = "API для работы с договорами")
public class ContractRestController {
    private final ContractService contractService;

    public ContractRestController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping
    @ApiOperation(value = "Возвращает список договоров", notes = "Возвращает список ContractDto", response = ContractDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение списка договоров"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<ContractDto>> getAll() {
        return ResponseEntity.ok(contractService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Возвращает договор по id", notes = "Возвращает ContractDto", response = ContractDto.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное получение договора"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<ContractDto> getById(
            @ApiParam(name = "id", value = "Значение поля Id объекта которого хотим получить", example = "1", required = true)
            @PathVariable("id") Long id) {
        return ResponseEntity.ok(contractService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Создает договор")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание договора"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> create(
            @ApiParam(name = "ContractDto", value = "Объект ContractDto который нужно сохранить в программе")
            @RequestBody ContractDto contractDto) {
        contractService.create(contractDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    @ApiOperation(value = "Изменяет договор")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное изменение договора"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> update(
            @ApiParam(name = "ContractDto", value = "Объект ContractDto который нужно изменить в программе")
            @RequestBody ContractDto contractDto) {
        contractService.update(contractDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаляет договор по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное удаление договора"),
            @ApiResponse(responseCode = "404", description = "Данный контролер не найден"),
            @ApiResponse(responseCode = "403", description = "Операция запрещена"),
            @ApiResponse(responseCode = "401", description = "Нет доступа к данной операции")}
    )
    public ResponseEntity<Void> deleteById(
            @ApiParam(name = "id", value = "Значение поля Id объекта который хотим удалить", example = "1", required = true)
            @PathVariable("id") long id) {
        contractService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}

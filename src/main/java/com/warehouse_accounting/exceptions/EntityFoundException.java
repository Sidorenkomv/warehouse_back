package com.warehouse_accounting.exceptions;

public class EntityFoundException extends RuntimeException {
    public EntityFoundException(String message) {
        super(message);
    }
}

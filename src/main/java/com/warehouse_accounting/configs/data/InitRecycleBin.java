package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.RecycleBinDto;
import com.warehouse_accounting.services.interfaces.RecycleBinService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;

@Component("initRecycleBin")
@RequiredArgsConstructor
@Log4j2
public class InitRecycleBin {

    private final RecycleBinService recycleBinService;

    @PostConstruct
    private void initRecycleBin() {
        try {
            recycleBinService.create(RecycleBinDto.builder()
                    .id(1L)
                    .documentType("тип")
                    .number("number")
                    .date(LocalDate.now())
                    .sum(BigDecimal.valueOf(500))
                    .warehouseID(1L)
                    .warehouseFrom("склад123")
                    .companyID(1L)
                    .contractorID(2L)
                    .status("OK")
                    .projectID(1L)
                    .shipped("shipped")
                    .printed("printed")
                    .comment("коммент")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу recycleBin");
        }
    }
}

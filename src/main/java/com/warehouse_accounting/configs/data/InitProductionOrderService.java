package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProductionOrderDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.ProductionOrderService;
import com.warehouse_accounting.services.interfaces.ProjectService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initProductionOrderService")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initCompany", "initProject", "initWarehouses", "initTechnologicalMap"})
public class InitProductionOrderService {

    private final ProductionOrderService productionOrderService;
    private final CompanyService companyService;
    private final ProjectService projectService;
    private final WarehouseService warehouseService;
    private final TechnologicalMapService technologicalMapService;

    @PostConstruct
    private void initProductionOrderService() {
        productionOrderService.create(ProductionOrderDto.builder()
                // .id(1L)
                .companyId(companyService.getById(1L).getId())
                .projectId(projectService.getById(1L).getId())
                .warehouseId(warehouseService.getById(1L).getId())
                .technologicalMapId(technologicalMapService.getById(1L).getId())
                .build()
        );
    }
}

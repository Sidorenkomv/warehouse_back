package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SerialNumberDto;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.SerialNumberService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initSerialNumberService")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initProduct", "initWarehouses"})
public class InitSerialNumberService {

    private final SerialNumberService serialNumberService;
    private final ProductService productService;
    private final WarehouseService warehouseService;

    @PostConstruct
    private void initSerialNumberService() {
        try {
            serialNumberService.create(SerialNumberDto.builder()
                    .id(1L)
                    .serialNumber(1L)
                    .code(11L)
                    .vendor_code(111L)
                    .product(productService.getById(1L))
                    .warehouse(warehouseService.getById(1L))
                    .typeOfDoc("2L")
                    .number(22L)
                    .description("Супер товар, бери!")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу serialNumber");
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SalesChannelDto;
import com.warehouse_accounting.services.interfaces.SalesChannelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initSalesChannel")
@RequiredArgsConstructor
@Log4j2
public class InitSalesChannel {

    private final SalesChannelService salesChannelService;

    @PostConstruct
    private void initSalesChannel() {
        try {
            salesChannelService.create(new SalesChannelDto.Builder()
                    .id(1L)
                    .name("Здесь")
                    .type("работает")
                    .description("\"Добавить\"")
                    .build()
            );
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу SalesChannel " + e);
        }
    }
}

package com.warehouse_accounting.configs.data;



import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.services.interfaces.BonusProgramService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;


@Component("initBonusProgram")
@RequiredArgsConstructor
@Log4j2
public class initBonusProgram {

    private final BonusProgramService bonusProgramService;


    @PostConstruct
    private void initBonusTransaction() {
        try {
            bonusProgramService.create(BonusProgramDto.builder()
                    .name("MyProgram")
                    .accrualRule(10)
                    .writeOffRule(20)
                    .maxPercentage(10)
                    .bonusDelay(3)
                    .award(5)
                    .build());

            bonusProgramService.create(BonusProgramDto.builder()
                    .name("TestProgram")
                    .accrualRule(50)
                    .writeOffRule(10)
                    .maxPercentage(20)
                    .bonusDelay(1)
                    .award(7)
                    .build());


        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу BonusTransaction", e);
        }
    }
}



package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SubscriptionDto;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.RequisitesService;
import com.warehouse_accounting.services.interfaces.SubscriptionService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Date;

@Component("initSubscription")
@DependsOn({"initRequisites","initEmployees"})
@RequiredArgsConstructor
@Log4j2
public class InitSubscription {

    private final SubscriptionService subscriptionService;
    private final RequisitesService requisitesService;
    private final EmployeeService employeeService;

    @PostConstruct
    private void initSubscription() {
        try {
            Date date = new Date();

            subscriptionService.create(SubscriptionDto.builder()
                    .id(1L)
                    .subscriptionExpirationDate(date)
                    .requisites(requisitesService.getById(1L))
                    .employee(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .tariff(Collections.emptySet())
                    .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу subscription", e);
        }
    }
}

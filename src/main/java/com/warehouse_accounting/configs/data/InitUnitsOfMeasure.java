package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import com.warehouse_accounting.services.interfaces.UnitsOfMeasureService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

@Component("initUnitsOfMeasure")
@RequiredArgsConstructor
@Log4j2
public class InitUnitsOfMeasure {

    @Value("${data-init.unit-data2}")
    private File unitsOfMeasure_init_file;

    private final UnitsOfMeasureService unitsOfMeasureService;

    @PostConstruct
    public void initUnitsOfMeasure() {
        try (FileInputStream fileInputStream = new FileInputStream(unitsOfMeasure_init_file)) {
            HSSFSheet sheet = new HSSFWorkbook(fileInputStream).getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row currentRow = rowIterator.next();
                if (currentRow.getRowNum() == 0) {
                    currentRow = rowIterator.next();
                }
                unitsOfMeasureService.create(UnitsOfMeasureDto.builder()
                        .type("Системный")
                        .name(currentRow.getCell(0).getStringCellValue())
                        .fullName(currentRow.getCell(1).getStringCellValue())
                        .code(currentRow.getCell(2).getStringCellValue())
                        .build());
            }
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу unitsOfMeasure", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.TypeOfPriceDto;
import com.warehouse_accounting.services.interfaces.TypeOfPriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initTypeOfPrice")
@RequiredArgsConstructor
@Log4j2
public class InitTypeOfPrice {

    private final TypeOfPriceService typeOfPriceService;

    @PostConstruct
    private void initTypeOfPrice() {
        try {
            typeOfPriceService.create(TypeOfPriceDto.builder()
                    .name("Розничная")
                    .sortNumber("1")
                    .build());
            typeOfPriceService.create(TypeOfPriceDto.builder()
                    .name("Оптовая")
                    .sortNumber("2")
                    .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу type_of_prices", e);
        }
    }
}

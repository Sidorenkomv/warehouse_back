package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.services.interfaces.PurchaseHistoryOfSalesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("initPurchaseHistoryOfSales")
@RequiredArgsConstructor
@Log4j2
public class InitPurchaseHistoryOfSales {

    private final PurchaseHistoryOfSalesService purchaseHistoryOfSalesService;

    @PostConstruct
    private void initPurchaseHistoryOfSales() {
        try {
            purchaseHistoryOfSalesService.create(PurchaseHistoryOfSalesDto.builder()
                    .id(1L)
                    .number(100L)
                    .productPriceId(1L)
                    .sum(BigDecimal.valueOf(500))
                    .profit(BigDecimal.valueOf(500))
                    .profitability(BigDecimal.valueOf(500))
                    .saleOfDay(BigDecimal.valueOf(500))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу purchases_history_of_sales");
        }
    }
}

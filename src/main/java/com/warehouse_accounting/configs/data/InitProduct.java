package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.services.interfaces.ProductGroupService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.UnitService;
import com.warehouse_accounting.services.interfaces.UnitsOfMeasureService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("initProduct")
@DependsOn({"initUnits", "initUnitsOfMeasure", "initProductGroup"})
@RequiredArgsConstructor
@Log4j2
public class InitProduct {

    private final ProductService productService;
    private final UnitService unitService;
    private final UnitsOfMeasureService unitsOfMeasureService;
    private final ProductGroupService productGroupService;

    @PostConstruct
    public void initProduct() {

        var unitsOfMeasureDto1 = unitsOfMeasureService.getById(26L);
        var unitsOfMeasureDto2 = unitsOfMeasureService.getById(57L);
        var unitsOfMeasureDto3 = unitsOfMeasureService.getById(58L);
        var unitsOfMeasureDto4 = unitsOfMeasureService.getById(26L);

        var unitDto1 = unitService.getById(25L);
        var unitDto2 = unitService.getById(57L);
        var unitDto3 = unitService.getById(58L);
        var unitDto4 = unitService.getById(26L);

        var productGroupDto1 = productGroupService.getById(1L);
        try {
            productService.create(
                    ProductDto.builder()
                            .name("Вода")
                            .description("Описание первого товара")
                            .articul("articul_555")
                            .code(BigDecimal.valueOf(555))
                            .outCode(BigDecimal.valueOf(555))
                            .unitsOfMeasureDto(unitsOfMeasureDto1)
                            .weight(BigDecimal.valueOf(1))
                            .volume(BigDecimal.valueOf(1))
                            .purchasePrice(BigDecimal.valueOf(2))
                            .nds(18f)
                            .unitDto(unitDto1)
                            .productGroupDto(productGroupDto1)
                            .build()
            );

            productService.create(
                    ProductDto.builder()
                            .name("Газ в баллоне")
                            .description("Описание второго товара")
                            .articul("articul_444")
                            .code(BigDecimal.valueOf(444))
                            .outCode(BigDecimal.valueOf(444))
                            .unitsOfMeasureDto(unitsOfMeasureDto2)
                            .weight(BigDecimal.valueOf(50))
                            .volume(BigDecimal.valueOf(0.5))
                            .purchasePrice(BigDecimal.valueOf(2800))
                            .nds(10f)
                            .unitDto(unitDto2)
                            .productGroupDto(productGroupDto1)
                            .build()
            );

            productService.create(
                    ProductDto.builder()
                            .name("Бутылка стеклянная")
                            .description("Описание третьего товара")
                            .articul("articul_333")
                            .code(BigDecimal.valueOf(333))
                            .outCode(BigDecimal.valueOf(333))
                            .unitsOfMeasureDto(unitsOfMeasureDto3)
                            .weight(BigDecimal.valueOf(0, 2))
                            .volume(BigDecimal.valueOf(0.001))
                            .purchasePrice(BigDecimal.valueOf(4))
                            .nds(20f)
                            .unitDto(unitDto3)
                            .productGroupDto(productGroupDto1)
                            .build()
            );

            productService.create(
                    ProductDto.builder()
                            .name("Что-то с чем-то")
                            .articul("articul_505")
                            .code(BigDecimal.valueOf(555))
                            .outCode(BigDecimal.valueOf(555))
                            .unitsOfMeasureDto(unitsOfMeasureDto4)
                            .weight(BigDecimal.valueOf(1))
                            .volume(BigDecimal.valueOf(1))
                            .purchasePrice(BigDecimal.valueOf(2))
                            .nds(18f)
                            .unitDto(unitDto4)
                            .productGroupDto(productGroupDto1)
                            .build()
            );
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Product", e);
        }
    }
}

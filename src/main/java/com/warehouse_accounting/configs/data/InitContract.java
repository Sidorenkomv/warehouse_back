package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("initContract")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initContractors","initCompany"})
public class InitContract {

    private final ContractService contractService;
    private final ContractorService contractorService;
    private final CompanyService companyService;

    @PostConstruct
    private void initContract() {
        try {
            contractService.create(ContractDto.builder()
                    .id(1L)
                    .amount(BigDecimal.valueOf(10L))
                    .contractor(contractorService.getById(1L))
                    .company(companyService.getById(1L))
                    .number("1")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу contracts", e);
        }
    }
}

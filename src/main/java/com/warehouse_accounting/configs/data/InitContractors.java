package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import com.warehouse_accounting.services.interfaces.ContractorGroupService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.TypeOfPriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initContractors")
@DependsOn("initTypeOfPrice")
@RequiredArgsConstructor
@Log4j2
public class InitContractors {

    private final ContractorService contractorService;
    private final ContractorGroupService contractorGroupService;
    private final TypeOfPriceService typeOfPriceService;

    @PostConstruct
    private void initContractors() {
        try {
            contractorService.create(ContractorDto.builder()
                    .name("Best_Contractor")
                    .status("VIP_status")
                    .code("contractor1_code_11")
                    .outerCode("contractor1_outerCode_11")
                    .sortNumber("contractor1_sortNumber_11")
                    .phone("+79001234567")
                    .fax("contractor1_fax_1234567890")
                    .email("contractor1@mail.com")
                    .address(AddressDto.builder().fullAddress("contractorFullAddress").build())
                    .numberDiscountCard("discount_1234567890")
                    .contractorGroup(contractorGroupService.getById(1L))
                    .typeOfPrice(typeOfPriceService.getById(1L))
                    .legalDetail(LegalDetailDto.builder().build())
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Contractors", e);
        }
    }
}

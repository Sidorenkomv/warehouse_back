package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.UnitDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGetService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.UnitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initGoodsToRealizeGet")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initUnits", "initUnitsOfMeasure", "initProductGroup", "initTechnologicalMap", "initPointOfSales"})
public class InitGoodsToRealizeGet {
    private final GoodsToRealizeGetService goodsToRealizeGetService;
    private final ProductService productService;
    private final UnitService unitService;

    @PostConstruct
    private void initGoodsToRealizeGet() {
        ProductDto productDto = productService.getById(1L);
        UnitDto unitDto = unitService.getById(2L);
        try {
            goodsToRealizeGetService.create(GoodsToRealizeGetDto
                    .builder()
                    .id(2L)
                    .productDtoId(productDto.getId())
                    .productDtoName(productDto.getName())
                    .unitId(unitDto.getId())
                    .unitName(unitDto.getShortName())
                    .getGoods(5)
                    .quantity(55)
                    .amount(9)
                    .arrive(11)
                    .remains(14)
                    .quantity_report(55)
                    .amount_report(54)
                    .quantity_Noreport(45)
                    .amount_Noreport(44)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу GoodsToRealizeGet " + e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProjectDto;
import com.warehouse_accounting.services.interfaces.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initProject")
@RequiredArgsConstructor
@Log4j2
public class InitProject {

    private final ProjectService projectService;

    @PostConstruct
    private void initProject() {
        try {
            projectService.create(ProjectDto.builder()
                    .id(2L)
                    .name("Проект1")
                    .code("project_code_1")
                    .description("Описание1")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу projects", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.services.interfaces.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initRoles")
@RequiredArgsConstructor
@Log4j2
public class InitRoles {

    private final RoleService roleService;

    @PostConstruct
    private void initRoles() {
        roleService.create(Role.builder()
                .id(1L)
                .name("admin")
                .sortNumber("sort_admin")
                .build()
        );

        roleService.create(Role.builder()
                .id(2L)
                .name("user")
                .sortNumber("sort_user")
                .build()
        );
    }
}

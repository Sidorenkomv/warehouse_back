package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PostingDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.PostingService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component("initPostingService")
@DependsOn({"initWarehouses","initCompany"})
@RequiredArgsConstructor
@Log4j2
public class InitPostingService {

    private final PostingService postingService;
    private final WarehouseService warehouseService;
    private final CompanyService companyService;

    @PostConstruct
    private void initPostingService() {
        try {
            postingService.create(PostingDto.builder()
                    .id(1L)
                    .dateOfCreation(LocalDateTime.now())
                    .warehouseTo(warehouseService.getById(1L))
                    .company(companyService.getById(1L))
                    .moved(true)
                    .printed(true)
                    .comment("Выгодно")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу posting");
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.services.interfaces.PurchasesManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initPurchasesManagement")
@RequiredArgsConstructor
@Log4j2
public class InitPurchasesManagement {

    private final PurchasesManagementService purchasesManagementService;

    @PostConstruct
    private void initPurchasesManagement() {
        try {
            purchasesManagementService.create(PurchasesManagementDto.builder()
                    .id(1L)
                    .productId(1L)
                    .purchasesHistoryOfSalesId(1L)
                    .purchasesForecastId(1L)
                    .purchasesCurrentBalanceId(1L)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу purchasesManagement");
        }
    }
}

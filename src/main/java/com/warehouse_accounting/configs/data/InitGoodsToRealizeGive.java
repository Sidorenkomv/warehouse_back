package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.models.dto.UnitDto;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGiveService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.UnitService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initGoodsToRealizeGive")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initProduct","initUnits"})
public class InitGoodsToRealizeGive {

    private final ProductService productService;
    private final UnitService unitService;
    private final GoodsToRealizeGiveService goodsToRealizeGiveService;

    @PostConstruct
    private void initGoodsToRealizeGive() {
        ProductDto productDto = productService.getById(2L);
        UnitDto unitDto = unitService.getById(3L);
        try {
            goodsToRealizeGiveService.create(GoodsToRealizeGiveDto
                    .builder()
                    .id(1L)
                    .productDtoId(productDto.getId())
                    .productDtoName(productDto.getName())
                    .unitId(unitDto.getId())
                    .unitName(unitDto.getShortName())
                    .giveGoods(5)
                    .quantity(55)
                    .amount(9)
                    .arrive(11)
                    .remains(14)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу GoodsToRealizeGive " + e);
        }
    }
}

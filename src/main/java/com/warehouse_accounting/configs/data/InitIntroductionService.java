package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.IntroductionDto;
import com.warehouse_accounting.services.interfaces.IntroductionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component("initIntroductionService")
@RequiredArgsConstructor
@Log4j2
public class InitIntroductionService {

    private final IntroductionService introductionService;

    @PostConstruct
    private void initIntroductionService() {
        introductionService.create(IntroductionDto.builder()
                .time(LocalDateTime.now())
                .pointId(1L)
                .fromWhomId(1L)
                .organizationId(1L)
                .sum(BigDecimal.valueOf(100))
                .generalAccess(true)
                .ownerDepartmentId(1L)
                .ownerEmployeeId(1L)
                .sent(true)
                .printed(true)
                .comment("Комментарий")
                .dateOfEdit(null)
                .editorEmployeeId(1L)
                .build()
        );
    }
}

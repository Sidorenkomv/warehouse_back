package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.DiscountDto;
import com.warehouse_accounting.services.interfaces.DiscountService;
import com.warehouse_accounting.services.interfaces.DiscountTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Collections;

@Component("initDiscount")
@RequiredArgsConstructor
@Log4j2
public class InitDiscount {

    private final DiscountService discountService;
    private final DiscountTypeService discountTypeService;

    @PostConstruct
    private void initDiscount() {
        try {

            discountService.create(DiscountDto.builder()
                    .active(false)
                    .name("free")
                    .discountType(discountTypeService.getById(1L))
                    .products(Collections.emptySet())
                    .contractors(Collections.emptySet())
                    .priceTypeProductCard(BigDecimal.valueOf(5))
                    .fixedDiscount(BigDecimal.valueOf(5))
                    .sumCumulative(BigDecimal.valueOf(5))
                    .discountPercent(BigDecimal.valueOf(5))
                    .accrualPoints(BigDecimal.valueOf(5))
                    .writeOff(5)
                    .maxPercentPayment(5)
                    .pointsAwardedInDays(5)
                    .SimultaneousAccrualAndWriteOff(false)
                    .welcomePoints(false)
                    .welcomePointsAccrual(5)
                    .firstPurchase(false)
                    .registrationBonusProgram(false)
                    .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу discount", e);
        }
    }
}

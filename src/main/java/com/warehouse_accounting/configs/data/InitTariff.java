package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.TariffDto;
import com.warehouse_accounting.services.interfaces.TariffService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.time.LocalDate;


@Component("initTariff")
@RequiredArgsConstructor
@Log4j2
public class InitTariff {

    private final TariffService tariffService;

    @PostConstruct
    private void initTariff() {
        try {
            tariffService.create(TariffDto.builder()
                    .id(1L)
                    .tariffName("Базовый")
                    .dataSpace(1)
                    .salePointCount(1)
                    .onlineStoreCount(1)
                    .paidApplicationOptionCount(1)
                    .isCRM(true)
                    .isScripts(true)
                    .extendedBonusProgram(true)
                    .paymentPeriod(1)
                    .totalPrice(1)
                    .dateStartSubscription(LocalDate.now())
                    .dateEndSubscription(LocalDate.now())
                    .build());

            tariffService.create(TariffDto.builder()
                    .id(1L)
                    .tariffName("Старт")
                    .dataSpace(2)
                    .salePointCount(5)
                    .onlineStoreCount(12)
                    .paidApplicationOptionCount(1)
                    .isCRM(false)
                    .isScripts(true)
                    .extendedBonusProgram(true)
                    .paymentPeriod(1)
                    .totalPrice(1)
                    .dateStartSubscription(LocalDate.now())
                    .dateEndSubscription(LocalDate.now())
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу tariff", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.ProjectDto;
import com.warehouse_accounting.models.dto.SalesChannelDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.services.interfaces.DocumentService;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.ProjectService;
import com.warehouse_accounting.services.interfaces.SalesChannelService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component("initDocument")
@DependsOn({
        "initWarehouses",
        "initContractors",
        "initContract",
        "initCompany",
        "initEmployees",
        "initProject",
        "initSalesChannel"
})
@RequiredArgsConstructor
@Log4j2
public class InitDocument {

    private final WarehouseService warehouseService;
    private final ContractorService contractorService;
    private final ContractService contractService;
    private final CompanyService companyService;
    private final DepartmentService departmentService;
    private final EmployeeService employeeService;
    private final ProjectService projectService;
    private final SalesChannelService salesChannelService;
    private final DocumentService documentService;


    @PostConstruct
    private void initDocument() {
        WarehouseDto warehouseDto1 = warehouseService.getById(1L);
        WarehouseDto warehouseDto2 = warehouseService.getById(2L);
        WarehouseDto warehouseDto3 = warehouseService.getById(3L);
        ContractorDto contractorDto1 = contractorService.getById(1L);
        ContractDto contractDto = contractService.getById(1L);
        CompanyDto company = companyService.getById(1L);
        DepartmentDto departmentDto = departmentService.getById(5L);
        EmployeeDto employeeDto = ConverterDto.convertToDto(employeeService.findById(1L));
        ProjectDto projectDto = projectService.getById(1L);
        SalesChannelDto salesChannelDto = salesChannelService.getById(1L);

        try {
            documentService.create(DocumentDto
                    .builder()
                    .type("type1")
                    .comments("1")
                    .docNumber("docNumber1")
                    .date(LocalDateTime.now())
                    .sum(BigDecimal.valueOf(10L))
                    .warehouseFromId(warehouseDto1.getId())
                    .warehouseFromName(warehouseDto1.getName())
                    .warehouseToId(warehouseDto2.getId())
                    .companyId(company.getId())
                    .contrAgentId(contractorDto1.getId())
                    .contractId(contractDto.getId())
                    .departmentId(departmentDto.getId())
                    .departmentName(departmentDto.getName())
                    .employeeId(employeeDto.getId())
                    .employeeFirstname(employeeDto.getFirstName())
                    .updatedFromEmployeeId(employeeDto.getId())
                    .updatedFromEmployeeFirstname(employeeDto.getFirstName())
                    .projectId(projectDto.getId())
                    .projectName(projectDto.getName())
                    .salesChannelId(salesChannelDto.getId())
                    .salesChannelName(salesChannelDto.getName())
                    .build());
            documentService.create(DocumentDto
                    .builder()
                    .type("type2")
                    .comments("2")
                    .docNumber("docNumber2")
                    .date(LocalDateTime.now())
                    .sum(BigDecimal.valueOf(15L))
                    .warehouseFromId(warehouseDto2.getId())
                    .warehouseFromName(warehouseDto2.getName())
                    .warehouseToId(warehouseDto2.getId())
                    .companyId(company.getId())
                    .contrAgentId(contractorDto1.getId())
                    .contractId(contractDto.getId())
                    .departmentId(departmentDto.getId())
                    .departmentName(departmentDto.getName())
                    .employeeId(employeeDto.getId())
                    .employeeFirstname(employeeDto.getFirstName())
                    .updatedFromEmployeeId(employeeDto.getId())
                    .updatedFromEmployeeFirstname(employeeDto.getFirstName())
                    .projectId(projectDto.getId())
                    .projectName(projectDto.getName())
                    .salesChannelId(salesChannelDto.getId())
                    .salesChannelName(salesChannelDto.getName())
                    .build());
            documentService.create(DocumentDto
                    .builder()
                    .type("type3")
                    .comments("3")
                    .docNumber("docNumber3")
                    .date(LocalDateTime.now())
                    .sum(BigDecimal.valueOf(56L))
                    .warehouseFromId(warehouseDto3.getId())
                    .warehouseFromName(warehouseDto3.getName())
                    .warehouseToId(warehouseDto2.getId())
                    .companyId(company.getId())
                    .contrAgentId(contractorDto1.getId())
                    .contractId(contractDto.getId())
                    .departmentId(departmentDto.getId())
                    .departmentName(departmentDto.getName())
                    .employeeId(employeeDto.getId())
                    .employeeFirstname(employeeDto.getFirstName())
                    .updatedFromEmployeeId(employeeDto.getId())
                    .updatedFromEmployeeFirstname(employeeDto.getFirstName())
                    .projectId(projectDto.getId())
                    .projectName(projectDto.getName())
                    .salesChannelId(salesChannelDto.getId())
                    .salesChannelName(salesChannelDto.getName())
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу documents " + e);
        }
    }
}

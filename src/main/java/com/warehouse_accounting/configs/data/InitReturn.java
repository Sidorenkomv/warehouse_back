package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ReturnDto;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.FileService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.ProjectService;
import com.warehouse_accounting.services.interfaces.ReturnService;
import com.warehouse_accounting.services.interfaces.TaskService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Component("initReturn")
@DependsOn({
        "initContractors",
        "initContract",
        "initProject",
        "initFile",
        "initTask",
        "initProduct",
        "initWarehouses"
})
@RequiredArgsConstructor
@Log4j2
public class InitReturn {

    private final ReturnService returnService;
    private final ContractorService contractorService;
    private final ContractService contractService;
    private final ProjectService projectService;
    private final FileService fileService;
    private final TaskService taskService;
    private final ProductService productService;
    private final WarehouseService warehouseService;


    @PostConstruct
    private void initReturn() {
        try {
            var warehouseDto = warehouseService.getById(1L);
            var contractorDto = contractorService.getById(1L);
            var contractDto = contractService.getById(1L);
            var projectDto = projectService.getById(1L);
            var fileDto = fileService.getById(1L);
            var taskDto = taskService.getById(1L);
            var productDto = productService.getById(1L);

            returnService.create(ReturnDto.builder()
                    .dataTime(LocalDateTime.now())
                    .warehouseDto(warehouseDto)
                    .contractorDto(contractorDto)
                    .contractDto(contractDto)
                    .projectDto(projectDto)
                    .fileDtos(List.of(fileDto))
                    .taskDtos(List.of(taskDto))
                    .productDtos(List.of(productDto))
                    .sum(BigDecimal.valueOf(100))
                    .isSent(true)
                    .isPrinted(true)
                    .comment("Return")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу returns", e);
        }
    }
}

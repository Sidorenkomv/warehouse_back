package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initWarehouses")
@RequiredArgsConstructor
@Log4j2
public class InitWarehouses {

    private final WarehouseService warehouseService;

    @PostConstruct
    private void initWarehouses() {
        try {
            warehouseService.create(WarehouseDto.builder()
                    .id(1L)
                    .name("Вспомогательный")
                    .sortNumber("4")
                    .address(AddressDto.builder()
                            .postCode("511312")
                            .build())
                    .comment("Маленький склад")
                    .build());
            warehouseService.create(WarehouseDto.builder()
                    .id(2L)
                    .name("Основной")
                    .sortNumber("1")
                    .address(AddressDto.builder()
                            .postCode("312455")
                            .build())
                    .comment("Большой склад")
                    .build());
            warehouseService.create(WarehouseDto.builder()
                    .id(3L)
                    .name("Южный")
                    .sortNumber("3")
                    .address(AddressDto.builder()
                            .postCode("775566")
                            .build())
                    .comment("Средний склад")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу warehouses " + e);
        }
    }
}

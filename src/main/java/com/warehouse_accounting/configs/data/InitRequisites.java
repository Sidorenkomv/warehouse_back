package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.RequisitesDto;
import com.warehouse_accounting.services.interfaces.RequisitesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initRequisites")
@RequiredArgsConstructor
@Log4j2
public class InitRequisites {

    private final RequisitesService requisitesService;

    @PostConstruct
    private void initRequisites() {
        try {
            requisitesService.create(RequisitesDto.builder()
                    .id(1L)
                    .organization("JM")
                    .legalAddress(AddressDto.builder()
                            .postCode("postCodeRequisites1")
                            .build())
                    .INN(123)
                    .KPP(123)
                    .BIK(123)
                    .checkingAccount(123)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу requisites", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.services.interfaces.ShipmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initShipment")
@RequiredArgsConstructor
@Log4j2
public class InitShipment {

    private final ShipmentService shipmentService;

    @PostConstruct
    private void initShipment() {
//        try {
//            ProductDto productDto = ProductDto.builder()
//                    .id(1L)
//                    .name("prod")
//                    .build();
//            shipmentService.create(ShipmentDto.builder()
//                    .id(1L)
//                    .dateOfCreation(LocalDateTime.now())
//                    .warehouseId(1L)
//                    .contractId(1L)
//                    .contractorId(1L)
//                    .companyId(1L)
//                    .sum(BigDecimal.valueOf(777))
//                    .productDtos(List.of(productDto))
//                    .isSent(true)
//                    .isPrinted(true)
//                    .comment("Shipment1")
//                    .consigneeId(1L)
//                    .carrierId(1L)
//                    .isPaid(true)
//                    .deliveryAddress(AddressDto.builder()
//                            .postCode("postCodeShipment1")
//                            .build())
//                    .build());
//
//        } catch (Exception e) {
//            log.error("Не удалось заполнить таблицу shipment", e);
//        }
    }
}

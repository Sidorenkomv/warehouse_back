package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SupplyDto;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.SupplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Component("initSupply")
@RequiredArgsConstructor
@Log4j2
@DependsOn("initProduct")
public class InitSupply {

    private final SupplyService supplyService;
    private final ProductService productService;


    @PostConstruct
    private void initSupply() {
        try {
            supplyService.create(SupplyDto.builder()
                    .dateOfCreation(LocalDateTime.now())
                    .contractorId(1L)
                    .contractId(1L)
                    .companyId(1L)
                    .warehouseId(1L)
                    .productDtos(List.of(productService.getById(1L), productService.getById(2L)))
                    .sum(BigDecimal.valueOf(555))
                    .isSent(false)
                    .isPrinted(true)
                    .comment("text")
                    .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу supply", e);
        }
    }
}

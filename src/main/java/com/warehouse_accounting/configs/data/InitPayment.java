package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.TypeOfPayment;
import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.models.dto.PaymentDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("initPayment")
@DependsOn({"initCompany", "initContractors"})
@RequiredArgsConstructor
@Log4j2
public class InitPayment {

    private final PaymentService paymentService;
    private final ContractorService contractorService;
    private final CompanyService companyService;

    @PostConstruct
    private void initPayment() {
        try {
            ContractorDto contractorDto = contractorService.getById(1L);
            CompanyDto companyDto = companyService.getById(1L);

            paymentService.create(PaymentDto.builder()
                    .number("1")
                    .amount(BigDecimal.valueOf(10L))
                    .tax(BigDecimal.valueOf(10L))
                    .typeOfPayment(TypeOfPayment.INCOMING_PAYMENT)
                    .projectId(1L)
                    .projectName("Проект")
                    .contractId(1L)
                    .contractNumber("1")
                    .contractorDto(contractorDto)
                    .companyDto(companyDto)
                    .purpose("Цель")
                    .isDone(true)
                    .comment("Комментарий")
                    .paymentExpenditureId(1L)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу payments", e);
        }
    }
}

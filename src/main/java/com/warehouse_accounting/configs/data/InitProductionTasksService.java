package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProductionTasksDto;
import com.warehouse_accounting.services.interfaces.ProductionTasksService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initProductionTasksService")
@RequiredArgsConstructor
@Log4j2
public class InitProductionTasksService {

    private final ProductionTasksService productionTasksService;

    @PostConstruct
    private void initProductionTasksService() {
        productionTasksService.create(ProductionTasksDto.builder()
                .id(1L)
                .taskId(123L)
                .organization("ООО Рога и Мохито")
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .isAccessed(true)
                .ownerDepartmentId(1L)
                .ownerEmployeeId(1L)
                .description("Задача для производства")
                .editEmployeeId(1L)
                .build()
        );
    }
}

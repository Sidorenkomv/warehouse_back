package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.RetailShiftDto;
import com.warehouse_accounting.services.interfaces.BankAccountService;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import com.warehouse_accounting.services.interfaces.RetailShiftService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;

@Component("initRetailShift")
@DependsOn({"initPointOfSales", "initWarehouses", "initCompany", "initBankAccount", "initEmployees"})
@RequiredArgsConstructor
@Log4j2
public class InitRetailShift {

    private final RetailShiftService retailShiftService;
    private final PointOfSalesService pointOfSalesService;
    private final WarehouseService warehouseService;
    private final CompanyService companyService;
    private final BankAccountService bankAccountService;
    private final DepartmentService departmentService;
    private final EmployeeService employeeService;


    @PostConstruct
    private void initRetailShift() {
        try {
            retailShiftService.create(RetailShiftDto.builder()
                    .id(1L)
                    .dateOfOpen(LocalDate.now())
                    .dateOfClose(LocalDate.now())
                    .pointOfSales(pointOfSalesService.getById(1L))
                    .warehouse(warehouseService.getById(1L))
                    .company(companyService.getById(1L))
                    .bank(bankAccountService.getById(1L))
                    .cashlessShiftRevenue(BigDecimal.valueOf(12))
                    .cashShiftRevenue(BigDecimal.valueOf(7))
                    .shiftRevenue(BigDecimal.valueOf(19))
                    .recevied(BigDecimal.valueOf(11))
                    .sale(BigDecimal.valueOf(3))
                    .comission(BigDecimal.valueOf(2))
                    .isAccessed(true)
                    .ownerDepartment(departmentService.getById(1L))
                    .ownerEmployee(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .isSent(true)
                    .isPrinted(false)
                    .description("comment")
                    .dateOfEdit(LocalDate.now())
                    .editEmployee(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .build());

            retailShiftService.create(RetailShiftDto.builder()
                    .id(2L)
                    .dateOfOpen(LocalDate.now())
                    .dateOfClose(LocalDate.now())
                    .pointOfSales(pointOfSalesService.getById(1L))
                    .warehouse(warehouseService.getById(1L))
                    .company(companyService.getById(1L))
                    .bank(bankAccountService.getById(1L))
                    .cashlessShiftRevenue(BigDecimal.valueOf(12))
                    .cashShiftRevenue(BigDecimal.valueOf(7))
                    .shiftRevenue(BigDecimal.valueOf(19))
                    .recevied(BigDecimal.valueOf(11))
                    .sale(BigDecimal.valueOf(3))
                    .comission(BigDecimal.valueOf(2))
                    .isAccessed(true)
                    .ownerDepartment(departmentService.getById(1L))
                    .ownerEmployee(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .isSent(true)
                    .isPrinted(false)
                    .description("comment")
                    .dateOfEdit(LocalDate.now())
                    .editEmployee(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу retail_shift", e);
        }
    }
}

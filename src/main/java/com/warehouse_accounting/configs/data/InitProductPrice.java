package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.services.interfaces.ProductPriceService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.TypeOfPriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;

@Component("initProductPrice")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initProduct", "initTypeOfPrice"})
public class InitProductPrice {

    private final ProductPriceService productPriceService;
    private final ProductService productService;
    private final TypeOfPriceService typeOfPriceService;

    @PostConstruct
    private void initProductPrice() {
        try {
            productPriceService.create(List.of(ProductPriceDto.builder()
                    .productDto(productService.getById(1L))
                    .typeOfPriceDto(typeOfPriceService.getById(1L))
                    .price(BigDecimal.valueOf(190)).
                    build()));

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Product Prices");
        }
    }
}

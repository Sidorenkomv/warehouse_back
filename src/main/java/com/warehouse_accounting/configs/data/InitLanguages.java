package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.LanguageDto;
import com.warehouse_accounting.services.interfaces.LanguageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initLanguages")
@RequiredArgsConstructor
@Log4j2
public class InitLanguages {

    private final LanguageService languageService;

    @PostConstruct
    private void initLanguages() {
        try {
            languageService.create(LanguageDto.builder()
                    .id(1L)
                    .language("English")
                    .build());
            languageService.create(LanguageDto.builder()
                    .id(2L)
                    .language("Russian")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Language", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import com.warehouse_accounting.services.interfaces.LegalDetailService;
import com.warehouse_accounting.services.interfaces.TypeOfContractorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initLegalDetail")
@RequiredArgsConstructor
@Log4j2
public class InitLegalDetail {

    private final LegalDetailService legalDetailService;
    private final TypeOfContractorService typeOfContractorService;

    @PostConstruct
    private void initLegalDetail() {
        try {
            legalDetailService.create(LegalDetailDto.builder()
                    .fullName("legalDetail1")
                    .inn("27731537604")
                    .address(AddressDto.builder()
                            .postCode("postCodeLegalDetail1")
                            .build())
                    .typeOfContractor(typeOfContractorService.getById(1L))
                    .build());

            legalDetailService.create(LegalDetailDto.builder()
                    .fullName("legalDetail2")
                    .address(AddressDto.builder()
                            .postCode("postCodeLegalDetail2")
                            .build())
                    .inn("17731537604")
                    .okpo("93318947")
                    .ogrn("1067746195330")
                    .kpp("111111111")
                    .typeOfContractor(typeOfContractorService.getById(1L))
                    .build());

            legalDetailService.create(LegalDetailDto.builder()
                    .fullName("legalDetail3")
                    .address(AddressDto.builder()
                            .postCode("postCodeLegalDetail3")
                            .build())
                    .inn("45531537604")
                    .okpo("93318947")
                    .ogrn("1067746195330")
                    .kpp("111111111")
                    .typeOfContractor(typeOfContractorService.getById(1L))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу legal_details", e);
        }
    }
}

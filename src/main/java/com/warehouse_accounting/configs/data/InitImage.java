package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.services.interfaces.ImageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initImage")
@RequiredArgsConstructor
@Log4j2
public class InitImage {

    private final ImageService imageService;

    @PostConstruct
    private void initImage() {
        try {
            imageService.create(ImageDto.builder()
                    .id(1L)
                    .imageUrl("imageUrl")
                    .sortNumber("sortNumber")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Images", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.services.interfaces.PurchaseCurrentBalanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initPurchaseCurrentBalance")
@RequiredArgsConstructor
@Log4j2
public class InitPurchaseCurrentBalance {

    private final PurchaseCurrentBalanceService purchaseCurrentBalanceService;

    @PostConstruct
    private void initPurchaseCurrentBalance() {
        try {
            purchaseCurrentBalanceService.create(PurchaseCurrentBalanceDto.builder()
                    .id(1L)
                    .remainder(100L)
                    .productReserved(100L)
                    .productsAwaiting(200L)
                    .productAvailableForOrder(100L)
                    .daysStoreOnTheWarehouse(5L)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу purchases_current_balance");
        }
    }
}

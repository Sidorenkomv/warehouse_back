package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.ProductGroupDto;
import com.warehouse_accounting.services.interfaces.ProductGroupService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initProductGroup")
@RequiredArgsConstructor
@Log4j2
public class InitProductGroup {

    private final ProductGroupService productGroupService;

    @PostConstruct
    private void initProductGroup() {
        try {
            productGroupService.create(ProductGroupDto.builder()
                    .id(1L)
                    .name("Parent Product")
                    .sortNumber("42")
                    .parentId(2L)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу product_groups", e);
        }
    }
}

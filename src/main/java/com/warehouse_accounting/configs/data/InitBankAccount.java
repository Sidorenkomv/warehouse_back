package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.services.interfaces.BankAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initBankAccount")
@RequiredArgsConstructor
@Log4j2
public class InitBankAccount {

    private final BankAccountService bankAccountService;

    @PostConstruct
    private void initBankAccount() {
        try {
            bankAccountService.create(BankAccountDto.builder()
                    .id(1L)
                    .rcbic("new")
                    .bank("new Bank")
                    .correspondentAccount("new correspondent_account")
                    .account("new account")
                    .mainAccount(true)
                    .sortNumber("new number")
//                    .address(AddressDto.builder().fullAddress("new Address").build())
//                    .contractor(ContractorDto.builder().name("Junior").build())
//                    .contractor(contractorService.getById(1L))
                    .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу bank_accounts", e);
        }
    }
}

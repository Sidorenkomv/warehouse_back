package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.BonusTransaction;
import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.models.dto.FileDto;
import com.warehouse_accounting.services.interfaces.BonusProgramService;
import com.warehouse_accounting.services.interfaces.BonusTransactionService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.FileService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Component("initBonusTransaction")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initContractors", "initEmployees", "initBonusProgram", "initFile"})
public class InitBonusTransaction {

    private final BonusTransactionService bonusTransactionService;
    private final ContractorService contractorService;
    private final EmployeeService employeeService;
    private final BonusProgramService bonusProgramService;
    private final FileService fileService;
    private final DepartmentService departmentService;

    @PostConstruct
    private void initBonusTransaction() {
        List<FileDto> files = new ArrayList<>();
        FileDto file = fileService.getById(2L);
        files.add(file);


        try {
            bonusTransactionService.create(BonusTransactionDto.builder()
                    .id(1L)
                    .created(LocalDate.now())
                    .transactionType(BonusTransaction.TransactionType.EARNING)
                    .bonusValue(500L)
                    .transactionStatus(BonusTransaction.TransactionStatus.COMPLETED)
                    .executionDate(LocalDate.now())
                    .departmentDto(departmentService.getById(3L))
                    .bonusProgramDto(bonusProgramService.getById(1L))
                    .dateChange(LocalDate.now())
                    .filesDto(files)
                    .contractorDto(contractorService.getById(1L))
                    .comment("комментарий")
                    .ownerChangedDto(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .ownerDto(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу BonusTransaction", e);
        }
    }
}

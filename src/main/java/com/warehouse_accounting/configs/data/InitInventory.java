package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.InventoryDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.InventoryService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component("initInventory")
@DependsOn({"initWarehouses","initCompany"})
@RequiredArgsConstructor
@Log4j2
public class InitInventory {

    private final InventoryService inventoryService;
    private final WarehouseService warehouseService;
    private final CompanyService companyService;

    @PostConstruct
    private void initInventory() {
        try {
            inventoryService.create(InventoryDto.builder()
                    .id(1L)
                    .dateOfCreation(LocalDateTime.now())
                    .warehouseFrom(warehouseService.getById(1L))
                    .company(companyService.getById(1L))
                    .moved(true)
                    .printed(true)
                    .comment("Коммент")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу inventory");
        }
    }
}

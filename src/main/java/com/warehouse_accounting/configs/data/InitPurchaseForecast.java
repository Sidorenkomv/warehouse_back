package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.services.interfaces.PurchaseForecastService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initPurchaseForecast")
@RequiredArgsConstructor
@Log4j2
public class InitPurchaseForecast {

    private final PurchaseForecastService purchaseForecastService;

    @PostConstruct
    private void initPurchaseForecast() {
        try {
            purchaseForecastService.create(PurchaseForecastDto.builder()
                    .id(1L)
                    .reservedDays(100L)
                    .reservedProduct(500L)
                    .ordered(false)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу purchases_forecast");
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.ImageService;
import com.warehouse_accounting.services.interfaces.PositionService;
import com.warehouse_accounting.services.interfaces.RoleService;
import com.warehouse_accounting.services.interfaces.TariffService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Set;

@Component("initEmployees")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initImage", "initRoles", "initTariff"})
public class InitEmployees {

    private final EmployeeService employeeService;
    private final DepartmentService departmentService;
    private final PositionService positionService;
    private final ImageService imageService;
    private final RoleService roleService;
    private final TariffService tariffService;


    @PostConstruct
    private void initEmployees() {
        try {
            employeeService.create(Employee.builder()
                    .lastName("admin")
                    .firstName("admin")
                    .middleName("admin")
                    .sortNumber("sortNumber")
                    .phone("phone")
                    .inn("inn")
                    .description("description")
                    .email("admin")
                    .password("{noop}admin")
                    .department(ConverterDto.convertToModel(departmentService.getById(1L)))
                    .position(ConverterDto.convertToModel(positionService.getById(1L)))
                    .image(ConverterDto.convertToModel(imageService.getById(1L)))
                    .roles(Set.of(roleService.getById(1L)))
                    .tariff(Set.of(ConverterDto.convertToModel(tariffService.getById(1L))))
                    .build());
            employeeService.create(Employee.builder()
                    .lastName("lastName")
                    .firstName("firstName")
                    .middleName("middleName")
                    .sortNumber("sortNumber")
                    .phone("phone")
                    .inn("inn")
                    .description("description")
                    .email("some@mail.ru")
                    .password("password")
                    .department(ConverterDto.convertToModel(departmentService.getById(1L)))
                    .position(ConverterDto.convertToModel(positionService.getById(1L)))
                    .image(ConverterDto.convertToModel(imageService.getById(1L)))
                    .roles(Set.of(roleService.getById(1L)))
                    .tariff(Collections.emptySet())
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Employees", e);
        }
    }
}

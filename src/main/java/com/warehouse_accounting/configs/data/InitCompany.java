package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.CompanyDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.LegalDetailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initCompany")
@RequiredArgsConstructor
@Log4j2
@DependsOn("initLegalDetail")
public class InitCompany {

    private final CompanyService companyService;
    private final LegalDetailService legalDetailService;

    @PostConstruct
    private void initCompany() {
        try {
//            companyService.create(CompanyDto.builder()
//                    .id(1L)
//                    .name("Company1")
//                    .inn("430023")
//                    .sortNumber("3")
//                    .phone("906-304-00-00")
//                    .fax("345-45-67")
//                    .email("mail@com1.com")
//                    .payerVat(true)
//                    .address(AddressDto.builder()
//                            .postCode("postCodeCompany1")
//                            .build())
//                    .leader("leader1")
//                    .leaderManagerPosition("leaderManagerPosition1")
//                    .leaderSignature("leaderSignature1")
//                    .chiefAccountant("chiefAccountant1")
//                    .chiefAccountantSignature("chiefAccountantSignature1")
//                    .stamp("stamp1")
//                    .legalDetailDto(legalDetailService.getById(1L))
//                    .build());

            companyService.create(CompanyDto.builder()
                    .id(2L)
                    .name("Company2")
                    .inn("430025")
                    .sortNumber("4")
                    .phone("906-304-01-01")
                    .fax("346-45-56")
                    .email("mail@com2.com")
                    .payerVat(false)
                    .address(AddressDto.builder()
                            .postCode("postCodeCompany2")
                            .build())
                    .leader("leader2")
                    .leaderManagerPosition("leaderManagerPosition2")
                    .leaderSignature("leaderSignature2")
                    .chiefAccountant("chiefAccountant2")
                    .chiefAccountantSignature("chiefAccountantSignature2")
                    .stamp("stamp2")
                    .legalDetailDto(legalDetailService.getById(2L))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу companies", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.services.interfaces.AdjustmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initAdjustments")
@RequiredArgsConstructor
@Log4j2
public class InitAdjustments {

//    private final AdjustmentService adjustmentService;

    @PostConstruct
    private void initAdjustments() {
//
//        adjustmentService.create(AdjustmentDto.builder()
//                .id(1L)
//                .number("00001")
//                .dateTimeAdjustment(LocalDateTime.now())
//                .companyId(1L)
//                .companyName("Организация1")
//                .contractorId(1L)
//                .contractorName("first_Contractor")
//                .type(TypeOfAdjustment.ACCOUNTBALANCE)
//                .currentBalance(BigDecimal.valueOf(1000.00))
//                .totalBalance(BigDecimal.valueOf(0.00))
//                .adjustmentAmount(BigDecimal.valueOf(1000.00))
//                .comment("1 Корректировка")
//                .whenСhanged(LocalDateTime.now())
//                .build());
//
//        adjustmentService.create(AdjustmentDto.builder()
//                .id(2L)
//                .number("00002")
//                .dateTimeAdjustment(LocalDateTime.now())
//                .companyId(1L)
//                .companyName("Организация1")
//                .contractorId(1L)
//                .contractorName("first_Contractor")
//                .type(TypeOfAdjustment.CASHBALANCE)
//                .currentBalance(BigDecimal.valueOf(1000.00))
//                .totalBalance(BigDecimal.valueOf(2000.00))
//                .adjustmentAmount(BigDecimal.valueOf(3000.00))
//                .comment("2 Корректировка")
//                .whenСhanged(LocalDateTime.now())
//                .build());
//
//        adjustmentService.create(AdjustmentDto.builder()
//                .id(3L)
//                .number("00003")
//                .dateTimeAdjustment(LocalDateTime.now())
//                .companyId(1L)
//                .companyName("Организация1")
//                .contractorId(1L)
//                .contractorName("first_Contractor")
//                .type(TypeOfAdjustment.COUNTERPARTY)
//                .currentBalance(BigDecimal.valueOf(2000.00))
//                .totalBalance(BigDecimal.valueOf(500.00))
//                .adjustmentAmount(BigDecimal.valueOf(1500.00))
//                .comment("3 Корректировка")
//                .whenСhanged(LocalDateTime.now())
//                .build());
    }
}

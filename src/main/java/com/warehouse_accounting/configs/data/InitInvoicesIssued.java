package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.services.interfaces.InvoicesIssuedService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component("initInvoicesIssued")
@RequiredArgsConstructor
@Log4j2
public class InitInvoicesIssued {

    private final InvoicesIssuedService invoicesIssuedService;

    @PostConstruct
    private void initInvoicesIssued() {
        try {
            invoicesIssuedService.create(InvoicesIssuedDto.builder()
                    .id(1L)
                    .data(LocalDateTime.now())
                    .sum(BigDecimal.valueOf(500))
                    .sent(false)
                    .printed(false)
                    .companyId(1L)
                    .contractorId(1L)
                    .comment("Инициировано")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось создать таблицу invoice_issued");
        }
    }
}

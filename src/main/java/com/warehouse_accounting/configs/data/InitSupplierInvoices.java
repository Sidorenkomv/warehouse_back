package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import com.warehouse_accounting.services.interfaces.SupplierInvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initSupplierInvoices")
@RequiredArgsConstructor
@Log4j2
public class InitSupplierInvoices {

    private final SupplierInvoiceService supplierInvoiceService;

    @PostConstruct
    private void initSupplierInvoices() {
        supplierInvoiceService.create(SupplierInvoiceDto.builder()
                .id(1L)
                .invoiceNumber("1")
                .dateInvoiceNumber("2022-01-01")
                .checkboxProd(false)
                .organization("Robin Good Production")
                .warehouse("Северный")
                .contrAgent("Zena Group")
                .contract("Договор как договор")
                .datePay("2022-01-10")
                .project("Морская черепаха")
                .incomingNumber("999")
                .dateIncomingNumber("2022-01-01")
                .checkboxName(false)
                .checkboxNDS(false)
                .checkboxOnNDS(false)
                .addPosition("Скипидар")
                .addComment("Оплачено")
                .build()
        );
    }
}

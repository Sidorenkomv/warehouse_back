package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.models.dto.ContactFaceDto;
import com.warehouse_accounting.services.interfaces.ContactFaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("initContactFace")
@RequiredArgsConstructor
@Log4j2
public class InitContactFace {

    private final ContactFaceService contactFaceService;

    @PostConstruct
    private void initContactFace() {
        try {
            contactFaceService.create(ContactFaceDto.builder()
                    .id(1L)
                    .name("contactFace1")
                    .description("description1")
                    .phone("phone1")
                    .email("email1")
                    .externalCode("externalCode1")
                    .address(AddressDto.builder()
                            .postCode("postCodeContactFace1")
                            .build())
                    .position("position1")
                    .linkContractor("linkContractor1")
                    .comment("comment1")
                    .build());

            contactFaceService.create(ContactFaceDto.builder()
                    .id(2L)
                    .name("contactFace2")
                    .description("description2")
                    .phone("phone2")
                    .email("email2")
                    .externalCode("externalCode2")
                    .address(AddressDto.builder()
                            .postCode("postCodeContactFace2")
                            .build())
                    .position("position2")
                    .linkContractor("linkContractor2")
                    .comment("comment2")
                    .build());

            contactFaceService.create(ContactFaceDto.builder()
                    .id(3L)
                    .name("contactFace3")
                    .description("description3")
                    .phone("phone3")
                    .email("email3")
                    .externalCode("externalCode3")
                    .address(AddressDto.builder()
                            .postCode("postCodeContactFace3")
                            .build())
                    .position("position3")
                    .linkContractor("linkContractor3")
                    .comment("comment3")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу contact_face " + e);
        }
    }
}

package com.warehouse_accounting.configs.data;


import com.warehouse_accounting.models.dto.FileDto;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.FileService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.time.LocalDate;


@Component("initFile")
@RequiredArgsConstructor
@Log4j2
public class InitFile {

    private final FileService fileService;
    private final EmployeeService employeeService;
    @PostConstruct
    private void initFile() {
        try {

            fileService.create(FileDto.builder()
                    .id(1L)
                    .name("file")
                    .size(7)
                    .createdDate(LocalDate.now())
                    .employeeDto(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .build());

            fileService.create(FileDto.builder()
                    .id(2L)
                    .name("newFile")
                    .size(3.5)
                    .createdDate(LocalDate.now())
                    .employeeDto(ConverterDto.convertToDto(employeeService.findById(1L)))
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу file", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.TaskDto;
import com.warehouse_accounting.services.interfaces.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component("initTask")
@RequiredArgsConstructor
@Log4j2
public class InitTask {

    private final TaskService taskService;

    @PostConstruct
    private void initTask() {
        try {
            taskService.create(TaskDto.builder()
                    .description("Первая просто задача")
                    .deadline(LocalDateTime.now().plusDays(3))
                    .dateOfCreation(LocalDateTime.now())
                    .build());

            taskService.create(TaskDto.builder()
                    .description("Вторая задача с документом тех операции 1")
                    .deadline(LocalDateTime.now().plusDays(3))
                    .dateOfCreation(LocalDateTime.now())
                    .documentId(1L)
                    .contractorId(1L)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу Tasks", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.PointOfSalesDto;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("initPointOfSales")
@RequiredArgsConstructor
@Log4j2
public class InitPointOfSales {

    private final PointOfSalesService pointOfSalesService;


    @PostConstruct
    private void initPointOfSales() {
        try {
            pointOfSalesService.create(PointOfSalesDto.builder()
                    .id(1L)
                    .name("точка продажи")
                    .activity("yes")
                    .type("тип")
                    .revenue(BigDecimal.valueOf(500))
                    .cheque(BigDecimal.valueOf(500))
                    .averageСheck(BigDecimal.valueOf(500))
                    .moneyInTheCashRegister(BigDecimal.valueOf(500))
                    .cashiers("Galya")
                    .synchronization("yes")
                    .FN("1234")
                    .validityPeriodFN("data")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу pointOfSales");
        }
    }
}

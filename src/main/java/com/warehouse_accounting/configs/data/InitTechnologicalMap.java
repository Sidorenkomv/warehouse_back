package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.TechnologicalMapDto;
import com.warehouse_accounting.models.dto.TechnologicalMapGroupDto;
import com.warehouse_accounting.models.dto.TechnologicalMapMaterialDto;
import com.warehouse_accounting.models.dto.TechnologicalMapProductDto;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapGroupService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapMaterialService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapProductService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component("initTechnologicalMap")
@DependsOn("initProduct")
@RequiredArgsConstructor
@Log4j2
public class InitTechnologicalMap {

    private final TechnologicalMapGroupService technologicalMapGroupService;
    private final TechnologicalMapService technologicalMapService;
    private final ProductService productService;
    private final TechnologicalMapMaterialService technologicalMapMaterialService;
    private final TechnologicalMapProductService technologicalMapProductService;

    @PostConstruct
    private void initTechnologicalMap() {
        try {
            TechnologicalMapGroupDto dto1 = TechnologicalMapGroupDto.builder()
                    .name("Группа 1")
                    .code("гр1ст1")
                    .comment("Очень важная группа")
                    .parentTechnologicalMapGroupId(null)
                    .build();
            TechnologicalMapGroupDto dto2 = TechnologicalMapGroupDto.builder()
                    .name("Группа 2")
                    .code("гр2ст1")
                    .comment("Так себе группа")
                    .parentTechnologicalMapGroupId(null)
                    .build();
            TechnologicalMapGroupDto dto3 = TechnologicalMapGroupDto.builder()
                    .name("Группа 1-1")
                    .code("гр1ст1_о1")
                    .comment("Не на столько важная группа")
                    .parentTechnologicalMapGroupId(dto2.getId())
                    .build();
            technologicalMapGroupService.create(dto1);
            technologicalMapGroupService.create(dto2);
            technologicalMapGroupService.create(dto3);
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу TechnologicalMapGroup", e);
        }

        try {
            technologicalMapService.create(
                    TechnologicalMapDto.builder()
                            .name("Изготовление газировки")
                            .productionCost(BigDecimal.valueOf(100500))
                            .isArchived(false)
                            .comment("Секретный рецепт производства газировки")
                            .materials(null)
                            .finishedProducts(null)
                            .technologicalMapGroupId(technologicalMapGroupService.getById(1L).getId())
                            .build()
            );

            technologicalMapService.create(
                    TechnologicalMapDto.builder()
                            .name("Технология - найди сам и реализуй")
                            .productionCost(BigDecimal.valueOf(0))
                            .isArchived(false)
                            .comment("Пришел с идеей - ушел с заданием")
                            .materials(null)
                            .finishedProducts(null)
                            .technologicalMapGroupId(technologicalMapGroupService.getById(2L).getId())
                            .build()
            );

            TechnologicalMapDto technologicalMap = technologicalMapService.getById(1L);

            List<TechnologicalMapMaterialDto> materialDtos = new ArrayList<>();
            materialDtos.add(TechnologicalMapMaterialDto.builder()
                    .materialId(productService.getById(1L).getId())
                    .materialName(productService.getById(1L).getName())
                    .count(BigDecimal.valueOf(1))
                    .technologicalMapDto(technologicalMap)
                    .build());
            materialDtos.add(TechnologicalMapMaterialDto.builder()
                    .materialId(productService.getById(2L).getId())
                    .materialName(productService.getById(2L).getName())
                    .count(BigDecimal.valueOf(2))
                    .technologicalMapDto(technologicalMap)
                    .build());
            materialDtos.add(TechnologicalMapMaterialDto.builder()
                    .materialId(productService.getById(3L).getId())
                    .materialName(productService.getById(3L).getName())
                    .count(BigDecimal.valueOf(3))
                    .technologicalMapDto(technologicalMap)
                    .build());
            materialDtos.forEach(technologicalMapMaterialService::create);

            List<TechnologicalMapProductDto> productDtos = new ArrayList<>();
            productDtos.add(TechnologicalMapProductDto.builder()
                    .finishedProductId(productService.getById(4L).getId())
                    .finishedProductsName(productService.getById(4L).getName())
                    .count(BigDecimal.valueOf(1))
                    .technologicalMapDto(technologicalMap)
                    .build());
            productDtos.forEach(technologicalMapProductService::create);

            technologicalMap.setMaterials(materialDtos);
            technologicalMap.setFinishedProducts(productDtos);

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу TechnologicalMap", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import com.warehouse_accounting.services.interfaces.ProjectService;
import com.warehouse_accounting.services.interfaces.SupplierOrdersService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component("initSupplierOrders")
@DependsOn({
        "initContractors",
        "initCompany",
        "initProject",
        "initContract",
        "initEmployees"})
@RequiredArgsConstructor
@Log4j2
public class InitSupplierOrders {

    private final SupplierOrdersService supplierOrdersService;
    private final ContractorService contractorService;
    private final CompanyService companyService;
    private final ProjectService projectService;
    private final ContractService contractService;
    private final DepartmentService departmentService;
    private final EmployeeService employeeService;


    @PostConstruct
    private void initSupplierOrders() {
        try {
            supplierOrdersService.create(SupplierOrdersDto.builder()
                    .id(2L)
                    .number("100500L")
                    .createdAt(LocalDateTime.now())
                    .contrAgentId(1L)
                    .contrAgentName(contractorService.getById(1L).getName())
                    .contrAgentAccount(213123L)
                    .companyId(1L)
                    .companyName(companyService.getById(1L).getName())
                    .companyAccount(123123L)
                    .sum(BigDecimal.valueOf(1000000L))
                    .invoiceIssued(10L)
                    .paid(800000L)
                    .notPaid(200000L)
                    .accepted(500000L)
                    .waiting(300000L)
                    .refundAmount(0L)
                    .acceptanceDate(LocalDateTime.now().plusDays(30L))
                    .projectId(1L)
                    .projectName(projectService.getById(1L).getName())
                    .warehouseId(1L)
                    .warehouseName("Основной склад")
                    .contractId(1L)
                    .contractNumber(contractService.getById(1L).getNumber())
                    .generalAccess(true)
                    .departmentId(1L)
                    .departmentName(departmentService.getById(1L).getName())
                    .employeeId(1L)
                    .employeeFirstname(ConverterDto.convertToDto(employeeService.findById(1L)).getFirstName())
                    .sent(false)
                    .print(false)
                    .comment(null)
                    .updatedAt(LocalDateTime.now())
                    .updatedFromEmployeeId(1L)
                    .updatedFromEmployeeFirstname(ConverterDto.convertToDto(employeeService.findById(1L)).getFirstName())
                    .build()

            );
        } catch (Exception e) {
            log.error("Не удалось загрузить таблицу Supplier_orders", e);
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.WriteOffsDto;
import com.warehouse_accounting.services.interfaces.CompanyService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import com.warehouse_accounting.services.interfaces.WriteOffsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component("initWriteOffs")
@DependsOn({"initWarehouses","initCompany"})
@RequiredArgsConstructor
@Log4j2
public class InitWriteOffs {

    private final WriteOffsService writeOffsService;
    private final WarehouseService warehouseService;
    private final CompanyService companyService;

    @PostConstruct
    private void initWriteOffs() {
        try {
            writeOffsService.create(WriteOffsDto.builder()
                    .id(1L)
                    .dateOfCreation(LocalDateTime.now())
                    .warehouseFrom(warehouseService.getById(1L))
                    .warehouseTo(warehouseService.getById(2L))
                    .company(companyService.getById(1L))
                    .sum(BigDecimal.valueOf(1000))
                    .moved(true)
                    .printed(true)
                    .comment("Коммент")
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу write_offs");
        }
    }
}

package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.FeedDto;
import com.warehouse_accounting.services.interfaces.FeedService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

@Component("initFeed")
@RequiredArgsConstructor
@Log4j2
public class InitFeed {

    private final FeedService feedService;

    @PostConstruct
    private void initFeed() {
        try {
            Date date = new Date();
            feedService.create(FeedDto.builder()
                    .id(1L)
                    .feedHead("Заголовок")
                    .feedBody("Тело новости")
                    .feedDate(date)
                    .build());
        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу feeds", e);
        }
    }
}

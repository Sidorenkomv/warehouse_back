package com.warehouse_accounting.configs.data;

import com.warehouse_accounting.models.dto.TaskDto;
import com.warehouse_accounting.models.dto.TechnologicalOperationDto;
import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.services.interfaces.TaskService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapService;
import com.warehouse_accounting.services.interfaces.TechnologicalOperationService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component("initTechnologicalOperation")
@RequiredArgsConstructor
@Log4j2
@DependsOn({"initWarehouses", "initTechnologicalMap", "initTask"})
public class InitTechnologicalOperation {

    private final WarehouseService warehouseService;
    private final TechnologicalOperationService technologicalOperationService;
    private final TechnologicalMapService technologicalMapService;
    private final TaskService taskService;

    @PostConstruct
    private void initTechnologicalOperation() {
        WarehouseDto warehouseDto = warehouseService.getById(1L);
        try {
            technologicalOperationService.create(
                    TechnologicalOperationDto.builder()
                            .number("Оп-1")
                            .technologicalOperationDateTime(LocalDateTime.now())
                            .technologicalMapId(technologicalMapService.getById(1L).getId())
                            .technologicalMapName(technologicalMapService.getById(1L).getName())
                            .volumeOfProduction(BigDecimal.valueOf(100))
                            .warehouseForMaterialsId(warehouseDto.getId())
                            .warehouseForMaterialsName(warehouseDto.getName())
                            .warehouseForProductId(warehouseDto.getId())
                            .warehouseForProductName(warehouseDto.getName())
                            .build());


            technologicalOperationService.create(
                    TechnologicalOperationDto.builder()
                            .number("Оп-2")
                            .technologicalOperationDateTime(LocalDateTime.now().minusDays(1))
                            .technologicalMapId(technologicalMapService.getById(1L).getId())
                            .technologicalMapName(technologicalMapService.getById(1L).getName())
                            .volumeOfProduction(BigDecimal.valueOf(100))
                            .warehouseForMaterialsId(warehouseDto.getId())
                            .warehouseForMaterialsName(warehouseDto.getName())
                            .warehouseForProductId(warehouseDto.getId())
                            .warehouseForProductName(warehouseDto.getName())
                            .build());

            TechnologicalOperationDto operationDto = technologicalOperationService.getAll().get(1);
            List<TaskDto> taskDtos = new ArrayList<>();
            taskDtos.add(new TaskDto().builder()
                    .description("1# Первая таска для Тех операции 2")
                    .deadline(LocalDateTime.now().plusDays(1))
                    .dateOfCreation(LocalDateTime.now())
                    .documentId(operationDto.getId())
                    .build());
            taskDtos.add(new TaskDto().builder()
                    .description("2# Вторая таска для Тех операции 2")
                    .deadline(LocalDateTime.now().plusDays(2))
                    .dateOfCreation(LocalDateTime.now())
                    .documentId(operationDto.getId())
                    .build());
            taskDtos.forEach(taskService::create);

            operationDto.setTasks(taskDtos);

            List<TaskDto> taskDtos3 = new ArrayList<>();
            taskDtos3.add(new TaskDto().builder()
                    .description("3# Первая таска для Тех операции 3")
                    .deadline(LocalDateTime.now().plusDays(1))
                    .dateOfCreation(LocalDateTime.now())
                    .build());
            taskDtos3.add(new TaskDto().builder()
                    .description("4# Вторая таска для Тех операции 3")
                    .deadline(LocalDateTime.now().plusDays(2))
                    .dateOfCreation(LocalDateTime.now())
                    .documentId(3L)
                    .build());


            technologicalOperationService.create(
                    TechnologicalOperationDto.builder()
                            .number("Оп-3")
                            .technologicalOperationDateTime(LocalDateTime.now().minusDays(5))
                            .technologicalMapId(technologicalMapService.getById(1L).getId())
                            .technologicalMapName(technologicalMapService.getById(1L).getName())
                            .volumeOfProduction(BigDecimal.valueOf(100))
                            .warehouseForMaterialsId(1L)
                            .warehouseForMaterialsName("Основной склад")
                            .warehouseForProductId(1L)
                            .warehouseForProductName("Основной склад")
                            .tasks(taskDtos3)
                            .build());

        } catch (Exception e) {
            log.error("Не удалось заполнить таблицу TechnologicalOperation", e);
        }
    }
}

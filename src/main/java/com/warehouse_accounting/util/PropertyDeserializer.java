package com.warehouse_accounting.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.warehouse_accounting.models.interfaces.Property;

import java.io.IOException;

public class PropertyDeserializer extends StdDeserializer<Property> {

    public PropertyDeserializer() {
        this(null);
    }

    public PropertyDeserializer(Class<?> vc) {
        super(vc);
    }
    @Override
    public Property deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode idNode = node.get("id");
        Long id = idNode != null ? idNode.longValue() : null;
        String type = node.get("type").asText();
        JsonNode value = node.get("value");

        return AdditionalFieldUtil.returnRightProperty(type, id, value);
    }
}

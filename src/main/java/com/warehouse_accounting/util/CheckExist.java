package com.warehouse_accounting.util;

import com.warehouse_accounting.exceptions.EntityNotFoundException;
import com.warehouse_accounting.models.dto.CurrencyDto;
import com.warehouse_accounting.repositories.CurrencyRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public class CheckExist<T extends JpaRepository> {

    private static CurrencyRepository currencyRepository;

    public CheckExist(CurrencyRepository repository) {
        this.currencyRepository = repository;
    }

    public static void check(Long id, Class<CurrencyDto> dtoClass) {

        if(!currencyRepository.existsById(id)) {
            throw new EntityNotFoundException(dtoClass.getSimpleName() + " с id=" + id + " не найден");
        }
    }
}

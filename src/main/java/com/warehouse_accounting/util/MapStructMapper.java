package com.warehouse_accounting.util;

import com.warehouse_accounting.models.Department;
import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.models.Image;
import com.warehouse_accounting.models.IpAddress;
import com.warehouse_accounting.models.IpNetwork;
import com.warehouse_accounting.models.Position;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.Tariff;
import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.models.dto.EmployeeDto;
import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.models.dto.IpNetworkDto;
import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.models.dto.RoleDto;
import com.warehouse_accounting.models.dto.TariffDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MapStructMapper {

    DepartmentDto convert(Department department);
    Department convert(DepartmentDto departmentDto);

    EmployeeDto convert(Employee employee);
    Employee convert(EmployeeDto employeeDto);

    ImageDto convert(Image image);
    Image convert(ImageDto imageDto);

    IpAddressDto convert(IpAddress ipAddress);
    IpAddress convert(IpAddressDto ipAddressDto);

    IpNetworkDto convert(IpNetwork ipNetwork);
    IpNetwork convert(IpNetworkDto ipNetworkDto);

    PositionDto convert(Position position);
    Position convert(PositionDto positionDto);

    RoleDto convert(Role role);
    Role convert(RoleDto roleDto);

    TariffDto convert(Tariff tariff);
    Tariff convert(TariffDto tariffDto);
}

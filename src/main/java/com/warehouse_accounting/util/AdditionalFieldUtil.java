package com.warehouse_accounting.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.warehouse_accounting.models.interfaces.Property;
import com.warehouse_accounting.models.property.IntegerProperty;
import com.warehouse_accounting.models.property.StringProperty;
import lombok.experimental.UtilityClass;

@UtilityClass
public class AdditionalFieldUtil {

    public static Property returnRightProperty(String type, Long id, JsonNode value) {

        Property result;
        switch (type) {
            case "Integer":
                result = IntegerProperty.builder().id(id).value(value != null ? value.intValue() : 0).build(); break;
            case "String" :
                result = StringProperty.builder().id(id).value(value != null ? value.asText() : "").build(); break;
            default:
                result = null;
        }

        return result;
    }
}

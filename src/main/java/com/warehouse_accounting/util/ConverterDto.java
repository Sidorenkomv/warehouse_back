package com.warehouse_accounting.util;

import com.warehouse_accounting.models.*;
import com.warehouse_accounting.models.dto.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Deprecated(forRemoval = true)
/**
 * @deprecated Use MapStructMapper bean if possible
 */
public class ConverterDto {

    private ConverterDto() {
    }

    public static Region convertToModel(RegionDto region) {
        return Region.builder()
                .id(region.getId())
                .name(region.getName())
                .socr(region.getSocr())
                .code(region.getCode())
                .ocatd(region.getOcatd())
                .build();
    }

    public static BonusProgramDto convertToDto(BonusProgram bonusProgram) {
        return BonusProgramDto.builder()
                .id(bonusProgram.getId())
                .name(bonusProgram.getName())
                .accrualRule(bonusProgram.getAccrualRule())
                .writeOffRule(bonusProgram.getWriteOffRule())
                .maxPercentage(bonusProgram.getMaxPercentage())
                .bonusDelay(bonusProgram.getBonusDelay())
                .award(bonusProgram.getAward())

                .build();


    }

    public static List<BonusProgramDto> convertBonusProgramToListDto(List<BonusProgram> bonusPrograms) {

        return bonusPrograms.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
    }

    public static List<ContractorGroupDto> convertContractorGroupToListDto(List<ContractorGroup> contractorGroupList) {
        return contractorGroupList.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
    }

    public static List<ContractorGroup> convertContractorGroupDtoToListModel(List<ContractorGroupDto> contractorGroupListDto) {
        return contractorGroupListDto.stream()
                .map(ConverterDto::convertToModel)
                .collect(Collectors.toList());
    }

    public static BonusProgram convertToModel(BonusProgramDto bonusProgramDto) {
        return BonusProgram.builder()
                .id(bonusProgramDto.getId())
                .name(bonusProgramDto.getName())
                .accrualRule(bonusProgramDto.getAccrualRule())
                .writeOffRule(bonusProgramDto.getWriteOffRule())
                .maxPercentage(bonusProgramDto.getMaxPercentage())
                .bonusDelay(bonusProgramDto.getBonusDelay())
                .award(bonusProgramDto.getAward())
                .build();
    }

    public static RegionDto convertToDto(Region region) {
        return RegionDto.builder()
                .id(region.getId())
                .name(region.getName())
                .socr(region.getSocr())
                .code(region.getCode())
                .ocatd(region.getOcatd())
                .build();
    }

    public static Building convertToModel(BuildingDto building) {
        return Building.builder()
                .id(building.getId())
                .name(building.getName())
                .socr(building.getSocr())
                .code(building.getCode())
                .index(building.getIndex())
                .build();
    }

    public static BuildingDto convertToDto(Building building) {
        return BuildingDto.builder()
                .id(building.getId())
                .name(building.getName())
                .socr(building.getSocr())
                .code(building.getCode())
                .index(building.getIndex())
                .build();
    }

    public static Address convertToModel(AddressDto addressDto) {
        if (addressDto == null) {
            return null;
        }

        return Address.builder()
                .id(addressDto.getId())
                .country(addressDto.getCountryId() != null
                        ? Country.builder().id(addressDto.getCountryId()).build()
                        : null)
                .postCode(addressDto.getPostCode())
                .region(addressDto.getRegionId() != null
                        ? Region.builder().id(addressDto.getRegionId()).build()
                        : null)
                .city(addressDto.getCityId() != null
                        ? City.builder().id(addressDto.getCityId()).build()
                        : null)
                .cityName(addressDto.getCityName())
                .street(addressDto.getStreetId() != null
                        ? Street.builder().id(addressDto.getStreetId()).build()
                        : null)
                .streetName(addressDto.getStreetName())
                .building(addressDto.getBuildingId() != null
                        ? Building.builder().id(addressDto.getBuildingId()).build()
                        : null)
                .buildingName(addressDto.getBuildingName())
                .office(addressDto.getOffice())
                .fullAddress(addressDto.getFullAddress())
                .other(addressDto.getOther())
                .comment(addressDto.getComment())
                .build();
    }

    public static AddressDto convertToDto(Address address) {
        if (address == null) {
            return null;
        }

        return AddressDto.builder()
                .id(address.getId())
                .countryId(address.getCountry() != null ? address.getCountry().getId() : null)
                .postCode(address.getPostCode())
                .regionId(address.getRegion() != null ? address.getRegion().getId() : null)
                .cityId(address.getCity() != null ? address.getCity().getId() : null)
                .cityName(address.getCityName())
                .streetId(address.getStreet() != null ? address.getStreet().getId() : null)
                .streetName(address.getStreetName())
                .buildingId(address.getBuilding() != null ? address.getBuilding().getId() : null)
                .buildingName(address.getBuildingName())
                .office(address.getOffice())
                .fullAddress(address.getFullAddress())
                .other(address.getOther())
                .comment(address.getComment())
                .build();

    }

    public static Application convertToModel(ApplicationDto application) {
        return Application.builder()
                .id(application.getId())
                .name(application.getName())
                .description(application.getDescription())
//                .isAuthorized(application.getIsAuthorized())
//                .developer(application.getDeveloper())
//                .devMail(application.getDevMail())
//                .devSite(application.getDevSite())
//                .isFree(application.getIsFree())
//                .logoId(application.getLogoId())
                .vkUserId(application.getVkUserId())
                .vkAccessToken(application.getVkAccessToken())
                .vkGroupId(application.getVkGroupId())
                .build();
    }

    public static ApplicationDto convertToDto(Application application) {
        return ApplicationDto.builder()
                .id(application.getId())
                .name(application.getName())
                .description(application.getDescription())
//                .isAuthorized(application.getIsAuthorized())
//                .developer(application.getDeveloper())
//                .devMail(application.getDevMail())
//                .devSite(application.getDevSite())
//                .isFree(application.getIsFree())
//                .logoId(application.getLogoId())
                .vkUserId(application.getVkUserId())
                .vkAccessToken(application.getVkAccessToken())
                .vkGroupId(application.getVkGroupId())
                .build();
    }

    public static SupplierInvoice convertToModel(SupplierInvoiceDto supplierInvoiceDto) {
        return SupplierInvoice.builder()
                .id(supplierInvoiceDto.getId())
                .invoiceNumber(supplierInvoiceDto.getInvoiceNumber())
                .dateInvoiceNumber(supplierInvoiceDto.getDateInvoiceNumber())
                .checkboxProd(supplierInvoiceDto.getCheckboxProd())
                .organization(supplierInvoiceDto.getOrganization())
                .warehouse(supplierInvoiceDto.getWarehouse())
                .contrAgent(supplierInvoiceDto.getContrAgent())
                .contract(supplierInvoiceDto.getContract())
                .datePay(supplierInvoiceDto.getDatePay())
                .project(supplierInvoiceDto.getProject())
                .incomingNumber(supplierInvoiceDto.getIncomingNumber())
                .dateIncomingNumber(supplierInvoiceDto.getDateIncomingNumber())
                .checkboxName(supplierInvoiceDto.getCheckboxName())
                .checkboxNDS(supplierInvoiceDto.getCheckboxNDS())
                .checkboxOnNDS(supplierInvoiceDto.getCheckboxOnNDS())
                .addPosition(supplierInvoiceDto.getAddPosition())
                .addComment(supplierInvoiceDto.getAddComment())
                .build();
    }

    public static SupplierInvoiceDto convertToDto(SupplierInvoice supplierInvoice) {
        return SupplierInvoiceDto.builder()
                .id(supplierInvoice.getId())
                .invoiceNumber(supplierInvoice.getInvoiceNumber())
                .dateInvoiceNumber(supplierInvoice.getDateInvoiceNumber())
                .checkboxProd(supplierInvoice.getCheckboxProd())
                .organization(supplierInvoice.getOrganization())
                .warehouse(supplierInvoice.getWarehouse())
                .contrAgent(supplierInvoice.getContrAgent())
                .contract(supplierInvoice.getContract())
                .datePay(supplierInvoice.getDatePay())
                .project(supplierInvoice.getProject())
                .incomingNumber(supplierInvoice.getIncomingNumber())
                .dateIncomingNumber(supplierInvoice.getDateIncomingNumber())
                .checkboxName(supplierInvoice.getCheckboxName())
                .checkboxNDS(supplierInvoice.getCheckboxNDS())
                .checkboxOnNDS(supplierInvoice.getCheckboxOnNDS())
                .addPosition(supplierInvoice.getAddPosition())
                .addComment(supplierInvoice.getAddComment())
                .build();
    }

    public static SupplierOrdersDto convertToDto(SupplierOrders supplierOrders) {
        return SupplierOrdersDto.builder()
                .id(supplierOrders.getId())
                .number(supplierOrders.getDocNumber())
                .createdAt(supplierOrders.getDate())
                .contrAgentId(supplierOrders.getContrAgent() != null ? supplierOrders.getContrAgent().getId() : null)
                .contrAgentName(supplierOrders.getCompany() != null ? supplierOrders.getContrAgent().getName() : null)
                .contrAgentAccount(supplierOrders.getContrAgentAccount())
                .companyId(supplierOrders.getCompany() != null ? supplierOrders.getCompany().getId() : null)
                .companyName(supplierOrders.getCompany() != null ? supplierOrders.getCompany().getName() : null)
                .companyAccount(supplierOrders.getCompanyAccount())
                .sum(supplierOrders.getSum())
                .invoiceIssued(supplierOrders.getInvoiceIssued())
                .paid(supplierOrders.getPaid())
                .notPaid(supplierOrders.getNotPaid())
                .accepted(supplierOrders.getAccepted())
                .waiting(supplierOrders.getWaiting())
                .refundAmount(supplierOrders.getRefundAmount())
                .acceptanceDate(supplierOrders.getAcceptanceDate())
                .projectId(supplierOrders.getProject() != null ? supplierOrders.getProject().getId() : null)
                .projectName(supplierOrders.getProject() != null ? supplierOrders.getProject().getName() : null)
                .warehouseId(supplierOrders.getWarehouseFrom() != null ? supplierOrders.getWarehouseFrom().getId() : null)
                .warehouseName(supplierOrders.getWarehouseFrom() != null ? supplierOrders.getWarehouseFrom().getName() : null)
                .contractId(supplierOrders.getContract() != null ? supplierOrders.getContract().getId() : null)
                .contractNumber(supplierOrders.getContract() != null ? supplierOrders.getContract().getNumber() : null)
                .generalAccess(supplierOrders.getIsSharedAccess())
                .departmentId(supplierOrders.getDepartment() != null ? supplierOrders.getDepartment().getId() : null)
                .departmentName(supplierOrders.getDepartment() != null ? supplierOrders.getDepartment().getName() : null)
                .employeeId(supplierOrders.getEmployee() != null ? supplierOrders.getEmployee().getId() : null)
                .employeeFirstname(supplierOrders.getEmployee() != null ? supplierOrders.getEmployee().getFirstName() : null)
                .sent(supplierOrders.getSent())
                .print(supplierOrders.getPrint())
                .comment(supplierOrders.getComments())
                .updatedAt(supplierOrders.getUpdatedAt())
                .updatedFromEmployeeId(supplierOrders.getUpdatedFromEmployee() != null ? supplierOrders.getUpdatedFromEmployee().getId() : null)
                .updatedFromEmployeeFirstname(supplierOrders.getUpdatedFromEmployee() != null ? supplierOrders.getUpdatedFromEmployee().getFirstName() : null)
                .build();
    }

    public static SupplierOrders convertToModel(SupplierOrdersDto dto) {
        Contractor contractor = new Contractor();
        contractor.setId(dto.getContrAgentId());

        Company company = new Company();
        company.setId(dto.getCompanyId());

        Project project = new Project();
        project.setId(dto.getProjectId());

        Warehouse warehouse = new Warehouse();
        warehouse.setId(dto.getWarehouseId());

        Contract contract = new Contract();
        contract.setId(dto.getContractId());

        Department department = new Department();
        department.setId(dto.getDepartmentId());

        Employee employee = new Employee();
        employee.setId(dto.getEmployeeId());

        Employee updaterFromEmployee = new Employee();
        updaterFromEmployee.setId(dto.getUpdatedFromEmployeeId());

        return SupplierOrders.builder()
                .id(dto.getId())
                .docNumber(dto.getNumber())
                .date(dto.getCreatedAt())
                .contrAgent(contractor)
                .contrAgentAccount(dto.getContrAgentAccount())
                .company(company)
                .companyAccount(dto.getCompanyAccount())
                .sum(dto.getSum())
                .invoiceIssued(dto.getInvoiceIssued())
                .paid(dto.getPaid())
                .notPaid(dto.getNotPaid())
                .accepted(dto.getAccepted())
                .waiting(dto.getWaiting())
                .refundAmount(dto.getRefundAmount())
                .acceptanceDate(dto.getAcceptanceDate())
                .project(project)
                .warehouseFrom(warehouse)
                .contract(contract)
                .isSharedAccess(dto.getGeneralAccess())
                .department(department)
                .employee(employee)
                .sent(dto.getSent())
                .print(dto.getPrint())
                .comments(dto.getComment())
                .updatedAt(dto.getUpdatedAt())
                .updatedFromEmployee(updaterFromEmployee)
                .build();
    }

    public static Document convertToModel(DocumentDto documentDto) {
        Warehouse warehouseFrom = new Warehouse();
        warehouseFrom.setId(documentDto.getWarehouseFromId());

        Warehouse warehouseTo = new Warehouse();
        warehouseTo.setId(documentDto.getWarehouseToId());

        Company company = new Company();
        company.setId(documentDto.getCompanyId());

        Contractor contractor = new Contractor();
        contractor.setId(documentDto.getContrAgentId());

        Project project = new Project();
        project.setId(documentDto.getProjectId());

        SalesChannel salesChannel = new SalesChannel();
        salesChannel.setId(documentDto.getSalesChannelId());

        Contract contract = new Contract();
        contract.setId(documentDto.getContractId());

        Department department = new Department();
        department.setId(documentDto.getDepartmentId());

        Employee employee = new Employee();
        employee.setId(documentDto.getEmployeeId());

        Employee updatedFromEmployee = new Employee();
        updatedFromEmployee.setId(documentDto.getUpdatedFromEmployeeId());

        return Document.builder()
                .id(documentDto.getId())
                .type(documentDto.getType())
                .docNumber(documentDto.getDocNumber())
                .date(documentDto.getDate())
                .sum(documentDto.getSum())
                .warehouseFrom(warehouseFrom)
                .warehouseTo(warehouseTo)
                .company(company)
                .contrAgent(contractor)
                .project(project)
                .salesChannel(salesChannel)
                .contract(contract)
                .isSharedAccess(documentDto.getIsSharedAccess())
                .department(department)
                .employee(employee)
                .sent(documentDto.getSent())
                .print(documentDto.getPrint())
                .comments(documentDto.getComments())
                .updatedAt(documentDto.getUpdatedAt())
                .updatedFromEmployee(updatedFromEmployee)
                .tasks(documentDto.getTasks() != null ? documentDto.getTasks()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .files(documentDto.getFiles() != null ? documentDto.getFiles()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .build();
    }

    public static DocumentDto convertToDto(Document document) {
        return DocumentDto.builder()
                .id(document.getId())
                .type(document.getType())
                .docNumber(document.getDocNumber())
                .date(document.getDate())
                .sum(document.getSum())
                .warehouseFromId(document.getWarehouseFrom() != null ? document.getWarehouseFrom().getId() : null)
                .warehouseFromName(document.getWarehouseFrom() != null ? document.getWarehouseFrom().getName() : null)
                .warehouseToId(document.getWarehouseTo() != null ? document.getWarehouseTo().getId() : null)
                .warehouseToName(document.getWarehouseTo() != null ? document.getWarehouseTo().getName() : null)
                .companyId(document.getCompany() != null ? document.getCompany().getId() : null)
                .companyName(document.getCompany() != null ? document.getCompany().getName() : null)
                .contrAgentId(document.getContrAgent() != null ? document.getContrAgent().getId() : null)
                .contrAgentName(document.getContrAgent() != null ? document.getContrAgent().getName() : null)
                .projectId(document.getProject() != null ? document.getProject().getId() : null)
                .projectName(document.getProject() != null ? document.getProject().getName() : null)
                .salesChannelId(document.getSalesChannel() != null ? document.getSalesChannel().getId() : null)
                .salesChannelName(document.getSalesChannel() != null ? document.getSalesChannel().getName() : null)
                .contractId(document.getContract() != null ? document.getContract().getId() : null)
                .contractNumber(document.getContract() != null ? document.getContract().getNumber() : null)
                .isSharedAccess(document.getIsSharedAccess())
                .departmentId(document.getDepartment() != null ? document.getDepartment().getId() : null)
                .departmentName(document.getDepartment() != null ? document.getDepartment().getName() : null)
                .employeeId(document.getEmployee() != null ? document.getEmployee().getId() : null)
                .employeeFirstname(document.getEmployee() != null ? document.getEmployee().getFirstName() : null)
                .sent(document.getSent())
                .print(document.getPrint())
                .comments(document.getComments())
                .updatedAt(document.getUpdatedAt())
                .updatedFromEmployeeId(document.getUpdatedFromEmployee() != null ? document.getUpdatedFromEmployee().getId() : null)
                .updatedFromEmployeeFirstname(document.getUpdatedFromEmployee() != null ? document.getUpdatedFromEmployee().getFirstName() : null)
                .build();
    }

    public static Currency convertToModel(CurrencyDto currencyDto) {
        return Currency.builder()
                .id(currencyDto.getId())
                .name(currencyDto.getName())
                .charcode(currencyDto.getCharcode())
                .nominal(currencyDto.getNominal())
                .numcode(currencyDto.getNumcode())
                .value(currencyDto.getValue())
                .build();
    }

    public static CurrencyDto convertToDto(Currency currency) {
        return CurrencyDto.builder()
                .id(currency.getId())
                .name(currency.getName())
                .charcode(currency.getCharcode())
                .nominal(currency.getNominal())
                .numcode(currency.getNumcode())
                ._Value(currency.get_Value())
                .build();
    }


    public static BonusTransaction convertToModel(BonusTransactionDto bonusTransactionDto) {
        return BonusTransaction.builder()
                .id(bonusTransactionDto.getId())
                .created(bonusTransactionDto.getCreated())
                .transactionType(bonusTransactionDto.getTransactionType())
                .bonusValue(bonusTransactionDto.getBonusValue())
                .ownerChanged(convertToModel(bonusTransactionDto.getOwnerChangedDto()))
                .department(convertToModel(bonusTransactionDto.getDepartmentDto()))
                .transactionStatus(bonusTransactionDto.getTransactionStatus())
                .dateChange(bonusTransactionDto.getDateChange())
                .generalAccess(bonusTransactionDto.isGeneralAccess())
                .executionDate(bonusTransactionDto.getExecutionDate())
                .files(bonusTransactionDto.getFilesDto().stream()
                        .map(ConverterDto::convertToModel).collect(Collectors.toList()))
                .bonusProgram(convertToModel(bonusTransactionDto.getBonusProgramDto()))
                .comment(bonusTransactionDto.getComment())
                .contragent(convertToModel(bonusTransactionDto.getContractorDto()))
                .owner(convertToModel(bonusTransactionDto.getOwnerDto()))
                .build();
    }

    public static BonusTransactionDto convertToDto(BonusTransaction bonusTransaction) {
        return BonusTransactionDto.builder()
                .id(bonusTransaction.getId())
                .created(bonusTransaction.getCreated())
                .transactionType(bonusTransaction.getTransactionType())
                .bonusValue(bonusTransaction.getBonusValue())
                .transactionStatus(bonusTransaction.getTransactionStatus())
                .executionDate(bonusTransaction.getExecutionDate())
                .filesDto(bonusTransaction.getFiles().stream()
                        .map(ConverterDto::convertToDto).collect(Collectors.toList()))
                .ownerDto(convertToDto(bonusTransaction.getOwner()))
                .generalAccess(bonusTransaction.isGeneralAccess())
                .dateChange(bonusTransaction.getDateChange())
                .bonusProgramDto(convertToDto(bonusTransaction.getBonusProgram()))
                .comment(bonusTransaction.getComment())
                .contractorDto(convertToDto(bonusTransaction.getContragent()))
                .ownerChangedDto(convertToDto(bonusTransaction.getOwnerChanged()))
                .departmentDto(convertToDto(bonusTransaction.getDepartment()))
                .build();
    }

    public static AdjustmentDto convertToDto(Adjustment adjustment) {
        return AdjustmentDto.builder()
                .id(adjustment.getId())
                .number(adjustment.getNumber())
                .dateTimeAdjustment(adjustment.getDateTimeAdjustment())
                .companyId(adjustment.getCompany() != null ? adjustment.getCompany().getId() : null)
                .companyName(adjustment.getCompany() != null ? adjustment.getCompany().getName() : null)
                .contractorId(adjustment.getContractor() != null ? adjustment.getContractor().getId() : null)
                .contractorName(adjustment.getContractor() != null ? adjustment.getContractor().getName() : null)
                .type(adjustment.getType())
                .currentBalance(adjustment.getCurrentBalance())
                .totalBalance(adjustment.getTotalBalance())
                .adjustmentAmount(adjustment.getAdjustmentAmount())
                .comment(adjustment.getComment())
                .whenСhanged(adjustment.getWhenСhanged())
                .build();
    }

    public static Adjustment convertToModel(AdjustmentDto dto) {
        Company company = new Company();
        company.setId(dto.getCompanyId());
        Contractor contractor = new Contractor();
        contractor.setId(dto.getContractorId());
        return Adjustment.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .company(company)
                .contractor(contractor)
                .type(dto.getType())
                .currentBalance(dto.getCurrentBalance())
                .totalBalance(dto.getTotalBalance())
                .adjustmentAmount(dto.getAdjustmentAmount())
                .comment(dto.getComment())
                .whenСhanged(dto.getWhenСhanged())
                .build();
    }

    public static AttributeOfCalculationObjectDto convertToDto(AttributeOfCalculationObject model) {
        if (model == null) {
            return null;
        }
        return AttributeOfCalculationObjectDto.builder()
                .id(model.getId())
                .name(model.getName())
                .sortNumber(model.getSortNumber())
                .isService(model.getIsService())
                .build();
    }

    public static AttributeOfCalculationObject convertToModel(AttributeOfCalculationObjectDto dto) {
        return AttributeOfCalculationObject.builder()
                .id(dto.getId())
                .name(dto.getName())
                .sortNumber(dto.getSortNumber())
                .isService(dto.getIsService())
                .build();
    }

    public static Department convertToModel(DepartmentDto departmentDto) {
        if (departmentDto == null) {
            return null;
        }
        return Department.builder()
                .id(departmentDto.getId())
                .name(departmentDto.getName())
                .sortNumber(departmentDto.getSortNumber())
                .build();
    }

    public static DepartmentDto convertToDto(Department department) {
        if (department == null) {
            return null;
        }
        return DepartmentDto.builder()
                .id(department.getId())
                .name(department.getName())
                .sortNumber(department.getSortNumber())
                .build();
    }

    public static BankAccount convertToModel(BankAccountDto bankAccountDto) {
        if (bankAccountDto == null) {
            return null;
        }

        return BankAccount.builder()
                .id(bankAccountDto.getId())
                .rcbic(bankAccountDto.getRcbic())
                .bank(bankAccountDto.getBank())
                .correspondentAccount(bankAccountDto.getCorrespondentAccount())
                .account(bankAccountDto.getAccount())
                .mainAccount(bankAccountDto.getMainAccount())
                .sortNumber(bankAccountDto.getSortNumber())
                .bankAddress(bankAccountDto.getBankAddress())
                .contractor(ConverterDto.convertToModel(bankAccountDto.getContractor()))
                .build();
    }

    public static BankAccountDto convertToDto(BankAccount bankAccount) {
        return BankAccountDto.builder()
                .id(bankAccount.getId())
                .rcbic(bankAccount.getRcbic())
                .bank(bankAccount.getBank())
                .correspondentAccount(bankAccount.getCorrespondentAccount())
                .account(bankAccount.getAccount())
                .mainAccount(bankAccount.getMainAccount())
                .sortNumber(bankAccount.getSortNumber())
                .bankAddress(bankAccount.getBankAddress())
                .contractor(ConverterDto.convertToDto(bankAccount.getContractor()))
                .build();
    }

    public static Discount convertToModel(DiscountDto discountDto) {
        if (discountDto == null) {
            return null;
        }
        return Discount.builder()
                .id(discountDto.getId())
                .active(discountDto.getActive())
                .name(discountDto.getName())
                .discountType(convertToModel(discountDto.getDiscountType()))
                .priceTypeProductCard(discountDto.getPriceTypeProductCard())
                .fixedDiscount(discountDto.getFixedDiscount())
                .sumCumulative(discountDto.getSumCumulative())
                .discountPercent(discountDto.getDiscountPercent())
                .accrualPoints(discountDto.getAccrualPoints())
                .writeOff(discountDto.getWriteOff())
                .maxPercentPayment(discountDto.getMaxPercentPayment())
                .pointsAwardedInDays(discountDto.getPointsAwardedInDays())
                .SimultaneousAccrualAndWriteOff(discountDto.getSimultaneousAccrualAndWriteOff())
                .welcomePoints(discountDto.getWelcomePoints())
                .welcomePointsAccrual(discountDto.getWelcomePointsAccrual())
                .firstPurchase(discountDto.getFirstPurchase())
                .registrationBonusProgram(discountDto.getRegistrationBonusProgram())
                .build();
    }

    public static DiscountDto convertToDto(Discount discount) {
        if (discount == null) {
            return null;
        }
        return DiscountDto.builder()
                .id(discount.getId())
                .active(discount.getActive())
                .name(discount.getName())
                .discountType(convertToDto(discount.getDiscountType()))
                .priceTypeProductCard(discount.getPriceTypeProductCard())
                .fixedDiscount(discount.getFixedDiscount())
                .sumCumulative(discount.getSumCumulative())
                .discountPercent(discount.getDiscountPercent())
                .accrualPoints(discount.getAccrualPoints())
                .writeOff(discount.getWriteOff())
                .maxPercentPayment(discount.getMaxPercentPayment())
                .pointsAwardedInDays(discount.getPointsAwardedInDays())
                .SimultaneousAccrualAndWriteOff(discount.getSimultaneousAccrualAndWriteOff())
                .welcomePoints(discount.getWelcomePoints())
                .welcomePointsAccrual(discount.getWelcomePointsAccrual())
                .firstPurchase(discount.getFirstPurchase())
                .registrationBonusProgram(discount.getRegistrationBonusProgram())
                .build();
    }

    public static DiscountType convertToModel(DiscountTypeDto discountTypeDto) {
        if (discountTypeDto == null) {
            return null;
        }
        return DiscountType.builder()
                .id(discountTypeDto.getId())
                .name(discountTypeDto.getName())
                .build();
    }

    public static DiscountTypeDto convertToDto(DiscountType discountType) {
        if (discountType == null) {
            return null;
        }
        return DiscountTypeDto.builder()
                .id(discountType.getId())
                .name(discountType.getName())
                .build();
    }

    public static Unit convertToModel(UnitDto unitDto) {
        if (unitDto == null) {
            return null;
        }
        return Unit.builder()
                .id(unitDto.getId())
                .shortName(unitDto.getShortName())
                .fullName(unitDto.getFullName())
                .sortNumber(unitDto.getSortNumber())
                .build();
    }

    public static UnitDto convertToDto(Unit unit) {
        if (unit == null) {
            return null;
        }
        return UnitDto.builder()
                .id(unit.getId())
                .shortName(unit.getShortName())
                .fullName(unit.getFullName())
                .sortNumber(unit.getSortNumber())
                .build();
    }

    public static Warehouse convertToModel(WarehouseDto warehouseDto) {
        if (warehouseDto == null) {
            return null;
        }
        return Warehouse.builder()
                .id(warehouseDto.getId())
                .name(warehouseDto.getName())
                .sortNumber(warehouseDto.getSortNumber())
                .address(convertToModel(warehouseDto.getAddress()))
                .comment(warehouseDto.getComment())
                .build();
    }

    public static WarehouseDto convertToDto(Warehouse warehouse) {
        if (warehouse == null) {
            return null;
        }
        return WarehouseDto.builder()
                .id(warehouse.getId())
                .name(warehouse.getName())
                .sortNumber(warehouse.getSortNumber())
                .address(convertToDto(warehouse.getAddress()))
                .comment(warehouse.getComment())
                .build();
    }

    public static TaxSystem convertToModel(TaxSystemDto taxSystemDto) {
        return TaxSystem.builder()
                .id(taxSystemDto.getId())
                .name(taxSystemDto.getName())
                .sortNumber(taxSystemDto.getSortNumber())
                .build();
    }

    public static TaxSystemDto convertToDto(TaxSystem taxSystem) {
        if (taxSystem == null) {
            return null;
        }
        return TaxSystemDto.builder()
                .id(taxSystem.getId())
                .name(taxSystem.getName())
                .sortNumber(taxSystem.getSortNumber())
                .build();
    }

    public static Company convertToModel(CompanyDto dto) {
        if (dto == null) {
            return null;
        }
        return Company.builder()
                .address(convertToModel(dto.getAddress()))
                .chiefAccountant(dto.getChiefAccountant())
                .chiefAccountantSignature(dto.getChiefAccountantSignature())
                .email(dto.getEmail())
                .fax(dto.getFax())
                .id(dto.getId())
                .inn(dto.getInn())
                .leader(dto.getLeader())
                .leaderManagerPosition(dto.getLeaderManagerPosition())
                .leaderSignature(dto.getLeaderSignature())
                .legalDetail(dto.getLegalDetailDto() != null ? convertToModel(dto.getLegalDetailDto()) : null)
                .name(dto.getName())
                .payerVat(dto.getPayerVat())
                .phone(dto.getPhone())
                .sortNumber(dto.getSortNumber())
                .stamp(dto.getStamp())
                .build();
    }

    public static CompanyDto convertToDto(Company company) {
        if (company == null) {
            return null;
        }
        return CompanyDto.builder()
                .address(convertToDto(company.getAddress()))
                .chiefAccountant(company.getChiefAccountant())
                .chiefAccountantSignature(company.getChiefAccountantSignature())
                .email(company.getEmail())
                .fax(company.getFax())
                .id(company.getId())
                .inn(company.getInn())
                .leader(company.getLeader())
                .leaderManagerPosition(company.getLeaderManagerPosition())
                .leaderSignature(company.getLeaderSignature())
                .legalDetailDto(company.getLegalDetail() != null ? convertToDto(company.getLegalDetail()) : null)
                .name(company.getName())
                .payerVat(company.getPayerVat())
                .phone(company.getPhone())
                .sortNumber(company.getSortNumber())
                .stamp(company.getStamp())
                .build();
    }

    public static ContactFace convertToModel(ContactFaceDto dto) {
        if (dto == null) {
            return null;
        }
        return ContactFace.builder()
                .id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .phone(dto.getPhone())
                .email(dto.getEmail())
                .externalCode(dto.getExternalCode())
                .address(convertToModel(dto.getAddress()))
                .position(dto.getPosition())
                .linkContractor(dto.getLinkContractor())
                .comment(dto.getComment())
                .build();
    }

    public static ContactFaceDto convertToDto(ContactFace contactFace) {
        if (contactFace == null) {
            return null;
        }
        return ContactFaceDto.builder()
                .id(contactFace.getId())
                .name(contactFace.getName())
                .description(contactFace.getDescription())
                .phone(contactFace.getPhone())
                .email(contactFace.getEmail())
                .externalCode(contactFace.getExternalCode())
                .address(convertToDto(contactFace.getAddress()))
                .position(contactFace.getPosition())
                .linkContractor(contactFace.getLinkContractor())
                .comment(contactFace.getComment())
                .build();
    }

    public static ContractorGroup convertToModel(ContractorGroupDto contractorGroupDto) {
        return ContractorGroup.builder()
                .id(contractorGroupDto.getId())
                .name(contractorGroupDto.getName())
                .sortNumber(contractorGroupDto.getSortNumber())
                .build();
    }

    public static ContractorGroupDto convertToDto(ContractorGroup contractorGroup) {
        return ContractorGroupDto.builder()
                .id(contractorGroup.getId())
                .name(contractorGroup.getName())
                .sortNumber(contractorGroup.getSortNumber())
                .build();
    }

    public static Position convertToModel(PositionDto positionDto) {
        return Position.builder()
                .id(positionDto.getId())
                .name(positionDto.getName())
                .sortNumber(positionDto.getSortNumber())
                .build();
    }

    public static PositionDto convertToDto(Position position) {
        return PositionDto.builder()
                .id(position.getId())
                .name(position.getName())
                .sortNumber(position.getSortNumber())
                .build();
    }

    public static TypeOfContractorDto convertToDto(TypeOfContractor typeOfContractor) {
        return typeOfContractor != null ?
                TypeOfContractorDto.builder()
                        .id(typeOfContractor.getId())
                        .name(typeOfContractor.getName())
                        .sortNumber(typeOfContractor.getSortNumber())
                        .build()
                : null;
    }

    public static TypeOfContractor convertToModel(TypeOfContractorDto typeOfContractorDto) {
        return typeOfContractorDto != null ?
                TypeOfContractor.builder()
                        .id(typeOfContractorDto.getId())
                        .name(typeOfContractorDto.getName())
                        .sortNumber(typeOfContractorDto.getSortNumber())
                        .build()
                : null;
    }

    public static LegalDetail convertToModel(LegalDetailDto legalDetailDto) {
        if (legalDetailDto == null) {
            return null;
        }
//        TypeOfContractor typeOfContractor = new TypeOfContractor();
//        if (legalDetailDto.getTypeOfContractorName() != null) {
//            switch (legalDetailDto.getTypeOfContractorName()) {
//                case "Юридическое лицо":
//                    typeOfContractor.setId(1L);
//                    break;
//                case "Индивидуальный предприниматель":
//                    typeOfContractor.setId(2L);
//                    break;
//                case "Физическое лицо":
//                    typeOfContractor.setId(3L);
//                    break;
//                default:
//                    typeOfContractor.setId(legalDetailDto.getTypeOfContractorId());
//            }
//        } else {
//            typeOfContractor.setId(legalDetailDto.getTypeOfContractorId());
//        }


        return LegalDetail.builder()
                .id(legalDetailDto.getId())
                .fullName(legalDetailDto.getFullName())
                .firstName(legalDetailDto.getFirstName())
                .middleName(legalDetailDto.getMiddleName())
                .lastName(legalDetailDto.getLastName())
                .address(convertToModel(legalDetailDto.getAddress()))
                .inn(legalDetailDto.getInn())
                .kpp(legalDetailDto.getKpp())
                .okpo(legalDetailDto.getOkpo())
                .ogrn(legalDetailDto.getOgrn())
                .numberOfTheCertificate(legalDetailDto.getNumberOfTheCertificate())
                .dateOfTheCertificate(legalDetailDto.getDateOfTheCertificate())
                .typeOfContractor(convertToModel(legalDetailDto.getTypeOfContractor()))
                .build();
    }

    public static LegalDetailDto convertToDto(LegalDetail legalDetail) {
        if (legalDetail == null) {
            return null;
        }
        return LegalDetailDto.builder()
                .id(legalDetail.getId())
                .fullName(legalDetail.getFullName())
                .address(convertToDto(legalDetail.getAddress()))
                .inn(legalDetail.getInn())
                .kpp(legalDetail.getKpp())
                .okpo(legalDetail.getOkpo())
                .ogrn(legalDetail.getOgrn())
                .typeOfContractor(convertToDto(legalDetail.getTypeOfContractor()))
                .build();
    }

    public static ContractDto convertToDto(Contract contract) {
        if (contract == null) {
            return null;
        }
        return ContractDto.builder()
                .id(contract.getId())
                .number(contract.getNumber())
                .code(contract.getCode())
                .contractDate(contract.getContractDate())
//                .companyDto(convertToDto(contract.getCompany()))
//                .bankAccountDto(convertToDto(contract.getBankAccount()))
//                .contractorDto(convertToDto(contract.getContractor()))
                .amount(contract.getAmount())
                .archive(contract.getArchive())
                .comment(contract.getComment())
                .company(convertToDto(contract.getCompany()))
                .contractor(convertToDto(contract.getContractor()))
//                .legalDetail(convertToDto(contract.getLegalDetail()))
                .typeOfContract(contract.getTypeOfContract())
                .reward(contract.getReward())
                .percentageOfTheSaleAmount(contract.getPercentageOfTheSaleAmount())
                .enabled(contract.getEnabled())
                .build();
    }

    public static Contract convertToModel(ContractDto contractDto) {
        if (contractDto == null) {
            return null;
        }
        return Contract.builder()
                .id(contractDto.getId())
                .number(contractDto.getNumber())
                .code(contractDto.getCode())
                .contractDate(contractDto.getContractDate())
//                .company(convertToModel(contractDto.getCompanyDto()))
//                .bankAccount(convertToModel(contractDto.getBankAccountDto()))
//                .contractor(convertToModel(contractDto.getContractorDto()))
                .amount(contractDto.getAmount())
                .archive(contractDto.getArchive())
                .comment(contractDto.getComment())
                .company(convertToModel(contractDto.getCompany()))
                .contractor(convertToModel(contractDto.getContractor()))
//                .legalDetail(convertToModel(contractDto.getLegalDetail()))
                .typeOfContract(contractDto.getTypeOfContract())
                .reward(contractDto.getReward())
                .percentageOfTheSaleAmount(contractDto.getPercentageOfTheSaleAmount())
                .enabled(contractDto.getEnabled())
                .build();
    }

    public static Image convertToModel(ImageDto imageDto) {
        return Image.builder()
                .id(imageDto.getId())
                .imageUrl(imageDto.getImageUrl())
                .sortNumber(imageDto.getSortNumber())
                .build();
    }

    public static ImageDto convertToDto(Image image) {
        if (image == null) {
            return null;
        }
        return ImageDto.builder()
                .id(image.getId())
                .imageUrl(image.getImageUrl())
                .sortNumber(image.getSortNumber())
                .build();
    }

    public static ProductGroupDto convertToDto(ProductGroup productGroup) {
        if (productGroup == null) {
            return null;
        }
        return ProductGroupDto.builder()
                .id(productGroup.getId())
                .name(productGroup.getName())
                .sortNumber(productGroup.getSortNumber())
                .parentId(productGroup.getParentId())
                .build();
    }

    public static ProductGroup convertToModel(ProductGroupDto productGroupDto) {
        ProductGroup productGroup = new ProductGroup();
        productGroup.setId(productGroupDto.getParentId());
        return ProductGroup.builder()
                .id(productGroupDto.getId())
                .name(productGroupDto.getName())
                .sortNumber(productGroupDto.getSortNumber())
                .parentId(productGroupDto.getParentId())
                .build();
    }

    public static TypeOfPriceDto convertToDto(TypeOfPrice typeOfPrice) {
        if (typeOfPrice == null) {
            return null;
        }
        return TypeOfPriceDto.builder()
                .id(typeOfPrice.getId())
                .name(typeOfPrice.getName())
                .sortNumber(typeOfPrice.getSortNumber())
                .build();
    }

    public static TypeOfPrice convertToModel(TypeOfPriceDto typeOfPriceDto) {
        if (typeOfPriceDto == null) {
            return null;
        }
        return TypeOfPrice.builder()
                .id(typeOfPriceDto.getId())
                .name(typeOfPriceDto.getName())
                .sortNumber(typeOfPriceDto.getSortNumber())
                .build();
    }

    public static CallDto convertToDto(Call call) {
        return CallDto.builder()
                .id(call.getId())
                .callTime(call.getCallTime())
                .type(call.getType())
                .number(call.getNumber())
                .callDuration(call.getCallDuration())
                .comment(call.getComment())
                .callRecord(call.getCallRecord())
                .whenChanged(call.getWhenChanged())
                .contractorId(call.getContractor() != null
                        ? call.getContractor().getId() : null)
                .contractorName(call.getContractor() != null
                        ? call.getContractor().getName() : null)
                .employeeWhoCalledId(call.getEmployeeWhoCalled() != null
                        ? call.getEmployeeWhoCalled().getId() : null)
                .employeeWhoCalledName(call.getEmployeeWhoCalled() != null
                        ? call.getEmployeeWhoCalled().getFirstName() : null)
                .employeeWhoChangedId(call.getEmployeeWhoChanged() != null
                        ? call.getEmployeeWhoChanged().getId() : null)
                .employeeWhoChangedName(call.getEmployeeWhoChanged() != null
                        ? call.getEmployeeWhoChanged().getFirstName() : null)
                .build();
    }

    public static Call convertToModel(CallDto callDto) {
        Contractor contractor = new Contractor();
        contractor.setId(callDto.getContractorId());
        Employee employeeWhoCalled = new Employee();
        employeeWhoCalled.setId(callDto.getEmployeeWhoCalledId());
        Employee employeeWhoChanged = new Employee();
        employeeWhoChanged.setId(callDto.getEmployeeWhoChangedId());

        return Call.builder()
                .id(callDto.getId())
                .callTime(callDto.getCallTime())
                .type(callDto.getType())
                .number(callDto.getNumber())
                .callDuration(callDto.getCallDuration())
                .comment(callDto.getComment())
                .callRecord(callDto.getCallRecord())
                .whenChanged(callDto.getWhenChanged())
                .contractor(contractor)
                .employeeWhoChanged(employeeWhoChanged)
                .employeeWhoCalled(employeeWhoCalled)
                .build();
    }

    public static ContractorDto convertToDto(Contractor contractor) {
        if (contractor == null) {
            return null;
        }
        return ContractorDto.builder()
                .id(contractor.getId())
                .name(contractor.getName())
                .status(contractor.getStatus())
                .contractorGroup(convertToDto(contractor.getContractorGroup()))
                .code(contractor.getCode())
                .sortNumber(contractor.getSortNumber())
                .phone(contractor.getPhone())
                .fax(contractor.getFax())
                .email(contractor.getEmail())
                .address(convertToDto(contractor.getAddress()))
                .comment(contractor.getComment())
                .numberDiscountCard(contractor.getNumberDiscountCard())
                .typeOfPrice(convertToDto(contractor.getTypeOfPrice()))
                .legalDetail(convertToDto(contractor.getLegalDetail()))
                .callIds(contractor.getCalls() != null ?
                        contractor.getCalls().stream().map(Call::getId).collect(Collectors.toList()) : null)
                .build();
    }


    public static Contractor convertToModel(ContractorDto contractorDto) {
        if (contractorDto == null) {
            return null;
        }

        return Contractor.builder()
                .id(contractorDto.getId())
                .name(contractorDto.getName())
                .status(contractorDto.getStatus())
                .contractorGroup(convertToModel(contractorDto.getContractorGroup()))
                .code(contractorDto.getCode())
                .outerCode(contractorDto.getOuterCode())
                .status(contractorDto.getStatus())
                .sortNumber(contractorDto.getSortNumber())
                .phone(contractorDto.getPhone())
                .fax(contractorDto.getFax())
                .email(contractorDto.getEmail())
                .address(convertToModel(contractorDto.getAddress()))
                .comment(contractorDto.getComment())
                .numberDiscountCard(contractorDto.getNumberDiscountCard())
                .typeOfPrice(convertToModel(contractorDto.getTypeOfPrice()))
                .legalDetail(convertToModel(contractorDto.getLegalDetail()))
                .tasks(contractorDto.getTaskDtos() != null ?
                        contractorDto.getTaskDtos().stream()
                                .map(ConverterDto::convertToModel).collect(Collectors.toList()) : null)
                .calls(contractorDto.getCallIds() != null ? contractorDto.getCallIds().stream()
                        .map(x -> Call.builder().id(x).build()).collect(Collectors.toList()) : null)
                .build();
    }

    public static Set<ContractorDto> convertToDtoContractor(Set<Contractor> contractorSet) {
        if (contractorSet == null) {
            return Collections.emptySet();
        }
        return contractorSet.stream().map(contractor -> ContractorDto.builder()
                        .id(contractor.getId())
                        .name(contractor.getName())
                        .status(contractor.getStatus())
                        .contractorGroup(convertToDto(contractor.getContractorGroup()))
                        .sortNumber(contractor.getSortNumber())
                        .phone(contractor.getPhone())
                        .fax(contractor.getFax())
                        .email(contractor.getEmail())
                        .address(convertToDto(contractor.getAddress()))
                        .comment(contractor.getComment())
                        .numberDiscountCard(contractor.getNumberDiscountCard())
                        .typeOfPrice(convertToDto(contractor.getTypeOfPrice()))
                        .legalDetail(convertToDto(contractor.getLegalDetail()))
                        .build())
                .collect(Collectors.toSet());
    }

    public static Role convertToModel(RoleDto roleDto) {
        return Role.builder()
                .id(roleDto.getId())
                .name(roleDto.getName())
                .sortNumber(roleDto.getSortNumber())
                .build();
    }

    public static RoleDto convertToDto(Role role) {
        return RoleDto.builder()
                .id(role.getId())
                .name(role.getName())
                .sortNumber(role.getSortNumber())
                .build();
    }

    public static Employee convertToModel(EmployeeDto employeeDto) {
        if (employeeDto == null) {
            return null;
        }
        return Employee.builder()
                .id(employeeDto.getId())
                .lastName(employeeDto.getLastName())
                .firstName(employeeDto.getFirstName())
                .middleName(employeeDto.getMiddleName())
                .sortNumber(employeeDto.getSortNumber())
                .phone(employeeDto.getPhone())
                .inn(employeeDto.getInn())
                .description(employeeDto.getDescription())
                .email(employeeDto.getEmail())
                .password(employeeDto.getPassword())
                .department(employeeDto.getDepartment() != null ? convertToModel(employeeDto.getDepartment()) : null)
                .position(employeeDto.getPosition() != null ? convertToModel(employeeDto.getPosition()) : null)
                .roles(employeeDto.getRoles() != null ? employeeDto.getRoles().stream().map(ConverterDto::convertToModel).collect(Collectors.toSet()) : null)
                .image(employeeDto.getImage() != null ? convertToModel(employeeDto.getImage()) : null)
                .tariff(employeeDto.getTariff() != null ? employeeDto.getTariff().stream().map(ConverterDto::convertToModel).collect(Collectors.toSet()) : null)
                .ipAddress(employeeDto.getIpAddress() != null ? employeeDto.getIpAddress().stream().map(ConverterDto::convertToModel).collect(Collectors.toSet()) : null)
                .ipNetwork(employeeDto.getIpNetwork() != null ? convertToModel(employeeDto.getIpNetwork()) : null)
                .build();
    }


    public static EmployeeDto convertToDto(Employee employee) {
        if (employee == null) {
            return null;
        }
        return EmployeeDto.builder()
                .id(employee.getId())
                .lastName(employee.getLastName())
                .firstName(employee.getFirstName())
                .middleName(employee.getMiddleName())
                .sortNumber(employee.getSortNumber())
                .phone(employee.getPhone())
                .inn(employee.getInn())
                .description(employee.getDescription())
                .email(employee.getEmail())
                .password(employee.getPassword())
                .department(convertToDto(employee.getDepartment()))
                .position(convertToDto(employee.getPosition()))
                .roles(employee.getRoles().stream().map(ConverterDto::convertToDto).collect(Collectors.toSet()))
                .image(convertToDto(employee.getImage()))
                .tariff(employee.getTariff().stream().map(ConverterDto::convertToDto).collect(Collectors.toSet()))
                .ipAddress(employee.getIpAddress().stream().map(ConverterDto::convertToDto).collect(Collectors.toSet()))
                .ipNetwork(convertToDto(employee.getIpNetwork()))
                .build();
    }

    public static Movement convertToModel(MovementDto dto) {
        return Movement.builder()
                .id(dto.getId())
                .date(dto.getDateOfCreation())
                .warehouseFrom(convertToModel(dto.getWarehouseFrom()))
                .warehouseTo(convertToModel(dto.getWarehouseTo()))
                .company(convertToModel(dto.getCompany()))
                .sum(dto.getSum())
                .moved(dto.isMoved())
                .print(dto.isPrinted())
                .comments(dto.getComment()).build();
    }

    public static PriceList convertToModel(PriceListDto dto) {
        return PriceList.builder()
                .id(dto.getId())
                .dateTime(dto.getDateOfCreation())
                .company(convertToModel(dto.getCompany()))
                .moved(dto.isMoved())
                .printed(dto.isPrinted())
                .comment(dto.getComment()).build();
    }

    public static Invoice convertToModel(InvoiceDto dto) {
        Employee invoiceAuthor = new Employee();
        invoiceAuthor.setId(dto.getInvoiceAuthorId());
        Company company = new Company();
        company.setId(dto.getCompanyId());
        Project project = new Project();
        project.setId(dto.getProjectId());
        Warehouse warehouse = new Warehouse();
        warehouse.setId(dto.getWarehouseId());
        Contractor contractor = new Contractor();
        contractor.setId(dto.getContractorId());
        Contract contract = new Contract();
        contract.setId(dto.getContractId());
        return Invoice.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .invoiceDateTime(dto.getInvoiceDateTime())
                .type(TypeOfInvoice.valueOf(dto.getType()))
                .isPosted(dto.isPosted())
                .invoiceAuthor(invoiceAuthor)
                .company(company)
                .project(project)
                .warehouse(warehouse)
                .invoiceProducts(dto.getProductDtos() != null ? dto.getProductDtos().stream().map(ConverterDto::convertToModel).collect(Collectors.toList()) : null)
                .comment(dto.getComment())
                .contractor(contractor)
                .contract(contract)
                .edits(dto.getEdits() != null ? dto.getEdits().stream().map(ConverterDto::convertToModel).collect(Collectors.toList()) : null)
                .build();
    }

    public static InvoiceDto convertToDto(Invoice invoice) {
        return InvoiceDto.builder()
                .id(invoice.getId())
                .number(invoice.getNumber())
                .invoiceDateTime(invoice.getInvoiceDateTime())
                .type(invoice.getType().name())
                .isPosted(invoice.isPosted())
                .invoiceAuthorId(invoice.getInvoiceAuthor() != null ? invoice.getInvoiceAuthor().getId() : null)
                .invoiceAuthorLastName(invoice.getInvoiceAuthor() != null ? invoice.getInvoiceAuthor().getLastName() : null)
                .invoiceAuthorFirstName(invoice.getInvoiceAuthor() != null ? invoice.getInvoiceAuthor().getFirstName() : null)
                .companyId(invoice.getCompany() != null ? invoice.getCompany().getId() : null)
                .companyName(invoice.getCompany() != null ? invoice.getCompany().getName() : null)
                .projectId(invoice.getProject() != null ? invoice.getProject().getId() : null)
                .projectName(invoice.getProject() != null ? invoice.getProject().getName() : null)
                .warehouseId(invoice.getWarehouse() != null ? invoice.getWarehouse().getId() : null)
                .warehouseName(invoice.getWarehouse() != null ? invoice.getWarehouse().getName() : null)
                .productDtos(invoice.getInvoiceProducts().stream().map(ConverterDto::convertToDto).collect(Collectors.toList()))
                .comment(invoice.getComment())
                .contractorId(invoice.getContractor() != null ? invoice.getContractor().getId() : null)
                .contractorName(invoice.getContractor() != null ? invoice.getContractor().getName() : null)
                .contractId(invoice.getContract() != null ? invoice.getContract().getId() : null)
                .contractNumber(invoice.getContract() != null ? invoice.getContract().getNumber() : null)
                .contractDate(invoice.getContract() != null ? invoice.getContract().getContractDate() : null)
                .edits(invoice.getEdits().stream().map(ConverterDto::convertToDto).collect(Collectors.toList()))
                .build();
    }

    public static InvoiceEdit convertToModel(InvoiceEditDto dto) {
        return InvoiceEdit.builder()
                .id(dto.getId())
                .editAuthor(convertToModel(dto.getEditAuthorDto()))
                .dateTime(dto.getDateTime())
                .field(dto.getField())
                .before(dto.getBefore())
                .after(dto.getAfter())
                .build();
    }

    public static InvoiceEditDto convertToDto(InvoiceEdit invoiceEdit) {
        return InvoiceEditDto.builder()
                .id(invoiceEdit.getId())
                .editAuthorDto(convertToDto(invoiceEdit.getEditAuthor()))
                .dateTime(invoiceEdit.getDateTime())
                .field(invoiceEdit.getField())
                .before(invoiceEdit.getBefore())
                .after(invoiceEdit.getAfter())
                .build();
    }

    public static Project convertToModel(ProjectDto projectDto) {
        if (projectDto == null) {
            return null;
        }
        return Project.builder()
                .id(projectDto.getId())
                .name(projectDto.getName())
                .code(projectDto.getCode())
                .description(projectDto.getDescription())
                .build();
    }

    public static ProjectDto convertToDto(Project project) {
        if (project == null) {
            return null;
        }
        return ProjectDto.builder()
                .id(project.getId())
                .name(project.getName())
                .code(project.getCode())
                .description(project.getDescription())
                .build();
    }

    public static InvoiceProductDto convertToDto(InvoiceProduct invoiceProduct) {
        return InvoiceProductDto.builder()
                .id(invoiceProduct.getId())
//                .invoiceDto(convertToDto(invoiceProduct.getInvoice()))
                .productDto(convertToDto(invoiceProduct.getProduct()))
                .count(invoiceProduct.getCount())
                .price(invoiceProduct.getPrice())
                .sum(invoiceProduct.getSum())
                .nds(invoiceProduct.getNds())
                .discount(invoiceProduct.getDiscount())
                .build();
    }

    public static InvoiceProduct convertToModel(InvoiceProductDto invoiceProductDto) {
        return InvoiceProduct.builder()
                .id(invoiceProductDto.getId())
//                .invoice(convertToModel(invoiceProductDto.getInvoiceDto()))
                .product(convertToModel(invoiceProductDto.getProductDto()))
                .count(invoiceProductDto.getCount())
                .price(invoiceProductDto.getPrice())
                .sum(invoiceProductDto.getSum())
                .nds(invoiceProductDto.getNds())
                .discount(invoiceProductDto.getDiscount())
                .build();
    }


    public static Product convertToModel(ProductDto productDto) {
        if (productDto == null) {
            return null;
        }

        var product = Product.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .country(productDto.getCountry())
                .articul(productDto.getArticul())
                .code(productDto.getCode())
                .outCode(productDto.getOutCode())
                .weight(productDto.getWeight())
                .volume(productDto.getVolume())
                .purchasePrice(productDto.getPurchasePrice())
                .nds(productDto.getNds())
                .description(productDto.getDescription())
                .unit(productDto.getUnitDto() != null ? convertToModel(productDto.getUnitDto()) : null)
                .archive(productDto.getArchive())
//                .productGroup(productDto.getProductGroup())
                .unitsOfMeasure(productDto.getUnitsOfMeasureDto() != null ? convertToModel(productDto.getUnitsOfMeasureDto()) : null)
                .contractor(productDto.getContractorDto() != null ? convertToModel(productDto.getContractorDto()) : null)
                .taxSystem(productDto.getTaxSystemDto() != null ? convertToModel(productDto.getTaxSystemDto()) : null)
                .images(productDto.getImagesDto() != null ? productDto.getImagesDto().stream().map(ConverterDto::convertToModel).collect(Collectors.toList()) : null)
                .productGroup(productDto.getProductGroupDto() != null ? convertToModel(productDto.getProductGroupDto()) : null)
                .attributeOfCalculationObject(productDto.getAttributeOfCalculationObjectDto() != null ? convertToModel(productDto.getAttributeOfCalculationObjectDto()) : null)
                .build();
        
        var productPrices = productDto.getProductPricesDto();
        if (productPrices != null) {
            product.setProductPrices(
                    productPrices.stream()
                            .map(productPriceDto -> convertToModel(productPriceDto, product))
                            .collect(Collectors.toList())
            );
        }
        
        return product;
    }

    public static ProductDto convertToDto(Product product) {
        if (product == null) {
            return null;
        }

        var productDto = ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .country(product.getCountry())
                .articul(product.getArticul())
                .code(product.getCode())
                .outCode(product.getOutCode())
                .weight(product.getWeight())
                .volume(product.getVolume())
                .purchasePrice(product.getPurchasePrice())
                .nds(product.getNds())
                .description(product.getDescription())
                .unitDto(convertToDto(product.getUnit()))
                .archive(product.getArchive())
//                .productGroup(product.getProductGroup())
                .contractorDto(convertToDto(product.getContractor()))
                .taxSystemDto(convertToDto(product.getTaxSystem()))
                .unitsOfMeasureDto(convertToDto(product.getUnitsOfMeasure()))
                .imagesDto(product.getImages() != null ? product.getImages().stream().map(ConverterDto::convertToDto).collect(Collectors.toList()) : null)
//                .productGroupDto(convertToDto(product.getProductGroup()))
                .attributeOfCalculationObjectDto(convertToDto(product.getAttributeOfCalculationObject()))
                .build();

        var productPrices = product.getProductPrices();

        if (productPrices != null) {
            productDto.setProductPricesDto(
                    productPrices.stream()
                            .map(productPrice -> convertToDto(productPrice, productDto))
                            .collect(Collectors.toList())
            );
        }

        return productDto;
    }

    public static Set<ProductDto> convertToDtoProduct(Set<Product> productSet) {
        if (productSet == null) {
            return Collections.emptySet();
        }
        return productSet.stream().map(product -> ProductDto.builder()
                        .id(product.getId())
                        .name(product.getName())
                        .country(product.getCountry())
                        .articul(product.getArticul())
                        .code(product.getCode())
                        .outCode(product.getOutCode())
                        .weight(product.getWeight())
                        .volume(product.getVolume())
                        .purchasePrice(product.getPurchasePrice())
                        .description(product.getDescription())
                        .unitDto(convertToDto(product.getUnit()))
                        .archive(product.getArchive())
//                        .productGroup(product.getProductGroup())
                        .contractorDto(convertToDto(product.getContractor()))
                        .productPricesDto(product.getProductPrices().stream().map(ConverterDto::convertToDto).collect(Collectors.toList()))
                        .taxSystemDto(convertToDto(product.getTaxSystem()))
                        .imagesDto(product.getImages().stream().map(ConverterDto::convertToDto).collect(Collectors.toList()))
                        .productGroupDto(convertToDto(product.getProductGroup()))
                        .attributeOfCalculationObjectDto(convertToDto(product.getAttributeOfCalculationObject()))
                        .build())
                .collect(Collectors.toSet());
    }

    public static RecycleBinDto convertToDto(RecycleBin recycleBin) {
        return RecycleBinDto.builder()
                .id(recycleBin.getId())
                .documentType(recycleBin.getDocumentType())
                .number(recycleBin.getNumber())
                .date(recycleBin.getDate())
                .sum(recycleBin.getSum())
                .warehouseID(recycleBin.getWarehouse().getId())
                .warehouseFrom(recycleBin.getWarehouseFrom())
                .companyID(recycleBin.getCompany().getId())
                .contractorID(recycleBin.getContractor().getId())
                .status(recycleBin.getStatus())
                .projectID(recycleBin.getProject().getId())
                .shipped(recycleBin.getShipped())
                .printed(recycleBin.getPrinted())
                .comment(recycleBin.getComment())
                .build();
    }

    public static RecycleBin convertToModel(RecycleBinDto dto) {
        if (dto == null) {
            return null;
        }
        Warehouse warehouse = new Warehouse();
        warehouse.setId(dto.getWarehouseID());
        Company company = new Company();
        company.setId(dto.getCompanyID());
        ContractorGroup contractor = new ContractorGroup();
        contractor.setId(dto.getContractorID());
        Project project = new Project();
        project.setId(dto.getProjectID());
        return RecycleBin.builder()
                .id(dto.getId())
                .documentType(dto.getDocumentType())
                .number(dto.getNumber())
                .date(dto.getDate())
                .sum(dto.getSum())
                .warehouse(warehouse)
                .warehouseFrom(dto.getWarehouseFrom())
                .company(company)
                .contractor(contractor)
                .status(dto.getStatus())
                .project(project)
                .shipped(dto.getShipped())
                .printed(dto.getPrinted())
                .comment(dto.getComment())
                .build();

    }


    public static FileDto convertToDto(File file) {
        return FileDto.builder()
                .id(file.getId())
                .name(file.getName())
                .size(file.getSize())
                .createdDate(file.getCreatedDate())
                .employeeDto(ConverterDto.convertToDto(file.getEmployee()))
                .build();
    }

    public static File convertToModel(FileDto dto) {
        return File.builder()
                .id(dto.getId())
                .name(dto.getName())
                .size(dto.getSize())

                .createdDate(dto.getCreatedDate())
                .employee(ConverterDto.convertToModel(dto.getEmployeeDto()))
                .build();
    }

    public static ProductPrice convertToModel(ProductPriceDto dto) {
        return convertToModel(dto, convertToModel(dto.getProductDto()));
    }
    
    public static ProductPrice convertToModel(ProductPriceDto dto, Product product) {
        return ProductPrice.builder()
                .id(dto.getId())
                .product(product)
                .typeOfPrice(convertToModel(dto.getTypeOfPriceDto()))
                .price(dto.getPrice())
                .build();
    }

    public static ProductPriceDto convertToDto(ProductPrice productPrice) {
        return convertToDto(productPrice, convertToDto(productPrice.getProduct()));
    }
    
    public static ProductPriceDto convertToDto(ProductPrice productPrice, ProductDto productDto) {
        if (productPrice == null) {
            return null;
        }
        return ProductPriceDto.builder()
                .id(productPrice.getId())
                .productDto(productDto)
                .typeOfPriceDto(convertToDto(productPrice.getTypeOfPrice()))
                .price(productPrice.getPrice())
                .build();
    }

    public static ProductionStage convertToModel(ProductionStageDto stageDto) {
        return ProductionStage.builder()
                .id(stageDto.getId())
                .name(stageDto.getName())
                .description(stageDto.getDescription())
                .generalAccess(stageDto.isGeneralAccess())
                .archived(stageDto.isArchived())
                .sortNumber(stageDto.getSortNumber())
                .ownerDepartment(Department.builder().id(stageDto.getOwnerDepartmentId()).build())
                .ownerEmployee(Employee.builder().id(stageDto.getOwnerEmployeeId()).build())
                .dateOfEdit(stageDto.getDateOfEdit())
                .editorEmployee(Employee.builder().id(stageDto.getEditorEmployeeId()).build())
                .build();
    }

    public static ProductionStageDto convertToDto(ProductionStage stage) {
        return ProductionStageDto.builder()
                .id(stage.getId())
                .name(stage.getName())
                .description(stage.getDescription())
                .generalAccess(stage.isGeneralAccess())
                .archived(stage.isArchived())
                .sortNumber(stage.getSortNumber())
                .ownerDepartmentId(stage.getOwnerDepartment().getId())
                .ownerDepartmentName(stage.getOwnerDepartment().getName())
                .ownerEmployeeId(stage.getOwnerEmployee().getId())
                .ownerEmployeeName(stage.getOwnerEmployee().getLastName())
                .dateOfEdit(stage.getDateOfEdit())
                .editorEmployeeId(stage.getEditorEmployee().getId())
                .editorEmployeeName(stage.getEditorEmployee().getLastName())
                .build();
    }

    public static TechnologicalMap convertToModel(TechnologicalMapDto technologicalMapDto) {
        TechnologicalMapGroup technologicalMapGroup = new TechnologicalMapGroup();
        technologicalMapGroup.setId(technologicalMapDto.getTechnologicalMapGroupId());
        return TechnologicalMap.builder()
                .id(technologicalMapDto.getId())
                .name(technologicalMapDto.getName())
                .comment(technologicalMapDto.getComment())
                .isArchived(technologicalMapDto.isArchived())
                .isDeleted(technologicalMapDto.isDeleted())
                .productionCost(technologicalMapDto.getProductionCost())
                .technologicalMapGroup(technologicalMapGroup)
                .products((technologicalMapDto.getFinishedProducts() != null)
                        ? technologicalMapDto.getFinishedProducts()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .materials((technologicalMapDto.getMaterials() != null)
                        ? technologicalMapDto.getMaterials()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .build();
    }

    public static TechnologicalMapDto convertToDto(TechnologicalMap technologicalMap) {
        return TechnologicalMapDto.builder()
                .id(technologicalMap.getId())
                .name(technologicalMap.getName())
                .comment(technologicalMap.getComment())
                .isArchived(technologicalMap.isArchived())
                .isDeleted(technologicalMap.isDeleted())
                .productionCost(technologicalMap.getProductionCost())
                .technologicalMapGroupId((technologicalMap.getTechnologicalMapGroup() != null) ?
                        technologicalMap.getTechnologicalMapGroup().getId() : null)
                .technologicalMapGroupName((technologicalMap.getTechnologicalMapGroup() != null) ?
                        technologicalMap.getTechnologicalMapGroup().getName() : null)
                .build();
    }

    public static TechnologicalMapGroup convertToModel(TechnologicalMapGroupDto technologicalMapGroupDto) {
        TechnologicalMapGroup parentTechnologicalMapGroup = null;
        if (technologicalMapGroupDto.getParentTechnologicalMapGroupId() != null) {
            parentTechnologicalMapGroup = new TechnologicalMapGroup();
            parentTechnologicalMapGroup.setId(technologicalMapGroupDto.getParentTechnologicalMapGroupId());
            parentTechnologicalMapGroup.setName(technologicalMapGroupDto.getParentTechnologicalMapGroupName());
        }
        return TechnologicalMapGroup.builder()
                .id(technologicalMapGroupDto.getId())
                .name(technologicalMapGroupDto.getName())
                .code(technologicalMapGroupDto.getCode())
                .comment(technologicalMapGroupDto.getComment())
                .parentTechnologicalMapGroup(parentTechnologicalMapGroup)
                .build();
    }

    public static TechnologicalMapGroupDto convertToDto(TechnologicalMapGroup technologicalMapGroup) {
        return TechnologicalMapGroupDto.builder()
                .id(technologicalMapGroup.getId())
                .name(technologicalMapGroup.getName())
                .code(technologicalMapGroup.getCode())
                .comment(technologicalMapGroup.getComment())
                .parentTechnologicalMapGroupId((technologicalMapGroup.getParentTechnologicalMapGroup() != null) ? technologicalMapGroup.getParentTechnologicalMapGroup().getId() : null)
                .parentTechnologicalMapGroupName((technologicalMapGroup.getParentTechnologicalMapGroup() != null) ? technologicalMapGroup.getParentTechnologicalMapGroup().getName() : null)
                .build();
    }

    public static TechnologicalMapProduct convertToModel(TechnologicalMapProductDto technologicalMapProductDto) {
        Product finishedProducts = new Product();
        finishedProducts.setId(technologicalMapProductDto.getFinishedProductId());
        TechnologicalMap technologicalMap = new TechnologicalMap();
        technologicalMap.setId(technologicalMapProductDto.getTechnologicalMapDto().getId());
        return TechnologicalMapProduct.builder()
                .id(technologicalMapProductDto.getId())
                .products(finishedProducts)
                .count(technologicalMapProductDto.getCount())
                .technologicalMap(technologicalMap)
                .build();
    }

    public static TechnologicalMapProductDto convertToDto(TechnologicalMapProduct technologicalMapProduct) {
        return TechnologicalMapProductDto.builder()
                .id(technologicalMapProduct.getId())
                .finishedProductId((technologicalMapProduct.getProducts() != null) ? technologicalMapProduct.getProducts().getId() : null)
                .finishedProductsName((technologicalMapProduct.getProducts() != null) ? technologicalMapProduct.getProducts().getName() : null)
                .count(technologicalMapProduct.getCount())
                .technologicalMapDto(convertToDto(technologicalMapProduct.getTechnologicalMap()))
                .build();
    }

    public static TechnologicalMapMaterial convertToModel(TechnologicalMapMaterialDto technologicalMapMaterialDto) {
        Product materials = new Product();
        materials.setId(technologicalMapMaterialDto.getMaterialId());
        TechnologicalMap technologicalMap = new TechnologicalMap();
        technologicalMap.setId(technologicalMapMaterialDto.getTechnologicalMapDto().getId());
        return TechnologicalMapMaterial.builder()
                .id(technologicalMapMaterialDto.getId())
                .materials(materials)
                .count(technologicalMapMaterialDto.getCount())
                .technologicalMap(technologicalMap)
                .build();
    }

    public static TechnologicalMapMaterialDto convertToDto(TechnologicalMapMaterial technologicalMapMaterial) {
        return TechnologicalMapMaterialDto.builder()
                .id(technologicalMapMaterial.getId())
                .materialId((technologicalMapMaterial.getMaterials() != null) ? technologicalMapMaterial.getMaterials().getId() : null)
                .materialName((technologicalMapMaterial.getMaterials() != null) ? technologicalMapMaterial.getMaterials().getName() : null)
                .count(technologicalMapMaterial.getCount())
                .technologicalMapDto(convertToDto(technologicalMapMaterial.getTechnologicalMap()))
                .build();
    }


    public static ProductionOrder convertToModel(ProductionOrderDto dto) {
        Company company = new Company();
        company.setId(dto.getCompanyId());
        Warehouse warehouse = new Warehouse();
        warehouse.setId(dto.getWarehouseId());
        Project project = new Project();
        project.setId(dto.getProjectId());
        TechnologicalMap technologicalMap = new TechnologicalMap();
        technologicalMap.setId(dto.getTechnologicalMapId());
        return ProductionOrder.builder()
                .id(dto.getId())
                .docNumber(dto.getNumber())
                .date(dto.getDateTime())
                .company(dto.getCompanyId() != null ? company : null)
                .technologicalMap(dto.getTechnologicalMapId() != null ? technologicalMap : null)
                .volumeOfProduction(dto.getVolumeOfProduction())
                .warehouseFrom(dto.getWarehouseId() != null ? warehouse : null)
                .planDate(dto.getPlanDate())
                .project(dto.getProjectId() != null ? project : null)
                .comments(dto.getComment())
                .build();
    }

    public static ProductionOrderDto convertToDto(ProductionOrder productionOrder) {
        return ProductionOrderDto.builder()
                .id(productionOrder.getId())
                .number(productionOrder.getDocNumber())
                .dateTime(productionOrder.getDate())
                .companyId(productionOrder.getCompany() != null ? productionOrder.getCompany().getId() : null)
                .companyName(productionOrder.getCompany() != null ? productionOrder.getCompany().getName() : null)
                .technologicalMapId(productionOrder.getTechnologicalMap() != null ? productionOrder.getTechnologicalMap().getId() : null)
                .technologicalMapName(productionOrder.getTechnologicalMap() != null ? productionOrder.getTechnologicalMap().getName() : null)
                .volumeOfProduction(productionOrder.getVolumeOfProduction())
                .warehouseId(productionOrder.getWarehouseFrom() != null ? productionOrder.getWarehouseFrom().getId() : null)
                .warehouseName(productionOrder.getWarehouseFrom() != null ? productionOrder.getWarehouseFrom().getName() : null)
                .planDate(productionOrder.getPlanDate())
                .projectId(productionOrder.getProject() != null ? productionOrder.getProject().getId() : null)
                .projectName(productionOrder.getProject() != null ? productionOrder.getProject().getName() : null)
                .comment(productionOrder.getComments())
                .build();
    }

    public static TechnologicalOperation convertToModel(TechnologicalOperationDto technologicalOperationDto) {
        Warehouse warehouseForMaterials = new Warehouse();
        warehouseForMaterials.setId(technologicalOperationDto.getWarehouseForMaterialsId());
        Warehouse warehouseForProduct = new Warehouse();
        warehouseForProduct.setId(technologicalOperationDto.getWarehouseForProductId());
        Company company = new Company();
        company.setId(technologicalOperationDto.getCompanyId());
        Project project = new Project();
        project.setId(technologicalOperationDto.getProjectId());
        TechnologicalMap technologicalMap = new TechnologicalMap();
        technologicalMap.setId(technologicalOperationDto.getTechnologicalMapId());
        return TechnologicalOperation.builder()
                .id(technologicalOperationDto.getId())
                .isArchive(technologicalOperationDto.isArchive())
                .docNumber(technologicalOperationDto.getNumber())
                .date(technologicalOperationDto.getTechnologicalOperationDateTime())
                .company(technologicalOperationDto.getCompanyId() != null ? company : null)
                .technologicalMap(technologicalOperationDto.getTechnologicalMapId() != null ? technologicalMap : null)
                .volumeOfProduction(technologicalOperationDto.getVolumeOfProduction())
                .warehouseFrom(technologicalOperationDto.getWarehouseForMaterialsId() != null ? warehouseForMaterials : null)
                .warehouseTo(technologicalOperationDto.getWarehouseForProductId() != null ? warehouseForProduct : null)
                .project(technologicalOperationDto.getProjectId() != null ? project : null)
                .comments(technologicalOperationDto.getComments())
                .tasks((technologicalOperationDto.getTasks() != null)
                        ? technologicalOperationDto.getTasks()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .build();
    }

    public static TechnologicalOperationDto convertToDto(TechnologicalOperation technologicalOperation) {
        return TechnologicalOperationDto.builder()
                .id(technologicalOperation.getId())
                .number(technologicalOperation.getDocNumber())
                .isArchive(technologicalOperation.isArchive())
                .technologicalOperationDateTime(technologicalOperation.getDate())
                .companyId(technologicalOperation.getCompany() != null ? technologicalOperation.getCompany().getId() : null)
                .companyName(technologicalOperation.getCompany() != null ? technologicalOperation.getCompany().getName() : null)
                .technologicalMapId(technologicalOperation.getTechnologicalMap() != null ? technologicalOperation.getTechnologicalMap().getId() : null)
                .technologicalMapName(technologicalOperation.getTechnologicalMap() != null ? technologicalOperation.getTechnologicalMap().getName() : null)
                .volumeOfProduction(technologicalOperation.getVolumeOfProduction())
                .warehouseForMaterialsId(technologicalOperation.getWarehouseFrom() != null ? technologicalOperation.getWarehouseFrom().getId() : null)
                .warehouseForMaterialsName(technologicalOperation.getWarehouseFrom() != null ? technologicalOperation.getWarehouseFrom().getName() : null)
                .warehouseForProductId(technologicalOperation.getWarehouseTo() != null ? technologicalOperation.getWarehouseTo().getId() : null)
                .warehouseForProductName(technologicalOperation.getWarehouseTo() != null ? technologicalOperation.getWarehouseTo().getName() : null)
                .projectId(technologicalOperation.getProject() != null ? technologicalOperation.getProject().getId() : null)
                .projectName(technologicalOperation.getProject() != null ? technologicalOperation.getProject().getName() : null)
                .comments(technologicalOperation.getComments())
                .build();
    }

    public static City convertToModel(CityDto cityDto) {
        return City.builder()
                .id(cityDto.getId())
                .name(cityDto.getName())
                .socr(cityDto.getSocr())
                .code(cityDto.getCode())
                .index(cityDto.getIndex())
                .gninmb(cityDto.getGninmb())
                .uno(cityDto.getUno())
                .ocatd(cityDto.getOcatd())
                .status(cityDto.getStatus())
                .build();
    }

    public static CityDto convertToDto(City city) {
        return CityDto.builder()
                .id(city.getId())
                .name(city.getName())
                .socr(city.getSocr())
                .code(city.getCode())
                .index(city.getIndex())
                .gninmb(city.getGninmb())
                .uno(city.getUno())
                .ocatd(city.getOcatd())
                .status(city.getStatus())
                .build();
    }

    public static Street convertToModel(StreetDto streetDto) {
        return Street.builder()
                .id(streetDto.getId())
                .name(streetDto.getName())
                .socr(streetDto.getSocr())
                .code(streetDto.getCode())
                .index(streetDto.getIndex())
                .gninmb(streetDto.getGninmb())
                .uno(streetDto.getUno())
                .ocatd(streetDto.getOcatd())
                .build();
    }

    public static StreetDto convertToDto(Street street) {
        return StreetDto.builder()
                .id(street.getId())
                .name(street.getName())
                .socr(street.getSocr())
                .code(street.getCode())
                .index(street.getIndex())
                .gninmb(street.getGninmb())
                .uno(street.getUno())
                .ocatd(street.getOcatd())
                .build();
    }

    public static Country convertToModel(CountryDto countryDto) {
        return Country.builder()
                .id(countryDto.getId())
                .shortName(countryDto.getShortName())
                .longName(countryDto.getLongName())
                .code(countryDto.getCode())
                .codeOne(countryDto.getCodeOne())
                .codeTwo(countryDto.getCodeTwo())
                .build();
    }

    public static CountryDto convertToDto(Country country) {
        return CountryDto.builder()
                .id(country.getId())
                .shortName(country.getShortName())
                .longName(country.getLongName())
                .code(country.getCode())
                .codeOne(country.getCodeOne())
                .codeTwo(country.getCodeTwo())
                .build();
    }

    public static Task convertToModel(TaskDto taskDto) {
        // TODO: Просто заглушка для тестов, исполнитель должен быть всегда.
        Employee executor = null;
        if (taskDto.getExecutorId() != null) {
            executor = new Employee();
            executor.setId(taskDto.getExecutorId());
        }
        Contractor contractor = null;
        if (taskDto.getContractorId() != null) {
            contractor = new Contractor();
            contractor.setId(taskDto.getContractorId());
        }
        Document document = null;
        if (taskDto.getDocumentId() != null) {
            document = new TechnologicalOperation();
            document.setId(taskDto.getDocumentId());
        }
        return Task.builder()
                .id(taskDto.getId())
                .description(taskDto.getDescription())
                .deadline(taskDto.getDeadline())
                .dateOfCreation(taskDto.getDateOfCreation())
                .isDone(taskDto.getIsDone())
                .executor(executor)
                .contractor(contractor)
                .document(document)
                .build();
    }

    public static TaskDto convertToDto(Task task) {
        return TaskDto.builder()
                .id(task.getId())
                .description(task.getDescription())
                .deadline(task.getDeadline())
                .dateOfCreation(task.getDateOfCreation())
                .isDone(task.getIsDone())
                .executorId(task.getExecutor() != null ? task.getExecutor().getId() : null)
                .executorName(task.getExecutor() != null ? task.getExecutor().getFirstName() : null)
                .contractorId(task.getContractor() != null ? task.getContractor().getId() : null)
                .contractorName(task.getContractor() != null ? task.getContractor().getName() : null)
                .documentId(task.getDocument() != null ? task.getDocument().getId() : null)
                .documentName(task.getDocument() != null ? task.getDocument().getType() : null)
                .build();
    }

    public static Payment convertToModel(PaymentDto paymentDto) {
        Contract contract = new Contract();
        contract.setId(paymentDto.getContractId());
        Project project = new Project();
        project.setId(paymentDto.getProjectId());
        PaymentExpenditure paymentExpenditure = new PaymentExpenditure();
        paymentExpenditure.setId(paymentDto.getPaymentExpenditureId());


        return Payment.builder()
                .id(paymentDto.getId())
                .number(paymentDto.getNumber())
                .amount(paymentDto.getAmount())
                .typeOfPayment(paymentDto.getTypeOfPayment())
                .date(paymentDto.getDate())
                .purpose(paymentDto.getPurpose())
                .tax(paymentDto.getTax())
                .comment(paymentDto.getComment())
                .isDone(paymentDto.isDone())
                .contract(paymentDto.getContractId() != null ? contract : null)
                .project(paymentDto.getProjectId() != null ? project : null)
                .paymentExpenditure(paymentDto.getPaymentExpenditureId() != null ? paymentExpenditure : null)
                .contractor(paymentDto.getContractorDto() != null ? convertToModel(paymentDto.getContractorDto()) : null)
                .company(paymentDto.getCompanyDto() != null ? convertToModel(paymentDto.getCompanyDto()) : null)
                .documents(paymentDto.getDocuments()) // На данный момент не существует документов, которые можно было бы привязывать к платежам
                .tasks(paymentDto.getTaskDtos() != null
                        ? paymentDto.getTaskDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList())
                        : null)
                .build();
    }

    public static PaymentDto convertToDto(Payment payment) {
        return PaymentDto.builder()
                .id(payment.getId())
                .number(payment.getNumber())
                .date(payment.getDate())
                .amount(payment.getAmount())
                .purpose(payment.getPurpose())
                .tax(payment.getTax())
                .comment(payment.getComment())
                .isDone(payment.isDone())
                .typeOfPayment(payment.getTypeOfPayment())
                .contractId(payment.getContract() != null ? payment.getContract().getId() : null)
                .contractNumber(payment.getContract() != null ? payment.getContract().getNumber() : null)
                .projectId(payment.getProject() != null ? payment.getProject().getId() : null)
                .projectName(payment.getProject() != null ? payment.getProject().getName() : null)
                .paymentExpenditureId(payment.getPaymentExpenditure() != null ? payment.getPaymentExpenditure().getId() : null)
                .paymentExpenditureName(payment.getPaymentExpenditure() != null ? payment.getPaymentExpenditure().getName() : null)
                .companyDto(convertToDto(payment.getCompany()) != null ? convertToDto(payment.getCompany()) : null)
                .contractorDto(convertToDto(payment.getContractor()) != null ? convertToDto(payment.getContractor()) : null)
                .documents(payment.getDocuments()) // На данный момент не существует документов, которые можно было бы привязывать к платежам
                .taskDtos(payment.getTasks() != null
                        ? payment.getTasks()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList())
                        : null)
                .build();
    }

    public static MemoDto convertToDto(Memo memo) {
        return MemoDto.builder()
                .id(memo.getId())
                .createDate(memo.getCreateDate())
                .content(memo.getContent())
                .contractorId(memo.getContractor() != null
                        ? memo.getContractor().getId() : null)
                .contractorName(memo.getContractor() != null
                        ? memo.getContractor().getName() : null)
                .employeeWhoCreatedId(memo.getEmployeeWhoCreated() != null
                        ? memo.getEmployeeWhoCreated().getId() : null)
                .employeeWhoCreatedName(memo.getEmployeeWhoCreated() != null
                        ? memo.getEmployeeWhoCreated().getFirstName() : null)
                .employeeWhoEditedId(memo.getEmployeeWhoEdited() != null
                        ? memo.getEmployeeWhoEdited().getId() : null)
                .employeeWhoEditedName(memo.getEmployeeWhoEdited() != null
                        ? memo.getEmployeeWhoEdited().getFirstName() : null)
                .build();
    }

    public static Memo convertToModel(MemoDto memoDto) {
        Contractor contractor = new Contractor();
        contractor.setId(memoDto.getContractorId());
        Employee employeeWhoCreated = new Employee();
        employeeWhoCreated.setId(memoDto.getEmployeeWhoCreatedId());
        Employee employeeWhoEdited = new Employee();
        employeeWhoEdited.setId(memoDto.getEmployeeWhoEditedId());

        return Memo.builder()
                .id(memoDto.getId())
                .createDate(memoDto.getCreateDate())
                .content(memoDto.getContent())
                .employeeWhoCreated(employeeWhoCreated)
                .employeeWhoEdited(employeeWhoEdited)
                .contractor(contractor)
                .build();
    }

    public static Supply convertToModel(SupplyDto supplyDto) {
        if (supplyDto == null) {
            return null;
        }
        Warehouse warehouse = new Warehouse();
        warehouse.setId(supplyDto.getWarehouseId());
        Contract contract = new Contract();
        contract.setId(supplyDto.getContractId());
        Contractor contractor = new Contractor();
        contractor.setId(supplyDto.getContractorId());
        Company company = new Company();
        company.setId(supplyDto.getCompanyId());

        return Supply.builder()
                .id(supplyDto.getId())
                .dateOfCreation(supplyDto.getDateOfCreation())
                .contract(contract)
                .contractor(contractor)
                .company(company)
                .products(new ArrayList<>())
                .sum(supplyDto.getSum())
                .paid(supplyDto.getPaid())
                .isSent(supplyDto.getIsSent())
                .isPrinted(supplyDto.getIsPrinted())
                .comment(supplyDto.getComment())
                .warehouse(warehouse)
                .products(supplyDto.getProductDtos() != null ? supplyDto.getProductDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()) : null)
                .build();
    }

    public static SupplyDto convertToDto(Supply supply) {
        if (supply == null) {
            return null;
        }
        return SupplyDto.builder()
                .id(supply.getId())
                .dateOfCreation(supply.getDateOfCreation())
                .warehouseId(supply.getWarehouse().getId())
                .contractId(supply.getContract().getId())
                .contractorId(supply.getContractor().getId())
                .companyId(supply.getCompany().getId())
                .productDtos(supply.getProducts() != null ? supply.getProducts()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()) : null)
                .sum(supply.getSum())
                .paid(supply.getPaid())
                .isSent(supply.getIsSent())
                .isPrinted(supply.getIsPrinted())
                .comment(supply.getComment())
                .build();
    }

    public static Shipment convertToModel(ShipmentDto shipmentDto) {
        if (shipmentDto == null) {
            return null;
        }
        Warehouse warehouse = new Warehouse();
        warehouse.setId(shipmentDto.getWarehouseId());
        Contract contract = new Contract();
        contract.setId(shipmentDto.getContractId());
        Contractor contractor = new Contractor();
        contractor.setId(shipmentDto.getContractorId());
        Company company = new Company();
        company.setId(shipmentDto.getCompanyId());
        Contractor consignee = new Contractor();
        consignee.setId(shipmentDto.getConsigneeId());
        Contractor carrier = new Contractor();
        carrier.setId(shipmentDto.getCarrierId());

        return Shipment.builder()
                .id(shipmentDto.getId())
                .dateOfCreation(shipmentDto.getDateOfCreation())
                .contract(shipmentDto.getContractId() != null ? contract : null)
                .contractor(shipmentDto.getContractorId() != null ? contractor : null)
                .company(shipmentDto.getCompanyId() != null ? company : null)
                .sum(shipmentDto.getSum())
                .paid(shipmentDto.getPaid())
                .isSent(shipmentDto.getIsSent())
                .isPrinted(shipmentDto.getIsPrinted())
                .comment(shipmentDto.getComment())
                .warehouse(shipmentDto.getWarehouseId() != null ? warehouse : null)
                .products(shipmentDto.getProductDtos() != null ? shipmentDto.getProductDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()) : null)
                .consignee(shipmentDto.getConsigneeId() != null ? consignee : null)
                .carrier(shipmentDto.getCarrierId() != null ? carrier : null)
                .isPaid(shipmentDto.getIsPaid())
                .deliveryAddress(convertToModel(shipmentDto.getDeliveryAddress()))
                .build();
    }

    public static ShipmentDto convertToDto(Shipment shipment) {
        if (shipment == null) {
            return null;
        }

        return ShipmentDto.builder()
                .id(shipment.getId())
                .dateOfCreation(shipment.getDateOfCreation())
                .warehouseId(shipment.getWarehouse() != null ? shipment.getWarehouse().getId() : null)
                .contractId(shipment.getContract() != null ? shipment.getContract().getId() : null)
                .contractorId(shipment.getContractor() != null ? shipment.getContractor().getId() : null)
                .companyId(shipment.getCompany() != null ? shipment.getCompany().getId() : null)
                .productDtos(shipment.getProducts() != null ? shipment.getProducts()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()) : null)
                .sum(shipment.getSum())
                .paid(shipment.getPaid())
                .isSent(shipment.getIsSent())
                .isPrinted(shipment.getIsPrinted())
                .comment(shipment.getComment())
                .consigneeId(shipment.getConsignee() != null ? shipment.getConsignee().getId() : null)
                .carrierId(shipment.getCarrier() != null ? shipment.getCarrier().getId() : null)
                .isPaid(shipment.getIsPaid())
                .deliveryAddress(convertToDto(shipment.getDeliveryAddress()))
                .build();
    }

    public static Feed convertToModel(FeedDto feedDto) {
        return Feed.builder()
                .id(feedDto.getId())
                .feedHead(feedDto.getFeedHead())
                .feedBody(feedDto.getFeedBody())
                .feedDate(feedDto.getFeedDate())
                .build();
    }

    public static FeedDto convertToDto(Feed feed) {
        return FeedDto.builder()
                .id(feed.getId())
                .feedHead(feed.getFeedHead())
                .feedBody(feed.getFeedBody())
                .feedDate(feed.getFeedDate())
                .build();
    }


    public static TariffDto convertToDto(Tariff tariff) {
        return TariffDto.builder()
                .id(tariff.getId())
                .tariffName(tariff.getTariffName())
                .dataSpace(tariff.getDataSpace())
                .salePointCount(tariff.getSalePointCount())
                .onlineStoreCount(tariff.getOnlineStoreCount())
                .paidApplicationOptionCount(tariff.getPaidApplicationOptionCount())
                .isCRM(tariff.getIsCRM())
                .isScripts(tariff.getIsScripts())
                .extendedBonusProgram(tariff.getExtendedBonusProgram())
                .paymentPeriod(tariff.getPaymentPeriod())
                .totalPrice(tariff.getTotalPrice())
                .dateStartSubscription(tariff.getDateStartSubscription())
                .dateEndSubscription(tariff.getDateEndSubscription())
                .build();
    }

    public static Tariff convertToModel(TariffDto tariffDto) {
        return Tariff.builder()
                .id(tariffDto.getId())
                .tariffName(tariffDto.getTariffName())
                .dataSpace(tariffDto.getDataSpace())
                .salePointCount(tariffDto.getSalePointCount())
                .onlineStoreCount(tariffDto.getOnlineStoreCount())
                .paidApplicationOptionCount(tariffDto.getPaidApplicationOptionCount())
                .isCRM(tariffDto.getIsCRM())
                .isScripts(tariffDto.getIsScripts())
                .extendedBonusProgram(tariffDto.getExtendedBonusProgram())
                .paymentPeriod(tariffDto.getPaymentPeriod())
                .totalPrice(tariffDto.getTotalPrice())
                .dateStartSubscription(tariffDto.getDateStartSubscription())
                .dateEndSubscription(tariffDto.getDateEndSubscription())
                .build();
    }


    public static Set<Tariff> convertToModelTariff(Set<TariffDto> tariffDtoSet) {
        return tariffDtoSet.stream().map(tariffDto -> Tariff.builder()
                        .id(tariffDto.getId())
                        .tariffName(tariffDto.getTariffName())
                        .dataSpace(tariffDto.getDataSpace())
                        .salePointCount(tariffDto.getSalePointCount())
                        .onlineStoreCount(tariffDto.getOnlineStoreCount())
                        .paidApplicationOptionCount(tariffDto.getPaidApplicationOptionCount())
                        .isCRM(tariffDto.getIsCRM())
                        .isScripts(tariffDto.getIsScripts())
                        .extendedBonusProgram(tariffDto.getExtendedBonusProgram())
                        .paymentPeriod(tariffDto.getPaymentPeriod())
                        .totalPrice(tariffDto.getTotalPrice())
                        .dateStartSubscription(tariffDto.getDateStartSubscription())
                        .dateEndSubscription(tariffDto.getDateEndSubscription())
                        .build())
                .collect(Collectors.toSet());


    }

    public static Set<TariffDto> convertToDtoTariff(Set<Tariff> tariffSet) {
        return tariffSet.stream().map(tariff -> TariffDto.builder()
                        .id(tariff.getId())
                        .tariffName(tariff.getTariffName())
                        .dataSpace(tariff.getDataSpace())
                        .salePointCount(tariff.getSalePointCount())
                        .onlineStoreCount(tariff.getOnlineStoreCount())
                        .paidApplicationOptionCount(tariff.getPaidApplicationOptionCount())
                        .isCRM(tariff.getIsCRM())
                        .isScripts(tariff.getIsScripts())
                        .extendedBonusProgram(tariff.getExtendedBonusProgram())
                        .paymentPeriod(tariff.getPaymentPeriod())
                        .totalPrice(tariff.getTotalPrice())
                        .dateStartSubscription(tariff.getDateStartSubscription())
                        .dateEndSubscription(tariff.getDateEndSubscription())
                        .build())
                .collect(Collectors.toSet());
    }

    public static Requisites convertToModel(RequisitesDto requisitesDto) {
        return Requisites.builder()
                .id(requisitesDto.getId())
                .organization(requisitesDto.getOrganization())
                .legalAddress(convertToModel(requisitesDto.getLegalAddress()))
                .INN(requisitesDto.getINN())
                .KPP(requisitesDto.getKPP())
                .BIK(requisitesDto.getBIK())
                .checkingAccount(requisitesDto.getCheckingAccount())
                .build();
    }

    public static RequisitesDto convertToDto(Requisites requisites) {
        return RequisitesDto.builder()
                .id(requisites.getId())
                .organization(requisites.getOrganization())
                .legalAddress(convertToDto(requisites.getLegalAddress()))
                .INN(requisites.getINN())
                .KPP(requisites.getKPP())
                .BIK(requisites.getBIK())
                .checkingAccount(requisites.getCheckingAccount())
                .build();
    }

    public static Subscription convertToModel(SubscriptionDto subscriptionDto) {
        return Subscription.builder()
                .id(subscriptionDto.getId())
                .subscriptionExpirationDate(subscriptionDto.getSubscriptionExpirationDate())
                .employee(convertToModel(subscriptionDto.getEmployee()))
                .requisites(convertToModel(subscriptionDto.getRequisites()))
                .tariffs(subscriptionDto.getTariff().stream().map(ConverterDto::convertToModel).collect(Collectors.toSet()))
                .build();
    }

    public static SubscriptionDto convertToDto(Subscription subscription) {
        return SubscriptionDto.builder()
                .id(subscription.getId())
                .subscriptionExpirationDate(subscription.getSubscriptionExpirationDate())
                .requisites(convertToDto(subscription.getRequisites()))
                .employee(convertToDto(subscription.getEmployee()))
                .tariff(subscription.getTariffs().stream().map(ConverterDto::convertToDto).collect(Collectors.toSet()))
                .build();
    }

    // Settings (Настройки пользователя)
    public static Settings convertToModel(SettingsDto settingsDto) {

        return Settings.builder()
                .id(settingsDto.getId())
                .employee(convertToModel(settingsDto.getEmployeeDto()))
                .company(convertToModel(settingsDto.getCompanyDto()))
                .warehouse(convertToModel(settingsDto.getWarehouseDto()))
                .customer(convertToModel(settingsDto.getCustomerDto()))
                .producer(convertToModel(settingsDto.getProducerDto()))
                .project(convertToModel(settingsDto.getProjectDto()))
                .language(convertToModel(settingsDto.getLanguageDto()))
                .printingDocuments(convertToModel(settingsDto.getPrintingDocumentsDto()))
                .startScreen(convertToModel(settingsDto.getStartScreenDto()))
                .notifications(convertToModel(settingsDto.getNotificationsDto()))
                .signatureInLetters(settingsDto.isSignatureInLetters())
                .refreshReportsAuto(settingsDto.isRefreshReportsAuto())
                .numberOfAdditionalFieldsPerLine(settingsDto.getNumberOfAdditionalFieldsPerLine())
                .build();
    }

    public static SettingsDto convertToDto(Settings settings) {

        return SettingsDto.builder()
                .id(settings.getId())
                .employeeDto(convertToDto(settings.getEmployee()))
                .companyDto(convertToDto(settings.getCompany()))
                .warehouseDto(convertToDto(settings.getWarehouse()))
                .customerDto(convertToDto(settings.getCustomer()))
                .producerDto(convertToDto(settings.getProducer()))
                .projectDto(convertToDto(settings.getProject()))
                .languageDto(convertToDto(settings.getLanguage()))
                .printingDocumentsDto(convertToDto(settings.getPrintingDocuments()))
                .startScreenDto(convertToDto(settings.getStartScreen()))
                .notificationsDto(convertToDto(settings.getNotifications()))
                .signatureInLetters(settings.isSignatureInLetters())
                .refreshReportsAuto(settings.isRefreshReportsAuto())
                .numberOfAdditionalFieldsPerLine(settings.getNumberOfAdditionalFieldsPerLine())
                .build();
    }

    // Language
    public static Language convertToModel(LanguageDto languageDto) {
        if (languageDto == null)
            return null;
        return Language.builder()
                .id(languageDto.getId())
                .language(languageDto.getLanguage())
                .build();
    }

    public static LanguageDto convertToDto(Language language) {
        return LanguageDto.builder()
                .id(language.getId())
                .language(language.getLanguage())
                .build();
    }

    // printingDocuments
    public static PrintingDocuments convertToModel(PrintingDocumentsDto printingDocumentsDto) {
        if (printingDocumentsDto == null) {
            return null;
        }
        return PrintingDocuments.builder()
                .id(printingDocumentsDto.getId())
                .printDoc(printingDocumentsDto.getPrintDoc())
                .build();
    }

    public static PrintingDocumentsDto convertToDto(PrintingDocuments printingDocuments) {
        return PrintingDocumentsDto.builder()
                .id(printingDocuments.getId())
                .printDoc(printingDocuments.getPrintDoc())
                .build();
    }

    // startScreen
    public static StartScreen convertToModel(StartScreenDto startScreenDto) {
        if (startScreenDto == null) {
            return null;
        }
        return StartScreen.builder()
                .id(startScreenDto.getId())
                .startScreen(startScreenDto.getStartScreen())
                .build();
    }

    public static StartScreenDto convertToDto(StartScreen startScreen) {
        return StartScreenDto.builder()
                .id(startScreen.getId())
                .startScreen(startScreen.getStartScreen())
                .build();
    }

    // notification
    public static Notifications convertToModel(NotificationsDto notificationsDto) {
        if (notificationsDto == null) {
            return null;
        }
        return Notifications.builder()
                .id(notificationsDto.getId())
                .buyerOrders(convertToModel(notificationsDto.getBuyerOrders()))
                .buyersInvoices(convertToModel(notificationsDto.getBuyersInvoices()))
                .dataExchange(convertToModel(notificationsDto.getDataExchange()))
                .onlineStores(convertToModel(notificationsDto.getOnlineStores()))
                .remainder(convertToModel(notificationsDto.getRemainder()))
                .tasks(convertToModel(notificationsDto.getTasks()))
                .retail(convertToModel(notificationsDto.getRetail()))
                .scripts(convertToModel(notificationsDto.getScripts()))
                .build();
    }

    public static NotificationsDto convertToDto(Notifications notifications) {
        return NotificationsDto.builder()
                .id(notifications.getId())
                .buyerOrders(convertToDto(notifications.getBuyerOrders()))
                .buyersInvoices(convertToDto(notifications.getBuyersInvoices()))
                .dataExchange(convertToDto(notifications.getDataExchange()))
                .onlineStores(convertToDto(notifications.getOnlineStores()))
                .remainder(convertToDto(notifications.getRemainder()))
                .tasks(convertToDto(notifications.getTasks()))
                .retail(convertToDto(notifications.getRetail()))
                .scripts(convertToDto(notifications.getScripts()))
                .build();
    }

    // selector
    public static Selector convertToModel(SelectorDto selectorDto) {
        return Selector.builder()
                .id(selectorDto.getId())
                .activate(selectorDto.isActivate())
                .phone(selectorDto.isPhone())
                .post(selectorDto.isPost())
                .build();
    }

    public static SelectorDto convertToDto(Selector selector) {
        return SelectorDto.builder()
                .id(selector.getId())
                .activate(selector.isActivate())
                .phone(selector.isPhone())
                .post(selector.isPost())
                .build();
    }

    //InvoicesIssued
    public static InvoicesIssued convertToModel(InvoicesIssuedDto invoicesIssuedDto) {
        Company company = new Company();
        company.setId(invoicesIssuedDto.getCompanyId());
        Contractor contractor = new Contractor();
        contractor.setId(invoicesIssuedDto.getContractorId());

        return InvoicesIssued.builder()
                .id(invoicesIssuedDto.getId())
                .data(invoicesIssuedDto.getData())
                .sum(invoicesIssuedDto.getSum())
                .sent(invoicesIssuedDto.getSent())
                .printed(invoicesIssuedDto.getPrinted())
                .company(company)
                .contractor(contractor)
                .comment(invoicesIssuedDto.getComment())
                .build();
    }

    public static InvoicesIssuedDto convertToDto(InvoicesIssued invoicesIssued) {
        return InvoicesIssuedDto.builder()
                .id(invoicesIssued.getId())
                .data(invoicesIssued.getData())
                .sum(invoicesIssued.getSum())
                .sent(invoicesIssued.getSent())
                .printed(invoicesIssued.getPrinted())
                .companyId(invoicesIssued.getCompany().getId())
                .contractorId(invoicesIssued.getContractor().getId())
                .comment(invoicesIssued.getComment())
                .build();
    }


    //PurchaseManagement
    public static PurchasesManagement convertToModel(PurchasesManagementDto purchasesManagementDto) {

        Product product = new Product();
        product.setId(purchasesManagementDto.getProductId());
        PurchaseHistoryOfSales purchaseHistoryOfSales = new PurchaseHistoryOfSales();
        purchaseHistoryOfSales.setId(purchasesManagementDto.getPurchasesHistoryOfSalesId());
        PurchaseCurrentBalance purchaseCurrentBalance = new PurchaseCurrentBalance();
        purchaseCurrentBalance.setId(purchasesManagementDto.getPurchasesCurrentBalanceId());
        PurchaseForecast purchaseForecast = new PurchaseForecast();
        purchaseForecast.setId(purchasesManagementDto.getPurchasesForecastId());

        return PurchasesManagement.builder()
                .id(purchasesManagementDto.getId())
                .product(product)
                .historyOfSales(purchaseHistoryOfSales)
                .currentBalance(purchaseCurrentBalance)
                .forecast(purchaseForecast)
                .build();
    }


    public static PurchasesManagementDto convertToDto(PurchasesManagement purchasesManagement) {
        return PurchasesManagementDto.builder()
                .id(purchasesManagement.getId())
                .productId(purchasesManagement.getProduct().getId())
                .purchasesHistoryOfSalesId(purchasesManagement.getHistoryOfSales().getId())
                .purchasesCurrentBalanceId(purchasesManagement.getCurrentBalance().getId())
                .purchasesForecastId(purchasesManagement.getForecast().getId())
                .build();
    }

    //PurchasesHistoryOfSales
    public static PurchaseHistoryOfSales convertToModel(PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto) {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setId(purchaseHistoryOfSalesDto.getProductPriceId());
        return PurchaseHistoryOfSales.builder()
                .id(purchaseHistoryOfSalesDto.getId())
                .number(purchaseHistoryOfSalesDto.getNumber())
                .productPrice(productPrice)
                .sum(purchaseHistoryOfSalesDto.getSum())
                .profit(purchaseHistoryOfSalesDto.getProfit())
                .profitability(purchaseHistoryOfSalesDto.getProfitability())
                .saleOfDay(purchaseHistoryOfSalesDto.getSaleOfDay())
                .build();
    }

    public static PurchaseHistoryOfSalesDto convertToDto(PurchaseHistoryOfSales purchaseHistoryOfSales) {
        return PurchaseHistoryOfSalesDto.builder()
                .id(purchaseHistoryOfSales.getId())
                .number(purchaseHistoryOfSales.getNumber())
                .productPriceId(purchaseHistoryOfSales.getProductPrice().getId())
                .sum(purchaseHistoryOfSales.getSum())
                .profit(purchaseHistoryOfSales.getProfit())
                .profitability(purchaseHistoryOfSales.getProfitability())
                .saleOfDay(purchaseHistoryOfSales.getSaleOfDay())
                .build();
    }

    //PurchaseForecast
    public static PurchaseForecast convertToModel(PurchaseForecastDto purchaseForecastDto) {
        return PurchaseForecast.builder()
                .id(purchaseForecastDto.getId())
                .reservedDays(purchaseForecastDto.getReservedDays())
                .reservedProduct(purchaseForecastDto.getReservedProduct())
                .ordered(purchaseForecastDto.getOrdered())
                .build();
    }

    public static PurchaseForecastDto convertToDto(PurchaseForecast purchaseForecast) {
        return PurchaseForecastDto.builder()
                .id(purchaseForecast.getId())
                .reservedDays(purchaseForecast.getReservedDays())
                .reservedProduct(purchaseForecast.getReservedProduct())
                .ordered(purchaseForecast.getOrdered())
                .build();
    }

    //PurchaseCurrentBalance
    public static PurchaseCurrentBalance convertToModel(PurchaseCurrentBalanceDto purchaseCurrentBalanceDto) {
        return PurchaseCurrentBalance.builder()
                .id(purchaseCurrentBalanceDto.getId())
                .remainder(purchaseCurrentBalanceDto.getRemainder())
                .productReserved(purchaseCurrentBalanceDto.getProductReserved())
                .productsAwaiting(purchaseCurrentBalanceDto.getProductsAwaiting())
                .productAvailableForOrder(purchaseCurrentBalanceDto.getProductAvailableForOrder())
                .daysStoreOnTheWarehouse(purchaseCurrentBalanceDto.getDaysStoreOnTheWarehouse())
                .build();
    }

    public static PurchaseCurrentBalanceDto convertToDto(PurchaseCurrentBalance purchaseCurrentBalance) {
        return PurchaseCurrentBalanceDto.builder()
                .id(purchaseCurrentBalance.getId())
                .remainder(purchaseCurrentBalance.getRemainder())
                .productReserved(purchaseCurrentBalance.getProductReserved())
                .productsAwaiting(purchaseCurrentBalance.getProductsAwaiting())
                .productAvailableForOrder(purchaseCurrentBalance.getProductAvailableForOrder())
                .daysStoreOnTheWarehouse(purchaseCurrentBalance.getDaysStoreOnTheWarehouse())
                .build();
    }

    public static CustomerOrder convertToModel(CustomerOrderDto dto) {
        Warehouse warehouseFrom = new Warehouse();
        warehouseFrom.setId(dto.getWarehouseFromId());

        Company company = new Company();
        company.setId(dto.getCompanyId());

        Contractor contractor = new Contractor();
        contractor.setId(dto.getContrAgentId());

        Project project = new Project();
        project.setId(dto.getProjectId());

        SalesChannel salesChannel = new SalesChannel();
        salesChannel.setId(dto.getSalesChannelId());

        Contract contract = new Contract();
        contract.setId(dto.getContractId());

//        Department department = new Department();
//        department.setId(dto.getDepartmentId());

//        Employee employee = new Employee();
//        employee.setId(dto.getEmployeeId());

//        Employee updatedFromEmployee = new Employee();
//        updatedFromEmployee.setId(dto.getUpdatedFromEmployeeId());

        Address deliveryAddress = new Address();
        deliveryAddress.setId(dto.getDeliveryAddressId());

        return CustomerOrder.builder()
                .id(dto.getId())
                .type(dto.getType())
                .docNumber(dto.getDocNumber())
                .date(dto.getDate())
                .sum(dto.getSum())
                .warehouseFrom(warehouseFrom)
                .company(company)
                .contrAgent(contractor)
                .project(project)
                .salesChannel(salesChannel)
                .contract(contract)
                .isSharedAccess(dto.getIsSharedAccess())
//                .department(department)
//                .employee(employee)
                .sent(dto.getSent())
                .print(dto.getPrint())
                .comments(dto.getComments())
                .updatedAt(dto.getUpdatedAt())
//                .updatedFromEmployee(updatedFromEmployee)
                .isPaid(dto.getIsPaid())
                .deliveryAddress(deliveryAddress)
                .plannedShipmentDate(dto.getPlannedShipmentDate())
                .products(dto.getProducts()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .tasks(dto.getTasks()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .files(dto.getFiles()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .build();
    }


    public static UnitsOfMeasure convertToModel(UnitsOfMeasureDto unitsOfMeasureDto) {
        return UnitsOfMeasure.builder()
                .id(unitsOfMeasureDto.getId())
                .type(unitsOfMeasureDto.getType())
                .name(unitsOfMeasureDto.getName())
                .fullName(unitsOfMeasureDto.getFullName())
                .code(unitsOfMeasureDto.getCode())
                .build();
    }

    public static UnitsOfMeasureDto convertToDto(UnitsOfMeasure unitsOfMeasure) {
        if (unitsOfMeasure == null) {
            return null;
        }
        return UnitsOfMeasureDto.builder()
                .id(unitsOfMeasure.getId())
                .type(unitsOfMeasure.getType())
                .name(unitsOfMeasure.getName())
                .fullName(unitsOfMeasure.getFullName())
                .code(unitsOfMeasure.getCode())
                .build();
    }


    public static CustomerOrderDto convertToDto(CustomerOrder customerOrder) {
        return CustomerOrderDto.builder()
                .id(customerOrder.getId())
                .type(customerOrder.getType())
                .docNumber(customerOrder.getDocNumber())
                .date(customerOrder.getDate())
                .sum(customerOrder.getSum())
                .warehouseFromId(customerOrder.getWarehouseFrom() != null ? customerOrder.getWarehouseFrom().getId() : null)
                .warehouseFromName(customerOrder.getWarehouseFrom() != null ? customerOrder.getWarehouseFrom().getName() : null)
                .companyId(customerOrder.getCompany() != null ? customerOrder.getCompany().getId() : null)
                .companyName(customerOrder.getCompany() != null ? customerOrder.getCompany().getName() : null)
                .contrAgentId(customerOrder.getContrAgent() != null ? customerOrder.getContrAgent().getId() : null)
                .contrAgentName(customerOrder.getContrAgent() != null ? customerOrder.getContrAgent().getName() : null)
                .projectId(customerOrder.getProject() != null ? customerOrder.getProject().getId() : null)
                .projectName(customerOrder.getProject() != null ? customerOrder.getProject().getName() : null)
                .salesChannelId(customerOrder.getSalesChannel() != null ? customerOrder.getSalesChannel().getId() : null)
                .salesChannelName(customerOrder.getSalesChannel() != null ? customerOrder.getSalesChannel().getName() : null)
                .contractId(customerOrder.getContract() != null ? customerOrder.getContract().getId() : null)
                .contractNumber(customerOrder.getContract() != null ? customerOrder.getContract().getNumber() : null)
                .isSharedAccess(customerOrder.getIsSharedAccess())
//                .departmentId(customerOrder.getDepartment() != null ? customerOrder.getDepartment().getId() : null)
//                .departmentName(customerOrder.getDepartment() != null ? customerOrder.getDepartment().getName() : null)
//                .employeeId(customerOrder.getEmployee() != null ? customerOrder.getEmployee().getId() : null)
//                .employeeFirstname(customerOrder.getEmployee() != null ? customerOrder.getEmployee().getFirstName() : null)
                .sent(customerOrder.getSent())
                .print(customerOrder.getPrint())
                .comments(customerOrder.getComments())
                .updatedAt(customerOrder.getUpdatedAt())
//                .updatedFromEmployeeId(customerOrder.getUpdatedFromEmployee() != null ? customerOrder.getUpdatedFromEmployee().getId() : null)
//                .updatedFromEmployeeFirstname(customerOrder.getUpdatedFromEmployee() != null ? customerOrder.getUpdatedFromEmployee().getFirstName() : null)
                .isPaid(customerOrder.getIsPaid())
                .deliveryAddressId(customerOrder.getDeliveryAddress() != null ? customerOrder.getDeliveryAddress().getId() : null)
                .deliveryAddressFull(customerOrder.getDeliveryAddress() != null ? customerOrder.getDeliveryAddress().getFullAddress() : null)
                .plannedShipmentDate(customerOrder.getPlannedShipmentDate())
                .isPosted(customerOrder.getIsPosted())
                .products(customerOrder.getProducts()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .tasks(customerOrder.getTasks()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .files(customerOrder.getFiles()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .build();
    }

    public static CommissionReports convertToModel(CommissionReportsDto commissionReportsDto) {
        if (commissionReportsDto == null) {
            return null;
        }
        return CommissionReports.builder()
                .id(commissionReportsDto.getId())
                .dateOfCreation(commissionReportsDto.getDateOfCreation())
                .contract(ConverterDto.convertToModel(commissionReportsDto.getContractDto()))
                .contractor(ConverterDto.convertToModel(commissionReportsDto.getContractorDto()))
                .company(ConverterDto.convertToModel(commissionReportsDto.getCompanyDto()))
                .project(ConverterDto.convertToModel(commissionReportsDto.getProjectDto()))
                .sum(commissionReportsDto.getSum())
                .paid(commissionReportsDto.getPaid())
                .isSent(commissionReportsDto.getIsSent())
                .isPrinted(commissionReportsDto.getIsPrinted())
                .comment(commissionReportsDto.getComment())
                .startOfPeriod(commissionReportsDto.getStartOfPeriod())
                .endOfPeriod(commissionReportsDto.getEndOfPeriod())
                .sumOfReward(commissionReportsDto.getSumOfReward())
                .docType(commissionReportsDto.getDocType())
                .number(commissionReportsDto.getNumber())
                //  .salesChannel(ConverterDto.convertToModel(commissionReportsDto.getSalesChannelDto()))
                .accountCompany(commissionReportsDto.getAccountCompany())
                .accountContractor(commissionReportsDto.getAccountContractor())
                .sumOfCommittent(commissionReportsDto.getSumOfCommittent())
                .remainsToPay(commissionReportsDto.getRemainsToPay())
                .incomingNumber(commissionReportsDto.getIncomingNumber())
                .incomingDate(commissionReportsDto.getIncomingDate())
                .generalAccess(commissionReportsDto.getGeneralAccess())
                .ownerDepartment(commissionReportsDto.getOwnerDepartment())
                .ownerEmployee(commissionReportsDto.getOwnerEmployee())
                .status(commissionReportsDto.getStatus())
                .whenChanged(commissionReportsDto.getWhenChanged())
                .whoChanged(commissionReportsDto.getWhoChanged())
                .build();

    }

    public static CommissionReportsDto convertToDto(CommissionReports commissionReports) {
        if (commissionReports == null) {
            return null;
        }
        return CommissionReportsDto.builder()
                .id(commissionReports.getId())
                .dateOfCreation(commissionReports.getDateOfCreation())
                .contractDto(ConverterDto.convertToDto(commissionReports.getContract()))
                .contractorDto(ConverterDto.convertToDto(commissionReports.getContractor()))
                .companyDto(ConverterDto.convertToDto(commissionReports.getCompany()))
                .projectDto(ConverterDto.convertToDto(commissionReports.getProject()))
                // .salesChannelDto(ConverterDto.convertToDto(commissionReports.getSalesChannel()))
                .sum(commissionReports.getSum())
                .paid(commissionReports.getPaid())
                .isSent(commissionReports.getIsSent())
                .isPrinted(commissionReports.getIsPrinted())
                .comment(commissionReports.getComment())
                .startOfPeriod(commissionReports.getStartOfPeriod())
                .endOfPeriod(commissionReports.getEndOfPeriod())
                .sumOfReward(commissionReports.getSumOfReward())
                .docType(commissionReports.getDocType())
                .number(commissionReports.getNumber())
                .accountCompany(commissionReports.getAccountCompany())
                .accountContractor(commissionReports.getAccountContractor())
                .sumOfCommittent(commissionReports.getSumOfCommittent())
                .remainsToPay(commissionReports.getRemainsToPay())
                .incomingNumber(commissionReports.getIncomingNumber())
                .incomingDate(commissionReports.getIncomingDate())
                .generalAccess(commissionReports.getGeneralAccess())
                .ownerDepartment(commissionReports.getOwnerDepartment())
                .ownerEmployee(commissionReports.getOwnerEmployee())
                .status(commissionReports.getStatus())
                .whenChanged(commissionReports.getWhenChanged())
                .whoChanged(commissionReports.getWhoChanged())
                .build();
    }


    public static CustomerReturns convertToModel(CustomerReturnsDto customerReturnsDto) {
        return CustomerReturns.builder()
                .id(customerReturnsDto.getId())
                .date(customerReturnsDto.getDate())
                .sum(customerReturnsDto.getSum())
                .isPaid(customerReturnsDto.getIsPaid())
                .isSend(customerReturnsDto.getIsSend())
                .comment(customerReturnsDto.getComment())
                .warehouse(convertToModel(customerReturnsDto.getWarehouseDto()))
                .contract(convertToModel(customerReturnsDto.getContractDto()))
                .company(convertToModel(customerReturnsDto.getCompanyDto()))
                .contractor(convertToModel(customerReturnsDto.getContractorDto()))
                .products(customerReturnsDto.getProductDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .files(customerReturnsDto.getFileDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .tasks(customerReturnsDto.getTaskDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .build();
    }

    public static CustomerReturnsDto convertToDto(CustomerReturns customerReturns) {
        return CustomerReturnsDto
                .builder()
                .id(customerReturns.getId())
                .date(customerReturns.getDate())
                .sum(customerReturns.getSum())
                .isPaid(customerReturns.getIsPaid())
                .isSend(customerReturns.getIsSend())
                .comment(customerReturns.getComment())
                .warehouseDto(convertToDto(customerReturns.getWarehouse()))
                .contractDto(convertToDto(customerReturns.getContract()))
                .companyDto(convertToDto(customerReturns.getCompany()))
                .contractorDto(convertToDto(customerReturns.getContractor()))
                .productDtos(customerReturns.getProducts()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .fileDtos(customerReturns.getFiles()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .taskDtos(customerReturns.getTasks()
                        .stream()
                        .map(ConverterDto::convertToDto).collect(Collectors.toList()))
                .build();
    }

    public static Return convertToModel(ReturnDto returnDto) {
        if (returnDto == null) {
            return null;
        }

        return Return.builder()
                .id(returnDto.getId())
                .dataTime(returnDto.getDataTime())
                .warehouse(convertToModel(returnDto.getWarehouseDto()))
                .company(convertToModel(returnDto.getCompanyDto()))
                .contractor(convertToModel(returnDto.getContractorDto()))
                .contract(convertToModel(returnDto.getContractDto()))
                .project(convertToModel(returnDto.getProjectDto()))
                .files(returnDto.getFileDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .tasks(returnDto.getTaskDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .products(returnDto.getProductDtos()
                        .stream()
                        .map(ConverterDto::convertToModel)
                        .collect(Collectors.toList()))
                .sum(returnDto.getSum())
                .isSent(returnDto.getIsSent())
                .isPrinted(returnDto.getIsPrinted())
                .comment(returnDto.getComment())
                .build();
    }

    public static ReturnDto convertToDto(Return returns) {
        return ReturnDto
                .builder()
                .id(returns.getId())
                .dataTime(returns.getDataTime())
                .warehouseDto(convertToDto(returns.getWarehouse()))
                .companyDto(convertToDto(returns.getCompany()))
                .contractorDto(convertToDto(returns.getContractor()))
                .contractDto(convertToDto(returns.getContract()))
                .projectDto(convertToDto(returns.getProject()))
                .fileDtos(returns.getFiles()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .productDtos(returns.getProducts()
                        .stream()
                        .map(ConverterDto::convertToDto)
                        .collect(Collectors.toList()))
                .taskDtos(returns.getTasks()
                        .stream()
                        .map(ConverterDto::convertToDto).collect(Collectors.toList()))
                .sum(returns.getSum())
                .isSent(returns.getIsSent())
                .isPrinted(returns.getIsPrinted())
                .comment(returns.getComment())
                .build();
    }


    public static GoodsToRealizeGet convertToModel(GoodsToRealizeGetDto goodsToRealizeGetDto) {
        Product productNew =
                Product.builder()
                        .id(goodsToRealizeGetDto.getProductDtoId())
                        .name(goodsToRealizeGetDto.getProductDtoName())
                        .build();

        Unit unitNew =
                Unit.builder()
                        .id(goodsToRealizeGetDto.getUnitId())
                        .shortName(goodsToRealizeGetDto.getUnitName())
                        .build();


        return GoodsToRealizeGet.builder()
                .id(goodsToRealizeGetDto.getId())
                .product(productNew)
                .unit(unitNew)
                .getGoods(goodsToRealizeGetDto.getGetGoods())
                .quantity(goodsToRealizeGetDto.getQuantity())
                .amount(goodsToRealizeGetDto.getAmount())
                .arrive(goodsToRealizeGetDto.getArrive())
                .remains(goodsToRealizeGetDto.getRemains())
                .quantity_Noreport(goodsToRealizeGetDto.getQuantity_Noreport())
                .amount_Noreport(goodsToRealizeGetDto.getAmount_Noreport())
                .quantity_report(goodsToRealizeGetDto.getQuantity_report())
                .amount_report(goodsToRealizeGetDto.getAmount_report())
                .build();
    }

    public static GoodsToRealizeGive convertToModel(GoodsToRealizeGiveDto goodsToRealizeGiveDto) {
        Product productNew =
                Product.builder()
                        .id(goodsToRealizeGiveDto.getProductDtoId())
                        .name(goodsToRealizeGiveDto.getProductDtoName())
                        .build();
        Unit unitNew =
                Unit.builder()
                        .id(goodsToRealizeGiveDto.getUnitId())
                        .shortName(goodsToRealizeGiveDto.getUnitName())
                        .build();


        return GoodsToRealizeGive.builder()
                .id(goodsToRealizeGiveDto.getId())
                .product(productNew)
                .unit(unitNew)
                .giveGoods(goodsToRealizeGiveDto.getGiveGoods())
                .quantity(goodsToRealizeGiveDto.getQuantity())
                .amount(goodsToRealizeGiveDto.getAmount())
                .arrive(goodsToRealizeGiveDto.getArrive())
                .remains(goodsToRealizeGiveDto.getRemains())
                .build();
    }

    public static SalesChannelDto convertSalesChannelToDto(SalesChannel salesChannel) {
        return new SalesChannelDto.Builder()
                .id(salesChannel.getId())
                .name(salesChannel.getName())
                .type(salesChannel.getType())
                .description(salesChannel.getDescription())
                .generalAccessC(salesChannel.getGeneralAccessC())
                .ownerDepartment(salesChannel.getOwnerDepartment())
                .ownerEmployee(salesChannel.getOwnerEmployee())
                .whenChanged(salesChannel.getWhenChanged())
                .whoChanged(salesChannel.getWhoChanged())
                .build();
    }

    public static SalesChannel convertSalesChannelDtoToModel(SalesChannelDto salesChannelDto) {
        return new SalesChannel.Builder()
                .id(salesChannelDto.getId())
                .name(salesChannelDto.getName())
                .type(salesChannelDto.getType())
                .description(salesChannelDto.getDescription())
                .generalAccessC(salesChannelDto.getGeneralAccessC())
                .ownerDepartment(salesChannelDto.getOwnerDepartment())
                .ownerEmployee(salesChannelDto.getOwnerEmployee())
                .whenChanged(salesChannelDto.getWhenChanged())
                .whoChanged(salesChannelDto.getWhoChanged())
                .build();
    }

    public static Tasks convertToModel(TasksDto tasksDto) {
        // TODO: Просто заглушка для тестов, исполнитель должен быть всегда.
        Employee executor = null;
        if (tasksDto.getEmployeeId() != null) {
            executor = new Employee();
            executor.setId(tasksDto.getEmployeeId());
        }
        Contractor contractor = null;
        if (tasksDto.getContractorId() != null) {
            contractor = new Contractor();
            contractor.setId(tasksDto.getContractorId());
        }
        Contract contract = null;
        if (tasksDto.getContractId() != null) {
            contract = new Contract();
            contract.setId(tasksDto.getContractId());
        }
        return Tasks.builder()
                .id(tasksDto.getId())
                .description(tasksDto.getDescription())
                .deadline(tasksDto.getDeadline())
                .isDone(tasksDto.getIsDone())
                .executor(executor)
                .contractor(contractor)
                .contract(contract)
                .build();
    }

    public static TasksDto convertToDto(Tasks tasks) {
        return TasksDto.builder()
                .id(tasks.getId())
                .description(tasks.getDescription())
                .deadline(tasks.getDeadline())
                .isDone(tasks.getIsDone())
                .employeeId(tasks.getExecutor() != null ? tasks.getExecutor().getId() : null)
                .employeeName(tasks.getExecutor() != null ? tasks.getExecutor().getFirstName() : null)
                .contractorId(tasks.getContractor() != null ? tasks.getContractor().getId() : null)
                .contractorName(tasks.getContractor() != null ? tasks.getContractor().getName() : null)
                .contractId(tasks.getContract() != null ? tasks.getContract().getId() : null)
                .contractNumber(tasks.getContract() != null ? tasks.getContract().getNumber() : null)
                .build();
    }

    public static PointOfSalesDto convertPointOfSalesToDto(PointOfSales pointOfSales) {
        return PointOfSalesDto.builder()
                .id(pointOfSales.getId())
                .name(pointOfSales.getName())
                .type(pointOfSales.getType())
                .activity(pointOfSales.getActivity())
                .revenue(pointOfSales.getRevenue())
                .cheque(pointOfSales.getCheque())
                .averageСheck(pointOfSales.getAverageСheck())
                .moneyInTheCashRegister(pointOfSales.getMoneyInTheCashRegister())
                .cashiers(pointOfSales.getCashiers())
                .synchronization(pointOfSales.getSynchronization())
                .FN(pointOfSales.getFN())
                .validityPeriodFN(pointOfSales.getValidityPeriodFN())
                .build();
    }

    public static PointOfSales convertPointOfSalesDtoToModel(PointOfSalesDto pointOfSalesDto) {
        return PointOfSales.builder()
                .id(pointOfSalesDto.getId())
                .name(pointOfSalesDto.getName())
                .type(pointOfSalesDto.getType())
                .activity(pointOfSalesDto.getActivity())
                .revenue(pointOfSalesDto.getRevenue())
                .cheque(pointOfSalesDto.getCheque())
                .averageСheck(pointOfSalesDto.getAverageСheck())
                .moneyInTheCashRegister(pointOfSalesDto.getMoneyInTheCashRegister())
                .cashiers(pointOfSalesDto.getCashiers())
                .synchronization(pointOfSalesDto.getSynchronization())
                .FN(pointOfSalesDto.getFN())
                .validityPeriodFN(pointOfSalesDto.getValidityPeriodFN())
                .build();
    }

    public static ProductPriceForPriceListDto convertToDtoForPriceList(ProductPrice productPriceForPriceList) {
        if (productPriceForPriceList == null) {
            return null;
        }
        return ProductPriceForPriceListDto.builder()
                .id(productPriceForPriceList.getId())
                .typeOfPriceDto(convertToDto(productPriceForPriceList.getTypeOfPrice()))
                .price(productPriceForPriceList.getPrice())
                .build();
    }

    public static InvoicesReceived convertInvoiceReceivedDtoToModel(InvoiceReceivedDto invoiceReceivedDto) {
        return InvoicesReceived.builder()
                .id(invoiceReceivedDto.getId())
                .date(invoiceReceivedDto.getData())
                .contrAgent(convertToModel(invoiceReceivedDto.getContractor()))
                .company(convertToModel(invoiceReceivedDto.getCompany()))
                .sum(invoiceReceivedDto.getSum())
                .incomingNumber(invoiceReceivedDto.getIncomingNumber())
                .dateIncomingNumber(invoiceReceivedDto.getDateIncomingNumber())
                .sent(invoiceReceivedDto.getSent())
                .print(invoiceReceivedDto.getPrinted())
                .comments(invoiceReceivedDto.getComment())
                .build();
    }

    public static ProcurementManagement convertProcurementManagementDtoToModel(ProcurementManagementDto procurementManagementDto) {
        return ProcurementManagement.builder()
                .id(procurementManagementDto.getId())
                .name(procurementManagementDto.getName())
                .code(procurementManagementDto.getCode())
                .vendor_code(procurementManagementDto.getVendor_code())
                .number(procurementManagementDto.getNumber())
                .sum(procurementManagementDto.getSum())
                .cost_price(procurementManagementDto.getCost_price())
                .profit(procurementManagementDto.getProfit())
                .profitability(procurementManagementDto.getProfitability())
                .sales_day(procurementManagementDto.getSales_day())
                .balance(procurementManagementDto.getBalance())
                .reserve(procurementManagementDto.getReserve())
                .expectation(procurementManagementDto.getExpectation())
                .available(procurementManagementDto.getAvailable())
                .days_in_stock(procurementManagementDto.getDays_in_stock())
                .days_of_stock(procurementManagementDto.getDays_of_stock())
                .stock(procurementManagementDto.getStock())
                .build();
    }

    public static ProductionProcessTechnology convertToModel(ProductionProcessTechnologyDto productionProcessTechnologyDto) {
        Department department = new Department();
        department.setId(productionProcessTechnologyDto.getOwnerDepartmentId());
        Employee ownerEmployee = new Employee();
        ownerEmployee.setId(productionProcessTechnologyDto.getOwnerEmployeeId());
        Employee editEmployee = new Employee();
        editEmployee.setId(productionProcessTechnologyDto.getEditorEmployeeId());
        Set<ProductionStage> usedProductionStage = productionProcessTechnologyDto.getUsedProductionStageId()
                .stream()
                .map(x -> {
                    ProductionStage productionStage = new ProductionStage();
                    productionStage.setId(x);
                    return productionStage;
                })
                .collect(Collectors.toSet());

        return ProductionProcessTechnology.builder()
                .id(productionProcessTechnologyDto.getId())
                .name(productionProcessTechnologyDto.getName())
                .description(productionProcessTechnologyDto.getDescription())
                .generalAccess(productionProcessTechnologyDto.isGeneralAccess())
                .archived(productionProcessTechnologyDto.isArchived())
                .sortNumber(productionProcessTechnologyDto.getSortNumber())
                .ownerDepartment(department)
                .ownerEmployee(ownerEmployee)
                .editorEmployee(editEmployee)
                .dateOfEdit(productionProcessTechnologyDto.getDateOfEdit())
                .usedProductionStage(usedProductionStage)
                .build();
    }

    public static ProductionProcessTechnologyDto convertToDto(ProductionProcessTechnology productionProcessTechnology) {
        Set<Long> usedProductionStageId = productionProcessTechnology.getUsedProductionStage().stream()
                .map(ProductionStage::getId)
                .collect(Collectors.toSet());

        return ProductionProcessTechnologyDto.builder()
                .id(productionProcessTechnology.getId())
                .name(productionProcessTechnology.getName())
                .description(productionProcessTechnology.getDescription())
                .generalAccess(productionProcessTechnology.getGeneralAccess())
                .archived(productionProcessTechnology.isArchived())
                .sortNumber(productionProcessTechnology.getSortNumber())
                .ownerDepartmentId(productionProcessTechnology.getOwnerDepartment().getId())
                .ownerDepartmentName(productionProcessTechnology.getOwnerDepartment().getName())
                .ownerEmployeeId(productionProcessTechnology.getOwnerEmployee().getId())
                .ownerEmployeeName(productionProcessTechnology.getOwnerEmployee().getLastName())
                .editorEmployeeId(productionProcessTechnology.getEditorEmployee().getId())
                .editorEmployeeName(productionProcessTechnology.getEditorEmployee().getLastName())
                .dateOfEdit(productionProcessTechnology.getDateOfEdit())
                .usedProductionStageId(usedProductionStageId)
                .build();
    }

    public static ProductionTasks convertToModel(ProductionTasksDto productionTasksDto) {
        Department department = new Department();
        department.setId(productionTasksDto.getOwnerDepartmentId());
        Warehouse materialWarehouse = new Warehouse();
        materialWarehouse.setId(productionTasksDto.getMaterialWarehouseId());
        Employee editEmloyee = new Employee();
        editEmloyee.setId(productionTasksDto.getEditEmployeeId());
        Employee ownerEmloyee = new Employee();
        ownerEmloyee.setId(productionTasksDto.getOwnerEmployeeId());
        Warehouse productionWarehouse = new Warehouse();
        productionWarehouse.setId(productionTasksDto.getProductionWarehouseId());
        List<AdditionalField> additionalFieldList = productionTasksDto.getAdditionalFieldsIds() != null
                ? productionTasksDto.getAdditionalFieldsIds().stream()
                .map(x -> AdditionalField.builder().id(x).build()).collect(Collectors.toList())
                : Collections.emptyList();

        return ProductionTasks.builder()
                .id(productionTasksDto.getId())
                .taskId(productionTasksDto.getTaskId())
                .dateOfCreate(productionTasksDto.getDateOfCreate())
                .dateOfEdit(productionTasksDto.getDateOfEdit())
                .organization(productionTasksDto.getOrganization())
                .plannedDate(productionTasksDto.getPlannedDate())
                .materialWarehouse(materialWarehouse)
                .editEmployee(editEmloyee)
                .ownerEmployee(ownerEmloyee)
                .ownerDepartment(department)
                .productionWarehouse(productionWarehouse)
                .dateOfStart(productionTasksDto.getDateOfStart())
                .dateOfEnd(productionTasksDto.getDateOfEnd())
                .isAccessed(productionTasksDto.getIsAccessed())
                .dateOfSend(productionTasksDto.getDateOfSend())
                .dateOfPrint(productionTasksDto.getDateOfPrint())
                .description(productionTasksDto.getDescription())
                .additionalFields(additionalFieldList)
                .build();
    }

    public static ProductionTasksDto convertToDto(ProductionTasks productionTasks) {

        return ProductionTasksDto.builder()
                .id(productionTasks.getId())
                .taskId(productionTasks.getTaskId())
                .dateOfCreate(productionTasks.getDateOfCreate())
                .organization(productionTasks.getOrganization())
                .plannedDate(productionTasks.getPlannedDate())
                .materialWarehouseId(productionTasks.getMaterialWarehouse().getId())
                .materialWarehouseName(productionTasks.getMaterialWarehouse().getName())
                .productionWarehouseId(productionTasks.getProductionWarehouse().getId())
                .productionWarehouseName(productionTasks.getProductionWarehouse().getName())
                .isAccessed(productionTasks.isAccessed())
                .ownerDepartmentId(productionTasks.getOwnerDepartment().getId())
                .ownerDepartmentName(productionTasks.getOwnerDepartment().getName())
                .ownerEmployeeId(productionTasks.getOwnerEmployee().getId())
                .ownerEmployeeName(productionTasks.getOwnerEmployee().getLastName())
                .dateOfStart(productionTasks.getDateOfStart())
                .dateOfEnd(productionTasks.getDateOfEnd())
                .dateOfSend(productionTasks.getDateOfSend())
                .dateOfPrint(productionTasks.getDateOfPrint())
                .description(productionTasks.getDescription())
                .dateOfEdit(productionTasks.getDateOfEdit())
                .editEmployeeId(productionTasks.getEditEmployee().getId())
                .editEmployeeName(productionTasks.getEditEmployee().getLastName())
                .additionalFieldsIds(productionTasks.getAdditionalFields().stream()
                        .map(AdditionalField::getId).collect(Collectors.toList()))
                .additionalFieldsNames(productionTasks.getAdditionalFields().stream()
                        .map(AdditionalField::getName).collect(Collectors.toList()))
                .build();
    }

    public static AdditionalFieldDto convertToDto(AdditionalField additionalField) {

        return AdditionalFieldDto.builder()
                .id(additionalField.getId())
                .name(additionalField.getName())
                .property(additionalField.getProperty())
                .required(additionalField.isRequired())
                .hide(additionalField.isHide())
                .description(additionalField.getDescription())
                .build();
    }

    public static AdditionalField convertToModel(AdditionalFieldDto additionalFieldDto) {

        return AdditionalField.builder()
                .id(additionalFieldDto.getId())
                .name(additionalFieldDto.getName())
                .property(additionalFieldDto.getProperty())
                .required(additionalFieldDto.getRequired() != null && additionalFieldDto.getRequired())
                .hide(additionalFieldDto.getHide() != null && additionalFieldDto.getHide())
                .description(additionalFieldDto.getDescription())
                .build();
    }

    public static WriteOffs convertToModel(WriteOffsDto dto) {
        return WriteOffs.builder()
                .id(dto.getId())
                .dateTime(dto.getDateOfCreation())
                .warehouseFrom(convertToModel(dto.getWarehouseFrom()))
                .warehouseTo(convertToModel(dto.getWarehouseTo()))
                .company(convertToModel(dto.getCompany()))
                .sum(dto.getSum())
                .status(dto.getStatus())
                .warehouseName(dto.getWarehouseName())
                .moved(dto.isMoved())
                .printed(dto.isPrinted())
                .comment(dto.getComment())
                .build();
    }

    public static Posting convertToModel(PostingDto dto) {
        return Posting.builder()
                .id(dto.getId())
                .dateTime(dto.getDateOfCreation())
                .warehouseTo(convertToModel(dto.getWarehouseTo()))
                .company(convertToModel(dto.getCompany()))
                .sum(dto.getSum())
                .moved(dto.isMoved())
                .printed(dto.isPrinted())
                .comment(dto.getComment())
                .build();
    }

    public static Introduction convertToModel(IntroductionDto introductionDto) {
        PointOfSales point = new PointOfSales();
        point.setId(introductionDto.getPointId());
        Employee fromWhom = new Employee();
        fromWhom.setId(introductionDto.getFromWhomId());
        Company company = new Company();
        company.setId(introductionDto.getOrganizationId());
        Department ownerDepartment = new Department();
        ownerDepartment.setId(introductionDto.getOwnerDepartmentId());
        Employee ownerEmployee = new Employee();
        ownerEmployee.setId(introductionDto.getOwnerEmployeeId());
        Employee editorEmployee = new Employee();
        editorEmployee.setId(introductionDto.getEditorEmployeeId());

        return Introduction.builder()
                .id(introductionDto.getId())
                .docNumber(introductionDto.getNumber())
                .date(introductionDto.getTime())
                .point(point)
                .fromWhom(fromWhom)
                .company(company)
                .sum(introductionDto.getSum())
                .isSharedAccess(introductionDto.getGeneralAccess())
                .department(ownerDepartment)
                .employee(ownerEmployee)
                .sent(introductionDto.getSent())
                .print(introductionDto.getPrinted())
                .comments(introductionDto.getComment())
                .updatedAt(introductionDto.getDateOfEdit())
                .updatedFromEmployee(editorEmployee)
                .build();
    }

    public static IntroductionDto convertToDto(Introduction introduction) {

        return IntroductionDto.builder()
                .id(introduction.getId())
                .number(introduction.getDocNumber())
                .time(introduction.getDate())
                .pointId(introduction.getPoint().getId())
                .fromWhomId(introduction.getFromWhom().getId())
                .organizationId(introduction.getCompany().getId())
                .sum(introduction.getSum())
                .generalAccess(introduction.getIsSharedAccess())
                .ownerDepartmentId(introduction.getDepartment().getId())
                .ownerEmployeeId(introduction.getEmployee().getId())
                .sent(introduction.getSent())
                .printed(introduction.getPrint())
                .comment(introduction.getComments())
                .dateOfEdit(introduction.getUpdatedAt())
                .editorEmployeeId(introduction.getUpdatedFromEmployee().getId())
                .build();
    }

    public static Inventory convertToModel(InventoryDto dto) {
        return Inventory.builder()
                .id(dto.getId())
                .dateTime(dto.getDateOfCreation())
                .warehouseFrom(convertToModel(dto.getWarehouseFrom()))
                .company(convertToModel(dto.getCompany()))
                .moved(dto.isMoved())
                .printed(dto.isPrinted())
                .comment(dto.getComment())
                .build();
    }

    public static RetailShift convertToModel(RetailShiftDto retailShiftDto) {
        return RetailShift.builder()
                .id(retailShiftDto.getId())
                .dateOfOpen(retailShiftDto.getDateOfOpen())
                .dateOfClose(retailShiftDto.getDateOfClose())
                .pointOfSales(convertPointOfSalesDtoToModel(retailShiftDto.getPointOfSales()))
                .warehouse(convertToModel(retailShiftDto.getWarehouse()))
                .company(convertToModel(retailShiftDto.getCompany()))
                .bank(convertToModel(retailShiftDto.getBank()))
                .cashlessShiftRevenue(retailShiftDto.getCashlessShiftRevenue())
                .cashShiftRevenue(retailShiftDto.getCashShiftRevenue())
                .shiftRevenue(retailShiftDto.getShiftRevenue())
                .recevied(retailShiftDto.getRecevied())
                .sale(retailShiftDto.getSale())
                .comission(retailShiftDto.getComission())
                .isAccessed(retailShiftDto.isAccessed())
                .ownerDepartment(convertToModel(retailShiftDto.getOwnerDepartment()))
                .ownerEmployee(convertToModel(retailShiftDto.getOwnerEmployee()))
                .isSent(retailShiftDto.isSent())
                .isPrinted(retailShiftDto.isPrinted())
                .description(retailShiftDto.getDescription())
                .dateOfEdit(retailShiftDto.getDateOfEdit())
                .editEmployee(convertToModel(retailShiftDto.getEditEmployee()))
                .build();
    }

    public static RetailShiftDto convertToDto(RetailShift retailShift) {

        return RetailShiftDto.builder()
                .id(retailShift.getId())
                .dateOfOpen(retailShift.getDateOfOpen())
                .dateOfClose(retailShift.getDateOfClose())
                .pointOfSales(retailShift.getPointOfSales() != null ? convertPointOfSalesToDto(retailShift.getPointOfSales()) : null)
                .warehouse(retailShift.getWarehouse() != null ? convertToDto(retailShift.getWarehouse()) : null)
                .company(retailShift.getCompany() != null ? convertToDto(retailShift.getCompany()) : null)
                .bank(retailShift.getBank() != null ? convertToDto(retailShift.getBank()) : null)
                .cashlessShiftRevenue(retailShift.getCashlessShiftRevenue())
                .cashShiftRevenue(retailShift.getCashShiftRevenue())
                .shiftRevenue(retailShift.getShiftRevenue())
                .recevied(retailShift.getRecevied())
                .sale(retailShift.getSale())
                .comission(retailShift.getComission())
                .isAccessed(retailShift.isAccessed())
                .ownerDepartment(retailShift.getOwnerDepartment() != null ? convertToDto(retailShift.getOwnerDepartment()) : null)
                .ownerEmployee(retailShift.getOwnerEmployee() != null ? convertToDto(retailShift.getOwnerEmployee()) : null)
                .isSent(retailShift.isSent())
                .isPrinted(retailShift.isPrinted())
                .description(retailShift.getDescription())
                .dateOfEdit(retailShift.getDateOfEdit())
                .editEmployee(retailShift.getEditEmployee() != null ? convertToDto(retailShift.getEditEmployee()) : null)
                .build();
    }


    public static Script convertToModel(ScriptDto scriptDto) {
        Employee owner = new Employee();
        owner.setId(scriptDto.getOwnerId());

        return Script.builder()
                .id(scriptDto.getId())
                .name(scriptDto.getName())
                .activity(scriptDto.getActivity())
                .comment(scriptDto.getComment())
                .owner(owner)
                .event(scriptDto.getEvent())
                .action(scriptDto.getAction())
                .conditions(scriptDto.getConditions())
                .build();
    }

    public static ScriptDto convertToDto(Script script) {
        return ScriptDto.builder()
                .id(script.getId())
                .name(script.getName())
                .activity(script.getActivity())
                .comment(script.getComment())
                .ownerId(script.getOwner() != null ? script.getOwner().getId() : null)
                .firstNameOwner(script.getOwner() != null ? script.getOwner().getFirstName() : null)
                .event(script.getEvent())
                .action(script.getAction())
                .conditions(script.getConditions())
                .build();
    }

    public static InternalOrder convertToModel(InternalOrderDto dto) {
        if (dto == null) {
            return null;
        }

        return InternalOrder.builder()
                .id(dto.getId())
                .docNumber(dto.getNumber())
                .date(dto.getLocalDateTime())
                .company(convertToModel(dto.getOrganization()))
                .sum(dto.getSum())
                .shipped(dto.getShipped())
                .project(convertToModel(dto.getProjectDto()))
                .isSharedAccess(dto.getIsSharedAccess())
                .warehouseFrom(convertToModel(dto.getWarehouseDto()))
                .department(convertToModel(dto.getOwnerDepartment()))
                .employee(convertToModel(dto.getOwnerEmployee()))
                .sent(dto.getSent())
                .print(dto.getPrint())
                .comments(dto.getComment())
                .updatedFromEmployee(convertToModel(dto.getWhoChanged()))
                .updatedAt(dto.getWhenChanged())
                .tasks(dto.getTasksDto() != null ?
                        dto.getTasksDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .files(dto.getFilesDto() != null ?
                        dto.getFilesDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static InternalOrderDto convertToDto(InternalOrder internalOrder) {
        if (internalOrder == null) {
            return null;
        }

        return InternalOrderDto.builder()
                .id(internalOrder.getId())
                .number(internalOrder.getDocNumber())
                .localDateTime(internalOrder.getDate())
                .organization(convertToDto(internalOrder.getCompany()))
                .sum(internalOrder.getSum())
                .shipped(internalOrder.getShipped())
                .projectDto(convertToDto(internalOrder.getProject()))
                .isSharedAccess(internalOrder.getIsSharedAccess())
                .warehouseDto(convertToDto(internalOrder.getWarehouseFrom()))
                .ownerDepartment(convertToDto(internalOrder.getDepartment()))
                .ownerEmployee(convertToDto(internalOrder.getEmployee()))
                .sent(internalOrder.getSent())
                .print(internalOrder.getPrint())
                .comment(internalOrder.getComments())
                .whoChanged(convertToDto(internalOrder.getUpdatedFromEmployee()))
                .whenChanged(internalOrder.getUpdatedAt())
                .tasksDto(internalOrder.getTasks() != null ?
                        internalOrder.getTasks()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .filesDto(internalOrder.getFiles() != null ?
                        internalOrder.getFiles()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static Shipments convertToModel(ShipmentsDto dto) {
        if (dto == null) {
            return null;
        }

        return Shipments.builder()
                .id(dto.getId())
                .date(dto.getData())
                .warehouseFrom(convertToModel(dto.getWarehouse()))
                .contrAgent(convertToModel(dto.getContractor()))
                .company(convertToModel(dto.getCompany()))
                .consignee(convertToModel(dto.getConsignee()))
                .sum(dto.getSum())
                .paid(dto.getPaid())
                .sent(dto.getSent())
                .print(dto.getPrinted())
                .comments(dto.getComment())
                .tasks(dto.getTasksDto() != null ?
                        dto.getTasksDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .files(dto.getFilesDto() != null ?
                        dto.getFilesDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static ShipmentsDto convertToDto(Shipments shipments) {
        if (shipments == null) {
            return null;
        }

        return ShipmentsDto.builder()
                .id(shipments.getId())
                .data(shipments.getDate())
                .warehouse(convertToDto(shipments.getWarehouseFrom()))
                .contractor(convertToDto(shipments.getContrAgent()))
                .company(convertToDto(shipments.getCompany()))
                .consignee(convertToDto(shipments.getConsignee()))
                .sum(shipments.getSum())
                .paid(shipments.getPaid())
                .sent(shipments.getSent())
                .printed(shipments.getPrint())
                .comment(shipments.getComments())
                .tasksDto(shipments.getTasks() != null ?
                        shipments.getTasks()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .filesDto(shipments.getFiles() != null ?
                        shipments.getFiles()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static SerialNumber convertToModel(SerialNumberDto serialNumberDto) {
        return SerialNumber.builder()
                .id(serialNumberDto.getId())
                .code(serialNumberDto.getCode())
                .vendor_code(serialNumberDto.getVendor_code())
                .product(convertToModel(serialNumberDto.getProduct()))
                .warehouse(convertToModel(serialNumberDto.getWarehouse()))
                .typeOfDoc(serialNumberDto.getTypeOfDoc())
                .number(serialNumberDto.getNumber())
                .description(serialNumberDto.getDescription())
                .build();
    }

    public static Acceptances convertToModel(AcceptancesDto dto) {
        if (dto == null) {
            return null;
        }

        Warehouse warehouseTo = new Warehouse();
        warehouseTo.setId(dto.getWarehouseId());

        Contractor contractor = new Contractor();
        contractor.setId(dto.getContractorId());

        Company company = new Company();
        company.setId(dto.getCompanyId());

        Project project = new Project();
        project.setId(dto.getProjectId());

        Contract contract = new Contract();
        contract.setId(dto.getContractId());

        Employee ownerEmp = new Employee();
        ownerEmp.setId(dto.getOwnerEmployeeId());

        Department ownerDep = new Department();
        ownerDep.setId(dto.getOwnerDepartmentId());

        Employee whoChanged = new Employee();
        whoChanged.setId(dto.getIdWhoChanged());

        return Acceptances.builder()
                .id(dto.getId())
                .docNumber(dto.getNumber())
                .date(dto.getTime())
                .warehouseTo(warehouseTo)
                .contrAgent(contractor)
                .company(company)
                .sum(dto.getSum())
                .paid(dto.getPaid())
                .noPaid(dto.getNoPaid())
                .dateIncomingNumber(dto.getDateIncomingNumber())
                .incomingNumber(dto.getIncomingNumber())
                .project(project)
                .contract(contract)
                .overHeadCost(dto.getOverHeadCost())
                .returnSum(dto.getReturnSum())
                .isSharedAccess(dto.getIsSharedAccess())
                .employee(ownerEmp)
                .department(ownerDep)
                .sent(dto.getSend())
                .print(dto.getPrint())
                .comments(dto.getComment())
                .updatedAt(dto.getWhenChanged())
                .updatedFromEmployee(whoChanged)
                .tasks(dto.getTasksDto() != null ?
                        dto.getTasksDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .files(dto.getFilesDto() != null ?
                        dto.getFilesDto()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .products(dto.getProductDtos() != null ?
                        dto.getProductDtos()
                                .stream()
                                .map(ConverterDto::convertToModel)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static AcceptancesDto convertToDto(Acceptances acceptances) {
        if (acceptances == null) {
            return null;
        }

        return AcceptancesDto.builder()
                .id(acceptances.getId())
                .number(acceptances.getDocNumber())
                .time(acceptances.getDate())
                .warehouseId(acceptances.getWarehouseTo() != null ? acceptances.getWarehouseTo().getId() : null)
                .warehouseName(acceptances.getWarehouseTo() != null ? acceptances.getWarehouseTo().getName() : null)
                .contractorId(acceptances.getContrAgent() != null ? acceptances.getContrAgent().getId() : null)
                .contractorName(acceptances.getContrAgent() != null ? acceptances.getContrAgent().getName() : null)
                .companyId(acceptances.getCompany() != null ? acceptances.getCompany().getId() : null)
                .companyName(acceptances.getCompany() != null ? acceptances.getCompany().getName() : null)
                .sum(acceptances.getSum())
                .paid(acceptances.getPaid())
                .noPaid(acceptances.getNoPaid())
                .dateIncomingNumber(acceptances.getDateIncomingNumber())
                .incomingNumber(acceptances.getIncomingNumber())
                .projectId(acceptances.getProject() != null ? acceptances.getProject().getId() : null)
                .projectName(acceptances.getProject() != null ? acceptances.getProject().getName() : null)
                .contractId(acceptances.getContract() != null ? acceptances.getContract().getId() : null)
                .contractNumber(acceptances.getContract() != null ? acceptances.getContract().getNumber() : null)
                .overHeadCost(acceptances.getOverHeadCost())
                .returnSum(acceptances.getReturnSum())
                .isSharedAccess(acceptances.getIsSharedAccess())
                .ownerEmployeeId(acceptances.getEmployee() != null ? acceptances.getEmployee().getId() : null)
                .ownerEmployeeName(acceptances.getEmployee() != null ? acceptances.getEmployee().getFirstName() : null)
                .ownerDepartmentId(acceptances.getDepartment() != null ? acceptances.getDepartment().getId() : null)
                .ownerDepartmentName(acceptances.getDepartment() != null ? acceptances.getDepartment().getName() : null)
                .send(acceptances.getSent())
                .print(acceptances.getPrint())
                .comment(acceptances.getComments())
                .whenChanged(acceptances.getUpdatedAt())
                .idWhoChanged(acceptances.getUpdatedFromEmployee() != null ? acceptances.getUpdatedFromEmployee().getId() : null)
                .nameWhoChanget(acceptances.getUpdatedFromEmployee() != null ? acceptances.getUpdatedFromEmployee().getFirstName() : null)
                .tasksDto(acceptances.getTasks() != null ?
                        acceptances.getTasks()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .filesDto(acceptances.getFiles() != null ?
                        acceptances.getFiles()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .productDtos(acceptances.getProducts() != null ?
                        acceptances.getProducts()
                                .stream()
                                .map(ConverterDto::convertToDto)
                                .collect(Collectors.toList()) : List.of())
                .build();
    }

    public static Status convertToModel(StatusDto statusDto) {
        return Status.builder()
                .id(statusDto.getId())
                .nameOfClass(statusDto.getNameOfClass())
                .titleOfStatus(statusDto.getTitleOfStatus())
                .colorCode(statusDto.getColorCode())
                .build();
    }

    public static StatusDto convertToDto(Status status) {
        return StatusDto.builder()
                .id(status.getId())
                .nameOfClass(status.getNameOfClass())
                .titleOfStatus(status.getTitleOfStatus())
                .colorCode(status.getColorCode())
                .build();
    }


    public static IpNetwork convertToModel(IpNetworkDto ipNetworkDto) {
        return IpNetwork.builder()
                .id(ipNetworkDto.getId())
                .network(ipNetworkDto.getNetwork())
                .mask(ipNetworkDto.getMask())
                .build();
    }

    public static IpNetworkDto convertToDto(IpNetwork ipNetwork) {
        if (ipNetwork == null) {
            return null;
        }
        return IpNetworkDto.builder()
                .id(ipNetwork.getId())
                .network(ipNetwork.getNetwork())
                .mask(ipNetwork.getMask())
                .build();
    }

    public static IpAddress convertToModel(IpAddressDto ipAddressDto) {
        return IpAddress.builder()
                .id(ipAddressDto.getId())
                .address(ipAddressDto.getAddress())
                .build();
    }

    public static IpAddressDto convertToDto(IpAddress ipAddress) {
        if (ipAddress == null) {
            return null;
        }
        return IpAddressDto.builder()
                .id(ipAddress.getId())
                .address(ipAddress.getAddress())
                .build();
    }
}

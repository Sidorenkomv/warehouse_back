/**
 * Класс не участвует в работе приложения и может быть удалён
 * Предназначен для создания лёгковесных баз данных Кладр для тестирования
 */

package com.warehouse_accounting.util;

import com.linuxense.javadbf.DBFException;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFReader;
import com.linuxense.javadbf.DBFUtils;
import com.linuxense.javadbf.DBFWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Kladr {

    public Kladr() {
        copyKladr();
        copy(
                new File("src/main/resources/data_for_initializer/STREET.dbf"),
                new File("src/test/resources/STREET.dbf"),
                100
        );
        copy(
                new File("src/main/resources/data_for_initializer/DOMA.dbf"),
                new File("src/test/resources/DOMA.dbf"),
                100
        );
    }

    private void copy(File fileSource, File fileTarget, int limit) {
        DBFReader reader = null;
        try {
            reader = new DBFReader(new FileInputStream(fileSource));

            if (fileTarget.exists()) {
                fileTarget.delete();
            }

            DBFWriter writer = new DBFWriter(fileTarget, reader.getCharset());

            //копирование наименования полей
            int numberOfFields = reader.getFieldCount();
            DBFField[] fields = new DBFField[numberOfFields];
            for (int i = 0; i < numberOfFields; i++) {
                DBFField fieldSource = reader.getField(i);

                fields[i] = new DBFField();
                fields[i].setName(fieldSource.getName());
                fields[i].setType(fieldSource.getType());
                fields[i].setLength(fieldSource.getLength());
            }
            writer.setFields(fields);

            //копирование строк
            Object[] rowObjects;
            int counter = 0; //счётчик строк
            while ((rowObjects = reader.nextRecord()) != null) {
                counter++;
                if (counter > limit) {
                    break;
                }
                //копирование значений столбцов
                Object rowData[] = new Object[rowObjects.length];
                for (int i = 0; i < rowObjects.length; i++) {
                    rowData[i] = rowObjects[i];
                }
                writer.addRecord(rowData);
            }

            writer.close();

            System.out.println("Копирование в файл " + fileTarget.getName() + " завершено успешно.");

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            DBFUtils.close(reader);
        }
    }

    private void copyKladr() {
        DBFReader reader = null;
        try {
            File fileSource = new File("src/main/resources/data_for_initializer/KLADR.dbf");
            File fileTarget = new File("src/test/resources/KLADR.dbf");
            reader = new DBFReader(new FileInputStream(fileSource));

            if (fileTarget.exists()) {
                fileTarget.delete();
            }

            DBFWriter writer = new DBFWriter(fileTarget);


            //копирование наименования полей
            int numberOfFields = reader.getFieldCount();
            DBFField[] fields = new DBFField[numberOfFields];
            for (int i = 0; i < numberOfFields; i++) {
                DBFField fieldSource = reader.getField(i);

                fields[i] = new DBFField();
                fields[i].setName(fieldSource.getName());
                fields[i].setType(fieldSource.getType());
                fields[i].setLength(fieldSource.getLength());
            }
            writer.setFields(fields);

            //копирование строк
            Object[] rowObjects;
            int counter = 0; //счётчик строк
            int limit = 1000;//лимит
            while ((rowObjects = reader.nextRecord()) != null) {
                String code = rowObjects[2].toString();
                //в копию должны попасть все регионы
                if (!code.endsWith("00000000000")) {
                    counter++;
                    if (counter > limit) {
                        continue;
                    }
                }

                //копирование значений столбцов
                Object rowData[] = new Object[rowObjects.length];
                for (int i = 0; i < rowObjects.length; i++) {
                    rowData[i] = rowObjects[i];
                }
                writer.addRecord(rowData);
            }

            writer.close();

            System.out.println("Копирование в файл " + fileTarget.getName() + " завершено успешно.");

        } catch (DBFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            DBFUtils.close(reader);
        }
    }
}

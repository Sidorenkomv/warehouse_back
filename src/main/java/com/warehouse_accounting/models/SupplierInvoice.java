package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "supplier_invoices")
public class SupplierInvoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String  invoiceNumber;

    @Column
    private String dateInvoiceNumber;

    @Column
    private Boolean checkboxProd;

    @Column
    private String organization;

    @Column
    private String warehouse;

    @Column
    private String contrAgent;

    @Column
    private String contract;

    @Column
    private String datePay;

    @Column
    private String project;

    @Column
    private String incomingNumber;

    @Column
    private String dateIncomingNumber;

    @Column
    private Boolean checkboxName;

    @Column
    private Boolean checkboxNDS;

    @Column
    private Boolean checkboxOnNDS;

    @Column
    private String addPosition;

    @Column
    private String addComment;
}

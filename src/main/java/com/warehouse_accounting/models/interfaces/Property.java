package com.warehouse_accounting.models.interfaces;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.warehouse_accounting.util.PropertyDeserializer;

@JsonDeserialize(using = PropertyDeserializer.class)
public interface Property<T> {

    T getValue();

    default String getType() {
        return getValue().getClass().getSimpleName();
    }
}

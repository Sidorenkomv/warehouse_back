package com.warehouse_accounting.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "stage")
public class ProductionStage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private boolean generalAccess;

    @Column
    private boolean archived;

    @Column
    private String sortNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private Department ownerDepartment;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee ownerEmployee;

    @Column
    private Date dateOfEdit;

    @OneToOne
    private Employee editorEmployee;

    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "usedProductionStage")
    private Set<ProductionProcessTechnology> usedProductionProcessTechnology;
}

package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String country;

    @Column
    private String articul;

    @Column(scale = 3)
    private BigDecimal code;

    @Column(scale = 3)
    private BigDecimal outCode;

    @Column(scale = 3)
    private BigDecimal weight;

    @Column(scale = 6)
    private BigDecimal volume;

    @Column(scale = 2)
    private BigDecimal purchasePrice;

    private Float nds;

    @Column
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Unit unit;

    @Column
    private Boolean archive = false;

//    @Column
//    private String productGroup;

    @OneToOne(fetch = FetchType.LAZY)
    private UnitsOfMeasure unitsOfMeasure;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private List<ProductPrice> productPrices;

    @ManyToOne(fetch = FetchType.LAZY)
    private TaxSystem taxSystem;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Image> images;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductGroup productGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    private AttributeOfCalculationObject attributeOfCalculationObject;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private GoodsToRealizeGet goodsToRealizeGet;

    @ManyToMany(mappedBy = "products")
    private List<Shipment> shipments;

    @OneToMany(mappedBy = "materials", cascade = CascadeType.ALL)
    private List <TechnologicalMapMaterial> materials;

    @PreRemove
    private void preRemove() {
        shipments.forEach(shipment -> shipment.getProducts().remove(this));
    }


}

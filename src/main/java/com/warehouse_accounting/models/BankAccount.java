package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bank_accounts")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String rcbic;

    @Column
    private String bank;

    @Column
    private String correspondentAccount;

    @Column
    private String account;

    @Column
    private Boolean mainAccount;

    @Column
    private String sortNumber;

    @Column
    private String bankAddress;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "contractor_id")
    private Contractor contractor;

}

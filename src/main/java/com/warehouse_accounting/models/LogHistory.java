package com.warehouse_accounting.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "loghistory")
public class LogHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Document document;

    @Transient
    private InvoiceEdit invoiceEdit;

    @Column
    String editAuthor;

    @Column
    String date;

    @Column
    String field;

    public void WriteLogHistory(){
        editAuthor = invoiceEdit.getEditAuthor().getFirstName();
        date = invoiceEdit.getDateTime().toString();
        field = invoiceEdit.getField();
    }
}

package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contracts")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String number;

    @Column
    private String code;

    @Column
    private LocalDate contractDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Company company;

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    private BankAccount bankAccount;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Contractor contractor;

    @Column
    private BigDecimal amount = BigDecimal.valueOf(0);
//
//    @Column
//    private BigDecimal paid = BigDecimal.valueOf(0);
//
//    @Column
//    private BigDecimal done = BigDecimal.valueOf(0);

//    @Column
//    private Boolean isSharedAccess = true;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Department ownerDepartment;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Employee ownerEmployee;
//
//    @Column
//    private Boolean isSend;
//
//    @Column
//    private Boolean isPrint;

    @Column
    private Boolean archive = false;

    @Column
    private String comment;

//    @Column
//    private LocalDate whenChange;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Employee whoChange;

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
//    private LegalDetail legalDetail;

    @Column
    private String typeOfContract;

    @Column
    private String reward;

    @Column
    private Integer percentageOfTheSaleAmount;

    @Column(nullable = false)
    private Boolean enabled;

}

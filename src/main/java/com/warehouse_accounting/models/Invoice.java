package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


// Универсальная сущность для операций "ПРИХОД", "РАСХОД", "ОПРИХОДОВАНИЕ", "СПИСАНИЕ"

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "invoices")
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String number;

    @Column
    private LocalDateTime invoiceDateTime;

    @Column
    @Enumerated(EnumType.STRING)
    private TypeOfInvoice type;

    @Column
    private boolean isPosted;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee invoiceAuthor;

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouse;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<InvoiceProduct> invoiceProducts;

    @Column
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;

    @OneToMany(fetch = FetchType.LAZY)
    private List<InvoiceEdit> edits;
}

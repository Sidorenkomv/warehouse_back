/**
 * Здесь можно добавлять новые виды Property,
 * также для правильной работы необходимо сделать изменения в package-info пакета model,
 * классе PropertyDeserializer и классе AdditionalFieldService
 */
package com.warehouse_accounting.models.property;
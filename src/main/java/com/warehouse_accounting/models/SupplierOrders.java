package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * сущность "Заказы поставщикам"
 */

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "supplier_orders")
public class SupplierOrders extends Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //Айди контра))))))


    @Column
    private Long contrAgentAccount;         //Счет контрагента

    @Column
    private Long companyAccount;            //Счет организации

    @Column
    private Long invoiceIssued;             //Выставлено счетов

    @Column
    private Long paid;                      //Оплачено

    @Column
    private Long notPaid;                   //Не оплачено

    @Column
    private Long accepted;                  //Принято

    @Column
    private Long waiting;                   //В ожидании

    @Column
    private Long refundAmount;              //Сумма возвратов

    @Column
    private LocalDateTime acceptanceDate;   //Планируемая дата приемки

}


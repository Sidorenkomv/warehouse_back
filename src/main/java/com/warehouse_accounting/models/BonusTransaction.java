package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bonus_transactions")
public class BonusTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDate created;

    @Column
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column
    private Long bonusValue;

    @Column
    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;

    @Column
    private LocalDate executionDate;

    @Column
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contragent;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee owner;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee ownerChanged;

    @ManyToOne
    private Department department;

    @Column
    private boolean generalAccess = false;

    @Column
    private LocalDate dateChange;

    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "bonustrans_files",
            joinColumns = @JoinColumn(name = "bonus_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "file_id", referencedColumnName = "id")
    )
    private List<File> files;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bonusprogram_id", referencedColumnName = "id")
    private BonusProgram bonusProgram;

    public enum TransactionType {
        EARNING, SPENDING
    }

    public enum TransactionStatus {
        WAIT_PROCESSING, COMPLETED, CANCELED
    }

}

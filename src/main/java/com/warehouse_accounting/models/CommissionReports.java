package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static lombok.AccessLevel.PRIVATE;

/*
Блок Продажи:
Отчеты комиссионера
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "commission_reports")
public class CommissionReports extends MovingFields {

    // часть полей наследуется из класса MovingFields
    @Column
    LocalDateTime startOfPeriod;

    @Column
    LocalDateTime endOfPeriod;

    @Column
    BigDecimal sumOfReward;

    @Column
    String docType;

    @Column
    Long number;

    @Column
    BigDecimal accountCompany = BigDecimal.valueOf(0);

    @Column
    BigDecimal accountContractor = BigDecimal.valueOf(0);

    @Column
    BigDecimal sumOfCommittent = BigDecimal.valueOf(0);

    @Column
    BigDecimal remainsToPay = BigDecimal.valueOf(0);

    @Column
    Long incomingNumber;

    @Column
    LocalDateTime incomingDate;

    @Column
    String generalAccess;

    @Column
    String ownerDepartment;

    @Column
    String ownerEmployee;

    @Column
    String status;

    @Column
    LocalDateTime whenChanged;

    @Column
    String whoChanged;
}

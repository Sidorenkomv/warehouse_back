package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "purchases_forecast")
public class PurchaseForecast {
    @Id
    @Column
    private Long id;

    @Column
    private Long reservedDays;

    @Column
    private Long reservedProduct;

    @Column
    @ColumnDefault("false")
    private Boolean ordered;
}

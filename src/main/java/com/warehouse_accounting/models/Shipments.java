package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/*
    Продажи/Отгрузки
    id - Номер
    data - время
    warehouse - со склада
    contractor - контрагент
    consignee - грузополучатель
    company - организация
    sum - сумма
    paid - оплачено
    sent - отправлено
    printed - напечатано
    comment - комментарий

 */


@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "shipments")
public class Shipments extends Document {

    @ManyToOne(fetch = FetchType.LAZY)
    private Company consignee;

    @Column
    @ColumnDefault("0")
    private BigDecimal paid;
}

package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "production_tasks")
public class ProductionTasks {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long taskId;

    @Column
    @EqualsAndHashCode.Exclude
    private LocalDate dateOfCreate;

    @Column
    private String organization;

    @Column
    private LocalDate plannedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse materialWarehouse;

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse productionWarehouse;

    @Column
    private LocalDate dateOfStart;

    @Column
    private LocalDate dateOfEnd;

    @Column
    private boolean isAccessed;

    @ManyToOne(fetch = FetchType.LAZY)
    private Department ownerDepartment;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee ownerEmployee;

    @Column
    private LocalDate dateOfSend;

    @Column
    private LocalDate dateOfPrint;

    @Column
    private String description;

    @Column
    @EqualsAndHashCode.Exclude
    private LocalDate dateOfEdit;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee editEmployee;

    @OneToMany(fetch = FetchType.LAZY)
    private List<AdditionalField> additionalFields;

}

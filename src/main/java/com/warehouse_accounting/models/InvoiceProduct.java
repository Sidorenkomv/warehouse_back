package com.warehouse_accounting.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "invoice_products")
public class InvoiceProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne(fetch = FetchType.LAZY)
//    private Invoice invoice;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(scale = 2)
    private BigDecimal count= BigDecimal.valueOf(0);

    @Column(scale = 2)
    private BigDecimal price= BigDecimal.valueOf(0);

    @Column(scale = 2)
    private BigDecimal sum = count.multiply(price);

    @Column
    private Float nds = 0f;

    @Column
    private Float discount = 0f;

}

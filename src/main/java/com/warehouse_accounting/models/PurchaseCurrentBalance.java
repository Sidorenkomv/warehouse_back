package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "purchases_current_balance")
public class PurchaseCurrentBalance {

    @Id
    @Column
    private Long id;

    @Column
    private Long remainder;

    @Column
    private Long productReserved;

    @Column
    private Long productsAwaiting;

    @Column
    private Long productAvailableForOrder;

    @Column
    private Long daysStoreOnTheWarehouse;
}

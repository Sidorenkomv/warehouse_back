package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "procurement_management")
public class ProcurementManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private Long code;

    @Column
    private Long vendor_code;

    @Column
    private Long number;

    @Column
    private Long sum;

    @Column
    private Long cost_price;

    @Column
    private Long profit;

    @Column
    private Long profitability;

    @Column
    private Long sales_day;

    @Column
    private Long balance;

    @Column
    private Long reserve;

    @Column
    private Long expectation;

    @Column
    private Long available;

    @Column
    private Long days_in_stock;

    @Column
    private Long days_of_stock;

    @Column
    private Long stock;

}

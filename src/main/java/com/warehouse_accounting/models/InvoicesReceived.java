package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/*
    Счета-фактуры полученные
    id - №
    data - Время
    contractor - Контрагент
    company - Организация
    sum - Сумма
    incomingNumber - Входящий номер
    dateIncomingNumber - Входящая дата
    sent - Отправлено
    printed - Напечатано
    comment - Комментарий
 */

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "invoice_received")
public class InvoicesReceived extends Document {

    @Column
    private BigDecimal incomingNumber;

    @Column
    private LocalDateTime dateIncomingNumber;

}

/**
 * Этот клас представляет сущность - Контакное лицо контраагента. По сути
 * это ссылка на контактные лица фирмы Контрагента.
 *
 * ВНИМАНИЕ: Функция добавления контактных лиц, является премиальной и
 * работает либо в пробный период либо после оплаты.
 * Поэтому, при рассширении класса будьте внимательны
 *
 *
 *      * Вот это нужно будет добавить в будующем при развитии приложения
 *      * //    Контактные лица Контрагентов
 *      * //    id - ID в формате UUID Только для чтения
 *      * //
 *      * //    accountId - ID учетной записи Только для чтения
 *      * //
 *      * //    version - Версия сущности. Изменяется при обновлении Контрагента Только для чтения
 *      * //
 *      * //    updated - Момент последнего обновления Контрагента Только для чтения

 */

package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contact_face")
public class ContactFace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String phone;

    @Column
    private String email;

    //externalCode - Внешний код контактного лица
    @Column
    private String externalCode;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Address address;

    @Column
    private String position;

    //    agent - Ссылка на Контрагента (тип Мета)
    @Column
    private String linkContractor;

    @Column
    private String comment;

}
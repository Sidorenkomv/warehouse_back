package com.warehouse_accounting.models;


import lombok.Builder;
import lombok.extern.log4j.Log4j2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;


// Cущность "Правка, вносимая в накладную (Invoice)


@Entity
@Table(name = "invoice_edits")
@Builder
@Log4j2
public class InvoiceEdit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee editAuthor;

    @Column
    private LocalDateTime dateTime;

    @Column
    private String field;

    @Column
    private String before;

    @Column
    private String after;

    public InvoiceEdit(Long id, Employee editAuthor, LocalDateTime dateTime, String field, String before, String after) {
        this.id = id;
        this.editAuthor = editAuthor;
        this.dateTime = dateTime;
        this.field = field;
        this.before = before;
        this.after = after;
    }

    public InvoiceEdit() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        log.info("Счет-ID");
    }

    public Employee getEditAuthor() {
        return editAuthor;
    }

    public void setEditAuthor(Employee editAuthor) {
        this.editAuthor = editAuthor;
        log.info("Изменено");
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
        log.info("Входящая дата");
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
        log.info("Входящий номер");
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }
}

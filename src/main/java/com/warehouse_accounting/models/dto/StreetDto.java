package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StreetDto {

    private Long id;
    private String name;
    private String socr;
    private String code;
    private String index;
    private String gninmb;
    private String uno;
    private String ocatd;

}

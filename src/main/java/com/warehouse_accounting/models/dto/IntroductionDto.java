package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntroductionDto {
    private Long id;
    private String number;
    private LocalDateTime time;
    private Long pointId;
    private Long fromWhomId;
    private Long organizationId;
    private BigDecimal sum;
    private Boolean generalAccess;
    private Long ownerDepartmentId;
    private Long ownerEmployeeId;
    private Boolean sent;
    private Boolean printed;
    private String comment;
    private LocalDateTime dateOfEdit;
    private Long editorEmployeeId;
}

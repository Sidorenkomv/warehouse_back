package com.warehouse_accounting.models.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DiscountDto {

    private Long id;
    private Boolean active;
    private String name;
    private DiscountTypeDto discountType = new DiscountTypeDto();
    private Set<ProductDto> products;
    private Set<ContractorDto> contractors;

    private BigDecimal priceTypeProductCard;
    private BigDecimal fixedDiscount;
    private BigDecimal sumCumulative;
    private BigDecimal discountPercent;
    private BigDecimal accrualPoints;
    private Integer writeOff;
    private Integer  maxPercentPayment;
    private Integer pointsAwardedInDays;

    private Boolean SimultaneousAccrualAndWriteOff;
    private Boolean welcomePoints;
    private Integer welcomePointsAccrual;
    private Boolean firstPurchase;
    private Boolean registrationBonusProgram;

    public DiscountDto(Long id,
                       Boolean active,
                       String name,
                       Long discountType,
                       BigDecimal priceTypeProductCard,
                       BigDecimal fixedDiscount,
                       BigDecimal sumCumulative,
                       BigDecimal discountPercent,
                       BigDecimal accrualPoints,
                       Integer writeOff,
                       Integer  maxPercentPayment,
                       Integer pointsAwardedInDays,
                       Boolean SimultaneousAccrualAndWriteOff,
                       Boolean welcomePoints,
                       Integer welcomePointsAccrual,
                       Boolean firstPurchase,
                       Boolean registrationBonusProgram
    ) {
        this.id = id;
        this.active = active;
        this.name = name;
        this.discountType.setId(discountType);
        this.priceTypeProductCard = priceTypeProductCard;
        this.fixedDiscount = fixedDiscount;
        this.sumCumulative = sumCumulative;
        this.discountPercent = discountPercent;
        this.accrualPoints = accrualPoints;
        this.writeOff = writeOff;
        this.maxPercentPayment = maxPercentPayment;
        this.pointsAwardedInDays = pointsAwardedInDays;
        this.SimultaneousAccrualAndWriteOff = SimultaneousAccrualAndWriteOff;
        this.welcomePoints = welcomePoints;
        this.welcomePointsAccrual = welcomePointsAccrual;
        this.firstPurchase = firstPurchase;
        this.registrationBonusProgram = registrationBonusProgram;
    }
}

package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.BonusTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BonusTransactionDto {

    private Long id;

    private LocalDate created;

    private BonusTransaction.TransactionType transactionType;

    private Long bonusValue;

    private BonusTransaction.TransactionStatus transactionStatus;

    private LocalDate executionDate;

    private BonusProgramDto bonusProgramDto;

    private List<FileDto> filesDto;

    private ContractorDto contractorDto;

    private String comment;

    private LocalDate dateChange;

    private EmployeeDto ownerDto;

    private EmployeeDto ownerChangedDto;

    private DepartmentDto departmentDto;

    private boolean generalAccess;

}


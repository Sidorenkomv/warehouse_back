package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SerialNumberDto {
    private Long id;
    private Long serialNumber;
    private Long code;
    private Long vendor_code;
    private ProductDto product = new ProductDto();
    private WarehouseDto warehouse = new WarehouseDto();
    private String typeOfDoc;
    private Long number;
    private String description;

    public SerialNumberDto(Long id,
                           Long serialNumber,
                           Long code,
                           Long vendor_code,
                           Long productFromId,
                           Long warehouseToId,
                           String typeOfDoc,
                           Long number,
                           String description) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.code = code;
        this.vendor_code = vendor_code;
        this.product.setId(productFromId);
        this.warehouse.setId(warehouseToId);
        this.typeOfDoc = typeOfDoc;
        this.number = number;
        this.description = description;

    }
}

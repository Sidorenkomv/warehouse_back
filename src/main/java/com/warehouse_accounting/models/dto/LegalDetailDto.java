package com.warehouse_accounting.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.models.TypeOfContractor;
import com.warehouse_accounting.util.ConverterDto;
import com.warehouse_accounting.util.DateConvertor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LegalDetailDto {
    private Long id;

    private String fullName;

    private String firstName;

    private String middleName;

    private String lastName;

    private AddressDto address;

    private String inn;

    private String kpp;

    private String okpo;

    private String ogrn;

    private String numberOfTheCertificate;

    @DateTimeFormat(pattern = DateConvertor.datePattern)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateConvertor.datePattern)
    private LocalDate dateOfTheCertificate;

    private TypeOfContractorDto typeOfContractor;

    public LegalDetailDto(
            Long id,
            String fullName,
            String firstName,
            String middleName,
            String lastName,
            Address address,
            String inn,
            String kpp,
            String okpo,
            String ogrn,
            String numberOfTheCertificate,
            LocalDate dateOfTheCertificate,
            TypeOfContractor typeOfContractor
    ) {
        this.id = id;
        this.fullName = fullName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.address = ConverterDto.convertToDto(address);
        this.inn = inn;
        this.kpp = kpp;
        this.okpo = okpo;
        this.ogrn = ogrn;
        this.numberOfTheCertificate = numberOfTheCertificate;
        this.dateOfTheCertificate = dateOfTheCertificate;
        this.typeOfContractor = ConverterDto.convertToDto(typeOfContractor);
    }
}

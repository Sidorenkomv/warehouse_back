package com.warehouse_accounting.models.dto;

/*
 * сущность для получения на форме всех контрагентов
 */

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ContractorGetALLDto {

    private Long id;

    private String name;

    private String code;

    private String status;

    private String  sortNumber;

    private String phone;

    private String fax;

    private String email;

    private AddressDto address;

    private String comment;

    private String numberDiscountCard;

    private Long legalDetailId;

    private String legalDetailFullName;

    private AddressDto legalDetailAddress;

    private String legalDetailInn;

    private String legalDetailKpp;

    private Long legalDetailTypeOfContractorId;

    private String legalDetailTypeOfContractorName;

    private Long contractorGroupId;

    private String contractorGroupName;

    private Long typeOfPriceId;

    private String typeOfPriceName;

    public ContractorGetALLDto(
            Long id,
            String name,
            String code,
            String status,
            String sortNumber,
            String phone,
            String fax,
            String email,
            Address address,
            String comment,
            String numberDiscountCard,
            Long legalDetailId,
            String legalDetailFullName,
            Address legalDetailAddress,
            String legalDetailInn,
            String legalDetailKpp,
            Long legalDetailTypeOfContractorId,
            String legalDetailTypeOfContractorName,
            Long contractorGroupId,
            String contractorGroupName,
            Long typeOfPriceId,
            String typeOfPriceName
    ) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.status = status;
        this.sortNumber = sortNumber;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.address = ConverterDto.convertToDto(address);
        this.comment = comment;
        this.numberDiscountCard = numberDiscountCard;
        this.legalDetailId = legalDetailId;
        this.legalDetailFullName = legalDetailFullName;
        this.legalDetailAddress = ConverterDto.convertToDto(legalDetailAddress);
        this.legalDetailInn = legalDetailInn;
        this.legalDetailKpp = legalDetailKpp;
        this.legalDetailTypeOfContractorId = legalDetailTypeOfContractorId;
        this.legalDetailTypeOfContractorName = legalDetailTypeOfContractorName;
        this.contractorGroupId = contractorGroupId;
        this.contractorGroupName = contractorGroupName;
        this.typeOfPriceId = typeOfPriceId;
        this.typeOfPriceName = typeOfPriceName;
    }
}

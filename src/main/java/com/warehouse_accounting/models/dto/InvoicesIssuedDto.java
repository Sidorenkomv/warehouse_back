package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoicesIssuedDto {

    private Long id;

    private LocalDateTime data;

    private BigDecimal sum;

    private Boolean sent = false;

    private Boolean printed = false;

    private Long companyId;

    private Long contractorId;

    private String comment;

}

package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AcceptancesDto {

    private Long id;
    private String number;
    private LocalDateTime time;

    private Long warehouseId;
    private String warehouseName;

    private Long contractorId;
    private String contractorName;

    private Long companyId;
    private String companyName;

    private BigDecimal sum;
    private BigDecimal paid;
    private BigDecimal noPaid;

    private LocalDateTime dateIncomingNumber;
    private String incomingNumber;

    private Long projectId;
    private String projectName;

    private Long contractId;
    private String contractNumber;

    private BigDecimal overHeadCost;
    private BigDecimal returnSum;

    private Boolean isSharedAccess;

    private Long ownerEmployeeId;
    private String ownerEmployeeName;

    private Long ownerDepartmentId;
    private String ownerDepartmentName;

    private Boolean send;
    private Boolean print;
    private String comment;

    private LocalDateTime whenChanged;

    private Long idWhoChanged;
    private String nameWhoChanget;


    private List<TaskDto> tasksDto = new ArrayList<>();
    private List<FileDto> filesDto = new ArrayList<>();
    private List<ProductDto> productDtos = new ArrayList<>();

    public AcceptancesDto(Long id, String number, LocalDateTime time, Long warehouseId,
                          String warehouseName, Long contractorId, String contractorName,
                          Long companyId, String companyName, BigDecimal sum, BigDecimal paid,
                          BigDecimal noPaid, LocalDateTime dateIncomingNumber, String incomingNumber, Long projectId,
                          String projectName, Long contractId, String contractNumber, BigDecimal overHeadCost,
                          BigDecimal returnSum, Boolean isSharedAccess, Long ownerEmployeeId, String ownerEmployeeName,
                          Long ownerDepartmentId, String ownerDepartmentName, Boolean send, Boolean print,
                          String comment, LocalDateTime whenChanged, Long idWhoChanged, String nameWhoChanget) {
        this.id = id;
        this.number = number;
        this.time = time;
        this.warehouseId = warehouseId;
        this.warehouseName = warehouseName;
        this.contractorId = contractorId;
        this.contractorName = contractorName;
        this.companyId = companyId;
        this.companyName = companyName;
        this.sum = sum;
        this.paid = paid;
        this.noPaid = noPaid;
        this.dateIncomingNumber = dateIncomingNumber;
        this.incomingNumber = incomingNumber;
        this.projectId = projectId;
        this.projectName = projectName;
        this.contractId = contractId;
        this.contractNumber = contractNumber;
        this.overHeadCost = overHeadCost;
        this.returnSum = returnSum;
        this.isSharedAccess = isSharedAccess;
        this.ownerEmployeeId = ownerEmployeeId;
        this.ownerEmployeeName = ownerEmployeeName;
        this.ownerDepartmentId = ownerDepartmentId;
        this.ownerDepartmentName = ownerDepartmentName;
        this.send = send;
        this.print = print;
        this.comment = comment;
        this.whenChanged = whenChanged;
        this.idWhoChanged = idWhoChanged;
        this.nameWhoChanget = nameWhoChanget;
    }
}
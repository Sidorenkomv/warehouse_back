package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceProductDto {

    private Long id;

    private InvoiceDto invoiceDto = new InvoiceDto();

    private ProductDto productDto = new ProductDto();

    private BigDecimal count = BigDecimal.valueOf(0);

    private BigDecimal price = BigDecimal.valueOf(0);

    private BigDecimal sum = count.multiply(price);

    private Float nds = 0f;

    private Float discount = 0f;

    public InvoiceProductDto (Long id,
                              /*Long invoiceDtoId,*/
                              Long productDtoId,
                              BigDecimal count,
                              BigDecimal price,
                              BigDecimal sum,
                              Float nds,
                              Float discount) {
        this.id = id;
//        this.invoiceDto.setId(invoiceDtoId);
        this.productDto.setId(productDtoId);
        this.count = count;
        this.price = price;
        this.sum = sum;
        this.nds = nds;
        this.discount = discount;
    }
}

package com.warehouse_accounting.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.warehouse_accounting.util.DateConvertor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentDto {

    private Long id;

    private String type;

    private String docNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateConvertor.dateTimePattern)
    private LocalDateTime date;

    private BigDecimal sum;

    private Long warehouseFromId;
    private String warehouseFromName;

    private Long warehouseToId;
    private String warehouseToName;

    private Long companyId;
    private String companyName;

    private Long contrAgentId;
    private String contrAgentName;

    private Long projectId;
    private String projectName;

    private Long salesChannelId;
    private String salesChannelName;

    private Long contractId;
    private String contractNumber;

    private Boolean isSharedAccess;

    private Long departmentId;
    private String departmentName;

    private Long employeeId;
    private String employeeFirstname;

    private Boolean sent;

    private Boolean print;

    private String comments;

    private LocalDateTime updatedAt;

    private Long updatedFromEmployeeId;
    private String updatedFromEmployeeFirstname;

    private List<TaskDto> tasks = new ArrayList<>();

    private List<FileDto> files = new ArrayList<>();

    private List<ProductDto> products = new ArrayList<>();

    public DocumentDto(Long id, String type, String docNumber, LocalDateTime date, BigDecimal sum,
                       Long warehouseFromId, String warehouseFromName, Long warehouseToId,
                       String warehouseToName, Long companyId, String companyName, Long contrAgentId,
                       String contrAgentName, Long projectId, String projectName, Long salesChannelId,
                       String salesChannelIdName, Long contractId, String contractNumber,
                       Boolean isSharedAccess, Long departmentId, String departmentName, Long employeeId,
                       String employeeFirstname, Boolean sent, Boolean print, String comment, LocalDateTime updatedAt,
                       Long updatedFromEmployeeId, String updatedFromEmployeeFirstname) {
        this.id = id;
        this.type = type;
        this.docNumber = docNumber;
        this.date = date;
        this.sum = sum;
        this.warehouseFromId = warehouseFromId;
        this.warehouseFromName = warehouseFromName;
        this.warehouseToId = warehouseToId;
        this.warehouseToName = warehouseToName;
        this.companyId = companyId;
        this.companyName = companyName;
        this.contrAgentId = contrAgentId;
        this.contrAgentName = contrAgentName;
        this.projectId = projectId;
        this.projectName = projectName;
        this.salesChannelId = salesChannelId;
        this.salesChannelName = salesChannelIdName;
        this.contractId = contractId;
        this.contractNumber = contractNumber;
        this.isSharedAccess = isSharedAccess;
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.employeeId = employeeId;
        this.employeeFirstname = employeeFirstname;
        this.sent = sent;
        this.print = print;
        this.comments = comment;
        this.updatedAt = updatedAt;
        this.updatedFromEmployeeId = updatedFromEmployeeId;
        this.updatedFromEmployeeFirstname = updatedFromEmployeeFirstname;
    }
}

package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactFaceDto {

    private Long id;

    private String name;

    private String description;

    private String phone;

    private String email;

    //externalCode - Внешний код контактного лица
    private String externalCode;

    private AddressDto address;

    private String position;

    //    agent - Ссылка на Контрагента (тип Мета)
    private String linkContractor;

    private String comment;

    public ContactFaceDto(
            Long id,
            String name,
            String description,
            String phone,
            String email,
            String externalCode,
            Address address,
            String position,
            String linkContractor,
            String comment
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.phone = phone;
        this.email = email;
        this.externalCode = externalCode;
        this.address = ConverterDto.convertToDto(address);
        this.position = position;
        this.linkContractor = linkContractor;
        this.comment = comment;
    }
}

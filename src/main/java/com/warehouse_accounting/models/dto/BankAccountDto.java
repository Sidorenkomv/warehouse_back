package com.warehouse_accounting.models.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.models.Contract;
import com.warehouse_accounting.models.Contractor;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator= ObjectIdGenerators.UUIDGenerator.class, property="@id")
public class BankAccountDto {
    private Long id;

    private String rcbic;

    private String bank;

    private String correspondentAccount;

    private String account;

    private Boolean mainAccount;

    private String sortNumber;

    private String bankAddress;

    private ContractorDto contractor;

    public BankAccountDto(Long id, String rcbic, String bank, String correspondentAccount, String account,
                          Boolean mainAccount, String sortNumber, String bankAddress, Contractor contractor) {
        this.id = id;
        this.rcbic = rcbic;
        this.bank = bank;
        this.correspondentAccount = correspondentAccount;
        this.account = account;
        this.mainAccount = mainAccount;
        this.sortNumber = sortNumber;
        this.bankAddress = bankAddress;
        this.contractor = ConverterDto.convertToDto(contractor);
    }
}

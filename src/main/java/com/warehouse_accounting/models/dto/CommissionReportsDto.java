package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class CommissionReportsDto {
    // в комментариях представлены поля из фронта
    Long id;
    String docType;
    Long number;
    LocalDateTime dateOfCreation;
    CompanyDto companyDto = new CompanyDto();  //String organization фронт
    BigDecimal accountCompany = BigDecimal.valueOf(0);//  Long organizationAccount;
    ContractorDto contractorDto = new ContractorDto(); // String contractor;
    BigDecimal accountContractor = BigDecimal.valueOf(0);// Long contractorAccount;
    BigDecimal sum = BigDecimal.valueOf(0);
    ProjectDto projectDto = new ProjectDto();  // String project;
    ContractDto contractDto = new ContractDto();   // String contract;
    BigDecimal sumOfReward = BigDecimal.valueOf(0);
    BigDecimal sumOfCommittent = BigDecimal.valueOf(0);
    BigDecimal paid = BigDecimal.valueOf(0);
    BigDecimal remainsToPay = BigDecimal.valueOf(0);
    LocalDateTime startOfPeriod; //String startOfPeriod;
    LocalDateTime endOfPeriod;  //String endOfPeriod;
    Long incomingNumber;
    LocalDateTime incomingDate;//  String incomingDate;
    SalesChannelDto salesChannelDto = new SalesChannelDto();//данный класс не доделан String salesChannel;
    String generalAccess; //   заглушка
    String ownerDepartment; // заглушка
    String ownerEmployee; //  заглушка
    String status; //  заглушка
    Boolean isSent; // String sent;
    Boolean isPrinted; //String printed;
    String comment;  // commentary
    LocalDateTime whenChanged;//  String whenChanged;
    String whoChanged; //

    // данный конструктор нужен для репозитория, для передачи объектов по id например (Long contractId){ this.contractDto.setId(contractId);}
    public CommissionReportsDto(Long id,
                                LocalDateTime dateOfCreation,
                                Long contractId,
                                Long contractorId,
                                Long companyId,
                                Long projectId,
                                BigDecimal sum,
                                BigDecimal paid,
                                Boolean isSent,
                                Boolean isPrinted,
                                String comment,
                                LocalDateTime periodStart,
                                LocalDateTime periodEnd,
                                BigDecimal reward,
                                String docType,
                                Long number,
                                Long salesChannelId,
                                BigDecimal accountCompany,
                                BigDecimal accountContractor,
                                BigDecimal sumCommittee,
                                BigDecimal sumRemainsToPay,
                                Long incomingNumber,
                                LocalDateTime incomingDate,
                                String sharedAccess,
                                String ownerDepartment,
                                String ownerEmployee,
                                String status,
                                LocalDateTime whenChanged,
                                String whoChangedIt
    ) {
        this.id = id;
        this.dateOfCreation = dateOfCreation;
        this.contractDto.setId(contractId);
        this.contractorDto.setId(contractorId);
        this.companyDto.setId(companyId);
        this.projectDto.setId(projectId);
        this.sum = sum;
        this.paid = paid;
        this.isSent = isSent;
        this.isPrinted = isPrinted;
        this.comment = comment;
        this.startOfPeriod = periodStart;
        this.endOfPeriod = periodEnd;
        this.sumOfReward = reward;
        this.docType = docType;
        this.number = number;
        this.salesChannelDto.setId(salesChannelId);
        this.accountCompany = accountCompany;
        this.accountContractor = accountContractor;
        this.sumOfCommittent = sumCommittee;
        this.remainsToPay = sumRemainsToPay;
        this.incomingNumber = incomingNumber;
        this.incomingDate = incomingDate;
        this.generalAccess = sharedAccess;
        this.ownerDepartment = ownerDepartment;
        this.ownerEmployee = ownerEmployee;
        this.status = status;
        this.whenChanged = whenChanged;
        this.whoChanged = whoChangedIt;
    }
}

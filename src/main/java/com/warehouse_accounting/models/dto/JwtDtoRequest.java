package com.warehouse_accounting.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtDtoRequest {

    private String username;
    private String password;

    @JsonProperty(value = "isRemember")
    private boolean isRemember;
}

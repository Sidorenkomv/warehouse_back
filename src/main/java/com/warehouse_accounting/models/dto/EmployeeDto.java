package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.IpAddress;
import com.warehouse_accounting.models.IpNetwork;
import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDto {
    private Long id;

    private String lastName;

    private String firstName;

    private String middleName;

    private String sortNumber;

    private String phone;

    private String inn;

    private String description;

    private String email;

    private String password;

    private DepartmentDto department = new DepartmentDto();

    private PositionDto position = new PositionDto();

    private Set<RoleDto> roles = new HashSet<>();

    private ImageDto image = new ImageDto();

    private Set<TariffDto> tariff = new HashSet<>();

    private IpNetworkDto ipNetwork = new IpNetworkDto();

    private Set<IpAddressDto> ipAddress = new HashSet<>();


//    public EmployeeDto(Long id, String lastName, String firstName, String middleName, String sortNumber, String phone,
//                       String inn, String description, String email, String password,
//                       Long departmentId, Long positionId, Long imageId, HashSet<Long> roleIds) {
//        this.id = id;
//        this.lastName = lastName;
//        this.firstName = firstName;
//        this.middleName = middleName;
//        this.sortNumber = sortNumber;
//        this.phone = phone;
//        this.inn = inn;
//        this.description = description;
//        this.email = email;
//        this.password = password;
//        this.department.setId(departmentId);
//        this.position.setId(positionId);
//        this.image.setId(imageId);
//        for (Long rId: roleIds) {
//            RoleDto role = new RoleDto();
//            role.setId(rId);
//            this.roles.add(role);
//        }
//    }

    public EmployeeDto(Long id, String lastName, String firstName, String middleName, String sortNumber, String phone,
                       String inn, String description, String email, String password,
                       Long departmentId, Long positionId, Long imageId, Set<Role> roles, Set<TariffDto> tariff) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.sortNumber = sortNumber;
        this.phone = phone;
        this.inn = inn;
        this.description = description;
        this.email = email;
        this.password = password;
        this.department.setId(departmentId);
        this.position.setId(positionId);
        this.roles = roles.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toSet());
        this.image.setId(imageId);
        this.tariff = tariff;
    }

    public EmployeeDto(Long id, String lastName, String firstName, String middleName, String sortNumber, String phone,
                       String inn, String description, String email, String password,
                       Long departmentId, Long positionId, Long imageId) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.sortNumber = sortNumber;
        this.phone = phone;
        this.inn = inn;
        this.description = description;
        this.email = email;
        this.password = password;
        this.department.setId(departmentId);
        this.position.setId(positionId);
        this.image.setId(imageId);
    }
}

package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
//@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalesChannelDto {
    private Long id;
    private String name;
    private String type;
    private String description;
    private String generalAccessC;
    private String ownerDepartment;
    private String ownerEmployee;
    private String whenChanged;
    private String whoChanged;

    public static class Builder{
        private Long id;
        private String name;
        private String type;
        private String description;
        private String generalAccessC;
        private String ownerDepartment;
        private String ownerEmployee;
        private String whenChanged;
        private String whoChanged;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder generalAccessC(String val) {
            generalAccessC = val;
            return this;
        }

        public Builder ownerDepartment(String val) {
            ownerDepartment = val;
            return this;
        }

        public Builder ownerEmployee(String val) {
            ownerEmployee = val;
            return this;
        }

        public Builder whenChanged(String val) {
            whenChanged = val;
            return this;
        }

        public Builder whoChanged(String val) {
            whoChanged = val;
            return this;
        }

        public SalesChannelDto build() {
            return new SalesChannelDto(this);
        }
    }

    private SalesChannelDto(Builder builder) {
        id = builder.id;
        name = builder.name;
        type = builder.type;
        description = builder.description;
        generalAccessC = builder.generalAccessC;
        ownerDepartment = builder.ownerDepartment;
        ownerEmployee = builder.ownerEmployee;
        whenChanged = builder.whenChanged;
        whoChanged = builder.whoChanged;
    }
}

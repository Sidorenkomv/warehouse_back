package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequisitesDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String organization;

    private AddressDto legalAddress;

    private Integer INN;

    private Integer KPP;

    private Integer BIK;

    private Integer checkingAccount;

    public RequisitesDto(
            Long id,
            String organization,
            Address legalAddress,
            Integer INN,
            Integer KPP,
            Integer BIK,
            Integer checkingAccount
    ) {
        this.id = id;
        this.organization = organization;
        this.legalAddress = ConverterDto.convertToDto(legalAddress);
        this.INN = INN;
        this.KPP = KPP;
        this.BIK = BIK;
        this.checkingAccount = checkingAccount;
    }
}

package com.warehouse_accounting.models.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class ProductDto {

    private Long id;

    private String name;

    private String country;

    private String articul;

    private BigDecimal code;

    private BigDecimal outCode;

    private UnitsOfMeasureDto unitsOfMeasureDto;

    private BigDecimal weight;

    private BigDecimal volume;

    private BigDecimal purchasePrice;

    private Float nds;

    private String description;

    private Boolean archive = false;

    private ContractorDto contractorDto;

    private TaxSystemDto taxSystemDto;

    private List<ImageDto> imagesDto = new ArrayList<>();

    private UnitDto unitDto; // склад

    private ProductGroupDto productGroupDto;

    private TaxSystemDto taxSystem = new TaxSystemDto(); // Штрихкоды товара

    private AttributeOfCalculationObjectDto attributeOfCalculationObjectDto;

    private List<ProductPriceDto> productPricesDto;

    /*
        Данный конструктор используется в интерфейсе ProductRepository
        Инициализация полей содержащих DTO конструкторами без параметров ломает нормальную работу ProductRestController
        это можно было бы вылечить в нем, но я думаю надо работать с DTO объектами.
        В конце концов создать поле объекта конструктором без параметров и просто назначить ему ID не назначая остальных
        полей это странное поведение. Подобная же конструкция встречается и в других DTO - данный вопрос надо решать.
     */

    public ProductDto(Long id,
                      String name,
                      String country,
                      String articul,
                      BigDecimal code,
                      BigDecimal outCode,
                      BigDecimal weight,
                      BigDecimal volume,
                      BigDecimal purchasePrice,
                      Float nds,
                      String description,
                      Boolean archive,
                      Long unitDtoId,
                      Long contractorDtoId,
                      Long taxSystemDtoId,
                      Long productGroupDtoId,
                      Long attributeOfCalculationObjectDtoId,
                      Long unitOfMeasureDtoId) {

        // DTO initialize
        this.unitDto = new UnitDto();
        this.unitsOfMeasureDto = new UnitsOfMeasureDto();
        this.contractorDto = new ContractorDto();
        this.taxSystemDto = new TaxSystemDto();
        this.productGroupDto = new ProductGroupDto();
        this.attributeOfCalculationObjectDto = new AttributeOfCalculationObjectDto();

        this.id = id;
        this.name = name;
        this.country = country;
        this.articul = articul;
        this.code = code;
        this.outCode = outCode;
        this.weight = weight;
        this.volume = volume;
        this.purchasePrice = purchasePrice;
        this.nds = nds;
        this.description = description;
        this.unitDto.setId(unitDtoId); // DTO
        this.unitsOfMeasureDto.setId(unitOfMeasureDtoId);
        this.archive = archive;
        this.contractorDto.setId(contractorDtoId); // DTO
        this.taxSystemDto.setId(taxSystemDtoId); // DTO
        this.productGroupDto.setId(productGroupDtoId); // DTO
        this.attributeOfCalculationObjectDto.setId(attributeOfCalculationObjectDtoId); // DTO
    }
}

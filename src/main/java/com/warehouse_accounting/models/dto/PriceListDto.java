package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PriceListDto {

    private Long id;

    private LocalDateTime dateOfCreation;

    private CompanyDto company = new CompanyDto();

    private boolean moved;

    private boolean printed;

    private String comment;

    public PriceListDto(Long id,
                       LocalDateTime dateOfCreation,
                       Long companyId,
                       boolean moved,
                       boolean printed,
                       String comment) {
        this.id = id;
        this.dateOfCreation = dateOfCreation;
        this.company.setId(companyId);
        this.moved = moved;
        this.printed = printed;
        this.comment = comment;
    }
}

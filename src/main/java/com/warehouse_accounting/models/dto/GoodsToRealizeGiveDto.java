package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Unit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsToRealizeGiveDto {

    Long id;

    Long productDtoId;
    String productDtoName;
    //    int number; //это поле должно быть также в ProductDto но пока оно отсутствует
    //    String article; //это поле должно быть также в ProductDto но пока оно отсутствует
    Long unitId;
    String unitName;

    int giveGoods;
    int quantity;
    int amount;
    int arrive;
    int remains;
}

package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.models.ContractorGroup;
import com.warehouse_accounting.models.LegalDetail;
import com.warehouse_accounting.models.TypeOfPrice;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractorDto {
    private Long id;

    private String name;

    private String status;

    private ContractorGroupDto contractorGroup = new ContractorGroupDto();

    private String code;

    private String outerCode;

    private String sortNumber;

    private String phone;

    private String fax;

    private String email;

    private AddressDto address;

    private String comment;

    private String numberDiscountCard;

    private TypeOfPriceDto typeOfPrice;

    private LegalDetailDto legalDetail;

    private List<TaskDto> taskDtos = new ArrayList<>();

    private List<Long> callIds = new ArrayList<>();

    public ContractorDto(
            Long id,
            String name,
            String status,
            ContractorGroup contractorGroup,
            String code,
            String outerCode,
            String sortNumber,
            String phone,
            String fax,
            String email,
            Address address,
            String comment,
            String numberDiscountCard,
            TypeOfPrice typeOfPrice,
            LegalDetail legalDetail
    ) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.contractorGroup = ConverterDto.convertToDto(contractorGroup);
        this.code = code;
        this.outerCode = outerCode;
        this.sortNumber = sortNumber;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.address = ConverterDto.convertToDto(address);
        this.comment = comment;
        this.numberDiscountCard = numberDiscountCard;
        this.typeOfPrice = ConverterDto.convertToDto(typeOfPrice);
        this.legalDetail = ConverterDto.convertToDto(legalDetail);
    }
}

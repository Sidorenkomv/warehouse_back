package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseDto {
    private Long id;
    private String name;
    private String sortNumber;
    private AddressDto address;
    private String comment;

    public WarehouseDto(
            Long id,
            String name,
            String sortNumber,
            Address address,
            String comment
    ) {
        this.id = id;
        this.name = name;
        this.sortNumber = sortNumber;
        this.address = ConverterDto.convertToDto(address);
        this.comment = comment;
    }
}

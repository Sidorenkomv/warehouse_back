package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProcurementManagementDto {
    private Long id;

    private String name;

    private Long code;

    private Long vendor_code;

    private Long number;

    private Long sum;

    private Long cost_price;

    private Long profit;

    private Long profitability;

    private Long sales_day;

    private Long balance;

    private Long reserve;

    private Long expectation;

    private Long available;

    private Long days_in_stock;

    private Long days_of_stock;

    private Long stock;

}

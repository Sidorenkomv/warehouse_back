package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.interfaces.Property;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdditionalFieldDto {

    private Long id;
    private String name;
    private Property property;
    private Boolean required;
    private Boolean hide;
    private String description;
}

package com.warehouse_accounting.models.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import static lombok.AccessLevel.PRIVATE;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
public class ReturnDto {
    Long id;

    LocalDateTime dataTime;

    WarehouseDto warehouseDto = new WarehouseDto();

    CompanyDto companyDto = new CompanyDto();

    ContractorDto contractorDto = new ContractorDto();

    ContractDto contractDto = new ContractDto();

    ProjectDto projectDto = new ProjectDto();

    List<FileDto> fileDtos = new ArrayList<>();

    List<TaskDto> taskDtos = new ArrayList<>();

    List<ProductDto> productDtos = new ArrayList<>();

    BigDecimal sum = BigDecimal.valueOf(0);

    Boolean isSent;

    Boolean isPrinted;

    String comment;

    public ReturnDto(Long id,
                     LocalDateTime dataTime,
                     Long warehouseId,
                     Long companyId,
                     Long contractorId,
                     Long projectId,
                     BigDecimal sum,
                     Boolean isSent,
                     Boolean isPrinted,
                     String comment) {
        this.id = id;
        this.dataTime = dataTime;
        this.warehouseDto.setId(warehouseId);
        this.companyDto.setId(companyId);
        this.contractorDto.setId(contractorId);
        this.projectDto.setId(projectId);
        this.sum = sum;
        this.isSent = isSent;
        this.isPrinted = isPrinted;
        this.comment = comment;
    }

}
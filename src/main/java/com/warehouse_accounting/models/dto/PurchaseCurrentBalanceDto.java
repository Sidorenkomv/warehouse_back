package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PurchaseCurrentBalanceDto {

    private Long id;

    private Long remainder;

    private Long productReserved;

    private Long productsAwaiting;

    private Long productAvailableForOrder;

    private Long daysStoreOnTheWarehouse;
}

package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Currency;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyCourseDto implements Serializable {
    @XmlElement(name = "Valute")
    private List<Currency> valute;

    public List<Currency> getValute() {
        return valute;
    }

}

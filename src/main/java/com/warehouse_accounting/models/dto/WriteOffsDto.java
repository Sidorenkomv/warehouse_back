package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WriteOffsDto {

    private Long id;

    private LocalDateTime dateOfCreation;

    private WarehouseDto warehouseFrom = new WarehouseDto();

    private WarehouseDto warehouseTo = new WarehouseDto();

    private CompanyDto company = new CompanyDto();

    private BigDecimal sum = BigDecimal.valueOf(0);

    private String status;

    private String warehouseName;

    private boolean moved;

    private boolean printed;

    private String comment;

    public WriteOffsDto(Long id,
                        LocalDateTime dateOfCreation,
                        Long warehouseFromId,
                        Long warehouseToId,
                        Long companyId,
                        BigDecimal sum,
                        String status,
                        String warehouseName,
                        boolean moved,
                        boolean printed,
                        String comment) {
        this.id = id;
        this.dateOfCreation = dateOfCreation;
        this.warehouseFrom.setId(warehouseFromId);
        this.warehouseTo.setId(warehouseToId);
        this.company.setId(companyId);
        this.sum = sum;
        this.status = status;
        this.warehouseName = warehouseName;
        this.moved = moved;
        this.printed = printed;
        this.comment = comment;
    }

}
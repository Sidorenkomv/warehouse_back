package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Unit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsToRealizeGetDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Long productDtoId;
    String productDtoName;
    //    int number; //это поле должно быть также в ProductDto но пока оно отсутствует
    //    String article; //это поле должно быть также в ProductDto но пока оно отсутствует
    Long unitId;
    String unitName;

    int getGoods;
    int quantity;
    int amount;
    int arrive;
    int remains;
    int quantity_report;
    int amount_report;
    int quantity_Noreport;
    int amount_Noreport;
}

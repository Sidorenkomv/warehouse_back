package com.warehouse_accounting.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SupplierOrdersDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number;

    private LocalDateTime createdAt;

    private Long contrAgentId;
    private String contrAgentName;

    private Long contrAgentAccount;

    private Long companyId;
    private String companyName;

    private Long companyAccount;

    private BigDecimal sum;

    private Long invoiceIssued;

    private Long paid;

    private Long notPaid;

    private Long accepted;

    private Long waiting;

    private Long refundAmount;

    private LocalDateTime acceptanceDate;

    private Long projectId;
    private String projectName;

    private Long warehouseId;
    private String warehouseName;

    private Long contractId;
    private String contractNumber;

    private Boolean generalAccess;

    private Long departmentId;
    private String departmentName;

    private Long employeeId;
    private String employeeFirstname;

    private Boolean sent;

    private Boolean print;

    private String comment;

    private LocalDateTime updatedAt;

    private Long updatedFromEmployeeId;
    private String updatedFromEmployeeFirstname;
}

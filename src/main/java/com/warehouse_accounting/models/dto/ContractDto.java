package com.warehouse_accounting.models.dto;

import com.warehouse_accounting.models.Company;
import com.warehouse_accounting.models.Contractor;
import com.warehouse_accounting.util.ConverterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContractDto {

    private Long id;

    private String number;

    private LocalDate contractDate;

    private CompanyDto company;

//    private BankAccountDto bankAccountDto = new BankAccountDto();

    private ContractorDto contractor;

    private BigDecimal amount;

//    private BigDecimal paid;
//
//    private BigDecimal done;
//
//    private Boolean isSharedAccess;

//    private DepartmentDto ownerDepartment = new DepartmentDto();
//
//    private EmployeeDto ownerEmployee = new EmployeeDto();
//
//    private Boolean isSend;
//
//    private Boolean isPrint;

    private Boolean archive;

    private String comment;

//    private LocalDate whenChange;
//
//    private EmployeeDto whoChange = new EmployeeDto();
//
//    private LegalDetailDto legalDetail;

    private String code;

    private String typeOfContract;

    private String reward;

    private Integer percentageOfTheSaleAmount;

    private Boolean enabled;

    public ContractDto(
            Long id,
            String number,
            String code,
            LocalDate contractDate,
            BigDecimal amount,
//            BigDecimal paid,
//            BigDecimal done,
//            Boolean isSharedAccess,
//            Boolean isSend,
//            Boolean isPrint,
            Boolean archive,
            String comment,
//            LocalDate whenChange,
            Company company,
//            Long bankAccountDtoId,
            Contractor contractor,
//            Long ownerDepartmentId,
//            Long ownerEmployeeId,
//            Long whoChangeId,
//            LegalDetail legalDetail,
            String typeOfContract,
            String reward,
            Integer percentageOfTheSaleAmount,
            Boolean enabled) {

        this.id = id;
        this.number = number;
        this.code = code;
        this.contractDate = contractDate;
        this.amount = amount;
//        this.paid = paid;
//        this.done = done;
//        this.isSharedAccess = isSharedAccess;
//        this.isSend = isSend;
//        this.isPrint = isPrint;
        this.archive = archive;
//        this.whenChange = whenChange;
        this.comment = comment;
        this.company = ConverterDto.convertToDto(company);
//        this.bankAccountDto.setId(bankAccountDtoId);
        this.contractor = ConverterDto.convertToDto(contractor);
//        this.ownerDepartment.setId(ownerDepartmentId);
//        this.ownerEmployee.setId(ownerEmployeeId);
//        this.whoChange.setId(whoChangeId);
//        this.legalDetail = ConverterDto.convertToDto(legalDetail);
        this.typeOfContract = typeOfContract;
        this.reward = reward;
        this.percentageOfTheSaleAmount = percentageOfTheSaleAmount;
        this.enabled = enabled;
    }
}
package com.warehouse_accounting.models.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PurchasesManagementDto {

    private Long id;

    private Long productId;

    private Long purchasesHistoryOfSalesId;

    private Long purchasesCurrentBalanceId;

    private Long purchasesForecastId;

}
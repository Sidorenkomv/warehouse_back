package com.warehouse_accounting.models.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class ProductPriceForPriceListDto {

    Long id;

    TypeOfPriceDto typeOfPriceDto;

    BigDecimal price;
}

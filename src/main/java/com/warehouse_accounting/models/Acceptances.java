package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "acceptances")
public class Acceptances extends Document {

    @Column
    private BigDecimal paid;

    @Column
    private BigDecimal noPaid;

    @Column
    private LocalDateTime dateIncomingNumber;

    @Column
    private String incomingNumber;

    @Column
    private BigDecimal overHeadCost;

    @Column
    private BigDecimal returnSum;

}

@AnyMetaDef( name= "PropertyMetaDef", metaType = "string", idType = "long",
        metaValues = {
                @MetaValue(value = "S", targetEntity = StringProperty.class),
                @MetaValue(value = "I", targetEntity = IntegerProperty.class)
        }
)

package com.warehouse_accounting.models;

import com.warehouse_accounting.models.property.IntegerProperty;
import com.warehouse_accounting.models.property.StringProperty;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
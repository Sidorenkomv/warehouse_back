package com.warehouse_accounting.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/** @deprecated
 * см. BonusProgram
 *
 *
 */
@Deprecated
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "discount")
public class Discount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Boolean active;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private DiscountType discountType;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Product> products;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Contractor> contractors;

    //Специальная цена
    @Column(scale = 2)
    private BigDecimal priceTypeProductCard;

    @Column(scale = 2)
    private BigDecimal fixedDiscount;

    //Накопительная скидка
    @Column(scale = 2)
    private BigDecimal sumCumulative;

    @Column(scale = 2)
    private BigDecimal discountPercent;

    //Бонусная программа
    @Column(scale = 2)
    private BigDecimal accrualPoints;

    @Column
    private Integer writeOff;

    @Column
    private Integer  maxPercentPayment;

    @Column
    private Integer pointsAwardedInDays;

    @Column
    private Boolean SimultaneousAccrualAndWriteOff;

    @Column
    private Boolean welcomePoints;

    @Column
    private Integer welcomePointsAccrual;

    @Column
    private Boolean firstPurchase;

    @Column
    private Boolean registrationBonusProgram;
}

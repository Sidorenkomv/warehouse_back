package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contractors")
public class Contractor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String code;

    @Column
    private String outerCode;

    @Column
    private String status;

    @Column
    private String sortNumber;

    @Column
    private String phone;

    @Column
    private String fax;

    @Column
    private String email;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Address address;

    @Column
    private String comment;

    @Column
    private String numberDiscountCard;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private ContractorGroup contractorGroup;

    @ManyToOne(fetch = FetchType.LAZY, cascade =CascadeType.MERGE)
    private TypeOfPrice typeOfPrice;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private LegalDetail legalDetail;

    @OneToMany(mappedBy = "contractor", fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Task> tasks;

    @OneToMany(mappedBy = "contractor", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Memo> memoList;

    @OneToMany(mappedBy = "contractor", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Call> calls;

}

package com.warehouse_accounting.models;

import com.warehouse_accounting.models.interfaces.Property;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "additional_fields")
public class AdditionalField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Any(
            metaDef = "PropertyMetaDef",
            metaColumn = @Column( name = "property_type" )
    )
    @JoinColumn( name = "property_id" )
    @Cascade(CascadeType.ALL)
    private Property property;

    @Column
    private boolean required;

    @Column
    private boolean hide;

    @Column
    private String description;

}

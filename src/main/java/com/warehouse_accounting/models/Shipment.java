package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/*
Модель отгрузки доп поля
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@FieldDefaults(level = PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "shipments")
public class Shipment extends MovingFields {

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    Warehouse warehouse;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    List<Product> products;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    Contractor consignee;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    Contractor carrier;

    @Column
    Boolean isPaid;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    Address deliveryAddress;

}

package com.warehouse_accounting.models;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity (name = "retail_shift")
public class RetailShift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @EqualsAndHashCode.Exclude
    private LocalDate dateOfOpen; //Дата открытия

    @Column
    @EqualsAndHashCode.Exclude
    private LocalDate dateOfClose; //Дата закрытия

    @ManyToOne(fetch = FetchType.LAZY)
    private PointOfSales pointOfSales; //Точка продаж

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouse; //Склад

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company; //Организация

    @ManyToOne(fetch = FetchType.LAZY)
     private BankAccount bank; //Банк-эквайер

    @Column
    private BigDecimal  cashlessShiftRevenue; //Безналичная выручка за смену

    @Column
    private BigDecimal  cashShiftRevenue; //Наличная выручка за смену

    @Column
    private BigDecimal  shiftRevenue; //Выручка за смену

    @Column
    private BigDecimal recevied; //Поступило

    @Column
    private BigDecimal sale; //Скидка

    @Column
    private BigDecimal comission; //Комиссия

    @Column
    private boolean isAccessed; //Общий доступ

    @ManyToOne(fetch = FetchType.LAZY)
    private Department ownerDepartment; //Владелец-отдел

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee ownerEmployee; //Владелец-сотрудник

    @Column
    private boolean isSent; //Отправлено

    @Column
    private boolean isPrinted; //Напечатано

    @Column
    private String description; //Комментарий

    @Column
    @EqualsAndHashCode.Exclude
    private LocalDate dateOfEdit; //Дата и время изменений

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee editEmployee; //Автор изменений
}

package com.warehouse_accounting.models;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "documents")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String type;                        //Тип документа

    @Column
    private String docNumber;                        //№

    @Column
    private LocalDateTime date;                 //Время создания

    @Column
    private BigDecimal sum;                           //Сумма

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouseFrom;            //Со склада

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouseTo;              //На склад

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;                    //Организация

    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contrAgent;              //Контрагент

    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;                    //Проект

    @ManyToOne(fetch = FetchType.LAZY)
    private SalesChannel salesChannel;          //Склад

    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;                  //Договор

    @Column
    private Boolean isSharedAccess;             //Общий доступ

    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;              //Владелец-отдел

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee;                  //Владелец-сотрудник

    @Column
    @ColumnDefault("false")
    private Boolean sent;                       //Отправлено

    @Column
    @ColumnDefault("false")
    private Boolean print;                      //Напечатано

    @Column
    private String comments;                     //Комментарий

    @Column
    private LocalDateTime updatedAt;            //Когда изменен

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee updatedFromEmployee;       //Кем изменен

    @OneToMany(mappedBy = "document", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Task> tasks = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<File> files = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Product> products = new ArrayList<>();
}

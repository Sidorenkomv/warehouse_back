package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/*
    Счета-фактуры выданные
    id - №
    data - Время
    sum - Сумма
    sent - Отправлено
    printed - Напечатано
    company - Организация
    contractor - Контрагент
    comment - Комменнтарий
 */


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "invoice_issued")
public class InvoicesIssued {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime data;

    @Column
    @ColumnDefault("0")
    private BigDecimal sum;

    @Column
    @ColumnDefault("false")
    private Boolean sent;

    @Column
    @ColumnDefault("false")
    private Boolean printed;

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @Column
    private String comment;

}

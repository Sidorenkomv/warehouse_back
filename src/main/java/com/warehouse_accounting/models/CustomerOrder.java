package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/*
Заказы покупателей
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@FieldDefaults(level = PRIVATE)
@Entity
@Table(name = "customer_orders")
public class CustomerOrder extends Document {

/*    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    List<Product> products;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    List<Task> tasks;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    List<File> files;
*/
    @Column
    Boolean isPaid;

    @OneToOne(fetch = FetchType.LAZY)
    Address deliveryAddress;

    @Column
    LocalDate plannedShipmentDate;

    @Column
    Boolean isPosted;
}

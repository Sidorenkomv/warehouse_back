package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "pointOfSales")
public class PointOfSales {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Boolean checkboxEnabled;

    @Column
    private String name;

    @Column
    private String activity;

    @Column
    private String type;

    @Column
    private BigDecimal revenue;

    @Column
    private BigDecimal cheque;

    @Column
    private BigDecimal averageСheck;

    @Column
    private BigDecimal moneyInTheCashRegister;


    @Column
    private String cashiers;


    @Column
    private String synchronization;

    @Column
    private String FN;

    @Column
    private String validityPeriodFN;

}



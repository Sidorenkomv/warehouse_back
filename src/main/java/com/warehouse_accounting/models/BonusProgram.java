package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Бонусная программа. Ее можно найти в менюшке юзера справа Настройки/Скидки
 */


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bonusprogram")
public class BonusProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "accrual_rule")
    private int accrualRule;
    @Column(name = "write_rule")
    private int writeOffRule;
    @Column(name = "max_percentage")
    private int maxPercentage;
    @Column(name = "bonus_delay")
    //"Баллы начисляются через 5 дней"
    private int bonusDelay;
    @Column(name = "award")
    //"Начисляется 500 баллов"
    private int award;


}

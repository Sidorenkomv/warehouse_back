package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_groups")
public class ProductGroup {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String sortNumber;

    @Column
    private Long parentId;

    @OneToMany(mappedBy = "productGroup", cascade = CascadeType.PERSIST)
    private List<Product> products;

    @PreRemove
    private void preRemove() {
        products.forEach(product-> product.setProductGroup(null));
    }

}

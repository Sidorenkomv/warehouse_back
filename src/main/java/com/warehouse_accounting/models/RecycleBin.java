package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "recycleBin")
public class RecycleBin {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String documentType;

    @Column
    private String number;

    @Column
    private LocalDate date;

    @Column
    private BigDecimal sum;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Warehouse warehouse; //   склад

    @Column
    private String warehouseName;

    @Column
    private String warehouseFrom;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Company company; // организация

    @Column
    private String companyName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private ContractorGroup contractor; // Контрагент

    @Column
    private String contractorName;

    @Column
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Project project;

    @Column
    private String projectName;

    @Column
    private String shipped;

    @Column
    private String printed;

    @Column
    private String comment;



}
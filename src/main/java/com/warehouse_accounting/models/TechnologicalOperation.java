package com.warehouse_accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * This class is model is on the "Мой Склад" in the tab "Производсство".
 * This model is responsible for the "Тех. операции"
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "technological_operations")
public class TechnologicalOperation extends Document {

    @Column
    private boolean isArchive;

    @ManyToOne(fetch = FetchType.LAZY)
    private TechnologicalMap technologicalMap;

    @Column
    private BigDecimal volumeOfProduction;

    @Transient
    @OneToMany(fetch = FetchType.LAZY)
    private List<Task> taskList;

}

package com.warehouse_accounting.models;

import com.warehouse_accounting.models.dto.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "GoodsToRealizeGet")
@FieldDefaults(level = PRIVATE)
public class GoodsToRealizeGet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne (fetch = FetchType.LAZY)
    Product product;

    @ManyToOne(fetch = FetchType.LAZY) //Должна быть связь с контрактом "КОМИССИИ" но его пока нет
    Contract contract;

    @ManyToOne(fetch = FetchType.LAZY) //Должна быть связь с Закупки/Приемки, но его пока нет
    Supply supply;

    @ManyToOne(fetch = FetchType.LAZY) //Должна быть связь с Закупки/Приемки, но его пока нет
    Unit unit;

    @Column
    int getGoods;

    @Column
    int quantity;

    @Column
    int amount;

    @Column
    int arrive;

    @Column
    int remains;

    @Column
    int quantity_report;

    @Column
    int amount_report;

    @Column
    int quantity_Noreport;

    @Column
    int amount_Noreport;


}

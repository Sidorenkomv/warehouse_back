package com.warehouse_accounting.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;

@Component
public class JwtTokenUtil {

    @Value("@{security.jwt.token.secret-key}")
    private String secret;


    private EmployeeRepository employeeRepository;

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    //Алгоритм шифрования
    public Algorithm getAlgorithm() {
        return Algorithm.HMAC256(secret.getBytes(StandardCharsets.UTF_8));
    }

    public String createAccessToken(Employee employee) {
        Algorithm algorithm = getAlgorithm();
        return JWT.create()
                .withSubject(employee.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)) // Время действия токена (1 день)
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withClaim("role", 1) // Исправить
                .sign(algorithm);
    }

    public String createRefreshToken(Employee employee) {
        Algorithm algorithm = getAlgorithm();
        return JWT.create()
                .withSubject(employee.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 7 * 24 * 60 * 60 * 1000)) // Время действия токена (неделя)
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withClaim("role", 1) // Исправить
                .sign(algorithm);
    }

    public DecodedJWT checkToken(String token) {

        Algorithm algorithm = getAlgorithm();
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        return decodedJWT;
    }

    public UsernamePasswordAuthenticationToken getAuthenticationTokenByDecodedJwtToken(DecodedJWT decodedJWT) {

        String username = decodedJWT.getSubject();
        Optional<Employee> employee = employeeRepository.findByEmail(username);

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(employee.get(), null, employee.get().getAuthorities());
        return authenticationToken;
    }
}

package com.warehouse_accounting.security.jwt;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

public class JwtFilter extends OncePerRequestFilter {

    private JwtTokenUtil jwtTokenUtil;

    public JwtFilter (JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        final String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION);

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String token = authorizationHeader.substring("Bearer ".length());
                DecodedJWT decodedJWT = jwtTokenUtil.checkToken(token);
                UsernamePasswordAuthenticationToken authenticationToken = jwtTokenUtil.getAuthenticationTokenByDecodedJwtToken(decodedJWT);
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                UserDetails userPrincipal = (UserDetails) authenticationToken.getPrincipal();
                if (!userPrincipal.isEnabled()) {
                    throw new DisabledException("User is disabled");
                }
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } catch (JWTDecodeException exception) {
                httpServletResponse.setHeader("error", exception.getMessage());
                httpServletResponse.setStatus(403);
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                httpServletResponse.setContentType("application/json");
                new ObjectMapper().writeValue(httpServletResponse.getOutputStream(), error);
            } catch (DisabledException exception) {
                httpServletResponse.setHeader("error", exception.getMessage());
                httpServletResponse.setStatus(401);
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                httpServletResponse.setContentType("application/json");
                new ObjectMapper().writeValue(httpServletResponse.getOutputStream(), error);
            }
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.CustomerOrder;
import com.warehouse_accounting.models.dto.CustomerOrderDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.CustomerOrderDto(" +
            "customer_order.id, " +
            "customer_order.type, " +
            "customer_order.docNumber, " +
            "customer_order.date, " +
            "customer_order.sum, " +
            "wrFrom.id, " +
            "wrFrom.name, " +
            "comp.id, " +
            "comp.name, " +
            "con.id, " +
            "con.name, " +
            "pr.id, " +
            "pr.name, " +
            "salCh.id, " +
            "salCh.name, " +
            "contr.id, " +
            "contr.number, " +
            "customer_order.isSharedAccess, " +
//            "dep.id, " +
//            "dep.name, " +
//            "emp.id, " +
//            "emp.firstName, " +
            "customer_order.sent, " +
            "customer_order.print, " +
            "customer_order.comments, " +
            "customer_order.updatedAt, " +
//            "empUpd.id," +
//            "empUpd.firstName," +
            "customer_order.isPaid," +
            "deladr.id," +
            "deladr.fullAddress," +
            "customer_order.plannedShipmentDate," +
            "customer_order.isPosted)" +
            "FROM CustomerOrder customer_order " +
            "LEFT JOIN Contractor con ON (customer_order.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (customer_order.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (customer_order.project.id = pr.id) " +
            "LEFT JOIN SalesChannel salCh ON (customer_order.salesChannel.id = salCh.id) " +
            "LEFT JOIN Warehouse wrFrom ON (customer_order.warehouseFrom.id = wrFrom.id) " +
            "LEFT JOIN Warehouse wrTo ON (customer_order.warehouseTo.id = wrTo.id) " +
            "LEFT JOIN Contract contr ON (customer_order.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (customer_order.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (customer_order.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (customer_order.updatedFromEmployee.id = empUpd.id)" +
            "LEFT JOIN Address deladr ON (customer_order.deliveryAddress.id = deladr.id)")
    List<CustomerOrderDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.CustomerOrderDto(" +
            "customer_order.id, " +
            "customer_order.type, " +
            "customer_order.docNumber, " +
            "customer_order.date, " +
            "customer_order.sum, " +
            "wrFrom.id, " +
            "wrFrom.name, " +
            "comp.id, " +
            "comp.name, " +
            "con.id, " +
            "con.name, " +
            "pr.id, " +
            "pr.name, " +
            "salCh.id, " +
            "salCh.name, " +
            "contr.id, " +
            "contr.number, " +
            "customer_order.isSharedAccess, " +
//            "dep.id, " +
//            "dep.name, " +
//            "emp.id, " +
//            "emp.firstName, " +
            "customer_order.sent, " +
            "customer_order.print, " +
            "customer_order.comments, " +
            "customer_order.updatedAt, " +
//            "empUpd.id," +
//            "empUpd.firstName," +
            "customer_order.isPaid," +
            "deladr.id," +
            "deladr.fullAddress," +
            "customer_order.plannedShipmentDate," +
            "customer_order.isPosted)" +
            "FROM CustomerOrder customer_order " +
            "LEFT JOIN Contractor con ON (customer_order.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (customer_order.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (customer_order.project.id = pr.id) " +
            "LEFT JOIN SalesChannel salCh ON (customer_order.salesChannel.id = salCh.id) " +
            "LEFT JOIN Warehouse wrFrom ON (customer_order.warehouseFrom.id = wrFrom.id) " +
            "LEFT JOIN Warehouse wrTo ON (customer_order.warehouseTo.id = wrTo.id) " +
            "LEFT JOIN Contract contr ON (customer_order.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (customer_order.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (customer_order.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (customer_order.updatedFromEmployee.id = empUpd.id)" +
            "LEFT JOIN Address deladr ON (customer_order.deliveryAddress.id = deladr.id)" +
            "WHERE customer_order.id = :id")
    CustomerOrderDto getById(@Param("id") Long id);
}


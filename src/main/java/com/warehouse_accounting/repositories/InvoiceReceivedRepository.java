package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.InvoicesReceived;
import com.warehouse_accounting.models.dto.InvoiceReceivedDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceReceivedRepository extends JpaRepository<InvoicesReceived, Long> {
    @Query("SELECT new com.warehouse_accounting.models.dto.InvoiceReceivedDto(" +
            "i.id," +
            "i.date," +
            "i.contrAgent.id," +
            "i.company.id," +
            "i.sum," +
            "i.incomingNumber," +
            "i.dateIncomingNumber," +
            "i.sent," +
            "i.print," +
            "i.comments" +
            ")" +
            "FROM InvoicesReceived i")
    List<InvoiceReceivedDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.InvoiceReceivedDto(" +
            "i.id," +
            "i.date," +
            "i.contrAgent.id," +
            "i.company.id," +
            "i.sum," +
            "i.incomingNumber," +
            "i.dateIncomingNumber," +
            "i.sent," +
            "i.print," +
            "i.comments" +
            ")" +
            "FROM  InvoicesReceived  i WHERE i.id=:id")
    InvoiceReceivedDto getById(@Param("id") Long id);

}

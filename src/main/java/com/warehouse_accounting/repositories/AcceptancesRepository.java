package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Acceptances;
import com.warehouse_accounting.models.dto.AcceptancesDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcceptancesRepository extends JpaRepository<Acceptances, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.AcceptancesDto(" +
            "acc.id," +
            "acc.docNumber," +
            "acc.date," +
            "acc.warehouseTo.id," +
            "acc.warehouseTo.name," +
            "acc.contrAgent.id," +
            "acc.contrAgent.name," +
            "acc.company.id," +
            "acc.company.name," +
            "acc.sum," +
            "acc.paid," +
            "acc.noPaid," +
            "acc.dateIncomingNumber," +
            "acc.incomingNumber," +
            "acc.project.id," +
            "acc.project.name," +
            "acc.contract.id," +
            "acc.contract.number," +
            "acc.overHeadCost," +
            "acc.returnSum," +
            "acc.isSharedAccess," +
            "acc.employee.id," +
            "acc.employee.firstName," +
            "acc.department.id," +
            "acc.department.name," +
            "acc.sent," +
            "acc.print," +
            "acc.comments," +
            "acc.updatedAt," +
            "acc.updatedFromEmployee.id," +
            "acc.updatedFromEmployee.firstName) " +
            "FROM Acceptances acc " +
            "WHERE acc.id = :id")
    AcceptancesDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.AcceptancesDto(" +
            "acc.id," +
            "acc.docNumber," +
            "acc.date," +
            "acc.warehouseTo.id," +
            "acc.warehouseTo.name," +
            "acc.contrAgent.id," +
            "acc.contrAgent.name," +
            "acc.company.id," +
            "acc.company.name," +
            "acc.sum," +
            "acc.paid," +
            "acc.noPaid," +
            "acc.dateIncomingNumber," +
            "acc.incomingNumber," +
            "acc.project.id," +
            "acc.project.name," +
            "acc.contract.id," +
            "acc.contract.number," +
            "acc.overHeadCost," +
            "acc.returnSum," +
            "acc.isSharedAccess," +
            "acc.employee.id," +
            "acc.employee.firstName," +
            "acc.department.id," +
            "acc.department.name," +
            "acc.sent," +
            "acc.print," +
            "acc.comments," +
            "acc.updatedAt," +
            "acc.updatedFromEmployee.id," +
            "acc.updatedFromEmployee.firstName) " +
            "FROM Acceptances acc ")
    List<AcceptancesDto> getAll();
}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.RetailShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RetailShiftRepository extends JpaRepository<RetailShift, Long>{
}

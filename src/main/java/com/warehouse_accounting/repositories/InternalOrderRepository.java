package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.InternalOrder;
import com.warehouse_accounting.models.dto.InternalOrderDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InternalOrderRepository extends JpaRepository<InternalOrder, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.InternalOrderDto(" +
            "internal.id," +
            "internal.docNumber," +
            "internal.date," +
            "company.id," +
            "internal.sum," +
            "internal.shipped," +
            "internal.project.id," +
            "internal.isSharedAccess," +
            "warehouse.id," +
            "department.id," +
            "employee.id," +
            "internal.sent," +
            "internal.print," +
            "internal.comments," +
            "updatedFromEmployee.id," +
            "internal.updatedAt)" +
            "FROM InternalOrder internal " +
            "left join Company company on (internal.company.id = company.id)" +
            "left join Project project on (internal.project.id = project.id)" +
            "left join Warehouse warehouse on (internal.warehouseFrom.id = warehouse.id)" +
            "left join Department department on (internal.department.id = department.id)" +
            "left join Employee employee on (internal.employee.id = employee.id)" +
            "left join Employee updatedFromEmployee on (internal.employee.id = updatedFromEmployee.id) " +
            "WHERE internal.id = :id")
    InternalOrderDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.InternalOrderDto(" +
            "internal.id," +
            "internal.docNumber," +
            "internal.date," +
            "company.id," +
            "internal.sum," +
            "internal.shipped," +
            "project.id," +
            "internal.isSharedAccess," +
            "warehouse.id," +
            "department.id," +
            "employee.id," +
            "internal.sent," +
            "internal.print," +
            "internal.comments," +
            "updatedFromEmployee.id," +
            "internal.updatedAt)" +
            "FROM InternalOrder internal " +
            "left join Company company on (internal.company.id = company.id)" +
            "left join Project project on (internal.project.id = project.id)" +
            "left join Warehouse warehouse on (internal.warehouseFrom.id = warehouse.id)" +
            "left join Department department on (internal.department.id = department.id)" +
            "left join Employee employee on (internal.employee.id = employee.id)" +
            "left join Employee updatedFromEmployee on (internal.employee.id = updatedFromEmployee.id)")
    List<InternalOrderDto> getAll();
}

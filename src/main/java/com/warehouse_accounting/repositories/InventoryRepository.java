package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Inventory;
import com.warehouse_accounting.models.dto.InventoryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.InventoryDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseFrom.id," +
            "wo.company.id," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM Inventory wo")
    List<InventoryDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.InventoryDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseFrom.id," +
            "wo.company.id," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM Inventory wo WHERE wo.id=:id")
    InventoryDto getById(@Param("id") Long id);

}
package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.property.StringProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StringPropertyRepository extends JpaRepository<StringProperty, Long> {

}


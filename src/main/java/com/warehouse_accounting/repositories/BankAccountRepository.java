package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.BankAccount;
import com.warehouse_accounting.models.dto.BankAccountDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.BankAccountDto(" +
            "ba.id," +
            "ba.rcbic," +
            "ba.bank," +
            "ba.correspondentAccount," +
            "ba.account," +
            "ba.mainAccount," +
            "ba.sortNumber," +
            "ba.bankAddress," +
            "cntr" +
            ") " +
            "FROM BankAccount ba " +
            "LEFT JOIN ba.contractor cntr ")
    List<BankAccountDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.BankAccountDto(" +
            "ba.id," +
            "ba.rcbic," +
            "ba.bank," +
            "ba.correspondentAccount," +
            "ba.account," +
            "ba.mainAccount," +
            "ba.sortNumber," +
            "ba.bankAddress," +
            "cntr" +
            ") " +
            "FROM BankAccount ba " +
            "LEFT JOIN Contractor cntr ON (ba.contractor.id = cntr.id) "+
            "WHERE ba.id = :id")
    BankAccountDto getById(@Param("id") Long id);

//    @Query("SELECT c.bankAccounts FROM Contractor c WHERE c.id = :id")
//    List<BankAccount> getListById(@Param("id") Long id);
}
package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ProductPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {

    @Query("SELECT p.productPrices FROM Product p WHERE p.id = :id")
    List<ProductPrice> getListProductPriceById(@Param("id") Long id);

    List<ProductPrice> findAllByProductId(Long id);
}

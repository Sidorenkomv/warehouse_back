package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.dto.RoleDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role getById(Long id);

//    @Query("select em.role from Employee em where em.id = :id")
//    Role getRolesByEmployeeId(@Param("id") Long id);

    @Query("select em.roles from Employee em where em.id = :id")
    Set<Role> getRolesByEmployeeId(@Param("id") Long id);
}

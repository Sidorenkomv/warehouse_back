package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    @Query("SELECT f.files FROM Return f WHERE f.id = :id")
    List<File> getFileReturnById(@Param("id") Long id);

    @Query("SELECT d.files FROM Document d WHERE d.id = :id")
    List<File> getListFileById(@Param("id") Long id);

    @Query("select t.files FROM BonusTransaction t WHERE t.id = :id")
    List<File> getFilesByTransactionId(@Param("id") Long id);
}

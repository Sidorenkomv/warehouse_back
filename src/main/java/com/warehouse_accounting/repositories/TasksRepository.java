package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Tasks;
import com.warehouse_accounting.models.dto.TasksDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TasksRepository extends JpaRepository<Tasks, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.TasksDto(" +
            "t.id, " +
            "t.description, " +
            "t.deadline, " +
            "t.isDone, " +
            "e.id, " +
            "e.firstName, " +
            "c.id, " +
            "c.name, " +
            "d.id, " +
            "d.number) " +
            "FROM Tasks t " +
            "LEFT JOIN Employee e ON (t.executor.id = e.id) " +
            "LEFT JOIN Contractor c ON (t.contractor.id = c.id) " +
            "LEFT JOIN Contract d ON (t.contract.id = d.id)")
    List<TasksDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.TasksDto(" +
            "t.id, " +
            "t.description, " +
            "t.deadline, " +
            "t.isDone, " +
            "e.id, " +
            "e.firstName, " +
            "c.id, " +
            "c.name, " +
            "d.id, " +
            "d.number) " +
            "FROM Tasks t " +
            "LEFT JOIN Employee e ON (t.executor.id = e.id) " +
            "LEFT JOIN Contractor c ON (t.contractor.id = c.id) " +
            "LEFT JOIN Contract d ON (t.contract.id = d.id)" +
            "WHERE t.id = :id")
    TasksDto getById(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.document.id = :id")
    List<Tasks> getListTaskById(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.contractor.id = :id")
    List<Tasks> getListTaskOfContructorById(@Param("id") Long id);


}
package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.IpNetwork;
import com.warehouse_accounting.models.dto.IpNetworkDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface IpNetworkRepository extends JpaRepository<IpNetwork, Long> {
    @Query("SELECT new com.warehouse_accounting.models.dto.IpNetworkDto(" +
            "ipn.id," +
            "ipn.network," +
            "ipn.mask"+
            ")"+
            "FROM IpNetwork ipn")
    List<IpNetworkDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.IpNetworkDto(" +
            "ipn.id," +
            "ipn.network," +
            "ipn.mask"+
            ")"+
            "FROM IpNetwork ipn WHERE ipn.id=:id")
    IpNetworkDto getById(@Param("id") Long id);


    //check:
    @Query("SELECT e.ipNetwork FROM Employee e WHERE e.id = :id")
    IpNetwork getNetworkByEmployeeId(@Param("id") Long id);
}

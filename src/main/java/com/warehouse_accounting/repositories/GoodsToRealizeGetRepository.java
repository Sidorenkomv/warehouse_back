package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.GoodsToRealizeGet;
import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsToRealizeGetRepository extends JpaRepository<GoodsToRealizeGet, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.GoodsToRealizeGetDto("+
            "g.id, " +
            "p.id," +
            "p.name," +
            "u.id," +
            "u.shortName," +
            "g.getGoods," +
            "g.quantity," +
            "g.amount," +
            "g.arrive," +
            "g.remains," +
            "g.amount_Noreport," +
            "g.quantity_Noreport," +
            "g.amount_report," +
            "g.quantity_report" +
            ")"+
            "FROM GoodsToRealizeGet g " +
            "left join Unit u on (g.unit.id = u.id)" +
            "LEFT JOIN Product p ON (g.product.id = p.id)")
    List<GoodsToRealizeGetDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.GoodsToRealizeGetDto(" +
            "g.id, " +
            "p.id," +
            "p.name," +
            "u.id," +
            "u.shortName," +
            "g.getGoods," +
            "g.quantity," +
            "g.amount," +
            "g.arrive," +
            "g.remains," +
            "g.amount_Noreport," +
            "g.quantity_Noreport," +
            "g.amount_report," +
            "g.quantity_report" +
            ")"+
            "FROM GoodsToRealizeGet g " +
            "left join Unit u on (g.unit.id = u.id)" +
            "LEFT JOIN Product p ON (g.product.id = p.id) WHERE g.id=:id")
    GoodsToRealizeGetDto getById(@Param("id") Long id);
}

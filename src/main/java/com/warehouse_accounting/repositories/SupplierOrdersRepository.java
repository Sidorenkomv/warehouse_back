package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.SupplierOrders;
import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierOrdersRepository extends JpaRepository<SupplierOrders, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SupplierOrdersDto(" +
            "s.id, " +
            "s.docNumber, " +
            "s.date, " +
            "con.id, " +
            "con.name, " +
            "s.contrAgentAccount, " +
            "comp.id, " +
            "comp.name, " +
            "s.companyAccount, " +
            "s.sum, " +
            "s.invoiceIssued, " +
            "s.paid, " +
            "s.notPaid, " +
            "s.accepted, " +
            "s.waiting, " +
            "s.refundAmount, " +
            "s.acceptanceDate, " +
            "pr.id, " +
            "pr.name, " +
            "wr.id, " +
            "wr.name, " +
            "contr.id, " +
            "contr.number, " +
            "s.isSharedAccess, " +
            "dep.id, " +
            "dep.name, " +
            "emp.id, " +
            "emp.firstName, " +
            "s.sent, " +
            "s.print, " +
            "s.comments, " +
            "s.updatedAt, " +
            "empUpd.id, " +
            "empUpd.firstName" +
            ") " +
            "FROM SupplierOrders s " +
            "LEFT JOIN Contractor con ON (s.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (s.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (s.project.id = pr.id) " +
            "LEFT JOIN Warehouse wr ON (s.warehouseFrom.id = wr.id) " +
            "LEFT JOIN Contract contr ON (s.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (s.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (s.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (s.updatedFromEmployee.id = empUpd.id)")
    List<SupplierOrdersDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SupplierOrdersDto(" +
            "s.id, " +
            "s.docNumber, " +
            "s.date, " +
            "con.id, " +
            "con.name, " +
            "s.contrAgentAccount, " +
            "comp.id, " +
            "comp.name, " +
            "s.companyAccount, " +
            "s.sum, " +
            "s.invoiceIssued, " +
            "s.paid, " +
            "s.notPaid, " +
            "s.accepted, " +
            "s.waiting, " +
            "s.refundAmount, " +
            "s.acceptanceDate, " +
            "pr.id, " +
            "pr.name, " +
            "wr.id, " +
            "wr.name, " +
            "contr.id, " +
            "contr.number, " +
            "s.isSharedAccess, " +
            "dep.id, " +
            "dep.name, " +
            "emp.id, " +
            "emp.firstName, " +
            "s.sent, " +
            "s.print, " +
            "s.comments, " +
            "s.updatedAt, " +
            "empUpd.id, " +
            "empUpd.firstName" +
            ") " +
            "FROM SupplierOrders s " +
            "LEFT JOIN Contractor con ON (s.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (s.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (s.project.id = pr.id) " +
            "LEFT JOIN Warehouse wr ON (s.warehouseFrom.id = wr.id) " +
            "LEFT JOIN Contract contr ON (s.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (s.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (s.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (s.updatedFromEmployee.id = empUpd.id) " +
            "WHERE s.id = :id")
    SupplierOrdersDto getById(@Param("id") Long id);
}
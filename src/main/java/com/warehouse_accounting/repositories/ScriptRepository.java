package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Script;
import com.warehouse_accounting.models.dto.ScriptDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScriptRepository extends JpaRepository<Script, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ScriptDto(" +
            "sc.id," +
            "sc.name," +
            "sc.activity," +
            "sc.comment," +
            "e.id," +
            "e.firstName," +
            "sc.event," +
            "sc.action," +
            "sc.conditions)" +
            "FROM Script sc " +
            "LEFT JOIN Employee e ON (sc.owner.id = e.id)")
    List<ScriptDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ScriptDto(" +
            "sc.id," +
            "sc.name," +
            "sc.activity," +
            "sc.comment," +
            "e.id," +
            "e.firstName," +
            "sc.event," +
            "sc.action," +
            "sc.conditions)" +
            " FROM Script sc " +
            "LEFT JOIN Employee e ON (sc.owner.id = e.id)" +
            "WHERE sc.id=:id")
    ScriptDto getById(@Param("id") Long id);
}

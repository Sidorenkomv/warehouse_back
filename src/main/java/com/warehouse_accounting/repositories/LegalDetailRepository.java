package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.LegalDetail;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LegalDetailRepository extends JpaRepository<LegalDetail, Long>, JpaSpecificationExecutor<LegalDetail> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.LegalDetailDto(" +
            "ld.id," +
            "ld.fullName," +
            "ld.firstName," +
            "ld.middleName," +
            "ld.lastName," +
            "a," +
            "ld.inn," +
            "ld.kpp," +
            "ld.okpo," +
            "ld.ogrn," +
            "ld.numberOfTheCertificate," +
            "ld.dateOfTheCertificate," +
            "tc" +
            ") " +
            "FROM LegalDetail ld " +
            "LEFT JOIN ld.typeOfContractor tc " +
            "LEFT JOIN ld.address a ")
    List<LegalDetailDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.LegalDetailDto(" +
            "ld.id," +
            "ld.fullName," +
            "ld.firstName," +
            "ld.middleName," +
            "ld.lastName," +
            "a," +
            "ld.inn," +
            "ld.kpp," +
            "ld.okpo," +
            "ld.ogrn," +
            "ld.numberOfTheCertificate," +
            "ld.dateOfTheCertificate," +
            "tc" +
            ") " +
            "FROM LegalDetail ld " +
            "LEFT JOIN TypeOfContractor tc ON (ld.typeOfContractor.id = tc.id)" +
            "LEFT JOIN Address a ON (ld.address.id = a.id) " +
            "WHERE ld.id = :id")
    LegalDetailDto getById(@Param("id") Long id);
}

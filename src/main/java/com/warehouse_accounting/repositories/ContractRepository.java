package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Contract;
import com.warehouse_accounting.models.dto.ContractDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContractDto(" +
            "c.id," +
            "c.number," +
            "c.code," +
            "c.contractDate," +
            "c.amount," +
//            "c.paid," +
//            "c.done," +
//            "c.isSharedAccess," +
//            "c.isSend," +
//            "c.isPrint," +
            "c.archive," +
            "c.comment," +
//            "c.whenChange," +
            "cpmn," +
//            "c.bankAccount.id," +
            "cntr," +
//            "c.ownerDepartment.id," +
//            "c.ownerEmployee.id," +
//            "c.whoChange.id" +
//            "ld," +
            "c.typeOfContract," +
            "c.reward," +
            "c.percentageOfTheSaleAmount," +
            "c.enabled" +
            ")" +
            "FROM Contract c " +
            "LEFT JOIN c.company cpmn " +
            "LEFT JOIN c.contractor cntr " +
//            "LEFT JOIN c.legalDetail ld " +
            "WHERE c.enabled = true")
    List<ContractDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContractDto(" +
            "c.id," +
            "c.number," +
            "c.code," +
            "c.contractDate," +
            "c.amount," +
//            "c.paid," +
//            "c.done," +
//            "c.isSharedAccess," +
//            "c.isSend," +
//            "c.isPrint," +
            "c.archive," +
            "c.comment," +
//            "c.whenChange," +
            "cpmn," +
//            "c.bankAccount.id," +
            "cntr," +
//            "c.ownerDepartment.id," +
//            "c.ownerEmployee.id," +
//            "c.whoChange.id" +
//            "ld," +
            "c.typeOfContract," +
            "c.reward," +
            "c.percentageOfTheSaleAmount," +
            "c.enabled" +
            ")" +
            "FROM Contract c " +
            "LEFT JOIN Company cpmn ON (c.company.id = cpmn.id) " +
            "LEFT JOIN Contractor cntr ON (c.contractor.id = cntr.id) " +
//            "LEFT JOIN LegalDetail ld ON (c.legalDetail.id = ld.id) "+
            "WHERE c.id = :id AND c.enabled = true")
    ContractDto getById(@Param("id") Long id);

//    @Query("SELECT cr.contract FROM CommissionReports cr WHERE cr.id = :id")
//    List<Contract> getListContractById(@Param("id") Long id);
}

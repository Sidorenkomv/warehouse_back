package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Contractor;
import com.warehouse_accounting.models.dto.ContractorDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ContractorRepository extends JpaRepository<Contractor, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContractorDto(" +
            "c.id, " +
            "c.name, " +
            "c.status, " +
            "cg," +
            "c.code, " +
            "c.outerCode, " +
            "c.sortNumber, " +
            "c.phone, " +
            "c.fax, " +
            "c.email, " +
            "a, " +
            "c.comment, " +
            "c.numberDiscountCard, " +
            "tp, " +
            "ld" +
            ") " +
            "FROM Contractor c " +
            "LEFT JOIN c.contractorGroup cg " +
            "LEFT JOIN c.typeOfPrice tp " +
            "LEFT JOIN c.legalDetail ld " +
            "LEFT JOIN c.address a ")
    List<ContractorDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContractorDto(" +
            "c.id, " +
            "c.name, " +
            "c.status, " +
            "cg," +
            "c.code, " +
            "c.outerCode, " +
            "c.sortNumber, " +
            "c.phone, " +
            "c.fax, " +
            "c.email, " +
            "a, " +
            "c.comment, " +
            "c.numberDiscountCard, " +
            "tp, " +
            "ld" +
            ") " +
            "FROM Contractor c " +
            "LEFT JOIN ContractorGroup cg ON (c.contractorGroup.id = cg.id) " +
            "LEFT JOIN TypeOfPrice tp ON (c.typeOfPrice.id = tp.id) " +
            "LEFT JOIN LegalDetail ld ON (c.legalDetail.id = ld.id) " +
            "LEFT JOIN Address a ON (c.address.id = a.id) " +
            "WHERE c.id = :id")
    ContractorDto getById(@Param("id") Long id);


//    //for Discount
    @Query("SELECT contractor FROM Contractor contractor WHERE contractor.id = :id")
    Set<Contractor> getAllContractorById(@Param("id") Long id);
}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Document;
import com.warehouse_accounting.models.dto.DocumentDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.DocumentDto(" +
            "doc.id, " +
            "doc.type, " +
            "doc.docNumber, " +
            "doc.date, " +
            "doc.sum, " +
            "wrFrom.id, " +
            "wrFrom.name, " +
            "wrTo.id, " +
            "wrTo.name, " +
            "comp.id, " +
            "comp.name, " +
            "con.id, " +
            "con.name, " +
            "pr.id, " +
            "pr.name, " +
            "salCh.id, " +
            "salCh.name, " +
            "contr.id, " +
            "contr.number, " +
            "doc.isSharedAccess, " +
            "dep.id, " +
            "dep.name, " +
            "emp.id, " +
            "emp.firstName, " +
            "doc.sent, " +
            "doc.print, " +
            "doc.comments, " +
            "doc.updatedAt, " +
            "empUpd.id," +
            "empUpd.firstName)" +
            "FROM Document doc " +
            "LEFT JOIN Contractor con ON (doc.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (doc.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (doc.project.id = pr.id) " +
            "LEFT JOIN SalesChannel salCh ON (doc.salesChannel.id = salCh.id) " +
            "LEFT JOIN Warehouse wrFrom ON (doc.warehouseFrom.id = wrFrom.id) " +
            "LEFT JOIN Warehouse wrTo ON (doc.warehouseTo.id = wrTo.id) " +
            "LEFT JOIN Contract contr ON (doc.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (doc.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (doc.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (doc.updatedFromEmployee.id = empUpd.id)")
    List<DocumentDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.DocumentDto(" +
            "doc.id, " +
            "doc.type, " +
            "doc.docNumber, " +
            "doc.date, " +
            "doc.sum, " +
            "wrFrom.id, " +
            "wrFrom.name, " +
            "wrTo.id, " +
            "wrTo.name, " +
            "comp.id, " +
            "comp.name, " +
            "con.id, " +
            "con.name, " +
            "pr.id, " +
            "pr.name, " +
            "salCh.id, " +
            "salCh.name, " +
            "contr.id, " +
            "contr.number, " +
            "doc.isSharedAccess, " +
            "dep.id, " +
            "dep.name, " +
            "emp.id, " +
            "emp.firstName, " +
            "doc.sent, " +
            "doc.print, " +
            "doc.comments, " +
            "doc.updatedAt, " +
            "empUpd.id, " +
            "empUpd.firstName)" +
            "FROM Document doc " +
            "LEFT JOIN Contractor con ON (doc.contrAgent.id = con.id) " +
            "LEFT JOIN Company comp ON (doc.company.id = comp.id) " +
            "LEFT JOIN Project pr ON (doc.project.id = pr.id) " +
            "LEFT JOIN SalesChannel salCh ON (doc.salesChannel.id = salCh.id) " +
            "LEFT JOIN Warehouse wrFrom ON (doc.warehouseFrom.id = wrFrom.id) " +
            "LEFT JOIN Warehouse wrTo ON (doc.warehouseTo.id = wrTo.id) " +
            "LEFT JOIN Contract contr ON (doc.contract.id = contr.id) " +
            "LEFT JOIN Department dep ON (doc.department.id = dep.id) " +
            "LEFT JOIN Employee emp ON (doc.employee.id = emp.id) " +
            "LEFT JOIN Employee empUpd ON (doc.updatedFromEmployee.id = empUpd.id) " +
            "WHERE doc.id = :id")
    DocumentDto getById(@Param("id") Long id);
}

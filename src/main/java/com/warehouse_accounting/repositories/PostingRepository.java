package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Posting;
import com.warehouse_accounting.models.dto.PostingDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PostingRepository extends JpaRepository<Posting, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PostingDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseTo.id," +
            "wo.company.id," +
            "wo.sum," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM Posting wo")
    List<PostingDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PostingDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseTo.id," +
            "wo.company.id," +
            "wo.sum," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM Posting wo WHERE wo.id=:id")
    PostingDto getById(@Param("id") Long id);
}

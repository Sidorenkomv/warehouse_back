package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ProcurementManagement;
import com.warehouse_accounting.models.dto.ProcurementManagementDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProcurementManagementRepository  extends JpaRepository<ProcurementManagement, Long> {
    @Query("SELECT new com.warehouse_accounting.models.dto.ProcurementManagementDto(" +
            "i.id," +
            "i.name," +
            "i.code," +
            "i.vendor_code," +
            "i.number," +
            "i.sum," +
            "i.cost_price," +
            "i.profit," +
            "i.profitability," +
            "i.sales_day," +
            "i.balance," +
            "i.reserve," +
            "i.expectation," +
            "i.available," +
            "i.days_in_stock," +
            "i.days_of_stock," +
            "i.stock" +
            ")" +
            "FROM ProcurementManagement i")
    List<ProcurementManagementDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.ProcurementManagementDto(" +
            "i.id," +
            "i.name," +
            "i.code," +
            "i.vendor_code," +
            "i.number," +
            "i.sum," +
            "i.cost_price," +
            "i.profit," +
            "i.profitability," +
            "i.sales_day," +
            "i.balance," +
            "i.reserve," +
            "i.expectation," +
            "i.available," +
            "i.days_in_stock," +
            "i.days_of_stock," +
            "i.stock" +
            ")" +
            "FROM ProcurementManagement i WHERE i.id=:id")
    List<ProcurementManagementDto> getById(@Param("id") Long id);
}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.City;
import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.models.dto.RegionDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface CityRepository extends JpaRepository<City, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.CityDto(" +
            "c.id," +
            "c.name," +
            "c.socr," +
            "c.code," +
            "c.index," +
            "c.gninmb," +
            "c.uno," +
            "c.ocatd," +
            "c.status)" +
            "FROM City c " +
            "WHERE c.code LIKE :#{(#regionCode.length() > 2) ? #regionCode.substring(0, 2) + '%00' : #regionCode + '%00'} " +
            "ORDER BY c.name")
    List<CityDto> getAll(@Param("regionCode") String regionCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.CityDto(" +
            "c.id," +
            "c.name," +
            "c.socr," +
            "c.code," +
            "c.index," +
            "c.gninmb," +
            "c.uno," +
            "c.ocatd," +
            "c.status)" +
            "FROM City c " +
            "WHERE c.code LIKE :#{(#regionCode.length() > 2) ? #regionCode.substring(0, 2) + '%00' : #regionCode + '%00'} " +
            "AND LOWER(c.name) LIKE CONCAT('%', LOWER(:name), '%') " +
            "ORDER BY c.name")
    Slice<CityDto> getSlice(@Param("name") String name, @Param("regionCode") String regionCode, Pageable paging);

    @Query("SELECT COUNT(c) " +
            "FROM City c " +
            "WHERE c.code LIKE :#{(#regionCode.length() > 2) ? #regionCode.substring(0, 2) + '%00' : #regionCode + '%00'} " +
            "AND LOWER(c.name) LIKE CONCAT('%', LOWER(:name), '%')")
    int getCount(String name, @Param("regionCode") String regionCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.CityDto(" +
            "c.id," +
            "c.name," +
            "c.socr," +
            "c.code," +
            "c.index," +
            "c.gninmb," +
            "c.uno," +
            "c.ocatd," +
            "c.status)" +
            "FROM City c WHERE c.id = :id")
    CityDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.CityDto(" +
            "c.id," +
            "c.name," +
            "c.socr," +
            "c.code," +
            "c.index," +
            "c.gninmb," +
            "c.uno," +
            "c.ocatd," +
            "c.status)" +
            "FROM City c " +
            "WHERE c.code = :#{(#code.length() > 11) ? #code.substring(0, 11) + '00' : #code + '00'}")
    CityDto getByCode(@Param("code") String code);
}

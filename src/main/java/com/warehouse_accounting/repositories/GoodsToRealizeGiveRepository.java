package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.GoodsToRealizeGet;
import com.warehouse_accounting.models.GoodsToRealizeGive;
import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.models.dto.LanguageDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsToRealizeGiveRepository extends JpaRepository<GoodsToRealizeGive, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto("+
            "g.id, " +
            "p.id," +
            "p.name," +
            "u.id," +
            "u.shortName," +
            "g.giveGoods," +
            "g.quantity," +
            "g.amount," +
            "g.arrive," +
            "g.remains" +
            ")"+
            "FROM GoodsToRealizeGive g " +
            "left join Unit u on (g.unit.id = u.id)" +
            "LEFT JOIN Product p ON (g.product.id = p.id)")
    List<GoodsToRealizeGiveDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto(" +
            "g.id, " +
            "p.id," +
            "p.name," +
            "u.id," +
            "u.shortName," +
            "g.giveGoods," +
            "g.quantity," +
            "g.amount," +
            "g.arrive," +
            "g.remains" +
            ")"+
            "FROM GoodsToRealizeGive g " +
            "left join Unit u on (g.unit.id = u.id)" +
            "LEFT JOIN Product p ON (g.product.id = p.id) WHERE g.id=:id")
    GoodsToRealizeGiveDto getById(@Param("id") Long id);

}

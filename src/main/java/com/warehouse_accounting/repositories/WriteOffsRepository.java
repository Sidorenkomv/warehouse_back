package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.WriteOffs;
import com.warehouse_accounting.models.dto.WriteOffsDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WriteOffsRepository extends JpaRepository<WriteOffs, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.WriteOffsDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseFrom.id," +
            "wo.warehouseTo.id," +
            "wo.company.id," +
            "wo.sum," +
            "wo.status," +
            "wo.warehouseName," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM WriteOffs wo")
    List<WriteOffsDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.WriteOffsDto(" +
            "wo.id," +
            "wo.dateTime," +
            "wo.warehouseFrom.id," +
            "wo.warehouseTo.id," +
            "wo.company.id," +
            "wo.sum," +
            "wo.status," +
            "wo.warehouseName," +
            "wo.moved," +
            "wo.printed," +
            "wo.comment" +
            ")" +
            " FROM WriteOffs wo WHERE wo.id=:id")
    WriteOffsDto getById(@Param("id") Long id);

}
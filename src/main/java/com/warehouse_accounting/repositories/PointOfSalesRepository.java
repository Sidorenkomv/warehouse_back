package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.PointOfSales;
import com.warehouse_accounting.models.dto.PointOfSalesDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PointOfSalesRepository extends JpaRepository<PointOfSales, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PointOfSalesDto(" +
            "p.id," +
            "p.checkboxEnabled,"+
            "p.name," +
            "p.activity," +
            "p.type," +
            "p.revenue," +
            "p.cheque," +
            "p.averageСheck," +
            "p.moneyInTheCashRegister," +
            "p.cashiers," +
            "p.synchronization," +
            "p.FN," +
            "p.validityPeriodFN" +
            ")" +
            "FROM PointOfSales p")
    List<PointOfSalesDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PointOfSalesDto(" +
            "p.id," +
            "p.checkboxEnabled,"+
            "p.name," +
            "p.activity," +
            "p.type," +
            "p.revenue," +
            "p.cheque," +
            "p.averageСheck," +
            "p.moneyInTheCashRegister," +
            "p.cashiers," +
            "p.synchronization," +
            "p.FN," +
            "p.validityPeriodFN" +
            ")" +
            "FROM PointOfSales p WHERE p.id = :id")
    PointOfSalesDto getById(@Param("id") Long id);


}


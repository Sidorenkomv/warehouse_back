package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.AdditionalField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditionalFieldRepository extends JpaRepository<AdditionalField, Long> {

}


package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Status;
import com.warehouse_accounting.models.dto.StatusDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.StatusDto(" +
            "t.id, " +
            "t.nameOfClass, " +
            "t.titleOfStatus, " +
            "t.colorCode " +
            ")" +
            "FROM Status t WHERE t.nameOfClass = :nameOfClass")
    List<StatusDto> getAllByNameOfClass(@Param("nameOfClass") String nameOfClass);


    @Query("SELECT NEW com.warehouse_accounting.models.dto.StatusDto(" +
            "t.id, " +
            "t.nameOfClass, " +
            "t.titleOfStatus, " +
            "t.colorCode " +
            ")" +
            "FROM Status t ")
    List<StatusDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.StatusDto(" +
            "t.id, " +
            "t.nameOfClass, " +
            "t.titleOfStatus, " +
            "t.colorCode " +
            ")" +
            "FROM Status t WHERE t.id = :id")
    StatusDto getById(@Param("id") Long id);


}


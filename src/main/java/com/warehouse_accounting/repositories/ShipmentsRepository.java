package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Shipments;
import com.warehouse_accounting.models.dto.ShipmentsDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipmentsRepository extends JpaRepository<Shipments, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.ShipmentsDto(" +
            "sh.id," +
            "sh.date," +
            "sh.warehouseFrom.id," +
            "sh.contrAgent.id," +
            "sh.company.id," +
            "sh.consignee.id," +
            "sh.sum," +
            "sh.paid," +
            "sh.sent," +
            "sh.print," +
            "sh.comments" +
            ")" +
            " FROM Shipments sh ")
    List<ShipmentsDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ShipmentsDto(" +
            "sh.id," +
            "sh.date," +
            "sh.warehouseFrom.id," +
            "sh.contrAgent.id," +
            "sh.company.id," +
            "sh.consignee.id," +
            "sh.sum," +
            "sh.paid," +
            "sh.sent," +
            "sh.print," +
            "sh.comments" +
            ")" +
            " FROM Shipments sh WHERE sh.id = :id")
    ShipmentsDto getById(@Param("id") Long id);
}

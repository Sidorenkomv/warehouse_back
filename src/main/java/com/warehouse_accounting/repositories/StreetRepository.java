package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Street;
import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.models.dto.StreetDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.StreetDto(" +
            "s.id," +
            "s.name," +
            "s.socr," +
            "s.code," +
            "s.index," +
            "s.gninmb," +
            "s.uno," +
            "s.ocatd)" +
            "FROM Street s " +
            "WHERE s.code LIKE :#{(#regionCityCode.length() > 12) ? #regionCityCode.substring(0, 12) + '%00' : #regionCityCode + '%00'} " +
            "ORDER BY s.name")
    List<StreetDto> getAll(@Param("regionCityCode") String regionCityCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.StreetDto(" +
            "s.id," +
            "s.name," +
            "s.socr," +
            "s.code," +
            "s.index," +
            "s.gninmb," +
            "s.uno," +
            "s.ocatd)" +
            "FROM Street s " +
            "WHERE s.code LIKE :#{(#regionCityCode.length() > 12) ? #regionCityCode.substring(0, 12) + '%00' : #regionCityCode + '%00'} " +
            "AND LOWER(s.name) LIKE CONCAT('%', LOWER(:name), '%') " +
            "ORDER BY s.name")
    Slice<StreetDto> getSlice(@Param("name") String name, @Param("regionCityCode") String regionCityCode, Pageable paging);

    @Query("SELECT COUNT(s) " +
            "FROM Street s " +
            "WHERE s.code LIKE :#{(#regionCityCode.length() > 12) ? #regionCityCode.substring(0, 12) + '%00' : #regionCityCode + '%00'} " +
            "AND LOWER(s.name) LIKE CONCAT('%', LOWER(:name), '%')")
    int getCount(String name, @Param("regionCityCode") String regionCityCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.StreetDto(" +
            "s.id," +
            "s.name," +
            "s.socr," +
            "s.code," +
            "s.index," +
            "s.gninmb," +
            "s.uno," +
            "s.ocatd)" +
            "FROM Street s WHERE s.id = :id")
    StreetDto getById(@Param("id") Long id);
}


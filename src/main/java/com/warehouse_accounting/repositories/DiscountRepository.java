package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Discount;

import com.warehouse_accounting.models.dto.DiscountDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.DiscountDto (" +
            "d.id, " +
            "d.active," +
            "d.name," +
            "d.discountType.id," +
            "d.priceTypeProductCard," +
            "d.fixedDiscount," +
            "d.sumCumulative," +
            "d.discountPercent," +
            "d.accrualPoints ," +
            "d.writeOff ," +
            "d.maxPercentPayment ," +
            "d.pointsAwardedInDays ," +
            "d.SimultaneousAccrualAndWriteOff ," +
            "d.welcomePoints ," +
            "d.welcomePointsAccrual ," +
            "d.firstPurchase ," +
            "d.registrationBonusProgram)" +
            "FROM Discount d "
    )
    List<DiscountDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.DiscountDto (" +
            "d.id, " +
            "d.active," +
            "d.name," +
            "d.discountType.id," +
            "d.priceTypeProductCard," +
            "d.fixedDiscount," +
            "d.sumCumulative," +
            "d.discountPercent," +
            "d.accrualPoints ," +
            "d.writeOff ," +
            "d.maxPercentPayment ," +
            "d.pointsAwardedInDays ," +
            "d.SimultaneousAccrualAndWriteOff ," +
            "d.welcomePoints ," +
            "d.welcomePointsAccrual ," +
            "d.firstPurchase ," +
            "d.registrationBonusProgram)" +
            "FROM Discount d " +
            "WHERE d.id = :id")
    DiscountDto getById(@Param("id") Long id);
}

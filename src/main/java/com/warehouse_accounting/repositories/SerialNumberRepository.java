package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.SerialNumber;
import com.warehouse_accounting.models.dto.SerialNumberDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface SerialNumberRepository extends JpaRepository<SerialNumber, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SerialNumberDto(" +
            "wo.id," +
            "wo.serialNumber," +
            "wo.code," +
            "wo.vendor_code," +
            "wo.product.id," +
            "wo.warehouse.id," +
            "wo.typeOfDoc," +
            "wo.number," +
            "wo.description" +
            ")" +
            " FROM SerialNumber wo")
    List<SerialNumberDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SerialNumberDto(" +
            "wo.id," +
            "wo.serialNumber," +
            "wo.code," +
            "wo.vendor_code," +
            "wo.product.id," +
            "wo.warehouse.id," +
            "wo.typeOfDoc," +
            "wo.number," +
            "wo.description" +
            ")" +
            " FROM SerialNumber wo WHERE wo.id=:id")
    SerialNumberDto getById(@Param("id") Long id);

}
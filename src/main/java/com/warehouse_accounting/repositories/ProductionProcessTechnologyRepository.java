package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ProductionProcessTechnology;
import com.warehouse_accounting.models.dto.ProductionProcessTechnologyDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionProcessTechnologyRepository extends JpaRepository<ProductionProcessTechnology, Long> {

}


package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.SalesChannel;
import com.warehouse_accounting.models.dto.SalesChannelDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesChannelRepository extends JpaRepository<SalesChannel, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.SalesChannelDto(" +
        "s.id," +
        "s.name," +
        "s.type," +
        "s.description," +
        "s.generalAccessC," +
        "s.ownerDepartment," +
        "s.ownerEmployee," +
        "s.whenChanged," +
        "s.whoChanged" +
        ")"+
        "FROM SalesChannel s")
    List<SalesChannelDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.SalesChannelDto(" +
        "s.id," +
        "s.name," +
        "s.type," +
        "s.description," +
        "s.generalAccessC," +
        "s.ownerDepartment," +
        "s.ownerEmployee," +
        "s.whenChanged," +
        "s.whoChanged" +
        ")"+
        "FROM SalesChannel s WHERE s.id=:id")
    SalesChannelDto getById(@Param("id") Long id);
}

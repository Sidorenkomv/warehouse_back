package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Building;
import com.warehouse_accounting.models.dto.BuildingDto;
import com.warehouse_accounting.models.dto.CityDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long> {
    @Query("SELECT NEW com.warehouse_accounting.models.dto.BuildingDto(" +
            "b.id," +
            "b.name," +
            "b.socr," +
            "b.code," +
            "b.index" +
            ") " +
            "FROM Building b " +
            "WHERE b.code LIKE :#{(#regionCityStreetCode.length() > 16) ? #regionCityStreetCode.substring(0, 16) : #regionCityStreetCode}% " +
            "ORDER BY b.name")
    List<BuildingDto> getAll(@Param("regionCityStreetCode") String regionCityStreetCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.BuildingDto(" +
            "b.id," +
            "b.name," +
            "b.socr," +
            "b.code," +
            "b.index" +
            ") " +
            "FROM Building b " +
            "WHERE b.code LIKE :#{(#regionCityStreetCode.length() > 16) ? #regionCityStreetCode.substring(0, 16) : #regionCityStreetCode}% " +
            "AND LOWER(b.name) LIKE CONCAT('%', LOWER(:name), '%' )" +
            "ORDER BY b.name")
    Slice<BuildingDto> getSlice(@Param("name") String name, @Param("regionCityStreetCode") String regionCityStreetCode, Pageable paging);

    @Query("SELECT COUNT(b) " +
            "FROM Building b " +
            "WHERE b.code LIKE :#{(#regionCityStreetCode.length() > 16) ? #regionCityStreetCode.substring(0, 16) : #regionCityStreetCode}% " +
            "AND LOWER(b.name) LIKE CONCAT('%', LOWER(:name), '%' ) ")
    int getCount(String name, @Param("regionCityStreetCode") String regionCityStreetCode);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.BuildingDto(" +
            "b.id," +
            "b.name," +
            "b.socr," +
            "b.code, " +
            "b.index" +
            ") " +
            "FROM Building b WHERE b.id = :id")
    BuildingDto getById(@Param("id") Long id);
}

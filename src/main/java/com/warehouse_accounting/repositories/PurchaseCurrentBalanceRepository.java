package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.PurchaseCurrentBalance;
import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseCurrentBalanceRepository extends JpaRepository<PurchaseCurrentBalance, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto(" +
            "p.id, " +
            "p.remainder, " +
            "p.productReserved, " +
            "p.productsAwaiting, " +
            "p.productAvailableForOrder, " +
            "p.daysStoreOnTheWarehouse " +
            ") FROM PurchaseCurrentBalance p")
    List<PurchaseCurrentBalanceDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto(" +
            "p.id, " +
            "p.remainder, " +
            "p.productReserved, " +
            "p.productsAwaiting, " +
            "p.productAvailableForOrder, " +
            "p.daysStoreOnTheWarehouse " +
            ") FROM PurchaseCurrentBalance p where p.id=:id")
    PurchaseCurrentBalanceDto getById(@Param("id") Long id);

}

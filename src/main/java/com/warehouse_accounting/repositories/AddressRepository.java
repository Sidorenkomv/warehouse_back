package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Address;
import com.warehouse_accounting.models.dto.AddressDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.AddressDto(" +
            "a.id," +
            "c.id," +
            "a.postCode," +
            "r.id," +
            "ct.id," +
            "a.cityName," +
            "str.id," +
            "a.streetName," +
            "b.id," +
            "a.buildingName," +
            "a.office," +
            "a.fullAddress," +
            "a.other," +
            "a.comment) " +
            "FROM Address a " +
            "left join a.country c " +
            "left join a.region r " +
            "left join a.city ct " +
            "left join a.street str " +
            "left join a.building b ")
    List<AddressDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.AddressDto(" +
            "a.id," +
            "c.id," +
            "a.postCode," +
            "r.id," +
            "ct.id," +
            "a.cityName," +
            "str.id," +
            "a.streetName," +
            "b.id," +
            "a.buildingName," +
            "a.office," +
            "a.fullAddress," +
            "a.other," +
            "a.comment) " +
            "FROM Address a " +
            "left join a.country c " +
            "left join a.region r " +
            "left join a.city ct " +
            "left join a.street str " +
            "left join a.building b " +
            "WHERE a.id = :id")
    AddressDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.AddressDto(" +
            "a.id," +
            "c.id," +
            "a.postCode," +
            "r.id," +
            "ct.id," +
            "a.cityName," +
            "str.id," +
            "a.streetName," +
            "b.id," +
            "a.buildingName," +
            "a.office," +
            "a.fullAddress," +
            "a.other," +
            "a.comment) " +
            "FROM Address a " +
            "left join a.country c " +
            "left join a.region r " +
            "left join a.city ct " +
            "left join a.street str " +
            "left join a.building b " +
            "WHERE a.fullAddress = :fullAddress")
    AddressDto getByFullAddress(String fullAddress);

//    AddressDto findByFullAddress(String fullAddress);
}

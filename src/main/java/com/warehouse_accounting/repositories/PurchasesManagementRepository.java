package com.warehouse_accounting.repositories;


import com.warehouse_accounting.models.PurchasesManagement;
import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchasesManagementRepository extends JpaRepository<PurchasesManagement, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchasesManagementDto(" +
            "p.id, " +
            "p.product.id, " +
            "p.historyOfSales.id, " +
            "p.currentBalance.id, " +
            "p.forecast.id " +
            ") FROM PurchasesManagement p")
    List<PurchasesManagementDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchasesManagementDto(" +
            "p.id, " +
            "p.product.id, " +
            "p.historyOfSales.id, " +
            "p.currentBalance.id, " +
            "p.forecast.id " +
            ") FROM PurchasesManagement p where p.id=:id")
    PurchasesManagementDto getById(@Param("id") Long id);

}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.SupplierInvoice;
import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierInvoiceRepository extends JpaRepository<SupplierInvoice, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SupplierInvoiceDto(" +
        "app.id," +
        "app.invoiceNumber," +
        "app.dateInvoiceNumber," +
        "app.checkboxProd," +
        "app.organization," +
        "app.warehouse," +
        "app.contrAgent," +
            "app.contract," +
            "app.datePay," +
            "app.project," +
            "app.incomingNumber," +
            "app.dateIncomingNumber," +
            "app.checkboxName," +
            "app.checkboxNDS," +
            "app.checkboxOnNDS," +
            "app.addPosition," +
            "app.addComment" +
        ")" +
        " FROM SupplierInvoice app")
    List<SupplierInvoiceDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.SupplierInvoiceDto(" +
            "app.id," +
            "app.invoiceNumber," +
            "app.dateInvoiceNumber," +
            "app.checkboxProd," +
            "app.organization," +
            "app.warehouse," +
            "app.contrAgent," +
            "app.contract," +
            "app.datePay," +
            "app.project," +
            "app.incomingNumber," +
            "app.dateIncomingNumber," +
            "app.checkboxName," +
            "app.checkboxNDS," +
            "app.checkboxOnNDS," +
            "app.addPosition," +
            "app.addComment" +
            ")" +
            " FROM SupplierInvoice app WHERE app.id=:id")
    SupplierInvoiceDto getById(@Param("id") Long id);
}

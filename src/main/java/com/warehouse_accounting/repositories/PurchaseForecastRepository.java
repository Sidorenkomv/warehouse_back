package com.warehouse_accounting.repositories;


import com.warehouse_accounting.models.PurchaseForecast;
import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseForecastRepository extends JpaRepository<PurchaseForecast, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseForecastDto(" +
            "p.id, " +
            "p.reservedDays, " +
            "p.reservedProduct, " +
            "p.ordered " +
            ") FROM PurchaseForecast p")
    List<PurchaseForecastDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseForecastDto(" +
            "p.id, " +
            "p.reservedDays, " +
            "p.reservedProduct, " +
            "p.ordered " +
            ") FROM PurchaseForecast p where p.id=:id")
    PurchaseForecastDto getById(@Param("id") Long id);

}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.DiscountType;
import com.warehouse_accounting.models.dto.DiscountTypeDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface DiscountTypeRepository extends JpaRepository<DiscountType, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.DiscountTypeDto(" +
            "dt.id," +
            "dt.name" +
            ") FROM DiscountType dt")
    List<DiscountTypeDto> getAll();


    @Query("SELECT new com.warehouse_accounting.models.dto.DiscountTypeDto(" +
            "dt.id," +
            "dt.name" +
            ") FROM DiscountType dt where dt.id=:id")
    DiscountTypeDto getById(@Param("id") Long id);

}

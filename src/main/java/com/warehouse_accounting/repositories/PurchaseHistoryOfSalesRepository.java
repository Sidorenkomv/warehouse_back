package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.PurchaseHistoryOfSales;
import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseHistoryOfSalesRepository extends JpaRepository<PurchaseHistoryOfSales, Long> {

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto(" +
            "p.id, " +
            "p.number, " +
            "p.productPrice.id, " +
            "p.sum, " +
            "p.profit, " +
            "p.profitability, " +
            "p.saleOfDay " +
            ") FROM PurchaseHistoryOfSales p")
    List<PurchaseHistoryOfSalesDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto(" +
            "p.id, " +
            "p.number, " +
            "p.productPrice.id, " +
            "p.sum, " +
            "p.profit, " +
            "p.profitability, " +
            "p.saleOfDay " +
            ") FROM PurchaseHistoryOfSales p where p.id=:id")
    PurchaseHistoryOfSalesDto getById(@Param("id") Long id);

}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.BonusProgram;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BonusProgramRepository extends JpaRepository<BonusProgram, Long> {


}

package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.RecycleBin;
import com.warehouse_accounting.models.dto.RecycleBinDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecycleBinRepository extends JpaRepository<RecycleBin, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.RecycleBinDto(" +
            "r.id, " +
            "r.documentType, " +
            "r.number, " +
            "r.date, " +
            "r.sum, " +
            "w.id, " +
            "w.name, " +
            "r.warehouseFrom, " +
            "c.id, " +
            "c.name, " +
            "cg.id, " +
            "cg.name, " +
            "r.status, " +
            "p.id, " +
            "p.name, " +
            "r.shipped, " +
            "r.printed, " +
            "r.comment" +
            ")" +
            "FROM RecycleBin r "+
            "LEFT JOIN Warehouse w ON (r.warehouse.id = w.id)"+
            "LEFT JOIN Company c ON (r.company.id = c.id)"+
            "LEFT JOIN ContractorGroup cg ON (r.contractor.id = cg.id)"+
            "LEFT JOIN Project p ON (r.project.id = p.id)"
    )
    List<RecycleBinDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.RecycleBinDto(" +
            "r.id, " +
            "r.documentType, " +
            "r.number, " +
            "r.date, " +
            "r.sum, " +
            "w.id, " +
            "w.name, " +
            "r.warehouseFrom, " +
            "c.id, " +
            "c.name, " +
            "cg.id, " +
            "cg.name, " +
            "r.status, " +
            "p.id, " +
            "p.name, " +
            "r.shipped, " +
            "r.printed, " +
            "r.comment" +
            ")" +
            "FROM RecycleBin r " +
            "LEFT JOIN Warehouse w ON (r.warehouse.id = w.id)"+
            "LEFT JOIN Company c ON (r.company.id = c.id)"+
            "LEFT JOIN ContractorGroup cg ON (r.contractor.id = cg.id)"+
            "LEFT JOIN Project p ON (r.project.id = p.id)"+
            " WHERE r.id=:id")
    RecycleBinDto getById(@Param("id") Long id);
}


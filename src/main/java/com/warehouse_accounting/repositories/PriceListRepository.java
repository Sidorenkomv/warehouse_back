package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.PriceList;
import com.warehouse_accounting.models.dto.PriceListDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PriceListRepository extends JpaRepository<PriceList, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PriceListDto(" +
            "p.id," +
            "p.dateTime," +
            "p.company.id," +
            "p.moved," +
            "p.printed," +
            "p.comment" +
            ")" +
            " FROM PriceList p")
    List<PriceListDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.PriceListDto(" +
            "p.id," +
            "p.dateTime," +
            "p.company.id," +
            "p.moved," +
            "p.printed," +
            "p.comment" +
            ")" +
            " FROM PriceList p WHERE p.id=:id")
    PriceListDto getById(@Param("id") Long id);
}

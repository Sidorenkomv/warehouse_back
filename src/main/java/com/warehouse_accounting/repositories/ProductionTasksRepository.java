package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ProductionTasks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionTasksRepository extends JpaRepository<ProductionTasks, Long> {

}


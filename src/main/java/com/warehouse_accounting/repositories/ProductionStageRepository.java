package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ProductionStage;
import com.warehouse_accounting.models.dto.ProductionStageDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductionStageRepository extends JpaRepository<ProductionStage, Long> {


    @Query("SELECT NEW com.warehouse_accounting.models.dto.ProductionStageDto(" +
            "st.id, " +
            "st.name, " +
            "st.description, " +
            "st.generalAccess, " +
            "st.archived, " +
            "st.sortNumber, " +
            "od.id, " +
            "od.name, " +
            "oe.id, " +
            "oe.lastName, " +
            "st.dateOfEdit, " +
            "ee.id, " +
            "ee.lastName" +
            ")" +
            "FROM ProductionStage st " +
            "left join Department od on (st.ownerDepartment.id = od.id)" +
            "left join Employee oe on (st.ownerEmployee.id = oe.id)" +
            "left join Employee ee on (st.editorEmployee.id = ee.id) WHERE st.id = :id")
    ProductionStageDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ProductionStageDto(" +
            "st.id, " +
            "st.name, " +
            "st.description, " +
            "st.generalAccess, " +
            "st.archived, " +
            "st.sortNumber, " +
            "od.id, " +
            "od.name, " +
            "oe.id, " +
            "oe.lastName, " +
            "st.dateOfEdit, " +
            "ee.id, " +
            "ee.lastName" +
            ")" +
            "FROM ProductionStage st left join Department od on (st.ownerDepartment.id = od.id)" +
            "left join Employee oe on (st.ownerEmployee.id = oe.id)" +
            "left join Employee ee on (st.editorEmployee.id = ee.id) WHERE st.archived = :archived")
    List<ProductionStageDto> getAll(boolean archived);
}

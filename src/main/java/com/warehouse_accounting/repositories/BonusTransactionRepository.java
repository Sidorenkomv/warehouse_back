package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.BonusTransaction;
import com.warehouse_accounting.models.File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface BonusTransactionRepository extends JpaRepository<BonusTransaction, Long> {


}

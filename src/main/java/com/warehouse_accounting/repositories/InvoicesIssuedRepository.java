package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.InvoicesIssued;

import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.models.dto.LanguageDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoicesIssuedRepository extends JpaRepository<InvoicesIssued, Long> {
    @Query("SELECT new com.warehouse_accounting.models.dto.InvoicesIssuedDto(" +
            "i.id," +
            "i.data,"+
            "i.sum," +
            "i.sent," +
            "i.printed," +
            "i.company.id," +
            "i.contractor.id," +
            "i.comment" +
            ")" +
            "FROM InvoicesIssued i")
    List<InvoicesIssuedDto> getAll();

    @Query("SELECT new com.warehouse_accounting.models.dto.InvoicesIssuedDto(" +
            "i.id," +
            "i.data,"+
            "i.sum," +
            "i.sent," +
            "i.printed," +
            "i.company.id," +
            "i.contractor.id," +
            "i.comment" +
            ")" +
            "FROM  InvoicesIssued  i WHERE i.id=:id")
    InvoicesIssuedDto getById(@Param("id") Long id);

}

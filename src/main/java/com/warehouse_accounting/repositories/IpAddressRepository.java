package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.IpAddress;
import com.warehouse_accounting.models.dto.IpAddressDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Set;

@Repository
public interface IpAddressRepository extends JpaRepository<IpAddress, Long> {


    @Query("SELECT new com.warehouse_accounting.models.dto.IpAddressDto(" +
            "ipAddress.id," +
            "ipAddress.address"+
            ")"+
            "FROM IpAddress ipAddress")
    List<IpAddressDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.IpAddressDto(" +
            "ipAddress.id," +
            "ipAddress.address"+
            ")"+
            "FROM IpAddress ipAddress WHERE ipAddress.id=:id")
    IpAddressDto getById(@Param("id") Long id);

    //check:
    @Query("SELECT a FROM IpAddress a WHERE a.id = :id")
    Set<IpAddress> getAllIpAddressById(@Param("id") Long id);


    @Query("SELECT e.ipAddress FROM Employee e WHERE e.id = :id")
    Set<IpAddress> getIpAddressByEmployeeId(@Param("id") Long id);
}

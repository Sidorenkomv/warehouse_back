package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.Region;
import com.warehouse_accounting.models.dto.RegionDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.RegionDto(" +
            "r.id," +
            "r.name," +
            "r.socr," +
            "r.code," +
            "r.ocatd)" +
            "FROM Region r " +
            "WHERE r.code LIKE '%00' ORDER BY r.name")
    List<RegionDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.RegionDto(" +
            "r.id," +
            "r.name," +
            "r.socr," +
            "r.code," +
            "r.ocatd)" +
            "FROM Region r WHERE r.id = :id")
    RegionDto getById(@Param("id") Long id);

    @Query("SELECT NEW com.warehouse_accounting.models.dto.RegionDto(" +
            "r.id," +
            "r.name," +
            "r.socr," +
            "r.code," +
            "r.ocatd)" +
            "FROM Region r " +
            "WHERE r.code = :#{(#code.length() > 2) ? #code.substring(0, 2) + '00000000000' : #code + '00000000000'}")
    RegionDto getByCode(@Param("code") String code);
}


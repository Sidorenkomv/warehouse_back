package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.ContactFace;
import com.warehouse_accounting.models.dto.ContactFaceDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactFaceRepository extends JpaRepository<ContactFace, Long> {

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContactFaceDto(" +
            "cf.id," +
            "cf.name," +
            "cf.description," +
            "cf.phone," +
            "cf.email," +
            "cf.externalCode," +
            "a," +
            "cf.position," +
            "cf.linkContractor," +
            "cf.comment" +
            ") " +
            "FROM ContactFace cf " +
            "left join cf.address a")
    List<ContactFaceDto> getAll();

    @Query("SELECT NEW com.warehouse_accounting.models.dto.ContactFaceDto(" +
            "cf.id," +
            "cf.name," +
            "cf.description," +
            "cf.phone," +
            "cf.email," +
            "cf.externalCode," +
            "a," +
            "cf.position," +
            "cf.linkContractor," +
            "cf.comment" +
            ")" +
            " FROM ContactFace cf " +
            "left join Address a on (cf.address.id = a.id) " +
            "WHERE cf.id = :id")
    ContactFaceDto getById(@Param("id") Long id);

}
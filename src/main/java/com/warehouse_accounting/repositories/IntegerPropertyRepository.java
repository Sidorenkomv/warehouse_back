package com.warehouse_accounting.repositories;

import com.warehouse_accounting.models.property.IntegerProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegerPropertyRepository extends JpaRepository<IntegerProperty, Long> {

}


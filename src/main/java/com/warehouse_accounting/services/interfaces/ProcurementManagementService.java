package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.ProcurementManagementDto;

import java.util.List;

public interface ProcurementManagementService {
    List<ProcurementManagementDto> getAll();

    ProcurementManagementDto getById(Long id);

    void create(ProcurementManagementDto invoiceReceivedDto);

    void update(ProcurementManagementDto invoiceReceivedDto);

    void deleteById(Long id);
}

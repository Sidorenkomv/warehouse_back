package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.PostingDto;

import java.util.List;

public interface PostingService {

    List<PostingDto> getAll();

    PostingDto getById(Long id);

    void create(PostingDto dto);

    void update(PostingDto dto);

    void deleteById(Long id);

    List<PostingDto> getAllTest();

    PostingDto getByIdTest(Long id);
}

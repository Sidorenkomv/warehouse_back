package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.PurchaseForecastDto;

import java.util.List;

public interface PurchaseForecastService {

    List<PurchaseForecastDto> getAll();

    PurchaseForecastDto getById(Long id);

    void create(PurchaseForecastDto purchaseForecastDto);

    void update(PurchaseForecastDto purchaseForecastDto);

    void delete(Long id);
}

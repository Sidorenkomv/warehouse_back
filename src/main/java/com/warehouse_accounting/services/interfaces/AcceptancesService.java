package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.AcceptancesDto;

import java.util.List;

public interface AcceptancesService {

    List<AcceptancesDto> getAll();

    AcceptancesDto getById(Long id);

    void create(AcceptancesDto acceptancesDto);

    void update(AcceptancesDto acceptancesDto);

    void deleteById(Long id);
}

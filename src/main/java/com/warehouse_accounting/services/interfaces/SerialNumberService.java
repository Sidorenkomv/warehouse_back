package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.SerialNumberDto;

import java.util.List;

public interface SerialNumberService {
    List<SerialNumberDto> getAll();

    SerialNumberDto getById(Long id);

    void create(SerialNumberDto serialNumberDto);

    void update(SerialNumberDto serialNumberDto);

    void deleteById(Long id);
}

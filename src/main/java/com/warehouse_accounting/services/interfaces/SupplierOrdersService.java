package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.SupplierOrdersDto;

import java.util.List;

public interface SupplierOrdersService {

    List<SupplierOrdersDto> getAll();

    SupplierOrdersDto getById(Long id);

    void create(SupplierOrdersDto supplierOrdersDto);

    void update(SupplierOrdersDto supplierOrdersDto);

    void deleteById(Long id);
}

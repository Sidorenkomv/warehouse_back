package com.warehouse_accounting.services.interfaces;


import com.warehouse_accounting.models.dto.InvoicesIssuedDto;

import java.util.List;

public interface InvoicesIssuedService {
    List<InvoicesIssuedDto> getAll();

    InvoicesIssuedDto getById(Long id);

    void create(InvoicesIssuedDto invoicesIssuedDto);

    void update(InvoicesIssuedDto invoicesIssuedDto);

    void deleteById(Long id);

}


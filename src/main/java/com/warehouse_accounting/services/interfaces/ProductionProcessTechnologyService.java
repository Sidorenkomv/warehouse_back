package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.ProductionProcessTechnologyDto;

import java.util.List;

public interface ProductionProcessTechnologyService {
    List<ProductionProcessTechnologyDto> getAll();

    ProductionProcessTechnologyDto getById(Long id);

    void create(ProductionProcessTechnologyDto productionProcessTechnologyDto);

    void update(ProductionProcessTechnologyDto productionProcessTechnologyDto);

    void deleteById(Long id);
}

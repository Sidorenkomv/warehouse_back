package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.BankAccount;
import com.warehouse_accounting.models.dto.BankAccountDto;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BankAccountService {

    List<BankAccountDto> getAll();

    BankAccountDto getById(Long id);

    void create(BankAccountDto bankAccountDto);

    void update(BankAccountDto bankAccountDto);

    void deleteById(Long id);
}

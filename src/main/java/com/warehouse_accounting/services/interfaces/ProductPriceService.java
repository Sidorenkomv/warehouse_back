package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.models.dto.ProductPriceForPriceListDto;

import java.util.List;

public interface ProductPriceService {

    List<ProductPriceForPriceListDto> getAll();

    ProductPriceForPriceListDto getById(Long id);

    ProductPriceDto getProductPriceById(Long id);

    List<ProductPriceForPriceListDto> getListProductPriceByProductId(Long id);

    void create(List<ProductPriceDto> productPriceDto);

    void update(ProductPriceDto productPriceDto);

    void deleteById(Long id);
}

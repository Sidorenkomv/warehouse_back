package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.TypeOfContractor;
import com.warehouse_accounting.models.dto.TypeOfContractorDto;

import java.util.List;

public interface TypeOfContractorService {

    List<TypeOfContractorDto> getAll();

    TypeOfContractorDto getById(Long id);

    TypeOfContractor getByName(String name);

    void create(TypeOfContractorDto tcDTO);

    void deleteById(Long Id);

    void update(TypeOfContractorDto tcDTO);
}


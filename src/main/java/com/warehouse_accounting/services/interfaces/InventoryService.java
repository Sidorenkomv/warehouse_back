package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.InventoryDto;

import java.util.List;

public interface InventoryService {

    List<InventoryDto> getAll();

    List<InventoryDto> getAllTest();

    InventoryDto getById(Long id);

    InventoryDto getByIdTest(Long id);

    void create(InventoryDto dto);

    void update(InventoryDto dto);

    void deleteById(Long id);
}

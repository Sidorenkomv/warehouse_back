package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.AdditionalFieldDto;

import java.util.List;

public interface AdditionalFieldService {
    List<AdditionalFieldDto> getAll();

    AdditionalFieldDto getById(Long id);

    AdditionalFieldDto create(AdditionalFieldDto additionalFieldDto);

    AdditionalFieldDto update(AdditionalFieldDto additionalFieldDto);

    void deleteById(Long id);
}

package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.IntroductionDto;

import java.util.List;

public interface IntroductionService {
    List<IntroductionDto> getAll();

    IntroductionDto getById(Long id);

    void create(IntroductionDto introductionDto);

    void update(IntroductionDto introductionDto);

    void deleteById(Long id);
}

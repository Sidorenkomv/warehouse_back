package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;

import java.util.List;

public interface GoodsToRealizeGetService {

    GoodsToRealizeGetDto getById(Long id);

    List<GoodsToRealizeGetDto> getAll();

    void create(GoodsToRealizeGetDto goodsToRealizeGet);

    void update(GoodsToRealizeGetDto goodsToRealizeGet);

    void deleteById(Long id);


}

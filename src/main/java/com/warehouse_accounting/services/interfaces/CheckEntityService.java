package com.warehouse_accounting.services.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckEntityService {
    void checkExist(Long id, JpaRepository repository, String objectName);

    void checkNull(Object object, String message);
}

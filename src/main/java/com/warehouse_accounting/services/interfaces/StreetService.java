package com.warehouse_accounting.services.interfaces;
import com.warehouse_accounting.models.Street;
import com.warehouse_accounting.models.dto.StreetDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StreetService {

    List<StreetDto> getAll(String regionCityCode);

    List<StreetDto> getSlice(int offset, int limit, String name, String regionCityCode);

    int getCount(String name, String regionCityCode);

    StreetDto getById(Long id);

    void create(StreetDto streetDto);

    void createAll(Iterable<Street> streets);

    void update(StreetDto streetDto);

    void deleteById(Long id);
}


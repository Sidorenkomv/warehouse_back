package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;

import java.util.List;

public interface GoodsToRealizeGiveService {

    GoodsToRealizeGiveDto getById(Long id);

    List<GoodsToRealizeGiveDto> getAll();

    void create(GoodsToRealizeGiveDto goodsToRealizeGiveDto);

    void update(GoodsToRealizeGiveDto goodsToRealizeGiveDto);

    void deleteById(Long id);

}

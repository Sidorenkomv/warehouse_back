package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.DiscountTypeDto;
import java.util.List;


public interface DiscountTypeService {
    List<DiscountTypeDto> getAll();

    DiscountTypeDto getById(Long id);

    void create(DiscountTypeDto discountTypeDto);

    void update(DiscountTypeDto discountTypeDto);

    void deleteById(Long id);
}

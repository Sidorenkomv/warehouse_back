package com.warehouse_accounting.services.interfaces;
import com.warehouse_accounting.models.dto.RegionDto;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegionService {

    List<RegionDto> getAll();

    RegionDto getById(Long id);

    RegionDto getByCode(String code);

    void create(RegionDto regionDto);

    void update(RegionDto regionDto);

    void deleteById(Long id);

}


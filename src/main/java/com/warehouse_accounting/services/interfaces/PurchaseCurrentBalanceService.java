package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;


import java.util.List;

public interface PurchaseCurrentBalanceService {
    List<PurchaseCurrentBalanceDto> getAll();

    PurchaseCurrentBalanceDto getById(Long id);

    void create(PurchaseCurrentBalanceDto purchaseCurrentBalanceDto);

    void update(PurchaseCurrentBalanceDto purchaseCurrentBalanceDto);

    void delete(Long id);
}

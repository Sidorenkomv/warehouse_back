package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.exceptions.EntityFoundException;
import com.warehouse_accounting.exceptions.EntityNotFoundException;
import com.warehouse_accounting.models.Employee;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface EmployeeService extends UserDetailsService {
    List<Employee> findAll();
    Employee findById(Long id) throws EntityNotFoundException;
    Employee create(Employee employee) throws EntityFoundException;
    Employee update(Employee employee);
    void deleteById(Long id) throws EntityNotFoundException;
}

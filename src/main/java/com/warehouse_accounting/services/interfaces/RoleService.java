package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.models.dto.RoleDto;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(Long id);

    void create(Role dto);

    void update(Role dto);

    void deleteById(Long id);
}

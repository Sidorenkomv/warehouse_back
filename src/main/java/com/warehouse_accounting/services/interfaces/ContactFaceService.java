package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.ContactFaceDto;

import java.util.List;

public interface ContactFaceService {

    List<ContactFaceDto> getAll();

    ContactFaceDto getById(Long id);

    void create(ContactFaceDto dto);

    void update(ContactFaceDto dto);

    void deleteById(Long id);
}

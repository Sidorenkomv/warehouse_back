package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;


import java.util.List;

public interface PurchaseHistoryOfSalesService {
    List<PurchaseHistoryOfSalesDto> getAll();

    PurchaseHistoryOfSalesDto getById(Long id);

    void create(PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto);

    void update(PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto);

    void deleteById(Long id);
}

package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.DocumentDto;

import java.util.List;

public interface DocumentService {

    List<DocumentDto> getAll();

    DocumentDto getById(Long id);

    void create(DocumentDto documentDto);

    void update(DocumentDto documentDto);

    void deleteById(Long id);
}

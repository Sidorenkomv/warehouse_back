package com.warehouse_accounting.services.interfaces;

import com.warehouse_accounting.models.dto.PurchasesManagementDto;

import java.util.List;

public interface PurchasesManagementService {
    List<PurchasesManagementDto> getAll();

    PurchasesManagementDto getById(Long id);

    void create(PurchasesManagementDto purchasesManagementDto);

    void update(PurchasesManagementDto purchasesManagementDto);

    void deleteById(Long id);
}

package com.warehouse_accounting.services;

import com.warehouse_accounting.models.dto.StatusDto;
import com.warehouse_accounting.repositories.StatusRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.StatusService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    private final CheckEntityService checkEntityService;

    public StatusServiceImpl(StatusRepository statusRepository, CheckEntityService checkEntityService){
        this.statusRepository = statusRepository;
        this.checkEntityService = checkEntityService;
    }
    @Override
    public List<StatusDto> getAllByNameOfClass(String nameOfClass) {
        return statusRepository.getAllByNameOfClass(nameOfClass);
    }

    @Override
    public List<StatusDto> getAll() {
        return statusRepository.getAll();
    }

    @Override
    public StatusDto getById(Long id) {
        checkEntityService.checkExist(id, statusRepository, "Status");
        return statusRepository.getById(id);
    }

    @Override
    public void create(StatusDto dto) {
        statusRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(StatusDto dto) {
        checkEntityService.checkExist(dto.getId(), statusRepository, "Status");
        statusRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, statusRepository, "Status");
        statusRepository.deleteById(id);
    }
}

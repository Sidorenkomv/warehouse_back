package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.ProductionTasks;
import com.warehouse_accounting.models.dto.ProductionTasksDto;
import com.warehouse_accounting.repositories.ProductionTasksRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductionTasksService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductionTasksServiceImpl implements ProductionTasksService {

    private static final String NAME_OF_CLASS = "ProductionTasks";
    private final ProductionTasksRepository productionTasksRepository;

    private final CheckEntityService checkEntityService;

    public ProductionTasksServiceImpl(ProductionTasksRepository productionTasksRepository, CheckEntityService checkEntityService) {
        this.productionTasksRepository = productionTasksRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ProductionTasksDto> getAll() {
        List<ProductionTasks> listAllTasks = productionTasksRepository.findAll();
        return listAllTasks.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductionTasksDto getById(Long id) {
        checkEntityService.checkExist(id, productionTasksRepository, NAME_OF_CLASS);
        return ConverterDto.convertToDto(productionTasksRepository.findById(id).get());
    }

    @Override
    public void create(ProductionTasksDto productionTasksDto) {
        ProductionTasks productionTasks = ConverterDto.convertToModel(productionTasksDto);
        productionTasks.setDateOfCreate(LocalDate.now());
        productionTasksRepository.save(productionTasks);
    }

    @Override
    public void update(ProductionTasksDto productionTasksDto) {
        checkEntityService.checkExist(productionTasksDto.getId(), productionTasksRepository, NAME_OF_CLASS);

        ProductionTasks productionTasks = ConverterDto.convertToModel(productionTasksDto);
        productionTasks.setDateOfEdit(LocalDate.now());
        productionTasksRepository.save(productionTasks);
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, productionTasksRepository, NAME_OF_CLASS);
        productionTasksRepository.deleteById(id);
    }
}


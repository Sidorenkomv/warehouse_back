package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.IpNetworkDto;
import com.warehouse_accounting.repositories.IpNetworkRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.IpNetworkService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class IpNetworkServiceImpl implements IpNetworkService {

    private final IpNetworkRepository ipNetworkRepository;

    private final CheckEntityService checkEntityService;

    public IpNetworkServiceImpl(IpNetworkRepository ipNetworkRepository, CheckEntityService checkEntityService) {
        this.ipNetworkRepository = ipNetworkRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<IpNetworkDto> getAll() {
        return ipNetworkRepository.getAll();
    }

    @Override
    public IpNetworkDto getById(Long id) {
        checkEntityService.checkExist(id, ipNetworkRepository, "IpNetwork");
        return ipNetworkRepository.getById(id);
    }

    @Override
    public void create(IpNetworkDto ipNetworkDto) {
        ipNetworkRepository.save(ConverterDto.convertToModel(ipNetworkDto));
    }

    @Override
    public void update(IpNetworkDto ipNetworkDto) {
        checkEntityService.checkExist(ipNetworkDto.getId(), ipNetworkRepository, "IpNetwork");
        ipNetworkRepository.save(ConverterDto.convertToModel(ipNetworkDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, ipNetworkRepository, "IpNetwork");
        ipNetworkRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.UnitsOfMeasureDto;
import com.warehouse_accounting.repositories.UnitsOfMeasureRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.UnitsOfMeasureService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UnitsOfMeasureServiceImpl implements UnitsOfMeasureService {

    private final UnitsOfMeasureRepository unitsOfMeasureRepository;

    private final CheckEntityService checkEntityService;

    public UnitsOfMeasureServiceImpl(UnitsOfMeasureRepository unitsOfMeasureRepository, CheckEntityService checkEntityService) {
        this.unitsOfMeasureRepository = unitsOfMeasureRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<UnitsOfMeasureDto> getAll() {
        return unitsOfMeasureRepository.getAll();
    }

    @Override
    public UnitsOfMeasureDto getById(Long id) {
        checkEntityService.checkExist(id, unitsOfMeasureRepository, "UnitsOfMeasure");
        return unitsOfMeasureRepository.getById(id);
    }

    @Override
    public void create(UnitsOfMeasureDto dto) {
        unitsOfMeasureRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(UnitsOfMeasureDto dto) {
        checkEntityService.checkExist(dto.getId(), unitsOfMeasureRepository, "UnitsOfMeasure");
        unitsOfMeasureRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, unitsOfMeasureRepository, "UnitsOfMeasure");
        unitsOfMeasureRepository.deleteById(id);
    }
}
package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AcceptancesDto;
import com.warehouse_accounting.repositories.AcceptancesRepository;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.AcceptancesService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AcceptancesServiceImpl implements AcceptancesService {

    private final AcceptancesRepository acceptancesRepository;
    private final CheckEntityService checkEntityService;

    private final TaskRepository taskRepository;

    private final FileRepository fileRepository;

    private final ProductRepository productRepository;

    @Autowired
    public AcceptancesServiceImpl(AcceptancesRepository acceptancesRepository, CheckEntityService checkEntityService,
                                  TaskRepository taskRepository, FileRepository fileRepository, ProductRepository productRepository) {
        this.acceptancesRepository = acceptancesRepository;
        this.checkEntityService = checkEntityService;
        this.taskRepository = taskRepository;
        this.fileRepository = fileRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<AcceptancesDto> getAll() {
        List<AcceptancesDto> acceptancesDtos = acceptancesRepository.getAll();
        for (AcceptancesDto acceptancesDto : acceptancesDtos) {
            acceptancesDto.setTasksDto(taskRepository.getListTaskById(acceptancesDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));

            acceptancesDto.setFilesDto(fileRepository.getListFileById(acceptancesDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));

            acceptancesDto.setProductDtos(productRepository.getProductsByDocumentId(acceptancesDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }
        return acceptancesDtos;
    }

    @Override
    public AcceptancesDto getById(Long id) {
        checkEntityService.checkExist(id, acceptancesRepository, "Acceptances");
        AcceptancesDto acceptancesDto = acceptancesRepository.getById(id);

        acceptancesDto.setTasksDto(taskRepository.getListTaskById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));

        acceptancesDto.setFilesDto(fileRepository.getListFileById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));

        acceptancesDto.setProductDtos(productRepository.getProductsByDocumentId(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return acceptancesDto;
    }

    @Override
    public void create(AcceptancesDto acceptancesDto) {
        acceptancesRepository.save(ConverterDto.convertToModel(acceptancesDto));
    }

    @Override
    public void update(AcceptancesDto acceptancesDto) {
        checkEntityService.checkExist(acceptancesDto.getId(), acceptancesRepository, "Acceptances");
        acceptancesRepository.save(ConverterDto.convertToModel(acceptancesDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, acceptancesRepository, "Acceptances");
        acceptancesRepository.deleteById(id);
    }
}

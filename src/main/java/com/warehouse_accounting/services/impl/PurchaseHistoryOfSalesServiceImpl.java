package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PurchaseHistoryOfSalesDto;
import com.warehouse_accounting.repositories.PurchaseHistoryOfSalesRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PurchaseHistoryOfSalesService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseHistoryOfSalesServiceImpl implements PurchaseHistoryOfSalesService {

    private final PurchaseHistoryOfSalesRepository purchaseHistoryOfSalesRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public PurchaseHistoryOfSalesServiceImpl(PurchaseHistoryOfSalesRepository purchaseHistoryOfSalesRepository, CheckEntityService checkEntityService) {
        this.purchaseHistoryOfSalesRepository = purchaseHistoryOfSalesRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PurchaseHistoryOfSalesDto> getAll() {
        return purchaseHistoryOfSalesRepository.getAll();
    }

    @Override
    public PurchaseHistoryOfSalesDto getById(Long id) {
        checkEntityService.checkExist(id, purchaseHistoryOfSalesRepository, "PurchaseHistoryOfSales");
        return purchaseHistoryOfSalesRepository.getById(id);
    }

    @Override
    public void create(PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto) {
        purchaseHistoryOfSalesRepository.save(ConverterDto.convertToModel(purchaseHistoryOfSalesDto));
    }

    @Override
    public void update(PurchaseHistoryOfSalesDto purchaseHistoryOfSalesDto) {
        checkEntityService.checkExist(purchaseHistoryOfSalesDto.getId(), purchaseHistoryOfSalesRepository, "PurchaseHistoryOfSales");

        purchaseHistoryOfSalesRepository.save(ConverterDto.convertToModel(purchaseHistoryOfSalesDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, purchaseHistoryOfSalesRepository, "PurchaseHistoryOfSales");
        purchaseHistoryOfSalesRepository.deleteById(id);
    }
}

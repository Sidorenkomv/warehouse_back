package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.TypeOfPriceDto;
import com.warehouse_accounting.repositories.TypeOfPriceRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TypeOfPriceService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class TypeOfPriceServiceImpl implements TypeOfPriceService {

    private final TypeOfPriceRepository typeOfPriceRepository;

    private final CheckEntityService checkEntityService;
    public TypeOfPriceServiceImpl(TypeOfPriceRepository repository, CheckEntityService checkEntityService) {
        this.typeOfPriceRepository = repository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<TypeOfPriceDto> getAll() {
        return typeOfPriceRepository.getAll();
    }

    @Override
    public TypeOfPriceDto getById(Long id) {
        checkEntityService.checkExist(id, typeOfPriceRepository, "TypeOfPrice");
        return typeOfPriceRepository.getById(id);
    }

    @Override
    public void create(TypeOfPriceDto typeOfPriceDto) {
        typeOfPriceRepository.save(ConverterDto.convertToModel(typeOfPriceDto));
    }

    @Override
    public void update(TypeOfPriceDto typeOfPriceDto) {
        checkEntityService.checkExist(typeOfPriceDto.getId(), typeOfPriceRepository, "TypeOfPrice");
        typeOfPriceRepository.save(ConverterDto.convertToModel(typeOfPriceDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, typeOfPriceRepository, "TypeOfPrice");
        typeOfPriceRepository.deleteById(id);
    }
}
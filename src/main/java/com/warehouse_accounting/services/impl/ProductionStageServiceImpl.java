package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.ProductionStage;
import com.warehouse_accounting.models.dto.ProductionStageDto;
import com.warehouse_accounting.repositories.DepartmentRepository;
import com.warehouse_accounting.repositories.EmployeeRepository;
import com.warehouse_accounting.repositories.ProductionStageRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductionStageService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProductionStageServiceImpl implements ProductionStageService {
    private final ProductionStageRepository productionStageRepository;
    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    private final CheckEntityService checkEntityService;

    public ProductionStageServiceImpl(ProductionStageRepository productionStageRepository,
                                      EmployeeRepository employeeRepository,
                                      DepartmentRepository departmentRepository, CheckEntityService checkEntityService) {
        this.productionStageRepository = productionStageRepository;
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ProductionStageDto> getAll(boolean archived) {
        return productionStageRepository.getAll(archived);
    }

    @Override
    public ProductionStageDto getById(Long id) {
        checkEntityService.checkExist(id, productionStageRepository, "ProductionStage");
        return productionStageRepository.getById(id);
    }

    @Override
    public void create(ProductionStageDto productionStageDto) {
        productionStageDto.setEditorEmployeeId(productionStageDto.getOwnerEmployeeId());
        productionStageDto.setDateOfEdit(new Date());
        ProductionStage productionStage = ConverterDto.convertToModel(productionStageDto);
        if (!employeeRepository.existsById(productionStageDto.getEditorEmployeeId())) {
            productionStage.setOwnerEmployee(null);
            productionStage.setEditorEmployee(null);
        }
        if (!departmentRepository.existsById(productionStage.getOwnerDepartment().getId())) {
            productionStage.setOwnerDepartment(null);
        }
        System.out.println("hello");
        System.out.println(productionStageDto);
        productionStageRepository.save(productionStage);
    }

    @Override
    public void update(ProductionStageDto productionStageDto) {
        checkEntityService.checkExist(productionStageDto.getId(), productionStageRepository, "ProductionStage");

        productionStageDto.setDateOfEdit(new Date());
        ProductionStage productionStage = ConverterDto.convertToModel(productionStageDto);
        if (!employeeRepository.existsById(productionStage.getOwnerEmployee().getId())) {
            productionStage.setOwnerEmployee(null);
        }
        if (!employeeRepository.existsById(productionStage.getEditorEmployee().getId())) {
            productionStage.setEditorEmployee(null);
        }
        if (!departmentRepository.existsById(productionStage.getOwnerDepartment().getId())) {
            productionStage.setOwnerDepartment(null);
        }
        productionStageRepository.save(productionStage);
    }

    @Override
    public void delete(Long id) {
        checkEntityService.checkExist(id, productionStageRepository, "ProductionStage");
        productionStageRepository.deleteById(id);
    }
}

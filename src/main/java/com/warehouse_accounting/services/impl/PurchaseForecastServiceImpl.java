package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PurchaseForecastDto;
import com.warehouse_accounting.repositories.PurchaseForecastRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PurchaseForecastService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseForecastServiceImpl implements PurchaseForecastService {

    private final PurchaseForecastRepository purchaseForecastRepository;

    private final CheckEntityService checkEntityService;
    @Autowired
    public PurchaseForecastServiceImpl(PurchaseForecastRepository purchaseForecastRepository, CheckEntityService checkEntityService) {
        this.purchaseForecastRepository = purchaseForecastRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PurchaseForecastDto> getAll() {
        return purchaseForecastRepository.getAll();
    }

    @Override
    public PurchaseForecastDto getById(Long id) {
        checkEntityService.checkExist(id, purchaseForecastRepository, "PurchaseForecast");
        return purchaseForecastRepository.getById(id);
    }

    @Override
    public void create(PurchaseForecastDto purchaseForecastDto) {
        purchaseForecastRepository.save(ConverterDto.convertToModel(purchaseForecastDto));
    }

    @Override
    public void update(PurchaseForecastDto purchaseForecastDto) {
        checkEntityService.checkExist(purchaseForecastDto.getId(), purchaseForecastRepository, "PurchaseForecast");
        purchaseForecastRepository.save(ConverterDto.convertToModel(purchaseForecastDto));
    }

    @Override
    public void delete(Long id) {
        checkEntityService.checkExist(id, purchaseForecastRepository, "PurchaseForecast");
        purchaseForecastRepository.deleteById(id);
    }
}

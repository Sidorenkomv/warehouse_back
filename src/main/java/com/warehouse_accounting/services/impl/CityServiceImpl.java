package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.repositories.CityRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    private final CheckEntityService checkEntityService;

    public CityServiceImpl(CityRepository cityRepository, CheckEntityService checkEntityService) {
        this.cityRepository = cityRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CityDto> getAll(String regionCode) {
        return cityRepository.getAll(regionCode);
    }

    @Override
    public List<CityDto> getSlice(int offset, int limit, String name, String regionCode) {
        return cityRepository.getSlice(name,  regionCode, PageRequest.of(offset, limit)).getContent();
    }

    public int getCount(String name, String regionCode) {
        return cityRepository.getCount(name, regionCode);
    }

    @Override
    public CityDto getById(Long id) {
        checkEntityService.checkExist(id, cityRepository, "City");
        return cityRepository.getById(id);
    }

    @Override
    public CityDto getByCode(String code) {
        CityDto dto = cityRepository.getByCode(code);
        checkEntityService.checkNull(dto, "CityDto с кодом " + code + " не существует");
        return cityRepository.getByCode(code);
    }

    @Override
    public void create(CityDto cityDto) {
        cityRepository.save(ConverterDto.convertToModel(cityDto));
    }

    @Override
    public void update(CityDto cityDto) {
        checkEntityService.checkExist(cityDto.getId(), cityRepository, "City");
        cityRepository.save(ConverterDto.convertToModel(cityDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, cityRepository, "City");
        cityRepository.deleteById(id);

    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AttributeOfCalculationObjectDto;
import com.warehouse_accounting.repositories.AttributeOfCalculationObjectRepository;
import com.warehouse_accounting.services.interfaces.AttributeOfCalculationObjectService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AttributeOfCalculationObjectServiceIml implements AttributeOfCalculationObjectService {

    private final AttributeOfCalculationObjectRepository attributeOfCalculationObjectRepository;

    private final CheckEntityService checkEntityService;


    public AttributeOfCalculationObjectServiceIml(AttributeOfCalculationObjectRepository repository, CheckEntityService checkEntityService) {
        this.attributeOfCalculationObjectRepository = repository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<AttributeOfCalculationObjectDto> getAll() {
        return attributeOfCalculationObjectRepository.getAll();
    }

    @Override
    public AttributeOfCalculationObjectDto getById(Long id) {
        checkEntityService.checkExist(id, attributeOfCalculationObjectRepository, "AttributeOfCalculationObject");
        return attributeOfCalculationObjectRepository.getById(id);
    }

    @Override
    public void create(AttributeOfCalculationObjectDto dto) {
        attributeOfCalculationObjectRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(AttributeOfCalculationObjectDto dto) {
        checkEntityService.checkExist(dto.getId(), attributeOfCalculationObjectRepository, "AttributeOfCalculationObject");
        attributeOfCalculationObjectRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, attributeOfCalculationObjectRepository, "AttributeOfCalculationObject");
        attributeOfCalculationObjectRepository.deleteById(id);
    }
}

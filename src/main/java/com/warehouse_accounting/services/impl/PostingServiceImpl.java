package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PostingDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.PostingRepository;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PostingService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
@Transactional
public class PostingServiceImpl implements PostingService {


    private final PostingRepository postingRepository;
    private final WarehouseRepository warehouseRepository;
    private final CompanyRepository companyRepository;

    private final CheckEntityService checkEntityService;
    public PostingServiceImpl(PostingRepository postingRepository,
                              WarehouseRepository warehouseRepository,
                              CompanyRepository companyRepository, CheckEntityService checkEntityService) {
        this.postingRepository = postingRepository;
        this.warehouseRepository = warehouseRepository;
        this.companyRepository = companyRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PostingDto> getAll() {
        List<PostingDto> dtos = postingRepository.getAll();
        for (PostingDto postingDto : dtos) {
            postingDto.setWarehouseTo(warehouseRepository.getById(postingDto.getWarehouseTo().getId()));
            postingDto.setCompany(companyRepository.getById(postingDto.getCompany().getId()));
        }
        return dtos;
    }

    @Override
    public PostingDto getById(Long id) {
        checkEntityService.checkExist(id, postingRepository, "Posting");

        PostingDto postingDto = postingRepository.getById(id);
//        postingDto.setWarehouseTo(warehouseRepository.getById(postingDto.getWarehouseTo().getId()));
//        postingDto.setCompany(companyRepository.getById(postingDto.getCompany().getId()));
        return postingDto;
    }

    @Override
    public void create(PostingDto dto) {postingRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(PostingDto dto) {
        checkEntityService.checkExist(dto.getId(), postingRepository, "Posting");
        postingRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, postingRepository, "Posting");
        postingRepository.deleteById(id);
    }

    @Override
    public List<PostingDto> getAllTest() {
        return postingRepository.getAll();
    }

    @Override
    public PostingDto getByIdTest(Long id) {
        return postingRepository.getById(id);
    }
}

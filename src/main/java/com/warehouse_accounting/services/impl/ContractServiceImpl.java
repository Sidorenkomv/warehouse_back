package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ContractDto;
import com.warehouse_accounting.repositories.BankAccountRepository;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.ContractRepository;
import com.warehouse_accounting.repositories.ContractorRepository;
import com.warehouse_accounting.repositories.DepartmentRepository;
import com.warehouse_accounting.repositories.EmployeeRepository;
import com.warehouse_accounting.repositories.LegalDetailRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ContractService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContractServiceImpl implements ContractService {

    private final ContractRepository contractRepository;
    private final CompanyRepository companyRepository;
    private final BankAccountRepository bankAccountRepository;
    private final ContractorRepository contractorRepository;

    private final DepartmentRepository departmentRepository;

    private final EmployeeRepository employeeRepository;

    private final LegalDetailRepository legalDetailRepository;

    private final CheckEntityService checkEntityService;

    public ContractServiceImpl(
            ContractRepository contractRepository,
            CompanyRepository companyRepository,
            BankAccountRepository bankAccountRepository,
            ContractorRepository contractorRepository,
            DepartmentRepository departmentRepository, EmployeeRepository employeeRepository, LegalDetailRepository legalDetailRepository,
            CheckEntityService checkEntityService) {
        this.contractRepository = contractRepository;
        this.companyRepository = companyRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.contractorRepository = contractorRepository;
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
        this.legalDetailRepository = legalDetailRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ContractDto> getAll() {
        return contractRepository.getAll();
    }

    @Override
    public ContractDto getById(Long id) {
        checkEntityService.checkExist(id, contractRepository, "Contract");

        return contractRepository.getById(id);
    }

    @Override
    public void create(ContractDto contractDto) {
        contractDto.setEnabled(true);
        contractRepository.save(ConverterDto.convertToModel(contractDto));
    }

    @Override
    public void update(ContractDto contractDto) {
        checkEntityService.checkExist(contractDto.getId(), contractRepository, "Contract");
        contractRepository.save(ConverterDto.convertToModel(contractDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, contractRepository, "Contract");
        contractRepository.deleteById(id);
    }
}

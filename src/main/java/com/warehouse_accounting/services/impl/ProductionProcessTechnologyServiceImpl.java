package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.ProductionProcessTechnology;
import com.warehouse_accounting.models.dto.ProductionProcessTechnologyDto;
import com.warehouse_accounting.repositories.ProductionProcessTechnologyRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductionProcessTechnologyService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductionProcessTechnologyServiceImpl implements ProductionProcessTechnologyService {

    private final ProductionProcessTechnologyRepository productionProcessTechnologyRepository;

    private final CheckEntityService checkEntityService;

    public ProductionProcessTechnologyServiceImpl(ProductionProcessTechnologyRepository productionProcessTechnologyRepository, CheckEntityService checkEntityService) {
        this.productionProcessTechnologyRepository = productionProcessTechnologyRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ProductionProcessTechnologyDto> getAll() {
        List<ProductionProcessTechnology> listAllProductionProcessTechnology = productionProcessTechnologyRepository.findAll();
        List<ProductionProcessTechnologyDto> listAllProductionProcessTechnologyDTO = listAllProductionProcessTechnology.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
        return listAllProductionProcessTechnologyDTO;
    }

    @Override
    public ProductionProcessTechnologyDto getById(Long id) {
        checkEntityService.checkExist(id, productionProcessTechnologyRepository, "ProductionProcessTechnology");
        ProductionProcessTechnologyDto productionProcessTechnologyDto = ConverterDto.convertToDto(productionProcessTechnologyRepository.findById(id).get());
        return productionProcessTechnologyDto;
    }

    @Override
    public void create(ProductionProcessTechnologyDto productionProcessTechnologyDto) {
        ProductionProcessTechnology productionProcessTechnology = ConverterDto.convertToModel(productionProcessTechnologyDto);
        productionProcessTechnology.setDateOfEdit(new Date());
        productionProcessTechnologyRepository.save(productionProcessTechnology);
    }

    @Override
    public void update(ProductionProcessTechnologyDto productionProcessTechnologyDto) {
        checkEntityService.checkExist(productionProcessTechnologyDto.getId(), productionProcessTechnologyRepository, "ProductionProcessTechnology");

        ProductionProcessTechnology productionProcessTechnology = ConverterDto.convertToModel(productionProcessTechnologyDto);
        productionProcessTechnology.setDateOfEdit(new Date());
        productionProcessTechnologyRepository.save(productionProcessTechnology);
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, productionProcessTechnologyRepository, "ProductionProcessTechnology");
        productionProcessTechnologyRepository.deleteById(id);
    }
}


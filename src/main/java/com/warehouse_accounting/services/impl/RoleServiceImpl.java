package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.Role;
import com.warehouse_accounting.repositories.RoleRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    private final CheckEntityService checkEntityService;

    public RoleServiceImpl(RoleRepository roleRepository, CheckEntityService checkEntityService) {
        this.roleRepository = roleRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role getById(Long id) {
        checkEntityService.checkExist(id, roleRepository, "Role");
        return roleRepository.getById(id);
    }

    @Override
    public void create(Role role) {
        roleRepository.save(role);
    }

    @Override
    public void update(Role role) {
        checkEntityService.checkExist(role.getId(), roleRepository, "Role");
        roleRepository.save(role);
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, roleRepository, "Role");
        roleRepository.deleteById(id);
    }
}


package com.warehouse_accounting.services.impl;


import com.warehouse_accounting.models.dto.PointOfSalesDto;
import com.warehouse_accounting.repositories.PointOfSalesRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PointOfSalesServiceImpl implements PointOfSalesService {
    PointOfSalesRepository pointOfSalesRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public PointOfSalesServiceImpl(PointOfSalesRepository pointOfSalesRepository, CheckEntityService checkEntityService) {
        this.pointOfSalesRepository = pointOfSalesRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PointOfSalesDto> getAll() {

        return pointOfSalesRepository.getAll();
    }

    @Override
    public PointOfSalesDto getById(Long id) {
        checkEntityService.checkExist(id, pointOfSalesRepository, "PointOfSales");
        return pointOfSalesRepository.getById(id);
    }

    @Override
    public void create(PointOfSalesDto pointOfSalesDto) {
        pointOfSalesRepository.save(ConverterDto.convertPointOfSalesDtoToModel(pointOfSalesDto));
    }

    @Override
    public void update(PointOfSalesDto pointOfSalesDto) {
        checkEntityService.checkExist(pointOfSalesDto.getId(), pointOfSalesRepository, "PointOfSales");
        pointOfSalesRepository.save(ConverterDto.convertPointOfSalesDtoToModel(pointOfSalesDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, pointOfSalesRepository, "PointOfSales");
        pointOfSalesRepository.getById(id);
    }
}


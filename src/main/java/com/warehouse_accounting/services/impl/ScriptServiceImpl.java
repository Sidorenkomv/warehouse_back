package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ScriptDto;
import com.warehouse_accounting.repositories.ScriptRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ScriptService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ScriptServiceImpl implements ScriptService {

    private final ScriptRepository scriptRepository;

    private final CheckEntityService checkEntityService;

    public ScriptServiceImpl(ScriptRepository scriptRepository, CheckEntityService checkEntityService) {
        this.scriptRepository = scriptRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ScriptDto> getAll() {
        return scriptRepository.getAll();
    }

    @Override
    public ScriptDto getById(Long id) {
        checkEntityService.checkExist(id, scriptRepository, "Script");
        return scriptRepository.getById(id);
    }

    @Override
    public void create(ScriptDto dto) {
        scriptRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(ScriptDto dto) {
        checkEntityService.checkExist(dto.getId(), scriptRepository, "Script");
        scriptRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, scriptRepository, "Script");
        scriptRepository.deleteById(id);
    }
}

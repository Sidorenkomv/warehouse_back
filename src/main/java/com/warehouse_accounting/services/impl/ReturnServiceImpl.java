package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ReturnDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.repositories.ReturnRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ReturnService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReturnServiceImpl implements ReturnService {
    private final ReturnRepository returnRepository;
    private final ProductRepository productRepository;
    private final FileRepository fileRepository;
    private final TaskRepository taskRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public ReturnServiceImpl(ReturnRepository returnRepository, ProductRepository productRepository,
                             FileRepository fileRepository, TaskRepository taskRepository, CheckEntityService checkEntityService) {
        this.returnRepository = returnRepository;
        this.productRepository = productRepository;
        this.fileRepository = fileRepository;
        this.taskRepository = taskRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ReturnDto> getAll() {
        List<ReturnDto> returnDtos = returnRepository.getAll();
        for (ReturnDto dto : returnDtos) {
            dto.setFileDtos(fileRepository
                    .getFileReturnById(dto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
            dto.setTaskDtos(taskRepository
                    .getListTaskById(dto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
            dto.setProductDtos(productRepository
                    .getProductReturnById(dto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }

        return returnDtos;
    }

    @Override
    public ReturnDto getById(Long id) {
        checkEntityService.checkExist(id, returnRepository, "Return");

        ReturnDto returnDto = returnRepository.getById(id);
        returnDto.setFileDtos(fileRepository
                .getFileReturnById(returnDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        returnDto.setTaskDtos(taskRepository.
                getListTaskOfContructorById(returnDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        returnDto.setProductDtos(productRepository
                .getProductReturnById(returnDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return returnDto;
    }

    @Override
    public void create(ReturnDto returnDto) {
        returnRepository.save(ConverterDto.convertToModel(returnDto));
    }

    @Override
    public void update(ReturnDto returnDto) {
        checkEntityService.checkExist(returnDto.getId(), returnRepository, "Return");

        returnRepository.save(ConverterDto.convertToModel(returnDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, returnRepository, "Return");
        returnRepository.deleteById(id);
    }
}

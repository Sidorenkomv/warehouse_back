package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ApplicationDto;
import com.warehouse_accounting.repositories.ApplicationRepository;
import com.warehouse_accounting.services.interfaces.ApplicationService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;
    private final CheckEntityService checkEntityService;

    public ApplicationServiceImpl(ApplicationRepository applicationRepository, CheckEntityService checkEntityService) {
        this.applicationRepository = applicationRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<ApplicationDto> getAll() {
        return applicationRepository.getAll();
    }

    @Override
    public ApplicationDto getById(Long id) {
        checkEntityService.checkExist(id, applicationRepository, "Application");
        return applicationRepository.getById(id);
    }

    @Override
    public void create(ApplicationDto dto) {
        applicationRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(ApplicationDto dto) {
        checkEntityService.checkExist(dto.getId(), applicationRepository, "Application");
        applicationRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, applicationRepository, "Application");
        applicationRepository.deleteById(id);
    }
}

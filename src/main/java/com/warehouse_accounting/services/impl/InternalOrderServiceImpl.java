package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.InternalOrderDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.InternalOrderRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.InternalOrderService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class InternalOrderServiceImpl implements InternalOrderService {

    private final InternalOrderRepository internalOrderRepository;

    private final TaskRepository taskRepository;

    private final FileRepository fileRepository;

    private final CheckEntityService checkEntityService;

    public InternalOrderServiceImpl(InternalOrderRepository internalOrderRepository,
                                    TaskRepository taskRepository,
                                    FileRepository fileRepository, CheckEntityService checkEntityService) {
        this.internalOrderRepository = internalOrderRepository;
        this.taskRepository = taskRepository;
        this.fileRepository = fileRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<InternalOrderDto> getAll() {
        List<InternalOrderDto> internalOrderDtos = internalOrderRepository.getAll();
        for (InternalOrderDto orderDto : internalOrderDtos) {
            orderDto.setTasksDto(taskRepository.getListTaskById(orderDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
            orderDto.setFilesDto(fileRepository.getListFileById(orderDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }
        return internalOrderDtos;
    }

    @Override
    public InternalOrderDto getById(Long id) {
        checkEntityService.checkExist(id, internalOrderRepository, "InternalOrder");
        InternalOrderDto internalOrderDto = internalOrderRepository.getById(id);
        internalOrderDto.setTasksDto(taskRepository.getListTaskById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        internalOrderDto.setFilesDto(fileRepository.getListFileById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return internalOrderDto;
    }

    @Override
    public void create(InternalOrderDto internalOrderDto) {
        internalOrderRepository.save(ConverterDto.convertToModel(internalOrderDto));
    }

    @Override
    public void update(InternalOrderDto internalOrderDto) {
        checkEntityService.checkExist(internalOrderDto.getId(), internalOrderRepository, "InternalOrder");
        internalOrderRepository.save(ConverterDto.convertToModel(internalOrderDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, internalOrderRepository, "InternalOrder");
        internalOrderRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.exceptions.EntityFoundException;
import com.warehouse_accounting.exceptions.EntityNotFoundException;
import com.warehouse_accounting.models.Employee;
import com.warehouse_accounting.repositories.EmployeeRepository;
import com.warehouse_accounting.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findById(Long id) throws EntityNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);
        return employee.orElseThrow(
                () -> new EntityNotFoundException(String.format("Employee with id = %s not found!", id))
        );
    }

    @Override
    @Transactional
    public Employee create(Employee employee) throws EntityFoundException {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(employee.getEmail());
        if (employeeOptional.isPresent()) {
            throw new EntityFoundException(String.format("Employee with email %s exists!", employee.getEmail()));
        }
        return employeeRepository.save(employee);
    }

    @Override
    @Transactional
    public Employee update(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws EntityNotFoundException {
        boolean isEmployeeExists = employeeRepository.existsById(id);
        if (!isEmployeeExists) {
            throw new EntityNotFoundException(String.format("User with id = %s does not exist!", id));
        }
        employeeRepository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws EntityNotFoundException {
        return employeeRepository.findByEmail(email).orElseThrow(
                () -> new EntityNotFoundException(String.format("Employee with email = %s not found!", email))
        );
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SerialNumberDto;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.repositories.SerialNumberRepository;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SerialNumberService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SerialNumberServiceImpl implements SerialNumberService {
    private final SerialNumberRepository serialNumberRepository;
    private final ProductRepository productRepository;
    private final WarehouseRepository warehouseRepository;
    private final CheckEntityService checkEntityService;

    public SerialNumberServiceImpl(SerialNumberRepository serialNumberRepository, ProductRepository productRepository,
                                   WarehouseRepository warehouseRepository, CheckEntityService checkEntityService) {
        this.serialNumberRepository = serialNumberRepository;
        this.productRepository = productRepository;
        this.warehouseRepository = warehouseRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SerialNumberDto> getAll() {
        List<SerialNumberDto> serialNumberDtos = serialNumberRepository.getAll();
        for (SerialNumberDto serialNumberDto : serialNumberDtos) {
            serialNumberDto.setWarehouse(warehouseRepository.getById(serialNumberDto.getWarehouse().getId()));
            serialNumberDto.setProduct(productRepository.getById(serialNumberDto.getProduct().getId()));
        }
        return serialNumberDtos;
    }

    @Override
    public SerialNumberDto getById(Long id) {
        checkEntityService.checkExist(id, serialNumberRepository, "SerialNumber");

        SerialNumberDto serialNumberDto = serialNumberRepository.getById(id);
        serialNumberDto.setWarehouse(warehouseRepository.getById(serialNumberDto.getWarehouse().getId()));
        serialNumberDto.setProduct(productRepository.getById(serialNumberDto.getProduct().getId()));
        return serialNumberDto;
    }

    @Override
    public void create(SerialNumberDto serialNumberDto) {
        serialNumberRepository.save(ConverterDto.convertToModel(serialNumberDto));
    }

    @Override
    public void update(SerialNumberDto serialNumberDto) {
        checkEntityService.checkExist(serialNumberDto.getId(), serialNumberRepository, "SerialNumber");
        serialNumberRepository.save(ConverterDto.convertToModel(serialNumberDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, serialNumberRepository, "SerialNumber");
        serialNumberRepository.deleteById(id);
    }
}

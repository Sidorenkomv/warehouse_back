package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ProcurementManagementDto;
import com.warehouse_accounting.repositories.ProcurementManagementRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProcurementManagementService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProcurementManagementServiceImpl implements ProcurementManagementService {

    private final ProcurementManagementRepository procurementManagementRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public ProcurementManagementServiceImpl(ProcurementManagementRepository procurementManagementRepository, CheckEntityService checkEntityService) {
        this.procurementManagementRepository = procurementManagementRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<ProcurementManagementDto> getAll() {
        return procurementManagementRepository.getAll();
    }

    @Override
    public ProcurementManagementDto getById(Long id) {
        checkEntityService.checkExist(id, procurementManagementRepository, "ProcurementManagement");
        return (ProcurementManagementDto) procurementManagementRepository.getById(id);
    }

    @Override
    public void create(ProcurementManagementDto procurementManagementDto) {
        procurementManagementRepository.save(ConverterDto.convertProcurementManagementDtoToModel(procurementManagementDto));

    }

    @Override
    public void update(ProcurementManagementDto procurementManagementDto) {
        checkEntityService.checkExist(procurementManagementDto.getId(), procurementManagementRepository, "ProcurementManagement");
        procurementManagementRepository.save(ConverterDto.convertProcurementManagementDtoToModel(procurementManagementDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, procurementManagementRepository, "ProcurementManagement");
        procurementManagementRepository.deleteById(id);
    }
}

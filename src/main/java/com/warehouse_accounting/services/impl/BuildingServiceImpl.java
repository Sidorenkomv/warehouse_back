package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.BuildingDto;
import com.warehouse_accounting.models.dto.CityDto;
import com.warehouse_accounting.repositories.BuildingRepository;
import com.warehouse_accounting.services.interfaces.BuildingService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BuildingServiceImpl implements BuildingService {

    private final BuildingRepository buildingRepository;

    public BuildingServiceImpl(BuildingRepository buildingRepository) {
        this.buildingRepository = buildingRepository;
    }

    @Override
    public List<BuildingDto> getAll(String regionCityStreetCode) {
        return buildingRepository.getAll(regionCityStreetCode);
    }

    @Override
    public List<BuildingDto> getSlice(int offset, int limit, String name, String regionCityStreetCode) {
        return buildingRepository.getSlice(name,  regionCityStreetCode, PageRequest.of(offset, limit)).getContent();
    }

    public int getCount(String name, String regionCityStreetCode) {
        return buildingRepository.getCount(name, regionCityStreetCode);
    }

    @Override
    public BuildingDto getById(Long id) {
        return buildingRepository.getById(id);
    }

    @Override
    public void create(BuildingDto buildingDto) {
        buildingRepository.save(ConverterDto.convertToModel(buildingDto));
    }

    @Override
    public void update(BuildingDto buildingDto) {
        buildingRepository.save(ConverterDto.convertToModel(buildingDto));
    }

    @Override
    public void deleteById(Long id) {
        buildingRepository.deleteById(id);
    }
}

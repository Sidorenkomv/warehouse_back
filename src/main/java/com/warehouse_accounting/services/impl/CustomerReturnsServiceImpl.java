package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CustomerReturnsDto;
import com.warehouse_accounting.repositories.CustomerReturnsRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CustomerReturnsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerReturnsServiceImpl implements CustomerReturnsService {

    private final CustomerReturnsRepository customerReturnsRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public CustomerReturnsServiceImpl(CustomerReturnsRepository customerReturnsRepository, CheckEntityService checkEntityService) {
        this.customerReturnsRepository = customerReturnsRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CustomerReturnsDto> getAll() {
        return customerReturnsRepository.getAll();
    }

    @Override
    public CustomerReturnsDto getById(Long id) {
        checkEntityService.checkExist(id, customerReturnsRepository, "CustomerReturns");
        return customerReturnsRepository.getById(id);
    }

    @Override
    public void create(CustomerReturnsDto customerReturnsDto) {
        customerReturnsRepository.save(ConverterDto.convertToModel(customerReturnsDto));
    }

    @Override
    public void update(CustomerReturnsDto customerReturnsDto) {
        checkEntityService.checkExist(customerReturnsDto.getId(), customerReturnsRepository, "CustomerReturns");
        customerReturnsRepository.save(ConverterDto.convertToModel(customerReturnsDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, customerReturnsRepository, "CustomerReturns");
        customerReturnsRepository.deleteById(id);
    }
}

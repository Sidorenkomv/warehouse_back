package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AddressDto;
import com.warehouse_accounting.repositories.*;
import com.warehouse_accounting.services.interfaces.AddressService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final CountryRepository countryRepository;
    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;
    private final StreetRepository streetRepository;

    private final CheckEntityService checkEntityService;

    public AddressServiceImpl(AddressRepository addressRepository, CountryRepository countryRepository, RegionRepository regionRepository, CityRepository cityRepository, StreetRepository streetRepository, CheckEntityService checkEntityService) {
        this.addressRepository = addressRepository;
        this.countryRepository = countryRepository;
        this.regionRepository = regionRepository;
        this.cityRepository = cityRepository;
        this.streetRepository = streetRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<AddressDto> getAll() {
        return addressRepository.getAll();
    }

    @Override
    public AddressDto getById(Long id) {
        checkEntityService.checkExist(id, addressRepository, "AddressDto");
        return addressRepository.getById(id);
    }

    @Override
    public void create(AddressDto addressDto) {
        addressRepository.save(ConverterDto.convertToModel(addressDto));
    }

    @Override
    public void update(AddressDto addressDto) {
        checkEntityService.checkExist(addressDto.getId(), addressRepository, "Address");
        addressRepository.save(ConverterDto.convertToModel(addressDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, addressRepository, "Address");
        addressRepository.deleteById(id);
    }

    @Override
    public AddressDto getByFullAddress(String fullAddress){
        return addressRepository.getByFullAddress(fullAddress);
    }
}
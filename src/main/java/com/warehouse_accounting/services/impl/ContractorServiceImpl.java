package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.Contractor;
import com.warehouse_accounting.models.dto.ContractorDto;
import com.warehouse_accounting.repositories.BankAccountRepository;
import com.warehouse_accounting.repositories.ContractorRepository;
import com.warehouse_accounting.repositories.LegalDetailRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ContractorService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ContractorServiceImpl implements ContractorService {

    private final ContractorRepository contractorRepository;
    private final BankAccountRepository bankAccountRepository;
    private final LegalDetailRepository legalDetailRepository;
    private final TaskRepository taskRepository;

    private final CheckEntityService checkEntityService;

    public ContractorServiceImpl(
            ContractorRepository contractorRepository,
            BankAccountRepository bankAccountRepository,
            LegalDetailRepository legalDetailRepository,
            TaskRepository taskRepository,
            CheckEntityService checkEntityService) {
        this.contractorRepository = contractorRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.legalDetailRepository = legalDetailRepository;
        this.taskRepository = taskRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ContractorDto> getAll() {
        List<ContractorDto> contractorDtos = contractorRepository.findAll()
                .stream().map(ConverterDto::convertToDto).collect(Collectors.toList());
//        for (ContractorDto contractorDto : contractorDtos) {
////            contractorDto.setLegalDetailDto(legalDetailRepository.getById(
////                    contractorDto.getLegalDetailDto().getId()));
//        }
        return contractorDtos;
    }

    @Override
    public ContractorDto getById(Long id) {
        checkEntityService.checkExist(id, contractorRepository, "Contractor");

        ContractorDto contractorDto = contractorRepository.getById(id);

//        contractorDto.setLegalDetailDto(legalDetailRepository.getById(
//                contractorDto.getLegalDetailDto().getId()));

//        contractorDto.setTaskDtos(taskRepository.getListTaskOfContructorById(
//                contractorDto.getId()).stream()
//                .map(ConverterDto::convertToDto).collect(Collectors.toList()));

        return contractorDto;
    }

    @Override
    public void create(ContractorDto contractorDto) {
        Contractor contractor = ConverterDto.convertToModel(contractorDto);

        if (contractor.getOuterCode() != null && contractor.getOuterCode().equals("Generate")) {
            contractor.setOuterCode(getNewOuterCode());
        }

        contractorRepository.save(contractor);
//        if (contractorDto.getTaskDtos() != null) {
//            for (TaskDto task : contractorDto.getTaskDtos()) {
//                task.setContractorId(contractorDto.getId());
//                taskRepository.save(ConverterDto.convertToModel(task));
//                System.out.println(ConverterDto.convertToModel(taskRepository.getById(2L)));
//            }
//        }
    }

    @Override
    public void update(ContractorDto contractorDto) {
        checkEntityService.checkExist(contractorDto.getId(), contractorRepository, "Contractor");

        Contractor contractor = ConverterDto.convertToModel(contractorDto);

        if (contractor.getOuterCode() == null || contractor.getOuterCode().equals("Generate")) {
            contractor.setOuterCode(getNewOuterCode());
        }
        contractorRepository.save(contractor);
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, contractorRepository, "Contractor");
        contractorRepository.deleteById(id);
    }


    private String getNewOuterCode() {
        String set = "QWERTYUIOPASDFGHJKL:ZXCVBNM<>?qwertyuiopasdfghjkl;zxcvbnm_-%*&@#1234567890";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 21; i++) {
            builder.append(set.charAt((int) ((set.length() - 1) * Math.random())));
        }
        return builder.toString();
    }


}

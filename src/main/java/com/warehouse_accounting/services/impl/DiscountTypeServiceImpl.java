package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.DiscountTypeDto;
import com.warehouse_accounting.repositories.DiscountTypeRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.DiscountTypeService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class DiscountTypeServiceImpl implements DiscountTypeService {
    private final DiscountTypeRepository discountTypeRepository;

    private final CheckEntityService checkEntityService;

    public DiscountTypeServiceImpl(DiscountTypeRepository discountTypeRepository, CheckEntityService checkEntityService) {
        this.discountTypeRepository = discountTypeRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<DiscountTypeDto> getAll() {
        return discountTypeRepository.getAll();
    }

    @Override
    public DiscountTypeDto getById(Long id) {
        checkEntityService.checkExist(id, discountTypeRepository, "DiscountType");
        return discountTypeRepository.getById(id);
    }

    @Override
    public void create(DiscountTypeDto discountTypeDto) {
        discountTypeRepository.save(ConverterDto.convertToModel(discountTypeDto));
    }

    @Override
    public void update(DiscountTypeDto discountTypeDto) {
        checkEntityService.checkExist(discountTypeDto.getId(), discountTypeRepository, "DiscountType");
        discountTypeRepository.save(ConverterDto.convertToModel(discountTypeDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, discountTypeRepository, "DiscountType");
        discountTypeRepository.deleteById(id);
    }
}

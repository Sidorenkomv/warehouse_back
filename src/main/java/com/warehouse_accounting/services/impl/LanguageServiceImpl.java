package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.LanguageDto;
import com.warehouse_accounting.repositories.LanguageRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.LanguageService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LanguageServiceImpl implements LanguageService {
    private final LanguageRepository languageRepository;

    private final CheckEntityService checkEntityService;

    public LanguageServiceImpl(LanguageRepository languageRepository, CheckEntityService checkEntityService) {
        this.languageRepository = languageRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<LanguageDto> getAll() {
        return languageRepository.getAll();
    }

    @Override
    public LanguageDto getById(Long id) {
        checkEntityService.checkExist(id, languageRepository, "Language");
        return languageRepository.getById(id);
    }

    @Override
    public void create(LanguageDto languageDto) {
        languageRepository.save(ConverterDto.convertToModel(languageDto));
    }

    @Override
    public void update(LanguageDto languageDto) {
        checkEntityService.checkExist(languageDto.getId(), languageRepository, "Language");
        languageRepository.save(ConverterDto.convertToModel(languageDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, languageRepository, "Language");
        languageRepository.deleteById(id);
    }
}

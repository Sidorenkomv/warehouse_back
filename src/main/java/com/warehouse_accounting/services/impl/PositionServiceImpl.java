package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PositionDto;
import com.warehouse_accounting.repositories.PositionRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PositionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.warehouse_accounting.util.ConverterDto.convertToModel;

@Service
@Transactional
public class PositionServiceImpl implements PositionService {

    private final PositionRepository positionRepository;

    private final CheckEntityService checkEntityService;
    public PositionServiceImpl(PositionRepository positionRepository, CheckEntityService checkEntityService) {
        this.positionRepository = positionRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PositionDto> getAll() {
        return positionRepository.getAll();
    }

    @Override
    public PositionDto getById(Long id) {
        checkEntityService.checkExist(id, positionRepository, "Position");
        return positionRepository.getById(id);
    }

    @Override
    public void create(PositionDto positionDto) {
        positionRepository.save(convertToModel(positionDto));
    }

    @Override
    public void update(PositionDto positionDto) {
        checkEntityService.checkExist(positionDto.getId(), positionRepository, "Position");
        positionRepository.save(convertToModel(positionDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, positionRepository, "Position");
        positionRepository.deleteById(id);
    }

    @Override
    public List<PositionDto> getAllByLikeQuery(String value) {
        return positionRepository.getAllByLikeQuery(value);
    }
}

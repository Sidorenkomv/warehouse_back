package com.warehouse_accounting.services.impl;


import com.warehouse_accounting.models.dto.GoodsToRealizeGiveDto;
import com.warehouse_accounting.repositories.GoodsToRealizeGiveRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGiveService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GoodsToRealizeGiveImpl implements GoodsToRealizeGiveService {

    GoodsToRealizeGiveRepository goodsToRealizeGiveRepository;

    private final CheckEntityService checkEntityService;

    public GoodsToRealizeGiveImpl(GoodsToRealizeGiveRepository goodsToRealizeGiveRepository, CheckEntityService checkEntityService) {
        this.goodsToRealizeGiveRepository = goodsToRealizeGiveRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public GoodsToRealizeGiveDto getById(Long id) {
        checkEntityService.checkExist(id, goodsToRealizeGiveRepository, "Goods To Realize GIVE");
        return goodsToRealizeGiveRepository.getById(id);
    }

    @Override
    public List<GoodsToRealizeGiveDto> getAll() {
        return goodsToRealizeGiveRepository.getAll();
    }

    @Override
    public void create(GoodsToRealizeGiveDto goodsToRealizeGive) {
        goodsToRealizeGiveRepository.save(ConverterDto.convertToModel(goodsToRealizeGive));
    }

    @Override
    public void update(GoodsToRealizeGiveDto goodsToRealizeGive) {
        checkEntityService.checkExist(goodsToRealizeGive.getId(), goodsToRealizeGiveRepository, "Goods To Realize GIVE");
        goodsToRealizeGiveRepository.save(ConverterDto.convertToModel(goodsToRealizeGive));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, goodsToRealizeGiveRepository, "Goods To Realize GIVE");
        goodsToRealizeGiveRepository.deleteById(id);
    }

}
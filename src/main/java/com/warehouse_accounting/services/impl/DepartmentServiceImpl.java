package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.DepartmentDto;
import com.warehouse_accounting.repositories.DepartmentRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.DepartmentService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    private final CheckEntityService checkEntityService;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, CheckEntityService checkEntityService) {
        this.departmentRepository = departmentRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<DepartmentDto> getAll() {
        return departmentRepository.getAll();
    }

    @Override
    public DepartmentDto getById(Long id) {
        checkEntityService.checkExist(id, departmentRepository, "Department");
        return departmentRepository.getById(id);
    }

    @Override
    public void create(DepartmentDto departmentDto) {
        departmentRepository.save(ConverterDto.convertToModel(departmentDto));
    }

    @Override
    public void update(DepartmentDto departmentDto) {
        checkEntityService.checkExist(departmentDto.getId(), departmentRepository, "Department");
        departmentRepository.save(ConverterDto.convertToModel(departmentDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, departmentRepository, "Department");
        departmentRepository.deleteById(id);
    }


}

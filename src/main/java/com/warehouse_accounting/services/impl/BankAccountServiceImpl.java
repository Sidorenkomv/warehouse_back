package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.BankAccountDto;
import com.warehouse_accounting.repositories.BankAccountRepository;
import com.warehouse_accounting.services.interfaces.BankAccountService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService {

    private final BankAccountRepository bankAccountRepository;

    private final CheckEntityService checkEntityService;

    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, CheckEntityService checkEntityService) {
        this.bankAccountRepository = bankAccountRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<BankAccountDto> getAll() {
        return bankAccountRepository.getAll();
    }

    @Override
    public BankAccountDto getById(Long id) {
        checkEntityService.checkExist(id, bankAccountRepository, "BankAccount");
        return bankAccountRepository.getById(id);
    }

    @Override
    public void create(BankAccountDto bankAccountDto) {
        bankAccountRepository.save(ConverterDto.convertToModel(bankAccountDto));
    }

    @Override
    public void update(BankAccountDto bankAccountDto) {
        checkEntityService.checkExist(bankAccountDto.getId(), bankAccountRepository, "BankAccount");
        bankAccountRepository.save(ConverterDto.convertToModel(bankAccountDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, bankAccountRepository, "BankAccount");
        bankAccountRepository.deleteById(id);
    }

}

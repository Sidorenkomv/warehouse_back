package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SelectorDto;
import com.warehouse_accounting.repositories.SelectorRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SelectorService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SelectorServiceImpl implements SelectorService {
    private final SelectorRepository selectorRepository;

    private final CheckEntityService checkEntityService;

    public SelectorServiceImpl(SelectorRepository selectorRepository, CheckEntityService checkEntityService) {
        this.selectorRepository = selectorRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SelectorDto> getAll() {
        return selectorRepository.getAll();
    }

    @Override
    public SelectorDto getById(Long id) {
        checkEntityService.checkExist(id, selectorRepository, "Selector");
        return selectorRepository.getById(id);
    }

    @Override
    public void create(SelectorDto selectorDto) {
        selectorRepository.save(ConverterDto.convertToModel(selectorDto));
    }

    @Override
    public void update(SelectorDto selectorDto) {
        checkEntityService.checkExist(selectorDto.getId(), selectorRepository, "Selector");
        selectorRepository.save(ConverterDto.convertToModel(selectorDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, selectorRepository, "Selector");
        selectorRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PurchaseCurrentBalanceDto;
import com.warehouse_accounting.repositories.PurchaseCurrentBalanceRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PurchaseCurrentBalanceService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseCurrentBalanceServiceImpl implements PurchaseCurrentBalanceService {

    private final PurchaseCurrentBalanceRepository purchaseCurrentBalanceRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public PurchaseCurrentBalanceServiceImpl(PurchaseCurrentBalanceRepository purchaseCurrentBalanceRepository, CheckEntityService checkEntityService) {
        this.purchaseCurrentBalanceRepository = purchaseCurrentBalanceRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PurchaseCurrentBalanceDto> getAll() {
        return purchaseCurrentBalanceRepository.getAll();
    }

    @Override
    public PurchaseCurrentBalanceDto getById(Long id) {
        checkEntityService.checkExist(id, purchaseCurrentBalanceRepository, "PurchaseCurrentBalance");
        return purchaseCurrentBalanceRepository.getById(id);
    }

    @Override
    public void create(PurchaseCurrentBalanceDto purchaseCurrentBalanceDto) {
        purchaseCurrentBalanceRepository.save(ConverterDto.convertToModel(purchaseCurrentBalanceDto));
    }

    @Override
    public void update(PurchaseCurrentBalanceDto purchaseCurrentBalanceDto) {
        checkEntityService.checkExist(purchaseCurrentBalanceDto.getId(), purchaseCurrentBalanceRepository, "PurchaseCurrentBalance");

        purchaseCurrentBalanceRepository.save(ConverterDto.convertToModel(purchaseCurrentBalanceDto));
    }

    @Override
    public void delete(Long id) {
        checkEntityService.checkExist(id, purchaseCurrentBalanceRepository, "PurchaseCurrentBalance");
        purchaseCurrentBalanceRepository.deleteById(id);
    }
}

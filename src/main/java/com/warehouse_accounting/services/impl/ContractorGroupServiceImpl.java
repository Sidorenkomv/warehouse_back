package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ContractorGroupDto;
import com.warehouse_accounting.repositories.ContractorGroupRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ContractorGroupService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContractorGroupServiceImpl implements ContractorGroupService {

    private final ContractorGroupRepository contractorGroupRepository;

    private final CheckEntityService checkEntityService;

    public ContractorGroupServiceImpl(ContractorGroupRepository contractorGroupRepository, CheckEntityService checkEntityService) {
        this.contractorGroupRepository = contractorGroupRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ContractorGroupDto> getAll() {
        return contractorGroupRepository.getAll();
    }

    @Override
    public ContractorGroupDto getById(Long id) {
        checkEntityService.checkExist(id, contractorGroupRepository, "ContractorGroup");
        return contractorGroupRepository.getById(id);
    }

    @Override
    public void create(ContractorGroupDto contractorGroupDto) {
        contractorGroupRepository.save(ConverterDto.convertToModel(contractorGroupDto));
    }

    @Override
    public void update(ContractorGroupDto contractorGroupDto) {
        checkEntityService.checkExist(contractorGroupDto.getId(), contractorGroupRepository, "ContractorGroup");
        contractorGroupRepository.save(ConverterDto.convertToModel(contractorGroupDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, contractorGroupRepository, "ContractorGroup");
        contractorGroupRepository.deleteById(id);
    }
}

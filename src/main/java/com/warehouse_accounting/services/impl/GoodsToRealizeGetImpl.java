package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.GoodsToRealizeGetDto;
import com.warehouse_accounting.repositories.GoodsToRealizeGetRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.GoodsToRealizeGetService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GoodsToRealizeGetImpl implements GoodsToRealizeGetService {

    GoodsToRealizeGetRepository goodsToRealizeGetRepository;

    private final CheckEntityService checkEntityService;

    public GoodsToRealizeGetImpl(GoodsToRealizeGetRepository goodsToRealizeGetRepository, CheckEntityService checkEntityService) {
        this.goodsToRealizeGetRepository = goodsToRealizeGetRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public GoodsToRealizeGetDto getById(Long id) {
        checkEntityService.checkExist(id, goodsToRealizeGetRepository, "Goods To Realize GET");
        return goodsToRealizeGetRepository.getById(id);
    }

    @Override
    public List<GoodsToRealizeGetDto> getAll() {
        return goodsToRealizeGetRepository.getAll();
    }

    @Override
    public void create(GoodsToRealizeGetDto goodsToRealizeGet) {
        goodsToRealizeGetRepository.save(ConverterDto.convertToModel(goodsToRealizeGet));
    }

    @Override
    public void update(GoodsToRealizeGetDto goodsToRealizeGet) {
        checkEntityService.checkExist(goodsToRealizeGet.getId(), goodsToRealizeGetRepository, "Goods To Realize GET");
        goodsToRealizeGetRepository.save(ConverterDto.convertToModel(goodsToRealizeGet));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, goodsToRealizeGetRepository, "Goods To Realize GET");
        goodsToRealizeGetRepository.deleteById(id);
    }

}

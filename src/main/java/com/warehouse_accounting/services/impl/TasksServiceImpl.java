package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.TasksDto;
import com.warehouse_accounting.repositories.TasksRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TasksService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class TasksServiceImpl implements TasksService {

    private TasksRepository tasksRepository;

    private final CheckEntityService checkEntityService;

    public TasksServiceImpl(TasksRepository tasksRepository, CheckEntityService checkEntityService) {
        this.tasksRepository = tasksRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<TasksDto> getAll() {
        return tasksRepository.getAll();
    }

    @Override
    public TasksDto getById(Long id) {
        checkEntityService.checkExist(id, tasksRepository, "Task");
        return tasksRepository.getById(id);
    }

    @Override
    public void create(TasksDto taskDto) {
        tasksRepository.save(ConverterDto.convertToModel(taskDto));
    }

    @Override
    public void update(TasksDto taskDto) {
        checkEntityService.checkExist(taskDto.getId(), tasksRepository, "Task");
        tasksRepository.save(ConverterDto.convertToModel(taskDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, tasksRepository, "Task");
        tasksRepository.deleteById(id);
    }
}

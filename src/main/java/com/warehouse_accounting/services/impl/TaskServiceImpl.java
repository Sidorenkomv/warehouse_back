package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.TaskDto;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TaskService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Please use TasksServiceImpl
 * ---------------------------
 * <p>
 * <p>
 * This class implements interface {@link TaskService}
 *
 * @author pavelsmirnov
 * @version 0.1
 * Created 26.03.2021
 */

@Deprecated
@Service
@Transactional
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    private final CheckEntityService checkEntityService;

    public TaskServiceImpl(TaskRepository taskRepository, CheckEntityService checkEntityService) {
        this.taskRepository = taskRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<TaskDto> getAll() {
        return taskRepository.getAll();
    }

    @Override
    public TaskDto getById(Long id) {
        checkEntityService.checkExist(id, taskRepository, "Task");
        return taskRepository.getById(id);
    }

    @Override
    public void create(TaskDto taskDto) {
        taskRepository.save(ConverterDto.convertToModel(taskDto));
    }

    @Override
    public void update(TaskDto taskDto) {
        checkEntityService.checkExist(taskDto.getId(), taskRepository, "Task");
        taskRepository.save(ConverterDto.convertToModel(taskDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, taskRepository, "Task");
        taskRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ProductionOrderDto;
import com.warehouse_accounting.repositories.ProductionOrderRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductionOrderService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductionOrderServiceImpl implements ProductionOrderService {

    private final ProductionOrderRepository productionOrderRepository;
    private final CheckEntityService checkEntityService;

    public ProductionOrderServiceImpl(ProductionOrderRepository productionOrderRepository, CheckEntityService checkEntityService) {
        this.productionOrderRepository = productionOrderRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<ProductionOrderDto> getAll() {
        List<ProductionOrderDto> productionOrderDtoList = productionOrderRepository.getAll();
        return productionOrderDtoList;
    }

    @Override
    public ProductionOrderDto getById(Long id) {
        checkEntityService.checkExist(id, productionOrderRepository, "ProductionOrder");
        ProductionOrderDto productionOrderDto = productionOrderRepository.getById(id);
        return productionOrderDto;
    }

    @Override
    public void create(ProductionOrderDto productionOrderDto) {
        productionOrderRepository.save(ConverterDto.convertToModel(productionOrderDto));
    }

    @Override
    public void update(ProductionOrderDto productionOrderDto) {
        checkEntityService.checkExist(productionOrderDto.getId(), productionOrderRepository, "ProductionOrder");
        productionOrderRepository.save(ConverterDto.convertToModel(productionOrderDto));
    }

    @Override
    public void delete(Long id) {
        checkEntityService.checkExist(id, productionOrderRepository, "ProductionOrder");
        productionOrderRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PrintingDocumentsDto;
import com.warehouse_accounting.repositories.PrintingDocumentsRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PrintingDocumentsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PrintingDocumentsServiceImpl implements PrintingDocumentsService {
    private final PrintingDocumentsRepository printingDocumentsRepository;

    private final CheckEntityService checkEntityService;

    public PrintingDocumentsServiceImpl(PrintingDocumentsRepository printingDocumentsRepository, CheckEntityService checkEntityService) {
        this.printingDocumentsRepository = printingDocumentsRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PrintingDocumentsDto> getAll() {
        return printingDocumentsRepository.getAll();
    }

    @Override
    public PrintingDocumentsDto getById(Long id) {
        checkEntityService.checkExist(id, printingDocumentsRepository, "PrintingDocument");

        return printingDocumentsRepository.getById(id);
    }

    @Override
    public void create(PrintingDocumentsDto printingDocumentsDto) {
        printingDocumentsRepository.save(ConverterDto.convertToModel(printingDocumentsDto));

    }

    @Override
    public void update(PrintingDocumentsDto printingDocumentsDto) {
        checkEntityService.checkExist(printingDocumentsDto.getId(), printingDocumentsRepository, "PrintingDocument");
        printingDocumentsRepository.save(ConverterDto.convertToModel(printingDocumentsDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, printingDocumentsRepository, "PrintingDocument");
        printingDocumentsRepository.deleteById(id);
    }
}

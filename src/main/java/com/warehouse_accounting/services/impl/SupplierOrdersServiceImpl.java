package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import com.warehouse_accounting.repositories.SupplierOrdersRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SupplierOrdersService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SupplierOrdersServiceImpl implements SupplierOrdersService {

    private final SupplierOrdersRepository supplierOrdersRepository;

    private final CheckEntityService checkEntityService;
    @Autowired
    public SupplierOrdersServiceImpl(SupplierOrdersRepository supplierOrdersRepository, CheckEntityService checkEntityService) {
        this.supplierOrdersRepository = supplierOrdersRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SupplierOrdersDto> getAll() {
        return supplierOrdersRepository.getAll();
    }

    @Override
    public SupplierOrdersDto getById(Long id) {
        checkEntityService.checkExist(id, supplierOrdersRepository, "SupplierOrders");
        return supplierOrdersRepository.getById(id);
    }

    @Override
    public void create(SupplierOrdersDto supplierOrdersDto) {
        supplierOrdersRepository.save(ConverterDto.convertToModel(supplierOrdersDto));
    }

    @Override
    public void update(SupplierOrdersDto supplierOrdersDto) {
        checkEntityService.checkExist(supplierOrdersDto.getId(), supplierOrdersRepository, "SupplierOrders");
        supplierOrdersRepository.save(ConverterDto.convertToModel(supplierOrdersDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, supplierOrdersRepository, "SupplierOrders");
        supplierOrdersRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.IpAddressDto;
import com.warehouse_accounting.repositories.IpAddressRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.IpAddressService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class IpAddressServiceImpl implements IpAddressService {

    private final IpAddressRepository ipAddressRepository;

    private final CheckEntityService checkEntityService;

    public IpAddressServiceImpl(IpAddressRepository ipAddressRepository, CheckEntityService checkEntityService) {
        this.ipAddressRepository = ipAddressRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<IpAddressDto> getAll() {
        return ipAddressRepository.getAll();
    }

    @Override
    public IpAddressDto getById(Long id) {
        checkEntityService.checkExist(id, ipAddressRepository, "IpAddress");
        return ipAddressRepository.getById(id);
    }

    @Override
    public void create(IpAddressDto ipAddressDto) {
        ipAddressRepository.save(ConverterDto.convertToModel(ipAddressDto));
    }

    @Override
    public void update(IpAddressDto ipAddressDto) {
        checkEntityService.checkExist(ipAddressDto.getId(), ipAddressRepository, "IpNetwork");
        ipAddressRepository.save(ConverterDto.convertToModel(ipAddressDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, ipAddressRepository, "IpNetwork");
        ipAddressRepository.deleteById(id);
    }
}

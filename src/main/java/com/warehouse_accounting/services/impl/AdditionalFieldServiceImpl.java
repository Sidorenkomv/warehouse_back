package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.AdditionalField;
import com.warehouse_accounting.models.dto.AdditionalFieldDto;
import com.warehouse_accounting.repositories.AdditionalFieldRepository;
import com.warehouse_accounting.repositories.IntegerPropertyRepository;
import com.warehouse_accounting.repositories.StringPropertyRepository;
import com.warehouse_accounting.services.interfaces.AdditionalFieldService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AdditionalFieldServiceImpl implements AdditionalFieldService {

    private final AdditionalFieldRepository repository;

    private final IntegerPropertyRepository integerPropertyRepository;

    private final StringPropertyRepository stringPropertyRepository;

    public AdditionalFieldServiceImpl(AdditionalFieldRepository repository,
                                      IntegerPropertyRepository integerPropertyRepository,
                                      StringPropertyRepository stringPropertyRepository) {
        this.repository = repository;
        this.integerPropertyRepository = integerPropertyRepository;
        this.stringPropertyRepository = stringPropertyRepository;
    }

    @Override
    public List<AdditionalFieldDto> getAll() {
        List<AdditionalField> listAll = repository.findAll();
        return listAll.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public AdditionalFieldDto getById(Long id) {
        return ConverterDto.convertToDto(repository.findById(id).get());
    }

    @Override
    public AdditionalFieldDto create(AdditionalFieldDto dto) {
        AdditionalField additionalField = ConverterDto.convertToModel(dto);
        JpaRepository propertyRepository;

        switch (additionalField.getProperty().getType()) {
            case "Integer":
                propertyRepository = integerPropertyRepository;
                break;
            case "String":
                propertyRepository = stringPropertyRepository;
                break;
            default:
                propertyRepository = null;
        }

        propertyRepository.save(additionalField.getProperty());
        return ConverterDto.convertToDto(repository.save(additionalField));
    }

    @Override
    public AdditionalFieldDto update(AdditionalFieldDto dto) {
        AdditionalField additionalField = ConverterDto.convertToModel(dto);

        JpaRepository propertyRepository;

        switch (additionalField.getProperty().getType()) {
            case "Integer":
                propertyRepository = integerPropertyRepository;
                break;
            case "String":
                propertyRepository = stringPropertyRepository;
                break;
            default:
                propertyRepository = null;
        }

        propertyRepository.save(additionalField.getProperty());

        return ConverterDto.convertToDto(repository.save(additionalField));
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}


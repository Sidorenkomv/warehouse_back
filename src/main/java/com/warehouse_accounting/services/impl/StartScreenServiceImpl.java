package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.StartScreenDto;
import com.warehouse_accounting.repositories.StartScreenRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.StartScreenService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class StartScreenServiceImpl implements StartScreenService {
    private final StartScreenRepository startScreenRepository;

    private final CheckEntityService checkEntityService;

    public StartScreenServiceImpl(StartScreenRepository startScreenRepository, CheckEntityService checkEntityService) {
        this.startScreenRepository = startScreenRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<StartScreenDto> getAll() {
        return startScreenRepository.getAll();
    }

    @Override
    public StartScreenDto getById(Long id) {
        checkEntityService.checkExist(id, startScreenRepository, "StartScreen");
        return startScreenRepository.getById(id);
    }

    @Override
    public void create(StartScreenDto startScreenDto) {
        startScreenRepository.save(ConverterDto.convertToModel(startScreenDto));
    }

    @Override
    public void update(StartScreenDto startScreenDto) {
        checkEntityService.checkExist(startScreenDto.getId(), startScreenRepository, "StartScreen");
        startScreenRepository.save(ConverterDto.convertToModel(startScreenDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, startScreenRepository, "StartScreen");
        startScreenRepository.deleteById(id);

    }
}

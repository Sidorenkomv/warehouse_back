package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.InventoryDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.InventoryRepository;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.InventoryService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository inventoryRepository;
    private final WarehouseRepository warehouseRepository;
    private final CompanyRepository companyRepository;

    private final CheckEntityService checkEntityService;

    public InventoryServiceImpl(InventoryRepository inventoryRepository,
                                WarehouseRepository warehouseRepository,
                                CompanyRepository companyRepository, CheckEntityService checkEntityService) {
        this.inventoryRepository = inventoryRepository;
        this.warehouseRepository = warehouseRepository;
        this.companyRepository = companyRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<InventoryDto> getAll() {
        return inventoryRepository.getAll();
    }

    @Override
    public List<InventoryDto> getAllTest() {
        return inventoryRepository.getAll();
    }

    @Override
    public InventoryDto getById(Long id) {
        checkEntityService.checkExist(id, inventoryRepository, "Inventory");

        return inventoryRepository.getById(id);
    }

    @Override
    public InventoryDto getByIdTest(Long id) {
        return inventoryRepository.getById(id);
    }

    @Override
    public void create(InventoryDto dto) {
        inventoryRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(InventoryDto dto) {
        checkEntityService.checkExist(dto.getId(), inventoryRepository, "Inventory");
        inventoryRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, inventoryRepository, "Inventory");
        inventoryRepository.deleteById(id);
    }
}

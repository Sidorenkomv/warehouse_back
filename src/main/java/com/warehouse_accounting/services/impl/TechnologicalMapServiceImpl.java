package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ProductionOrderDto;
import com.warehouse_accounting.models.dto.TechnologicalMapDto;
import com.warehouse_accounting.models.dto.TechnologicalOperationDto;
import com.warehouse_accounting.repositories.TechnologicalMapMaterialRepository;
import com.warehouse_accounting.repositories.TechnologicalMapProductRepository;
import com.warehouse_accounting.repositories.TechnologicalMapRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TechnologicalMapService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class implements interface {@link TechnologicalMapService}
 *
 * @author pavelsmirnov
 * @version 0.1
 * Created 26.03.2021
 */
@Service
@Transactional
public class TechnologicalMapServiceImpl implements TechnologicalMapService {
    private final TechnologicalMapRepository technologicalMapRepository;
    private final TechnologicalMapProductRepository technologicalMapProductRepository;
    private final TechnologicalMapMaterialRepository technologicalMapMaterialRepository;

    private final TechnologicalOperationServiceImpl technologicalOperationService;
    private final CheckEntityService checkEntityService;
    private final ProductionOrderServiceImpl productionOrderService;

    public TechnologicalMapServiceImpl(TechnologicalMapRepository technologicalMapRepository,
                                       TechnologicalMapProductRepository technologicalMapProductRepository,
                                       TechnologicalMapMaterialRepository technologicalMapMaterialRepository, TechnologicalOperationServiceImpl technologicalOperationService, CheckEntityService checkEntityService, ProductionOrderServiceImpl productionOrderService) {
        this.technologicalMapRepository = technologicalMapRepository;
        this.technologicalMapProductRepository = technologicalMapProductRepository;
        this.technologicalMapMaterialRepository = technologicalMapMaterialRepository;
        this.technologicalOperationService = technologicalOperationService;
        this.checkEntityService = checkEntityService;
        this.productionOrderService = productionOrderService;
    }

    @Override
    public List<TechnologicalMapDto> getAll() {
        List<TechnologicalMapDto> technologicalMapDtos = technologicalMapRepository.getAll();
        return getTechnologicalMapDtoList(technologicalMapDtos);
    }

    @Override
    public List<TechnologicalMapDto> getAllWithIsDeleted() {
        List<TechnologicalMapDto> technologicalMapDtos = technologicalMapRepository.getAllWithIsDeleted();
        return getTechnologicalMapDtoList(technologicalMapDtos);
    }

    private List<TechnologicalMapDto> getTechnologicalMapDtoList(List<TechnologicalMapDto> technologicalMapDtos) {
        for (TechnologicalMapDto technologicalMapDto : technologicalMapDtos) {
            technologicalMapDto.setFinishedProducts(technologicalMapProductRepository
                    .getListTechnologicalMapProductById(technologicalMapDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
            technologicalMapDto.setMaterials(technologicalMapMaterialRepository
                    .getListTechnologicalMapMaterialById(technologicalMapDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }
        return technologicalMapDtos;
    }

    @Override
    public TechnologicalMapDto getById(Long id) {
        checkEntityService.checkExist(id, technologicalMapRepository, "TechnologicalMap");

        TechnologicalMapDto technologicalMapDto = technologicalMapRepository.getById(id);
        technologicalMapDto.setFinishedProducts(technologicalMapProductRepository
                .getListTechnologicalMapProductById(technologicalMapDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        technologicalMapDto.setMaterials(technologicalMapMaterialRepository
                .getListTechnologicalMapMaterialById(technologicalMapDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return technologicalMapDto;
    }

    @Override
    public void create(TechnologicalMapDto technologicalMapDto) {
        technologicalMapRepository.save(ConverterDto.convertToModel(technologicalMapDto));
    }

    @Override
    public void update(TechnologicalMapDto technologicalMapDto) {
        checkEntityService.checkExist(technologicalMapDto.getId(), technologicalMapRepository, "TechnologicalMap");
        technologicalMapRepository.save(ConverterDto.convertToModel(technologicalMapDto));
    }


    @Override
    public void deleteById(Long id) {
        //TODO необходимо реализовать не физическое удаление на бэкенде (флаг isDeleted)
        // на данный момент физическое удаление заменено на выставление флага

        checkEntityService.checkExist(id, technologicalMapRepository, "TechnologicalMap");

        clearProductOrders(id);
        clearTechnologicalOperation(id);
        technologicalMapRepository.deleteById(id);
    }

    @Override
    public void isDeletedById(Long id) {
        checkEntityService.checkExist(id, technologicalMapRepository, "TechnologicalMap");
        TechnologicalMapDto dto = getById(id);
        dto.setDeleted(true);
        technologicalMapRepository.save(ConverterDto.convertToModel(dto));
    }

    private void clearProductOrders(Long id) {
        for (ProductionOrderDto productionOrderDto : productionOrderService.getAll()) {
            if (productionOrderDto.getTechnologicalMapId() == id) {

                productionOrderService.delete(productionOrderDto.getId());
            }
        }
    }

    private void clearTechnologicalOperation(Long id) {
        for (TechnologicalOperationDto technologicalOperationDto : technologicalOperationService.getAll()) {
            if (technologicalOperationDto.getTechnologicalMapId() == id) {
                technologicalOperationService.deleteById(technologicalOperationDto.getId());
            }
        }
    }
}

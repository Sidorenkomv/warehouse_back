package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.DiscountDto;
import com.warehouse_accounting.repositories.ContractorRepository;
import com.warehouse_accounting.repositories.DiscountRepository;
import com.warehouse_accounting.repositories.DiscountTypeRepository;
import com.warehouse_accounting.repositories.ProductRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.DiscountService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.warehouse_accounting.util.ConverterDto.convertToDtoContractor;
import static com.warehouse_accounting.util.ConverterDto.convertToDtoProduct;

@Service
@Transactional
public class DiscountServiceImpl implements DiscountService {

    private final DiscountRepository discountRepository;
    private final DiscountTypeRepository discountTypeRepository;
    private final ProductRepository productRepository;
    private final ContractorRepository contractorRepository;

    private final CheckEntityService checkEntityService;

    public DiscountServiceImpl(DiscountRepository discountRepository,
                               DiscountTypeRepository discountTypeRepository,
                               ProductRepository productRepository,
                               ContractorRepository contractorRepository, CheckEntityService checkEntityService) {
        this.discountRepository = discountRepository;
        this.discountTypeRepository = discountTypeRepository;
        this.productRepository = productRepository;
        this.contractorRepository = contractorRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<DiscountDto> getAll() {
        List<DiscountDto> discountDtos = discountRepository.getAll();

        discountDtos.forEach(discountDto -> {
            discountDto.setDiscountType(discountTypeRepository.getById(discountDto.getDiscountType().getId()));
            discountDto.setProducts(convertToDtoProduct(productRepository.getAllProductById(discountDto.getId())));
            discountDto.setContractors(convertToDtoContractor(contractorRepository.getAllContractorById(discountDto.getId())));

        });
        return discountDtos;
    }


    @Override
    public DiscountDto getById(Long id) {
        checkEntityService.checkExist(id, discountRepository, "Discount");

        DiscountDto discountDto = discountRepository.getById(id);
        discountDto.setDiscountType(discountTypeRepository.getById(discountDto.getDiscountType().getId()));
        discountDto.setProducts(convertToDtoProduct(productRepository.getAllProductById(discountDto.getId())));
        discountDto.setContractors(convertToDtoContractor(contractorRepository.getAllContractorById(discountDto.getId())));
        return discountDto;
    }

    @Override
    public void create(DiscountDto discountDto) {
        discountRepository.save(ConverterDto.convertToModel(discountDto));
    }


    @Override
    public void update(DiscountDto discountDto) {
        checkEntityService.checkExist(discountDto.getId(), discountRepository, "Discount");
        discountRepository.save(ConverterDto.convertToModel(discountDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, discountRepository, "Discount");
        discountRepository.deleteById(id);
    }
}

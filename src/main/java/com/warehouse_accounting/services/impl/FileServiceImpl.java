package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.File;
import com.warehouse_accounting.models.dto.FileDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.FileService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository, CheckEntityService checkEntityService) {
        this.fileRepository = fileRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<FileDto> getAll() {
        return fileRepository.findAll().stream()
                .map(ConverterDto::convertToDto
                ).collect(Collectors.toList());
    }

    @Override
    public FileDto getById(Long id) {
        checkEntityService.checkExist(id, fileRepository, "File");

        return fileRepository.findById(id).stream()
                .map(ConverterDto::convertToDto)
                .findAny()
                .orElseThrow();
    }

    @Override
    public FileDto create(FileDto fileDto) {
         return ConverterDto.convertToDto(fileRepository.save(ConverterDto.convertToModel(fileDto)));
    }

    @Override
    public void update(FileDto fileDto) {
        checkEntityService.checkExist(fileDto.getId(), fileRepository, "File");
        File file = ConverterDto.convertToModel(fileDto);
        file.setId(fileDto.getId());
        fileRepository.save(file);
    }

    @Override
    public void delete(Long id) {
        checkEntityService.checkExist(id, fileRepository, "File");
        fileRepository.deleteById(id);
    }

    @Override
    public List<FileDto> getFilesByTransactionId(Long id) {
        return fileRepository.getFilesByTransactionId(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());

    }
}

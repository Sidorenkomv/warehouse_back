package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SubscriptionDto;
import com.warehouse_accounting.repositories.*;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SubscriptionService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.warehouse_accounting.util.ConverterDto.convertToDtoTariff;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final TariffRepository tariffRepository;
    private final RequisitesRepository requisitesRepository;
    private final EmployeeRepository employeeRepository;

    private final CheckEntityService checkEntityService;

    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository,
                                   RoleRepository roleRepository,
                                   RequisitesRepository requisitesRepository,
                                   EmployeeRepository employeeRepository,
                                   TariffRepository tariffRepository, CheckEntityService checkEntityService) {
        this.subscriptionRepository = subscriptionRepository;
        this.tariffRepository = tariffRepository;
        this.requisitesRepository = requisitesRepository;
        this.employeeRepository = employeeRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SubscriptionDto> getAll() {
        List<SubscriptionDto> subscriptionDtos = subscriptionRepository.getAll();

        subscriptionDtos.forEach(subscriptionDto -> {
            subscriptionDto.setRequisites(requisitesRepository.getById(subscriptionDto.getRequisites().getId()));
            // Костыль
            subscriptionDto.setEmployee(
                    ConverterDto.convertToDto(employeeRepository.findById(subscriptionDto.getEmployee().getId()).get())
            );
            subscriptionDto.setTariff(convertToDtoTariff(tariffRepository.getAllTariffById(subscriptionDto.getId())));
        });
        return subscriptionDtos;
    }

    @Override
    public SubscriptionDto getById(Long id) {
        checkEntityService.checkExist(id, subscriptionRepository, "Subscription");

        SubscriptionDto subscriptionDto = subscriptionRepository.getById(id);
        subscriptionDto.setRequisites(requisitesRepository.getById(subscriptionDto.getRequisites().getId()));
        // Костыль
        subscriptionDto.setEmployee(
                ConverterDto.convertToDto(employeeRepository.findById(subscriptionDto.getEmployee().getId()).get())
        );
        subscriptionDto.setTariff(convertToDtoTariff(tariffRepository.getAllTariffById(subscriptionDto.getId())));
        return subscriptionDto;
    }

    @Override
    public void create(SubscriptionDto subscriptionDto) {
        subscriptionRepository.save(ConverterDto.convertToModel(subscriptionDto));
    }

    @Override
    public void update(SubscriptionDto subscriptionDto) {
        checkEntityService.checkExist(subscriptionDto.getId(), subscriptionRepository, "Subscription");
        subscriptionRepository.save(ConverterDto.convertToModel(subscriptionDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, subscriptionRepository, "Subscription");
        subscriptionRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.TypeOfContractor;
import com.warehouse_accounting.models.dto.TypeOfContractorDto;
import com.warehouse_accounting.repositories.TypeOfContractorRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TypeOfContractorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TypeOfContractorServiceImpl implements TypeOfContractorService {
    private final TypeOfContractorRepository typeOfContractorRepository;

    private final CheckEntityService checkEntityService;

    public TypeOfContractorServiceImpl(TypeOfContractorRepository typeOfContractorRepository, CheckEntityService checkEntityService) {
        this.typeOfContractorRepository = typeOfContractorRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<TypeOfContractorDto> getAll() {
        return typeOfContractorRepository.getAll();
    }

    @Override
    public TypeOfContractorDto getById(Long id) {
        checkEntityService.checkExist(id, typeOfContractorRepository, "TypeOfContractor");
        return typeOfContractorRepository.getById(id);
    }

    @Override
    public void create(TypeOfContractorDto typeOfContractorDto) {
        typeOfContractorRepository.save(
                TypeOfContractor.builder()
                        .name(typeOfContractorDto.getName())
                        .sortNumber(typeOfContractorDto.getSortNumber())
                        .build()

        );
    }

    @Override
    public TypeOfContractor getByName(String name) {
        return typeOfContractorRepository.getByName(name);
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, typeOfContractorRepository, "TypeOfContractor");
        typeOfContractorRepository.deleteById(id);
    }

    @Override
    public void update(TypeOfContractorDto typeOfContractorDto) {
        checkEntityService.checkExist(typeOfContractorDto.getId(), typeOfContractorRepository, "TypeOfContractor");
        typeOfContractorRepository.save(
                TypeOfContractor.builder()
                        .id(typeOfContractorDto.getId())
                        .name(typeOfContractorDto.getName())
                        .sortNumber(typeOfContractorDto.getSortNumber())
                        .build()
        );
    }
}
package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.TariffDto;
import com.warehouse_accounting.repositories.TariffRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TariffService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TariffServiceImpl implements TariffService {

    private final TariffRepository tariffRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public TariffServiceImpl(TariffRepository tariffRepository, CheckEntityService checkEntityService) {
        this.tariffRepository = tariffRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<TariffDto> getAll() {
        return tariffRepository.getAll();
    }

    @Override
    public TariffDto getById(Long id) {
        checkEntityService.checkExist(id, tariffRepository, "Tariff");
        return tariffRepository.getById(id);
    }

    @Override
    public void create(TariffDto tariffDto) {
        tariffRepository.save(ConverterDto.convertToModel(tariffDto));
    }

    @Override
    public void update(TariffDto tariffDto) {
        checkEntityService.checkExist(tariffDto.getId(), tariffRepository, "Tariff");
        tariffRepository.save(ConverterDto.convertToModel(tariffDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, tariffRepository, "Tariff");
        tariffRepository.deleteById(id);
    }
}

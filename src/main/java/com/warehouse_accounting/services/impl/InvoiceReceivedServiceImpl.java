package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.InvoiceReceivedDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.ContractorRepository;
import com.warehouse_accounting.repositories.InvoiceReceivedRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.InvoiceReceivedService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InvoiceReceivedServiceImpl implements InvoiceReceivedService {

    private final InvoiceReceivedRepository invoiceReceivedRepository;
    private final CompanyRepository companyRepository;
    private final ContractorRepository contractorRepository;
    private final CheckEntityService checkEntityService;

    @Autowired
    public InvoiceReceivedServiceImpl(InvoiceReceivedRepository invoiceReceivedRepository,
                                      CompanyRepository companyRepository,
                                      ContractorRepository contractorRepository, CheckEntityService checkEntityService) {
        this.invoiceReceivedRepository = invoiceReceivedRepository;
        this.companyRepository = companyRepository;
        this.contractorRepository = contractorRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<InvoiceReceivedDto> getAll() {
        List<InvoiceReceivedDto> invoiceReceivedDtos = invoiceReceivedRepository.getAll();
        for (InvoiceReceivedDto invoiceReceivedDto : invoiceReceivedDtos) {
            invoiceReceivedDto.setCompany(
                    companyRepository.getById(
                            invoiceReceivedDto.getCompany().getId()
                    )
            );
            invoiceReceivedDto.setContractor(
                    contractorRepository.getById(
                            invoiceReceivedDto.getContractor().getId()
                    )
            );
        }
        return invoiceReceivedDtos;
    }

    @Override
    public InvoiceReceivedDto getById(Long id) {
        checkEntityService.checkExist(id, invoiceReceivedRepository, "InvoiceReceived");

        InvoiceReceivedDto invoiceReceivedDto = invoiceReceivedRepository.getById(id);
        invoiceReceivedDto.setCompany(
                companyRepository.getById(
                        invoiceReceivedDto.getCompany().getId()
                )
        );
        invoiceReceivedDto.setContractor(
                contractorRepository.getById(
                        invoiceReceivedDto.getContractor().getId()
                )
        );
        return invoiceReceivedDto;
    }

    @Override
    public void create(InvoiceReceivedDto invoiceReceivedDto) {
        invoiceReceivedRepository.save(ConverterDto.convertInvoiceReceivedDtoToModel(invoiceReceivedDto));
    }

    @Override
    public void update(InvoiceReceivedDto invoiceReceivedDto) {
        checkEntityService.checkExist(invoiceReceivedDto.getId(), invoiceReceivedRepository, "InvoiceReceived");
        invoiceReceivedRepository.save(ConverterDto.convertInvoiceReceivedDtoToModel(invoiceReceivedDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, invoiceReceivedRepository, "InvoiceReceived");
        invoiceReceivedRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.InvoicesIssuedDto;
import com.warehouse_accounting.repositories.InvoicesIssuedRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.InvoicesIssuedService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InvoicesIssuedServiceImpl implements InvoicesIssuedService {

    private final InvoicesIssuedRepository invoicesIssuedRepository;

    private final CheckEntityService checkEntityService;
    @Autowired
    public InvoicesIssuedServiceImpl(InvoicesIssuedRepository invoicesIssuedRepository, CheckEntityService checkEntityService) {
        this.invoicesIssuedRepository = invoicesIssuedRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<InvoicesIssuedDto> getAll() {
        return invoicesIssuedRepository.getAll();
    }

    @Override
    public InvoicesIssuedDto getById(Long id) {
        checkEntityService.checkExist(id, invoicesIssuedRepository, "InvoicesIssued");
        return invoicesIssuedRepository.getById(id);
    }

    @Override
    public void create(InvoicesIssuedDto invoicesIssuedDto) {
        invoicesIssuedRepository.save(ConverterDto.convertToModel(invoicesIssuedDto));
    }

    @Override
    public void update(InvoicesIssuedDto invoicesIssuedDto) {
        checkEntityService.checkExist(invoicesIssuedDto.getId(), invoicesIssuedRepository, "InvoicesIssued");
        invoicesIssuedRepository.save(ConverterDto.convertToModel(invoicesIssuedDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, invoicesIssuedRepository, "InvoicesIssued");
        invoicesIssuedRepository.deleteById(id);
    }
}

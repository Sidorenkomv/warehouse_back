package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.RetailShift;
import com.warehouse_accounting.models.dto.RetailShiftDto;
import com.warehouse_accounting.repositories.RetailShiftRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.RetailShiftService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class RetailShiftServiceImpl implements RetailShiftService {

    private final RetailShiftRepository retailShiftRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public RetailShiftServiceImpl(RetailShiftRepository retailShiftRepository, CheckEntityService checkEntityService) {
        this.retailShiftRepository = retailShiftRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<RetailShiftDto> getAll() {
        List<RetailShift> retailShift = retailShiftRepository.findAll();
        List<RetailShiftDto> retailShiftDtos = retailShift.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
        return retailShiftDtos;
    }

    @Override
    public RetailShiftDto getById(Long id) {
        checkEntityService.checkExist(id, retailShiftRepository, "RetailShift");
        return ConverterDto.convertToDto(retailShiftRepository.findById(id).get());
    }

    @Override
    public void create(RetailShiftDto retailShiftDto) {
        retailShiftRepository.save(ConverterDto.convertToModel(retailShiftDto));

    }

    @Override
    public void update(RetailShiftDto retailShiftDto) {
        checkEntityService.checkExist(retailShiftDto.getId(), retailShiftRepository, "RetailShift");
        retailShiftRepository.save(ConverterDto.convertToModel(retailShiftDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, retailShiftRepository, "RetailShift");
        retailShiftRepository.deleteById(id);
    }
}

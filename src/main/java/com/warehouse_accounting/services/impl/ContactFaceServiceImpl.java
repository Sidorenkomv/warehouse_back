package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ContactFaceDto;
import com.warehouse_accounting.repositories.ContactFaceRepository;
import com.warehouse_accounting.repositories.TypeOfContractorRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ContactFaceService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContactFaceServiceImpl implements ContactFaceService {

    private final ContactFaceRepository contactFaceRepository;

    private final CheckEntityService checkEntityService;

    public ContactFaceServiceImpl(ContactFaceRepository contactFaceRepository, TypeOfContractorRepository typeOfContractorRepository, CheckEntityService checkEntityService) {
        this.contactFaceRepository = contactFaceRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ContactFaceDto> getAll() {
        List<ContactFaceDto> contactFaceDtos = contactFaceRepository.getAll();
        return contactFaceDtos;
    }

    @Override
    public ContactFaceDto getById(Long id) {
        checkEntityService.checkExist(id, contactFaceRepository, "ContactFace");
        ContactFaceDto contactFaceDto = contactFaceRepository.getById(id);
        return contactFaceDto;
    }

    @Override
    public void create(ContactFaceDto dto) {
        contactFaceRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(ContactFaceDto dto) {
        checkEntityService.checkExist(dto.getId(), contactFaceRepository, "ContactFace");
        contactFaceRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, contactFaceRepository, "ContactFace");
        contactFaceRepository.deleteById(id);
    }
}
package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SupplierInvoiceDto;
import com.warehouse_accounting.repositories.SupplierInvoiceRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SupplierInvoiceService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SupplierInvoiceServiceImpl implements SupplierInvoiceService {

    private final SupplierInvoiceRepository supplierInvoiceRepository;

    private final CheckEntityService checkEntityService;
    public SupplierInvoiceServiceImpl(SupplierInvoiceRepository supplierInvoiceRepository, CheckEntityService checkEntityService) {
        this.supplierInvoiceRepository = supplierInvoiceRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SupplierInvoiceDto> getAll() {
        return supplierInvoiceRepository.getAll();
    }

    @Override
    public SupplierInvoiceDto getById(Long id) {
        checkEntityService.checkExist(id, supplierInvoiceRepository, "SupplierInvoice");
        return supplierInvoiceRepository.getById(id);
    }

    @Override
    public void create(SupplierInvoiceDto dto) {
        supplierInvoiceRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(SupplierInvoiceDto dto) {
        checkEntityService.checkExist(dto.getId(), supplierInvoiceRepository, "SupplierInvoice");
        supplierInvoiceRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, supplierInvoiceRepository, "SupplierInvoice");
        supplierInvoiceRepository.deleteById(id);
    }
}

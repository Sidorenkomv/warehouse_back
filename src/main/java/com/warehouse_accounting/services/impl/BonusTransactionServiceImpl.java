package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.BonusTransactionDto;
import com.warehouse_accounting.repositories.BonusTransactionRepository;
import com.warehouse_accounting.services.interfaces.BonusTransactionService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
//Не уверен, что проверки нужны. Работа будет происходить через фронт и до проверок не дойдет.
//Если визуально объектов не будет, то нельзя будет удалить или изменить например что-то чего нет и не видно.

@Service
@Transactional
public class BonusTransactionServiceImpl implements BonusTransactionService {
    private final BonusTransactionRepository bonusTransactionRepository;


    @Autowired
    public BonusTransactionServiceImpl(BonusTransactionRepository bonusTransactionRepository) {
        this.bonusTransactionRepository = bonusTransactionRepository;
    }

    @Override
    public List<BonusTransactionDto> getAll() {
        return bonusTransactionRepository.findAll().stream().map(ConverterDto::convertToDto).collect(Collectors.toList());

    }

    @Override
    public BonusTransactionDto getById(Long id) {

        return ConverterDto.convertToDto(bonusTransactionRepository.findById(id).get());

    }

    @Override
    public void create(BonusTransactionDto bonusTransactionDto) {
        bonusTransactionRepository.save(ConverterDto.convertToModel(bonusTransactionDto));
    }

    @Override
    public void update(BonusTransactionDto bonusTransactionDto) {

        bonusTransactionRepository.save(ConverterDto.convertToModel(bonusTransactionDto));
        
    }

    @Override
    public void deleteById(Long id) {

        bonusTransactionRepository.deleteById(id);

    }
}


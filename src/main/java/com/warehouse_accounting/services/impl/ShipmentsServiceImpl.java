package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ShipmentsDto;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.ShipmentsRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ShipmentsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ShipmentsServiceImpl implements ShipmentsService {

    private final ShipmentsRepository shipmentsRepository;
    private final TaskRepository taskRepository;

    private final FileRepository fileRepository;

    private final CheckEntityService checkEntityService;

    public ShipmentsServiceImpl(ShipmentsRepository shipmentsRepository, TaskRepository taskRepository, FileRepository fileRepository, CheckEntityService checkEntityService) {
        this.shipmentsRepository = shipmentsRepository;
        this.taskRepository = taskRepository;
        this.fileRepository = fileRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ShipmentsDto> getAll() {
        List<ShipmentsDto> shipmentsDtos = shipmentsRepository.getAll();
        for (ShipmentsDto shipmentsDto : shipmentsDtos) {
            shipmentsDto.setTasksDto(taskRepository.getListTaskById(shipmentsDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));

            shipmentsDto.setFilesDto(fileRepository.getListFileById(shipmentsDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }
        return shipmentsDtos;
    }

    @Override
    public ShipmentsDto getById(Long id) {
        checkEntityService.checkExist(id, shipmentsRepository, "Shipments");
        ShipmentsDto shipmentsDto = shipmentsRepository.getById(id);

        shipmentsDto.setTasksDto(taskRepository.getListTaskById(shipmentsDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));

        shipmentsDto.setFilesDto(fileRepository.getListFileById(shipmentsDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return shipmentsDto;
    }

    @Override
    public void create(ShipmentsDto shipmentsDto) {
        shipmentsRepository.save(ConverterDto.convertToModel(shipmentsDto));
    }

    @Override
    public void update(ShipmentsDto shipmentsDto) {
        checkEntityService.checkExist(shipmentsDto.getId(), shipmentsRepository, "Shipments");
        shipmentsRepository.save(ConverterDto.convertToModel(shipmentsDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, shipmentsRepository, "Shipments");
        shipmentsRepository.deleteById(id);
    }
}

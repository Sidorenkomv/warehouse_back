package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.Currency;
import com.warehouse_accounting.models.dto.CurrencyDto;
import com.warehouse_accounting.repositories.CurrencyRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CurrencyService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Data
public class CurrencyServiceImpl implements CurrencyService {

    private CurrencyRepository currencyRepository;

    private CheckEntityService checkEntityService;

    public CurrencyServiceImpl(CurrencyRepository currencyRepository, CheckEntityService checkEntityService) {
        this.currencyRepository = currencyRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CurrencyDto> getAll() {
        return currencyRepository.getAll();
    }

    @Override
    public CurrencyDto getById(Long id) {
        checkEntityService.checkExist(id, currencyRepository, "Currency");
        return currencyRepository.getById(id);
    }

    @Override
    public void create(CurrencyDto currencyDto) {

        currencyRepository.save(ConverterDto.convertToModel(currencyDto));
    }

    @Override
    public void update(CurrencyDto currencyDto) {
        checkEntityService.checkExist(currencyDto.getId(), currencyRepository, "Currency");
        currencyRepository.save(ConverterDto.convertToModel(currencyDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, currencyRepository, "Currency");
        currencyRepository.deleteById(id);
    }

    @Override
    public void saveAll(List<Currency> currencyList) {
        currencyRepository.saveAll(currencyList);
    }

}

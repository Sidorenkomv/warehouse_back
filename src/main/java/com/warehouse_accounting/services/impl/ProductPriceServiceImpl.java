package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.exceptions.EntityNotFoundException;
import com.warehouse_accounting.models.ProductPrice;
import com.warehouse_accounting.models.dto.ProductPriceDto;
import com.warehouse_accounting.models.dto.ProductPriceForPriceListDto;
import com.warehouse_accounting.repositories.ProductPriceRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductPriceService;
import com.warehouse_accounting.util.ConverterDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductPriceServiceImpl implements ProductPriceService {

    public static final String PRODUCT_PRICE = "ProductPrice";
    public static final String PRODUCT_PRICE_NOT_FOUND = "ProductPrice с id=%s не найден";
    public static final String PRODUCT_NOT_FOUND = "Product с id=%s не найден";

    private final ProductPriceRepository productPriceRepository;

    private final CheckEntityService checkEntityService;

    @Override
    public List<ProductPriceForPriceListDto> getAll() {
        return productPriceRepository.findAll().stream()
                .map(ConverterDto::convertToDtoForPriceList)
                .collect(toList());
    }

    @Override
    public ProductPriceForPriceListDto getById(Long id) {
        return productPriceRepository.findById(id)
                .map(ConverterDto::convertToDtoForPriceList)
                .orElseThrow(() -> new EntityNotFoundException(String.format(PRODUCT_PRICE_NOT_FOUND, id)));
    }

    @Override
    public ProductPriceDto getProductPriceById(Long id) {
        return productPriceRepository.findById(id)
                .map(ConverterDto::convertToDto)
                .orElseThrow(() -> new EntityNotFoundException(String.format(PRODUCT_PRICE_NOT_FOUND, id)));
    }

    @Override
    public List<ProductPriceForPriceListDto> getListProductPriceByProductId(Long id) {
        var productPrices = productPriceRepository.findAllByProductId(id);

        if (productPrices == null) throw new EntityNotFoundException(String.format(PRODUCT_NOT_FOUND, id));

        return productPrices.stream()
                .map(ConverterDto::convertToDtoForPriceList)
                .collect(toList());
    }

    @Override
    public void create(List<ProductPriceDto> productPriceDto) {
        List<ProductPrice> productPrice = productPriceDto.stream()
                .map(ConverterDto::convertToModel)
                .collect(toList());

        productPriceRepository.saveAll(productPrice);
    }

    @Override
    public void update(ProductPriceDto productPriceDto) {
        checkEntityService.checkExist(productPriceDto.getId(), productPriceRepository, PRODUCT_PRICE);

        productPriceRepository.save(ConverterDto.convertToModel(productPriceDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, productPriceRepository, PRODUCT_PRICE);

        productPriceRepository.deleteById(id);
    }
}

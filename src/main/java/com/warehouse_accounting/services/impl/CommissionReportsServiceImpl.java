package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CommissionReportsDto;
import com.warehouse_accounting.repositories.*;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CommissionReportsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommissionReportsServiceImpl implements CommissionReportsService {

    private static final String CHECK_NAME = "CommissionReports";
    private final CommissionReportsRepository commissionReportsRepository;
    private final ContractRepository contractRepository;
    private final ContractorRepository contractorRepository;
    private final CompanyRepository companyRepository;
    private final ProjectRepository projectRepository;
    private final SalesChannelRepository salesChannelRepository;

    private final CheckEntityService checkEntityService;

    public CommissionReportsServiceImpl(CommissionReportsRepository commissionReportsRepository,
                                        ContractRepository contractRepository,
                                        ContractorRepository contractorRepository,
                                        CompanyRepository companyRepository,
                                        ProjectRepository projectRepository,
                                        SalesChannelRepository salesChannelRepository, CheckEntityService checkEntityService) {
        this.commissionReportsRepository = commissionReportsRepository;
        this.contractRepository = contractRepository;
        this.contractorRepository = contractorRepository;
        this.companyRepository = companyRepository;
        this.projectRepository = projectRepository;
        this.salesChannelRepository = salesChannelRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CommissionReportsDto> getAll() {
        List<CommissionReportsDto> dtos = commissionReportsRepository.getAll();
        for (CommissionReportsDto cr : dtos) {
            cr.setContractDto(contractRepository.getById(cr.getContractDto().getId()));
            cr.setContractorDto(contractorRepository.getById(cr.getContractorDto().getId()));
            cr.setCompanyDto(companyRepository.getById(cr.getCompanyDto().getId()));
            cr.setProjectDto(projectRepository.getById(cr.getProjectDto().getId()));
            cr.setSalesChannelDto(salesChannelRepository.getById(cr.getSalesChannelDto().getId()));
        }
        return dtos;
    }

    @Override
    public CommissionReportsDto getById(Long id) {
        checkEntityService.checkExist(id, commissionReportsRepository, CHECK_NAME);

        CommissionReportsDto dto = commissionReportsRepository.getById(id);
        dto.setContractDto(contractRepository.getById(dto.getContractDto().getId()));
        dto.setContractorDto(contractorRepository.getById(dto.getContractorDto().getId()));
        dto.setCompanyDto(companyRepository.getById(dto.getCompanyDto().getId()));
        dto.setProjectDto(projectRepository.getById(dto.getProjectDto().getId()));
        dto.setSalesChannelDto(salesChannelRepository.getById(dto.getSalesChannelDto().getId()));
        return dto;
    }

    @Override
    public void create(CommissionReportsDto commissionReportsDto) {
        commissionReportsRepository.save(ConverterDto.convertToModel(commissionReportsDto));
    }

    @Override
    public void update(CommissionReportsDto commissionReportsDto) {
        checkEntityService.checkExist(commissionReportsDto.getId(), commissionReportsRepository, CHECK_NAME);
        commissionReportsRepository.save(ConverterDto.convertToModel(commissionReportsDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, commissionReportsRepository, CHECK_NAME);
        commissionReportsRepository.deleteById(id);
    }
}

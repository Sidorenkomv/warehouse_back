package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.NotificationsDto;
import com.warehouse_accounting.repositories.NotificationsRepository;
import com.warehouse_accounting.repositories.SelectorRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.NotificationsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class NotificationsServiceImpl implements NotificationsService {
    private final NotificationsRepository notificationsRepository;
    private final SelectorRepository selectorRepository;

    private final CheckEntityService checkEntityService;

    public NotificationsServiceImpl(NotificationsRepository notificationsRepository, SelectorRepository selectorRepository, CheckEntityService checkEntityService) {
        this.notificationsRepository = notificationsRepository;
        this.selectorRepository = selectorRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<NotificationsDto> getAll() {
        List<NotificationsDto> notificationsDtos = notificationsRepository.getAll();
        for (NotificationsDto notificationsDto : notificationsDtos) {
            notificationsDto.setBuyerOrders(selectorRepository.getById(notificationsDto.getBuyerOrders().getId()));
            notificationsDto.setBuyersInvoices(selectorRepository.getById(notificationsDto.getBuyersInvoices().getId()));
            notificationsDto.setDataExchange(selectorRepository.getById(notificationsDto.getDataExchange().getId()));
            notificationsDto.setRemainder(selectorRepository.getById(notificationsDto.getRemainder().getId()));
            notificationsDto.setOnlineStores(selectorRepository.getById(notificationsDto.getOnlineStores().getId()));
            notificationsDto.setRetail(selectorRepository.getById(notificationsDto.getRetail().getId()));
            notificationsDto.setScripts(selectorRepository.getById(notificationsDto.getScripts().getId()));
            notificationsDto.setTasks(selectorRepository.getById(notificationsDto.getTasks().getId()));
        }
        return notificationsDtos;


    }

    @Override
    public NotificationsDto getById(Long id) {
        checkEntityService.checkExist(id, notificationsRepository, "Notification");

        NotificationsDto notificationsDto = notificationsRepository.getById(id);
        notificationsDto.setBuyerOrders(selectorRepository.getById(notificationsDto.getBuyerOrders().getId()));
        notificationsDto.setBuyersInvoices(selectorRepository.getById(notificationsDto.getBuyersInvoices().getId()));
        notificationsDto.setDataExchange(selectorRepository.getById(notificationsDto.getDataExchange().getId()));
        notificationsDto.setRemainder(selectorRepository.getById(notificationsDto.getRemainder().getId()));
        notificationsDto.setOnlineStores(selectorRepository.getById(notificationsDto.getOnlineStores().getId()));
        notificationsDto.setRetail(selectorRepository.getById(notificationsDto.getRetail().getId()));
        notificationsDto.setScripts(selectorRepository.getById(notificationsDto.getScripts().getId()));
        notificationsDto.setTasks(selectorRepository.getById(notificationsDto.getTasks().getId()));
        return notificationsDto;
    }

    @Override
    public void create(NotificationsDto notificationsDto) {
        notificationsRepository.save(ConverterDto.convertToModel(notificationsDto));

    }

    @Override
    public void update(NotificationsDto notificationsDto) {
        checkEntityService.checkExist(notificationsDto.getId(), notificationsRepository, "Notification");
        notificationsRepository.save(ConverterDto.convertToModel(notificationsDto));

    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, notificationsRepository, "Notification");
        notificationsRepository.deleteById(id);
    }
}

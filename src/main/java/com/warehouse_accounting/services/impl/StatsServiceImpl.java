package com.warehouse_accounting.services.impl;


import com.warehouse_accounting.models.dto.AdjustmentDto;
import com.warehouse_accounting.models.dto.PaymentDto;
import com.warehouse_accounting.models.dto.PointOfSalesDto;
import com.warehouse_accounting.models.dto.StatsDto;
import com.warehouse_accounting.models.dto.SupplierOrdersDto;
import com.warehouse_accounting.services.interfaces.AdjustmentService;
import com.warehouse_accounting.services.interfaces.PaymentService;
import com.warehouse_accounting.services.interfaces.PointOfSalesService;
import com.warehouse_accounting.services.interfaces.StatsService;
import com.warehouse_accounting.services.interfaces.SupplierOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class StatsServiceImpl implements StatsService {

    private final PaymentService paymentService;
    private final PointOfSalesService pointOfSalesService;
    private final AdjustmentService adjustmentService;
    private final SupplierOrdersService supplierOrdersService;

    @Autowired
    public StatsServiceImpl(PaymentService paymentService, PointOfSalesService pointOfSalesService, AdjustmentService adjustmentService, SupplierOrdersService supplierOrdersService) {
        this.paymentService = paymentService;
        this.pointOfSalesService = pointOfSalesService;
        this.adjustmentService = adjustmentService;
        this.supplierOrdersService = supplierOrdersService;
    }

    @Override
    public StatsDto getStatsDto() {
        List<AdjustmentDto> adjustmentDtoList = adjustmentService.getAll();
        AdjustmentDto adjustmentDto = null;
        if (!adjustmentDtoList.isEmpty()) {
            adjustmentDto = adjustmentDtoList.get(adjustmentDtoList.size() - 1);
        }
        List<PaymentDto> paymentDtoList = paymentService.getAll();
        List<PointOfSalesDto> pointOfSalesDtoList = pointOfSalesService.getAll();
        BigDecimal allAmountOfPayments = new BigDecimal(0);
        if (!paymentDtoList.isEmpty()) {
            for (PaymentDto paymentDto : paymentDtoList) {
                allAmountOfPayments = allAmountOfPayments.add(paymentDto.getAmount());
            }
        }

        if (!pointOfSalesDtoList.isEmpty()) {
            for (PointOfSalesDto pointOfSalesDto : pointOfSalesDtoList) {
                allAmountOfPayments = allAmountOfPayments.add(pointOfSalesDto.getCheque());
            }
        }
        List<SupplierOrdersDto> supplierOrdersDtoList = supplierOrdersService.getAll();
        BigDecimal allAmountOfExpiredOrders = new BigDecimal(0);



        int totalCountOfExpiredOrders = 0;
        if (!supplierOrdersDtoList.isEmpty()) {
            for (SupplierOrdersDto supplierOrdersDto : supplierOrdersDtoList) {
                if (!supplierOrdersDto.getSent()) {
                    allAmountOfExpiredOrders = allAmountOfExpiredOrders.add(supplierOrdersDto.getSum());
                    totalCountOfExpiredOrders++;
                }
            }
        }


        return StatsDto.builder()
                .company(adjustmentDto.getCompanyName())
                .companyBalance(adjustmentDto.getTotalBalance())
                .paymentCount(paymentDtoList.size() + pointOfSalesDtoList.size())
                .paymentAmount(allAmountOfPayments)
                .expiredOrdersCount(totalCountOfExpiredOrders)
                .expiredOrdersAmount(allAmountOfExpiredOrders)
                .build();
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.RegionDto;
import com.warehouse_accounting.repositories.RegionRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.RegionService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;

    private final CheckEntityService checkEntityService;

    public RegionServiceImpl(RegionRepository regionRepository, CheckEntityService checkEntityService) {
        this.regionRepository = regionRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<RegionDto> getAll() {
        return regionRepository.getAll();
    }

    @Override
    public RegionDto getById(Long id) {
        checkEntityService.checkExist(id, regionRepository, "Region");
        return regionRepository.getById(id);
    }

    @Override
    public RegionDto getByCode(String code) {
        RegionDto dto = regionRepository.getByCode(code);
        checkEntityService.checkNull(dto, "RegionDto с кодом " + code + " не существует");
        return regionRepository.getByCode(code);
    }

    @Override
    public void create(RegionDto regionDto) {
        regionRepository.save(ConverterDto.convertToModel(regionDto));
    }

    @Override
    public void update(RegionDto regionDto) {
        checkEntityService.checkExist(regionDto.getId(), regionRepository, "Region");

        regionRepository.save(ConverterDto.convertToModel(regionDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, regionRepository, "Region");
        regionRepository.deleteById(id);
    }
}

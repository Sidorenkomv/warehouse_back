package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ProductDto;
import com.warehouse_accounting.repositories.*;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ProductService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductPriceRepository productPriceRepository;
    private final ImageRepository imageRepository;

    private final CheckEntityService checkEntityService;

    public ProductServiceImpl(ProductRepository productRepository, ProductPriceRepository productPriceRepository,
                              ImageRepository imageRepository, CheckEntityService checkEntityService) {
        this.productRepository = productRepository;
        this.productPriceRepository = productPriceRepository;
        this.imageRepository = imageRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ProductDto> getAll() {
        List<ProductDto> productDtos = productRepository.getAll();
        productDtos.forEach(productDto -> {
            productDto.setProductPricesDto(productPriceRepository.getListProductPriceById(productDto.getId()).stream()
                    .map(ConverterDto::convertToDto).collect(Collectors.toList()));
            productDto.setImagesDto(imageRepository.getListImageById(productDto.getId()).stream()
                    .map(ConverterDto::convertToDto).collect(Collectors.toList()));
        });
        return productDtos;
    }

    @Override
    public ProductDto getById(Long id) {
        checkEntityService.checkExist(id, productRepository, "Product");

        ProductDto productDto = productRepository.getById(id);
        productDto.setProductPricesDto(productPriceRepository.getListProductPriceById(productDto.getId()).stream()
                .map(ConverterDto::convertToDto).collect(Collectors.toList()));
        productDto.setImagesDto(imageRepository.getListImageById(productDto.getId()).stream()
                .map(ConverterDto::convertToDto).collect(Collectors.toList()));
        return productDto;
    }

    @Override
    public void create(ProductDto productDto) {
        productRepository.save(ConverterDto.convertToModel(productDto));
    }

    @Override
    public void update(ProductDto productDto) {
        checkEntityService.checkExist(productDto.getId(), productRepository, "Product");
        productRepository.save(ConverterDto.convertToModel(productDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, productRepository, "Product");
        productRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.WriteOffsDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.repositories.WriteOffsRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.WriteOffsService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WriteOffsServiceIml implements WriteOffsService {

    private final WriteOffsRepository writeOffsRepository;
    private final CompanyRepository companyRepository;
    private final WarehouseRepository warehouseRepository;

    private final CheckEntityService checkEntityService;

    public WriteOffsServiceIml(WriteOffsRepository writeOffsRepository, CompanyRepository companyRepository, WarehouseRepository warehouseRepository, CheckEntityService checkEntityService) {
        this.writeOffsRepository = writeOffsRepository;
        this.companyRepository = companyRepository;
        this.warehouseRepository = warehouseRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<WriteOffsDto> getAll() {
        List<WriteOffsDto> dtos = writeOffsRepository.getAll();
        for (WriteOffsDto wo : dtos) {
            wo.setWarehouseFrom(warehouseRepository.getById(wo.getWarehouseFrom().getId()));
            wo.setWarehouseTo(warehouseRepository.getById(wo.getWarehouseTo().getId()));
            wo.setCompany(companyRepository.getById(wo.getCompany().getId()));
        }
        return dtos;
    }

    @Override
    public List<WriteOffsDto> getAllTest() {
        return writeOffsRepository.getAll();
    }

    @Override
    public WriteOffsDto getById(Long id) {
        checkEntityService.checkExist(id, writeOffsRepository, "WriteOffs");

        return writeOffsRepository.getById(id);
    }

    @Override
    public WriteOffsDto getByIdTest(Long id) {
        return writeOffsRepository.getById(id);
    }

    @Override
    public void create(WriteOffsDto dto) {
        writeOffsRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(WriteOffsDto dto) {
        checkEntityService.checkExist(dto.getId(), writeOffsRepository, "WriteOffs");
        writeOffsRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, writeOffsRepository, "WriteOffs");
        writeOffsRepository.deleteById(id);
    }

}
package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.TaskDto;
import com.warehouse_accounting.models.dto.TechnologicalOperationDto;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.repositories.TechnologicalOperationRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.TechnologicalOperationService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TechnologicalOperationServiceImpl implements TechnologicalOperationService {

    private final TechnologicalOperationRepository technologicalOperationRepository;
    private final TaskRepository taskRepository;

    private final CheckEntityService checkEntityService;

    private final TaskServiceImpl taskService;

    public TechnologicalOperationServiceImpl(TechnologicalOperationRepository technologicalOperationRepository,
                                             TaskRepository taskRepository, CheckEntityService checkEntityService, TaskServiceImpl taskService) {
        this.technologicalOperationRepository = technologicalOperationRepository;
        this.taskRepository = taskRepository;
        this.checkEntityService = checkEntityService;
        this.taskService = taskService;
    }

    @Override
    public List<TechnologicalOperationDto> getAll() {
        List<TechnologicalOperationDto> technologicalOperationDtos = technologicalOperationRepository.getAll();
        for (TechnologicalOperationDto technologicalOperationDto : technologicalOperationDtos) {
            technologicalOperationDto.setTasks(taskRepository
                    .getListTaskById(technologicalOperationDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }

        return technologicalOperationDtos;
    }

    @Override
    public TechnologicalOperationDto getById(Long id) {
        checkEntityService.checkExist(id, technologicalOperationRepository, "TechnologicalOperation");

        TechnologicalOperationDto technologicalOperationDto = technologicalOperationRepository.getById(id);
        technologicalOperationDto.setTasks(taskRepository
                .getListTaskById(technologicalOperationDto.getId())
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return technologicalOperationDto;
    }

    @Override
    public void create(TechnologicalOperationDto technologicalOperationDto) {
        technologicalOperationRepository.save(ConverterDto.convertToModel(technologicalOperationDto));
    }

    @Override
    public void update(TechnologicalOperationDto technologicalOperationDto) {
        checkEntityService.checkExist(technologicalOperationDto.getId(), technologicalOperationRepository, "TechnologicalOperation");
        technologicalOperationRepository.save(ConverterDto.convertToModel(technologicalOperationDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, technologicalOperationRepository, "TechnologicalOperation");

        for (TaskDto t : taskService.getAll()) {
            if (t.getDocumentId() == id) {
                taskService.deleteById(t.getId());
            }
        }

        technologicalOperationRepository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.ImageDto;
import com.warehouse_accounting.repositories.ImageRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.ImageService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    private final CheckEntityService checkEntityService;

    public ImageServiceImpl(ImageRepository imageRepository, CheckEntityService checkEntityService) {
        this.imageRepository = imageRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<ImageDto> getAll() {
        return imageRepository.getAll();
    }

    @Override
    public ImageDto getById(Long id) {
        checkEntityService.checkExist(id, imageRepository, "Image");
        return imageRepository.getById(id);
    }

    @Override
    public void create(ImageDto imageDto) {
        imageRepository.save(ConverterDto.convertToModel(imageDto));
    }

    @Override
    public void update(ImageDto imageDto) {
        checkEntityService.checkExist(imageDto.getId(), imageRepository, "Image");
        imageRepository.save(ConverterDto.convertToModel(imageDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, imageRepository, "Image");
        imageRepository.deleteById(id);
    }
}

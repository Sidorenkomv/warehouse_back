package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.BonusProgram;
import com.warehouse_accounting.models.dto.BonusProgramDto;
import com.warehouse_accounting.repositories.BonusProgramRepository;
import com.warehouse_accounting.services.interfaces.BonusProgramService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BonusProgramServiceImpl implements BonusProgramService {

    private final BonusProgramRepository bonusProgramRepository;

    @Autowired
    public BonusProgramServiceImpl(BonusProgramRepository bonusProgramRepository) {
        this.bonusProgramRepository = bonusProgramRepository;
    }

    @Override
    public List<BonusProgramDto> getAll() {

        return ConverterDto.convertBonusProgramToListDto(bonusProgramRepository.findAll());
    }

    @Override
    public BonusProgramDto getById(Long id) {

        return ConverterDto.convertToDto(bonusProgramRepository.getOne(id));
    }

    @Override
    public void create(BonusProgramDto dto) {
        bonusProgramRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(BonusProgramDto dto) {
        bonusProgramRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        bonusProgramRepository.deleteById(id);
    }
}

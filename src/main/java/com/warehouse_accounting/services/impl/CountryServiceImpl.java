package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CountryDto;
import com.warehouse_accounting.repositories.CountryRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CountryService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    private final CheckEntityService checkEntityService;

    public CountryServiceImpl(CountryRepository countryRepository, CheckEntityService checkEntityService) {
        this.countryRepository = countryRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CountryDto> getAll() {
        return countryRepository.getAll();
    }

    @Override
    public CountryDto getById(Long id) {
        checkEntityService.checkExist(id, countryRepository, "Country");
        return countryRepository.getById(id);
    }

    @Override
    public void create(CountryDto countryDto) {
        countryRepository.save(ConverterDto.convertToModel(countryDto));
    }

    @Override
    public void update(CountryDto countryDto) {
        checkEntityService.checkExist(countryDto.getId(), countryRepository, "Country");
        countryRepository.save(ConverterDto.convertToModel(countryDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, countryRepository, "Country");
        countryRepository.deleteById(id);
    }
}

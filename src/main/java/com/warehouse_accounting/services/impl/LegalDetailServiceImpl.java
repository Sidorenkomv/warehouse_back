package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.LegalDetail;
import com.warehouse_accounting.models.dto.LegalDetailDto;
import com.warehouse_accounting.repositories.LegalDetailRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.LegalDetailService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LegalDetailServiceImpl implements LegalDetailService {

    private final LegalDetailRepository legalDetailRepository;

    private final CheckEntityService checkEntityService;
    public LegalDetailServiceImpl(LegalDetailRepository legalDetailRepository, CheckEntityService checkEntityService) {
        this.legalDetailRepository = legalDetailRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<LegalDetailDto> getAll() {
        return legalDetailRepository.getAll();
    }

    @Override
    public LegalDetailDto getById(Long id) {
        checkEntityService.checkExist(id, legalDetailRepository, "LegalDetail");
        return legalDetailRepository.getById(id);
    }

    @Override
    public void create(LegalDetailDto legalDetailDto) {
        legalDetailRepository.save(ConverterDto.convertToModel(legalDetailDto));
    }

    @Override
    public void update(LegalDetailDto legalDetailDto) {
        checkEntityService.checkExist(legalDetailDto.getId(), legalDetailRepository, "LegalDetail");
        legalDetailRepository.save(ConverterDto.convertToModel(legalDetailDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, legalDetailRepository, "LegalDetail");
        legalDetailRepository.deleteById(id);
    }

    @Override
    public List<LegalDetailDto> getAllBySpecification(Specification<LegalDetail> specifications) {
        return legalDetailRepository.findAll(specifications)
                .stream()
                .map(ConverterDto::convertToDto).collect(Collectors.toList());
    }
}

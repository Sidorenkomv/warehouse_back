package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.WarehouseDto;
import com.warehouse_accounting.repositories.WarehouseRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.WarehouseService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WarehouseServiceImpl implements WarehouseService {
    private final WarehouseRepository warehouseRepository;
    private final CheckEntityService checkEntityService;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, CheckEntityService checkEntityService) {
        this.warehouseRepository = warehouseRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public WarehouseDto getById(Long id) {
        checkEntityService.checkExist(id, warehouseRepository, "Warehouse");
        return warehouseRepository.getById(id);
    }

    @Override
    public void create(WarehouseDto warehouseDto) {
        warehouseRepository.save(ConverterDto.convertToModel(warehouseDto));
    }

    @Override
    public void update(WarehouseDto warehouseDto) {
        checkEntityService.checkExist(warehouseDto.getId(), warehouseRepository, "Warehouse");
        warehouseRepository.save(ConverterDto.convertToModel(warehouseDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, warehouseRepository, "Warehouse");
        warehouseRepository.deleteById(id);
    }

    @Override
    public List<WarehouseDto> getAll() {
        return warehouseRepository.getAll();
    }
}

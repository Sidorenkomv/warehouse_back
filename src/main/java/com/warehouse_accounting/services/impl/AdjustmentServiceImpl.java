package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.AdjustmentDto;
import com.warehouse_accounting.repositories.AdjustmentRepository;
import com.warehouse_accounting.services.interfaces.AdjustmentService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AdjustmentServiceImpl implements AdjustmentService {

    private final AdjustmentRepository adjustmentRepository;

    private final CheckEntityService checkEntityService;

    public AdjustmentServiceImpl(AdjustmentRepository repository, CheckEntityService checkEntityService) {
        this.adjustmentRepository = repository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<AdjustmentDto> getAll() {
        return adjustmentRepository.getAll();
    }

    @Override
    public AdjustmentDto getById(Long id) {
        checkEntityService.checkExist(id, adjustmentRepository, "Adjustment");
        return adjustmentRepository.getById(id);
    }

    @Override
    public void create(AdjustmentDto dto) {
        adjustmentRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(AdjustmentDto dto) {
        checkEntityService.checkExist(dto.getId(), adjustmentRepository, "Adjustment");
        adjustmentRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, adjustmentRepository, "Adjustment");
        adjustmentRepository.deleteById(id);
    }
}

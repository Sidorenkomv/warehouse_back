package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CallDto;
import com.warehouse_accounting.repositories.CallRepository;
import com.warehouse_accounting.services.interfaces.CallService;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CallServiceImpl implements CallService {

    private final CallRepository callRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public CallServiceImpl(CallRepository callRepository, CheckEntityService checkEntityService) {
        this.callRepository = callRepository;
        this.checkEntityService = checkEntityService;
    }


    @Override
    public List<CallDto> getAll() {
        return callRepository.getAll();
    }

    @Override
    public CallDto getById(Long id) {
        checkEntityService.checkExist(id, callRepository, "Call");
        return callRepository.getById(id);
    }

    @Override
    public void create(CallDto callDto) {
        callRepository.save(ConverterDto.convertToModel(callDto));
    }

    @Override
    public void update(CallDto callDto) {
        checkEntityService.checkExist(callDto.getId(), callRepository, "Call");
        callRepository.save(ConverterDto.convertToModel(callDto));

    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, callRepository, "Call");
        callRepository.deleteById(id);
    }
}

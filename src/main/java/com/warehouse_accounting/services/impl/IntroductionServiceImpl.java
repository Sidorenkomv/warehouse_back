package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.Introduction;
import com.warehouse_accounting.models.dto.IntroductionDto;
import com.warehouse_accounting.repositories.IntroductionRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.IntroductionService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class IntroductionServiceImpl implements IntroductionService {
    private final IntroductionRepository introductionRepository;

    private final CheckEntityService checkEntityService;

    public IntroductionServiceImpl(IntroductionRepository introductionRepository, CheckEntityService checkEntityService) {
        this.introductionRepository = introductionRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<IntroductionDto> getAll() {
        List<Introduction> introduction = introductionRepository.findAll();
        List<IntroductionDto> introductionDtoList = introduction.stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList());
        return introductionDtoList;
    }

    @Override
    public IntroductionDto getById(Long id) {
        checkEntityService.checkExist(id, introductionRepository, "Introduction");
        IntroductionDto introductionDto = ConverterDto.convertToDto(introductionRepository.findById(id).get());
        return introductionDto;
    }

    @Override
    public void create(IntroductionDto introductionDto) {
        introductionRepository.save(ConverterDto.convertToModel(introductionDto));
    }

    @Override
    public void update(IntroductionDto introductionDto) {
        checkEntityService.checkExist(introductionDto.getId(), introductionRepository, "Introduction");
        introductionRepository.save(ConverterDto.convertToModel(introductionDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, introductionRepository, "Introduction");
        introductionRepository.deleteById(id);
    }
}

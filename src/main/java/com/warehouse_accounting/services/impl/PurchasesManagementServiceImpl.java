package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PurchasesManagementDto;
import com.warehouse_accounting.repositories.PurchasesManagementRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PurchasesManagementService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchasesManagementServiceImpl implements PurchasesManagementService {

    private final PurchasesManagementRepository purchasesManagementRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public PurchasesManagementServiceImpl(PurchasesManagementRepository purchasesManagementRepository, CheckEntityService checkEntityService) {
        this.purchasesManagementRepository = purchasesManagementRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PurchasesManagementDto> getAll() {
        return purchasesManagementRepository.getAll();
    }

    @Override
    public PurchasesManagementDto getById(Long id) {
        checkEntityService.checkExist(id, purchasesManagementRepository, "PurchasesManagement");
        return purchasesManagementRepository.getById(id);
    }

    @Override
    public void create(PurchasesManagementDto purchasesManagementDto) {
        purchasesManagementRepository.save(ConverterDto.convertToModel(purchasesManagementDto));
    }

    @Override
    public void update(PurchasesManagementDto purchasesManagementDto) {
        checkEntityService.checkExist(purchasesManagementDto.getId(), purchasesManagementRepository, "PurchasesManagement");

        purchasesManagementRepository.save(ConverterDto.convertToModel(purchasesManagementDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, purchasesManagementRepository, "PurchasesManagement");
        purchasesManagementRepository.deleteById(id);
    }
}

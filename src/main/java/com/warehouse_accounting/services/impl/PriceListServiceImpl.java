package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.PriceListDto;
import com.warehouse_accounting.repositories.CompanyRepository;
import com.warehouse_accounting.repositories.PriceListRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.PriceListService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PriceListServiceImpl implements PriceListService {
    private final PriceListRepository priceListRepository;

    private final CompanyRepository companyRepository;

    private final CheckEntityService checkEntityService;

    public PriceListServiceImpl(PriceListRepository priceListRepository, CompanyRepository companyRepository, CheckEntityService checkEntityService) {
        this.priceListRepository = priceListRepository;
        this.companyRepository = companyRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<PriceListDto> getAll() {
        List<PriceListDto> priceListDtos = priceListRepository.getAll();
        for (PriceListDto priceListDto : priceListDtos) {
            priceListDto.setCompany(
                    companyRepository.getById(
                            priceListDto.getCompany().getId()
                    )
            );
        }
        return priceListDtos;
    }

    @Override
    public PriceListDto getById(Long id) {
        checkEntityService.checkExist(id, priceListRepository, "PriceList");

        PriceListDto priceListDto = priceListRepository.getById(id);
        priceListDto.setCompany(
                companyRepository.getById(
                        priceListDto.getCompany().getId()
                )
        );
        return priceListDto;
    }

    @Override
    public void create(PriceListDto dto) {
        priceListRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void update(PriceListDto dto) {
        checkEntityService.checkExist(dto.getId(), priceListRepository, "PriceList");
        priceListRepository.save(ConverterDto.convertToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, priceListRepository, "PriceList");
        priceListRepository.deleteById(id);
    }
}

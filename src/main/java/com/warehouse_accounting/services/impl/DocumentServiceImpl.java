package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.DocumentDto;
import com.warehouse_accounting.repositories.DocumentRepository;
import com.warehouse_accounting.repositories.FileRepository;
import com.warehouse_accounting.repositories.TaskRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.DocumentService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    private final TaskRepository taskRepository;

    private final FileRepository fileRepository;

    private final CheckEntityService checkEntityService;

    @Autowired
    public DocumentServiceImpl(DocumentRepository documentRepository, TaskRepository taskRepository,
                               FileRepository fileRepository, CheckEntityService checkEntityService) {
        this.documentRepository = documentRepository;
        this.taskRepository = taskRepository;
        this.fileRepository = fileRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<DocumentDto> getAll() {
        List<DocumentDto> documentDtos = documentRepository.getAll();
        for (DocumentDto documentDto : documentDtos) {
            documentDto.setTasks(taskRepository.getListTaskById(documentDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
            documentDto.setFiles(fileRepository.getListFileById(documentDto.getId())
                    .stream()
                    .map(ConverterDto::convertToDto)
                    .collect(Collectors.toList()));
        }
        return documentDtos;
    }

    @Override
    public DocumentDto getById(Long id) {
        checkEntityService.checkExist(id, documentRepository, "Document");

        DocumentDto documentDto = documentRepository.getById(id);
        documentDto.setTasks(taskRepository.getListTaskById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        documentDto.setFiles(fileRepository.getListFileById(id)
                .stream()
                .map(ConverterDto::convertToDto)
                .collect(Collectors.toList()));
        return documentDto;
    }

    @Override
    public void create(DocumentDto documentDto) {
        documentRepository.save(ConverterDto.convertToModel(documentDto));
    }

    @Override
    public void update(DocumentDto documentDto) {
        checkEntityService.checkExist(documentDto.getId(), documentRepository, "Document");
        documentRepository.save(ConverterDto.convertToModel(documentDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, documentRepository, "Document");
        taskRepository.updateBeforeDeletingDocuments(id);
        documentRepository.deleteById(id);
    }
}

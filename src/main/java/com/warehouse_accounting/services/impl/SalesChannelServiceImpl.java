package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.SalesChannelDto;
import com.warehouse_accounting.repositories.SalesChannelRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.SalesChannelService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SalesChannelServiceImpl implements SalesChannelService {

    private final SalesChannelRepository salesChannelRepository;

    private final CheckEntityService checkEntityService;
    public SalesChannelServiceImpl(SalesChannelRepository salesChannelRepository, CheckEntityService checkEntityService) {
        this.salesChannelRepository = salesChannelRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<SalesChannelDto> getAll() {
        return salesChannelRepository.getAll();
    }

    @Override
    public SalesChannelDto getById(Long id) {
        checkEntityService.checkExist(id, salesChannelRepository, "SalesChannel");
        return salesChannelRepository.getById(id);
    }

    @Override
    public void create(SalesChannelDto dto) {
        salesChannelRepository.save(ConverterDto.convertSalesChannelDtoToModel(dto));
    }

    @Override
    public void update(SalesChannelDto dto) {
        checkEntityService.checkExist(dto.getId(), salesChannelRepository, "SalesChannel");
        salesChannelRepository.save(ConverterDto.convertSalesChannelDtoToModel(dto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, salesChannelRepository, "SalesChannel");
        salesChannelRepository.deleteById(id);
    }
}

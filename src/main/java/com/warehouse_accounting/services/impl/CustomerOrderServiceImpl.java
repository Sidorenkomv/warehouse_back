package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.dto.CustomerOrderDto;
import com.warehouse_accounting.repositories.CustomerOrderRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.CustomerOrderService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private final CustomerOrderRepository repository;

    private final CheckEntityService checkEntityService;

    public CustomerOrderServiceImpl(CustomerOrderRepository repository, CheckEntityService checkEntityService) {
        this.repository = repository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<CustomerOrderDto> getAll() {
        return repository.getAll();
    }

    @Override
    public CustomerOrderDto getById(Long id) {
        checkEntityService.checkExist(id, repository, "CustomerOrder");
        return repository.getById(id);
    }

    @Override
    public void create(CustomerOrderDto customerOrderDto) {
        repository.save(ConverterDto.convertToModel(customerOrderDto));
    }

    @Override
    public void update(CustomerOrderDto customerOrderDto) {
        checkEntityService.checkExist(customerOrderDto.getId(), repository, "CustomerOrder");
        repository.save(ConverterDto.convertToModel(customerOrderDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, repository, "CustomerOrder");
        repository.deleteById(id);
    }
}

package com.warehouse_accounting.services.impl;

import com.warehouse_accounting.models.Street;
import com.warehouse_accounting.models.dto.StreetDto;
import com.warehouse_accounting.repositories.StreetRepository;
import com.warehouse_accounting.services.interfaces.CheckEntityService;
import com.warehouse_accounting.services.interfaces.StreetService;
import com.warehouse_accounting.util.ConverterDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StreetServiceImpl implements StreetService {

    private final StreetRepository streetRepository;

    private final CheckEntityService checkEntityService;
    public StreetServiceImpl(StreetRepository streetRepository, CheckEntityService checkEntityService) {
        this.streetRepository = streetRepository;
        this.checkEntityService = checkEntityService;
    }

    @Override
    public List<StreetDto> getAll(String regionCityCode) {
        return streetRepository.getAll(regionCityCode);
    }

    @Override
    public List<StreetDto> getSlice(int offset, int limit, String name, String regionCityCode) {
        return streetRepository.getSlice(name, regionCityCode, PageRequest.of(offset, limit)).getContent();
    }

    @Override
    public int getCount(String name, String regionCityCode) {
        return streetRepository.getCount(name, regionCityCode);
    }

    @Override
    public StreetDto getById(Long id) {
        checkEntityService.checkExist(id, streetRepository, "Street");
        return streetRepository.getById(id);
    }

    @Override
    public void create(StreetDto streetDto) {
        streetRepository.save(ConverterDto.convertToModel(streetDto));
    }

    @Override
    public void createAll(Iterable<Street> streets) {
        streetRepository.saveAll(streets);
    }

    @Override
    public void update(StreetDto streetDto) {
        checkEntityService.checkExist(streetDto.getId(), streetRepository, "Street");
        streetRepository.save(ConverterDto.convertToModel(streetDto));
    }

    @Override
    public void deleteById(Long id) {
        checkEntityService.checkExist(id, streetRepository, "Street");
        streetRepository.deleteById(id);

    }
}

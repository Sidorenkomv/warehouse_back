package com.warehouse_accounting.services.utilServices;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.warehouse_accounting.models.dto.RecycleBinDto;
import com.warehouse_accounting.repositories.RecycleBinRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
@Log4j2
public class ExportPdfSpreadsheetService {

    private final RecycleBinRepository recycleBinRepository;

    public ExportPdfSpreadsheetService(RecycleBinRepository recycleBinRepository) throws IOException {
        this.recycleBinRepository = recycleBinRepository;
    }


    // Creating a table
    float[] pointColumnWidths = {300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F, 300F};
    Table table = new Table(pointColumnWidths);


    // получение документа PDF
    public <T> InputStreamResource getPdf(Class<T> targetClass) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String FONT_FILENAME = "./src/main/resources/arial.ttf";
        PdfFont font = PdfFontFactory.createFont(FONT_FILENAME, PdfEncodings.IDENTITY_H);
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(byteArrayOutputStream));
        Document doc = new Document(pdfDoc, PageSize.A4.rotate()).setFontSize(8);

        doc.setFont(font);
        doc.add(new Paragraph("Документы").setFontSize(16));
        tableRecycleBin();
        defineClassAndCombineSheet(targetClass);
        doc.add(table);
        doc.close();
        table = new Table(pointColumnWidths);
        System.out.println("Lists added to table successfully..");
        return new InputStreamResource(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
    }


    // метод для определения метода заполнения таблицы в зависимости от целевого класса
    private <T> void defineClassAndCombineSheet(Class<T> targetClass) {
        switch (targetClass.getSimpleName()) {
            case "RecycleBinDto":
                combineRecycleBin(RecycleBinDto.class);
                break;
        }
    }

    //заполнение шапки таблицы
    public void tableRecycleBin() {
        table.addCell("ID");
        table.addCell(new Cell().add("Тип документа"));
        table.addCell(new Cell().add("Номер"));
        table.addCell(new Cell().add("Дата"));
        table.addCell(new Cell().add("Сумма"));
        table.addCell(new Cell().add("Со Склада"));
        table.addCell(new Cell().add("На склад"));
        table.addCell(new Cell().add("Организация"));
        table.addCell(new Cell().add("Контрагент"));
        table.addCell(new Cell().add("Статус"));
        table.addCell(new Cell().add("Проект"));
        table.addCell(new Cell().add("Отправлено"));
        table.addCell(new Cell().add("Комментарий"));
    }


    //заполнение таблицы
    public void combineRecycleBin(Class<RecycleBinDto> recycleBinDtoClass) {
        for (RecycleBinDto recycleBinDto : recycleBinRepository.getAll()) {
            table.addCell(new Cell().add(recycleBinDto.getId().toString()));
            table.addCell(new Cell().add(recycleBinDto.getDocumentType()));
            table.addCell(new Cell().add(recycleBinDto.getNumber()));
            table.addCell(new Cell().add(recycleBinDto.getDate().toString()));
            table.addCell(new Cell().add(recycleBinDto.getSum().toString()));
            table.addCell(new Cell().add(recycleBinDto.getWarehouseName()));
            table.addCell(new Cell().add(recycleBinDto.getWarehouseFrom()));
            table.addCell(new Cell().add("Закглушка"));
            //TODO разобраться и исправить ошибку при выполнении recycleBinDto.getCompanyName()

           // table.addCell(new Cell().add (recycleBinDto.getCompanyName())); ошибка
            table.addCell(new Cell().add(recycleBinDto.getContractorName()));
            table.addCell(new Cell().add(recycleBinDto.getStatus()));
            table.addCell(new Cell().add(recycleBinDto.getProjectName()));
            table.addCell(new Cell().add(recycleBinDto.getShipped()));
            table.addCell(new Cell().add(recycleBinDto.getComment()));
        }
    }
}











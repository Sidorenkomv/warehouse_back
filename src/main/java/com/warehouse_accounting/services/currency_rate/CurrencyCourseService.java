package com.warehouse_accounting.services.currency_rate;

import com.warehouse_accounting.models.Currency;
import com.warehouse_accounting.models.dto.CurrencyCourseDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CurrencyCourseService {
    public static final String URL_CBR = "https://cbr.ru/scripts/XML_daily.asp";
    final RestTemplate restTemplate = new RestTemplate();

    public List<Currency> getCourses() {
        CurrencyCourseDto response = restTemplate.getForObject(URL_CBR, CurrencyCourseDto.class);

        if (response != null) {
            response
                    .getValute()
                    .forEach(x -> {
                        x.setValue(Double.parseDouble(x.get_Value().replace(",", ".")));
                    });

            return response.getValute();
        }

        return null;
    }

}

package com.warehouse_accounting.services.currency_rate;

import com.warehouse_accounting.services.interfaces.CurrencyService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class RunAfterStartApplication {
    private final CurrencyCourseService currencyCourseService;

    private final CurrencyService currencyService;

    public RunAfterStartApplication(CurrencyCourseService currencyCourseService, CurrencyService currencyService) {
        this.currencyCourseService = currencyCourseService;
        this.currencyService = currencyService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runFillBase(){
        currencyService.saveAll(currencyCourseService.getCourses());
    }
}

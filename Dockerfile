FROM tomcat:latest
ADD target/warehouse-back.war /usr/local/tomcat/webapps/
RUN sed -i 's/port="8080"/port="4446"/' ${CATALINA_HOME}/conf/server.xml
CMD ["catalina.sh", "run"]



#Warehouse accounting
## Работа c git
### Клонирование проекта

1. На странице репозитория убедитесь, что выбрана ветка **dev** (1), нажмите кнопку **Clone** (2), скопируйте ссылку (3).

![](src/main/resources/static/images/git_tutor/git_clone_url.png)

2. Откройте **Intellij IDEA**, нажмите **Get from version control** на экране приветствия, либо **VCS | Git | Clone...** в меню.

![](src/main/resources/static/images/git_tutor/git_clone_get.png)

![](src/main/resources/static/images/git_tutor/git_clone_get_alt.png)

3. Вставьте скопированную ссылку в строку **URL**, нажмите **Clone**.

![](src/main/resources/static/images/git_tutor/git_clone_clone.png)

### Перед внесением изменений в код
Создайте новую ветку в git-репозитории и работайте в ней. Для этого:
1. Нажмите на текущую ветку **dev** в правом нижнем углу.


![](src/main/resources/static/images/git_tutor/git_branch.png)

2. Выберите **New branch**.

![](src/main/resources/static/images/git_tutor/git_branch_create.png)

3. Введите название своей новой ветки (на ваше усмотрение) и нажмите **Create**.

![](src/main/resources/static/images/git_tutor/git_branch_name.png)

### Добавление своего кода в общий репозиторий. Git push.

Прежде чем создать merge request вам необходимо подготовить вашу ветку к отправке в общий репозиторий.

1. Нажмите на текущую ветку в правом нижнем углу. Выберите опцию **dev | update**.
   Таким образом вы скачаете в свою локальную ветку **dev** все коммиты которые были замержены,
   пока вы работали в своей ветке.

![](src/main/resources/static/images/git_tutor/git_premerge_update_dev.png)

2. Убедитесь, что в данный момент активна ваша рабочая ветка (значек ярлыка слева от имени, как у ветки my-branch на скриншоте).
   Выберите опцию **dev | Merge into Current**. Таким образом вы добавите все изменения из ветки **dev** в вашу ветку. При возникновении конфликтов разрешите их.

![](src/main/resources/static/images/git_tutor/git_premerge_merge_dev.png)

3. ---**ВАЖНО**--- Убедитесь что проект собирается и запускается.

4. Выберите вашу ветку и нажмите на **Push...**, чтобы добавить её в общий репозиторий.

![](src/main/resources/static/images/git_tutor/git_premerge_push.png)

### Создание merge request

1. Создайте новый merge request. В качестве **Source branch** выберите свою ветку, **Target branch** - **dev**.

![](src/main/resources/static/images/git_tutor/git_merge_req.png)

![](src/main/resources/static/images/git_tutor/git_merge_req_new.png)

![](src/main/resources/static/images/git_tutor/git_merge_req_src_trg.png)

2. Проверьте данные, допишите комментарии при необходимости. Обратите внимание на опцию **Delete source branch when merge request is accepted**.
   Завершите создание реквеста, приложите ссылку на него в карточку таска на Trello.

![](src/main/resources/static/images/git_tutor/git_merge_req_final.png)



## Работа с JWT токеном через POSTMAN.
### Получение JWT токена.

Для получения токена необходимо отправить POST запрос по адресу http://localhost:4446/api/auth/token/.
Во вкладке "Body" выбрать "raw", и формат - "JSON".
````
{
"username" : "user",
"password" : "user"
}
````
Если username и password верные будет возвращен объект, содержащий строку token.

## Отправка запроса с использованием JWT токена.
Для отправки запроса с токеном необходимо во вкладке "Authorization"
выбрать "Type": "Bearer Token". Справа появится поле Token куда вставляется строка токена.


## Работа со Swagger
Swagger - это фреймворк для спецификации RESTful API, дает возможность интерактивно просматривать спецификацию,
и отправлять запросы (Swagger UI).

Swagger UI - интерфейс, который представляет документацию, позволяет возможность просмотреть какие типы запросов
есть, описание моделей и их типов данных.
URL для Swagger UI: http://localhost:4446/swagger-ui.html
При переходе на данную страницу - в окне поиска необходимо заменить "v3" на "v2".

При работе с данным UI нужно передать токен, который будет сгенерирован в POSTMAN. Для этого необходимо нажать на "Authorize",
и в поле "Value" указать значение: Bearer TOKEN. 

####Например, Value: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZSI6MSwiZXhwIjoxNjQ3NTI4MzM0LCJpYXQiOjE2NDc0NDE5MzR9.ME5PS9zCn4gKTgJzWVO6m050uGrBsE1j5f2h-mmnFoM

После чего можно выполнять запросы.

Swagger Editor - онлайн-редактор, позволяет писать документацию в YAML или JSON формата. (https://editor.swagger.io/)
URL для Swagger Editor: http://localhost:4446/v2/api-docs

````
@RestController
@RequestMapping("/api")
@Api("Swagger Controller")
public class SwaggerController {

    @GetMapping
    @ApiOperation("Получение списка всех записей")
    public ResponseEntity<UserDto> getAll() {
        //....
        return new ResponseEntity<UserDto>(HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation("Создание новой записи")
    public ResponseEntity<UserDto> addUser(@ApiParam(value = "Новый UserDto") @RequestBody UserDto userDto) {
        //....
        return new ResponseEntity<UserDto>(HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation("Обновление существующей записи")
    public ResponseEntity<UserDto> editUser(@ApiParam(name = "Обновленный UserDto") @RequestBody UserDto userDto){
        //....
        return new ResponseEntity<UserDto>(HttpStatus.OK);
    }
    
    @DeleteMapping
    @ApiOperation("Удаление записи")
    public ResponseEntity<?> deleteUser(@ApiParam(name = "Удаление UserDto") @RequestBody UserDto userDto) {
        //....
        return new ResponseEntity<UserDto>(HttpStatus.OK);
    }
}
````


